import * as _ from 'lodash';
import * as flat from 'flat';
import * as moment from 'moment';

import { EntitySchema, QueryOp } from '../types';

export type Primitive = number | string | boolean;

export class PostgarUtils
{
    public static prepareObjBeforeSave(data: object): object
    {
        const obj = {};

        Object.keys(data).forEach(key =>
        {
            const value     = data[key];
            const type      = typeof value;
            const condition =
                      value === null ||
                      type === 'string' ||
                      type === 'number' ||
                      type === 'boolean';

            if (condition)
            {
                obj[key] = value;
            }
        });

        return obj;
    }

    public static parse(value: string): object
    {
        return JSON.parse(value);
    }

    public static lowerFirstLetter(string: string): any
    {
        return string.charAt(0).toLowerCase() + string.slice(1);
    }

    public static stringify(obj_from_json: any): string
    {
        if (
            typeof obj_from_json !== 'object' &&
            !Array.isArray(obj_from_json)
        )
        {
            // not an object or array, stringify using native function
            return JSON.stringify(obj_from_json);
        }
        else if (Array.isArray(obj_from_json))
        {
            const array = obj_from_json.map(item => this.stringify(item));

            return `[${array.join(',')}]`;
        }

        const props = Object.keys(obj_from_json)
            .map(key => `${key}:${this.stringify(obj_from_json[key])}`)
            .join(',');
        return `{${props}}`;
    }

    public static isEmpty(obj = {}): boolean
    {
        for (const key in obj)
        {
            if (obj.hasOwnProperty(key))
            {
                return false;
            }
        }
        return true;
    }

    public static lowDashUpper(value: string): string
    {
        return (value || '').replace(/([A-Z])/g, '_$1').toUpperCase();
    }

    public static toLowerDash(value: string): string
    {
        return (value || '')
            .replace(/(?:^|\.?)([A-Z])/g, (x, y) =>
            {
                return '_' + y.toLowerCase();
            })
            .replace(/^_/, '');
    }

    public static camelCaseToDash(value: string): string
    {
        let word = (value || '')
            .replace(/(?:^|\.?)([A-Z])/g, (x, y) =>
            {
                return '-' + y.toLowerCase();
            })
            .replace(/^_/, '');

        if (word.startsWith('-'))
        {
            word = word.substr(1);
        }
        return word;
    }

    public static lowerClassName(value: string = ''): string
    {
        return this.camelCaseToDash(value.replace('Easy', ''));
    }

    public static filterArrayByString(mainArr, searchText): any
    {
        searchText = searchText || '';

        if (searchText === '')
        {
            return mainArr;
        }

        searchText = searchText.toLowerCase();

        return mainArr.filter(itemObj =>
        {
            return this.searchInObj(itemObj, searchText);
        });
    }

    public static searchInObj(itemObj, searchText): any
    {
        for (const prop in itemObj)
        {
            if (!itemObj.hasOwnProperty(prop))
            {
                continue;
            }

            const value = itemObj[prop];

            if (typeof value === 'string')
            {
                if (this.searchInString(value, searchText))
                {
                    return true;
                }
            }
            else if (Array.isArray(value))
            {
                if (this.searchInArray(value, searchText))
                {
                    return true;
                }
            }

            if (typeof value === 'object')
            {
                if (this.searchInObj(value, searchText))
                {
                    return true;
                }
            }
        }
    }

    public static searchInArray(arr, searchText): any
    {
        for (const value of arr)
        {
            if (typeof value === 'string')
            {
                if (this.searchInString(value, searchText))
                {
                    return true;
                }
            }

            if (typeof value === 'object')
            {
                if (this.searchInObj(value, searchText))
                {
                    return true;
                }
            }
        }
    }

    public static searchInString(value = '', searchText = ''): any
    {
        return value.toLowerCase().includes(searchText);
    }

    public static generateGUID(): string
    {
        function S4(): string
        {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return S4() + S4();
    }

    public static toggleInArray(item, array): any
    {
        if (array.indexOf(item) === -1)
        {
            array.push(item);
        }
        else
        {
            array.splice(array.indexOf(item), 1);
        }
    }

    public static handleize(text): string
    {
        return text
            .toString()
            .toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, ''); // Trim - from end of text
    }

    public static allFields(entityMap: {[name: string]: EntitySchema}, entityName: string): string
    {
        const entity = entityMap[entityName];

        return Object
            .keys(entity.fields)
            .reduce((str, fieldName) =>
            {
                if (isObject(entity.fields[fieldName]))
                {
                    const refEntityName      = entity.fields[fieldName]['name'];
                    const refEntity          = entityMap[refEntityName];
                    const refEntityFieldsStr = this.simpleFields(refEntity);

                    return `${str}
                        ${fieldName} {
                            ${refEntityFieldsStr}
                        }
                    `;
                }
                else
                {
                    return `${str} ${fieldName}`;
                }
            }, '');
    }

    public static simpleFields(entity: EntitySchema): string
    {
        return Object.keys(entity.fields).filter(fieldName => !isObject(entity.fields[fieldName])).join(' ');
    }
}

export function isObject(value: any): boolean
{
    return value !== null && typeof value === 'object';
}

export function compareDeep(a, b): boolean
{
    return JSON.stringify(a) === JSON.stringify(b);
}

/**
 * Get object without null props
 */
export const filteredObject = (obj: any, exclude: string[]) =>
{
    obj     = obj || {};
    exclude = exclude || [];

    if (typeof obj === 'number' || typeof obj === 'string' || typeof obj === 'boolean')
    {
        return obj;
    }

    return Object.keys(obj)
        .filter(k => !exclude.includes(obj[k]))
        .reduce((newObj, k) =>
        {
            if (_.isArray(obj[k]))
            {
                const array = obj[k].map(item => filteredObject(item, exclude));
                return Object.assign(newObj, {[k]: array});
            }
            if (_.isObject(obj[k]))
            {
                return Object.assign(newObj, {[k]: filteredObject(obj[k], exclude)});
            }
            return Object.assign(newObj, {[k]: obj[k]});
        }, {});
};

/**
 * Replace an undefined value by null
 */
export const undefinedToNull = (obj: object = {}) =>
    Object.keys(obj)
        .reduce((newObj, k) =>
        {
            if (_.isArray(obj[k]))
            {
                const array = obj[k].map(item => undefinedToNull(item));
                return Object.assign(newObj, {[k]: array});
            }
            if (_.isObject(obj[k]))
            {
                return Object.assign(newObj, {[k]: undefinedToNull(obj[k])});
            }
            return Object.assign(newObj, {[k]: obj[k] === undefined ? null : obj[k]});
        }, {});

export const EMPTY: any[] = [null, undefined, ''];

export const QueryOpShortedKeys = Object.keys(QueryOp).reduce((_obj, key, index) =>
{
    return {
        ..._obj,
        [`o${index}`]: QueryOp[key]
    };
}, {});

export const QueryOpShortedValues = Object.keys(QueryOp).reduce((_obj, key, index) =>
{
    return {
        ..._obj,
        [key]: `o${index}`
    };
}, {});

export const FlattenObject = (object: any, onlyExistProps: boolean = true) =>
{
    const flatten = flat(object);

    return Object.keys(flatten)
        .filter(key => onlyExistProps ? !EMPTY.includes(flatten[key]) : key)
        .reduce((newObj, key) =>
        {
            return {
                ...newObj,
                [key]: flatten[key]
            };
        }, {});
};

export const ShortedParams = (params) =>
{
    const shorted = shortedOperator(params);

    function shortedOperator(obj: object): any
    {
        obj = obj || {};
        return Object.keys(obj)
            .reduce((newObj, k) =>
            {
                if (_.isArray(obj[k]))
                {
                    const array = obj[k].map(item => shortedOperator(item));
                    return Object.assign(newObj, {[k]: array});
                }
                if (_.isObject(obj[k]))
                {
                    return Object.assign(newObj, {[k]: shortedOperator(obj[k])});
                }
                const newKey = QueryOpShortedValues.hasOwnProperty(k) ? QueryOpShortedValues[k] : k;
                return Object.assign(newObj, {[newKey]: obj[k]});
            }, {});
    }

    return FlattenObject(shorted);
};

export const NormalizeParams = (params) =>
{
    const unflatten = flat.unflatten(params);

    function normalizeKeys(obj: object): any
    {
        obj = obj || {};
        return Object.keys(obj)
            .reduce((newObj, k) =>
            {
                if (_.isArray(obj[k]))
                {
                    const array = obj[k].map(item => normalizeKeys(item));
                    return Object.assign(newObj, {[k]: array});
                }
                if (_.isObject(obj[k]))
                {
                    return Object.assign(newObj, {[k]: normalizeKeys(obj[k])});
                }
                const newKey = QueryOpShortedKeys.hasOwnProperty(k) ? QueryOpShortedKeys[k] : k;
                return Object.assign(newObj, {[newKey]: obj[k]});
            }, {});
    }

    return normalizeKeys(unflatten);
};

export function findPropInArray(array: any[], comparison: any, returnProp: string): any
{
    if (array.length === 0)
    {
        return null;
    }
    array.forEach(obj =>
    {
        Object.keys(obj).forEach(key =>
        {
            if (obj[key] === comparison)
            {
                console.log(obj[returnProp]);
                return obj[returnProp] || null;
            }
        });
    });

    return null;
}

export function momentBeforeSave(object): any
{
    return checkObject(object);
}

function checkObject(obj: any): any
{
    if (_.isArray(obj))
    {
        return obj;
    }
    return Object.keys(obj).reduce((newObj: any, key: string) =>
    {
        let value = obj[key];

        if (moment.isMoment(obj[key]))
        {
            value = moment(value).format();
        }
        else if (_.isObject(value))
        {
            value = checkObject(value);
        }

        return {
            ...newObj,
            [key]: value
        };
    }, {});
}
