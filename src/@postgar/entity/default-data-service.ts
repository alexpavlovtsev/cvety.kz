import { EntityCollectionService, EntityServices, Update } from 'ngrx-data';
import { Injectable, Optional } from '@angular/core';

import { Observable, of } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';

import { GraphQLClient } from '../graphql';
import { EntityFieldsMetadata } from './entity-fields-metadata';
import { momentBeforeSave, PostgarUtils } from '../utils/postgar-utils';
import { PageInfo } from './models/page-info';
import {
    ALL_NODES_QUERY,
    MUTATION_CREATE_NODE,
    MUTATION_DELETE_NODE,
    MUTATION_NODE_UPDATE,
    NODE_BY_ID_QUERY,
    QueryData,
    QueryDesignerService
} from './query-designer.service';
import { EntitySchema } from '../types';
import { View } from '@postgar/entity/models';


@Injectable()
export class DefaultDataService
{
    name: string;
    entityName: string;

    // private
    private _entitySchemaService: EntityCollectionService<EntitySchema>;

    /**
     * Constructor
     *
     * @param _graphQL
     * @param _queryDesigner
     * @param _entityServices
     */
    constructor(
        @Optional() private _graphQL: GraphQLClient,
        @Optional() private _queryDesigner: QueryDesignerService,
        @Optional() private _entityServices: EntityServices
    )
    {
        if (!_graphQL)
        {
            throw new Error('Where is ModelService?');
        }
        this.name = `Data service`;

        // Set private
        this._entitySchemaService = _entityServices.getEntityCollectionService('EntitySchema');
    }

    /**
     * Add
     *
     * @param entity
     * @returns {Observable<any>}
     */
    add(entity: any): Observable<any>
    {
        const query     = MUTATION_CREATE_NODE(this.entityName, this.lowerName);
        const variables = {
            input: {
                [this.lowerName]: validEntity(entity)
            }
        };

        return this._graphQL.mutation(query, variables).pipe(
            map(response =>
            {
                const id = response[`create${this.entityName}`][this.lowerName]['id'];

                return {
                    ...entity,
                    id
                };
            })
        );
    }

    /**
     * Update
     *
     * @param {Update<any>} update
     * @returns {Observable<Update<any>>}
     */
    update(update: Update<any>): Observable<Update<any>>
    {
        return this._entitySchemaService.entityMap$
            .pipe(
                take(1),
                switchMap((entityMap) =>
                {
                    const service   = this._entityServices.getEntityCollectionService(this.entityName);
                    const fields    = PostgarUtils.allFields(entityMap, this.entityName);
                    const query     = MUTATION_NODE_UPDATE(this.entityName, this.lowerName, fields);
                    const variables = {
                        input: {
                            id                        : update.id,
                            [`${this.lowerName}Patch`]: momentBeforeSave(update.changes)
                        }
                    };

                    return this._graphQL
                        .mutation(query, variables)
                        .pipe(
                            tap((res: Update<any>) =>
                            {
                                service.upsertOneInCache({...res.changes, id: update.id});
                            }),
                            map(() => update)
                        );
                })
            );
    }

    /**
     * Get all
     *
     * @returns {Observable<any>}
     */
    getAll(): Observable<any>
    {
        return this._entitySchemaService.entityMap$
            .pipe(
                take(1),
                switchMap((entityMap) =>
                {
                    const fields     = PostgarUtils.allFields(entityMap, this.entityName);
                    const pluralName = entityMap[this.entityName].pluralName;
                    const queryName  = `all${pluralName}`;
                    const query      = ALL_NODES_QUERY(queryName, fields);

                    return this._graphQL
                        .get(query, {})
                        .pipe(
                            map(response => [
                                ...response[queryName].nodes
                            ])
                        );
                })
            );
    }

    /**
     * Delete
     *
     * @param id
     * @returns {Observable<null>}
     */
    delete(id: any): Observable<null>
    {
        const query     = MUTATION_DELETE_NODE(this.entityName, this.lowerName);
        const variables = {
            input: {
                id: id
            }
        };

        return this._graphQL.mutation(query, variables);
    }

    /**
     * Get by id
     *
     * @param {number} id
     * @returns {Observable<any>}
     */
    getById(id: number): Observable<any>
    {
        const fields = EntityFieldsMetadata[this.entityName].allFields.join(' ');
        const query  = NODE_BY_ID_QUERY(this.entityName, fields);

        return this._graphQL
            .get(query, {id: id})
            .pipe(map(value => value[`${this.lowerName}ById`]));
    }

    /**
     * Get with query
     *
     * @param params
     * @returns {Observable<any[]>}
     */
    getWithQuery(params: View | {}): Observable<any[]>
    {
        params = params || {};

        return this._entitySchemaService.entityMap$
            .pipe(
                take(1),
                switchMap((entityMap) =>
                {
                    return this._queryDesigner
                        .viewWithQuery(this.entityName, params, entityMap)
                        .pipe(
                            switchMap(result =>
                            {
                                return this.getNodesInApi(result);
                            }),
                            take(1)
                        );
                })
            );
    }

    /**
     * Get nodes in api
     *
     * @param {QueryData} queryData
     * @returns {Observable<any[]>}
     */
    private getNodesInApi(queryData: QueryData): Observable<any[]>
    {
        const view = queryData.view;

        return this._graphQL.get(queryData.query, queryData.variables).pipe(
            map(response =>
            {
                const result = response[view.queryName] || {nodes: []};

                const nodes     = view.setof ? [...result.nodes] : [result];
                view.totalCount = view.setof ? result.totalCount : nodes.length;
                view.pageInfo   = new PageInfo(result.pageInfo);
                view.nodeIds    = nodes.map(item => item.id);
                view.loaded     = true;
                view.loading    = false;
                view.changed    = false;

                this._queryDesigner.setView(view);

                return nodes;
            })
        );
    }

    /**
     * Lowercase class name
     *
     * @returns {string}
     */
    get lowerName(): string
    {
        return PostgarUtils.lowerFirstLetter(this.entityName);
    }

    /**
     * Create data service
     *
     * @param {string} entityName
     * @returns {DefaultDataService}
     */
    createService(entityName: string): DefaultDataService
    {
        const service = new DefaultDataService(
            this._graphQL,
            this._queryDesigner,
            this._entityServices
        );

        service.setEntityName(entityName);

        return service;
    }

    setEntityName(entityName: string): void
    {
        this.entityName = entityName;
    }
}

function validEntity(entity: any): any
{
    entity = entity || {};

    if (entity.hasOwnProperty('id') && !entity['id'])
    {
        delete entity['id'];
    }

    return momentBeforeSave(entity);
}
