import { EntityMetadataMap } from 'ngrx-data';

export const entityMetadata: EntityMetadataMap = {
    View        : {},
    EntitySchema: {
        selectId: schemaSelectName
    },
};

export function schemaSelectName(entity: any): string
{
    return entity == null ? undefined : entity.name;
}

