import { Injectable, Optional } from '@angular/core';
import { EntityCollectionService, EntityServices } from 'ngrx-data';
import { EntitySchema } from '@postgar/types';
import { GraphQLClient } from '@postgar/graphql';
import { QueryDesignerService } from '@postgar/entity/query-designer.service';

@Injectable()
export class DefaultData
{
    name: string;
    entityName: string;

    // private
    private _entitySchemaService: EntityCollectionService<EntitySchema>;

    /**
     * Constructor
     *
     * @param _graphQL
     * @param _queryDesigner
     * @param _entityServices
     */
    constructor(
        @Optional() private _graphQL: GraphQLClient,
        @Optional() private _queryDesigner: QueryDesignerService,
        @Optional() private _entityServices: EntityServices
    )
    {
        if (!_graphQL)
        {
            throw new Error('Where is GraphQLClient?');
        }
        if (!_queryDesigner)
        {
            throw new Error('Where is QueryDesignerService?');
        }
        if (!_entityServices)
        {
            throw new Error('Where is EntityServices?');
        }
        this.name = `Data service`;

        // Set private
        this._entitySchemaService = _entityServices.getEntityCollectionService('EntitySchema');
    }
}
