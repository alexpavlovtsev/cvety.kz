import { ChangeDetectorRef, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { animate, AnimationBuilder, AnimationPlayer, style } from '@angular/animations';
import { EntityManagerService } from '@postgar/entity';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { fromEvent, Subject } from 'rxjs';


@Component({
    selector   : 'schema-sidebar',
    templateUrl: './schema-sidebar.component.html',
    styleUrls  : ['./schema-sidebar.component.scss']
})
export class SchemaSidebarComponent implements OnInit
{
    @ViewChild('filter')
    filter: ElementRef;

    @ViewChild('sidebar')
    sidebar: ElementRef;

    selectedEntity: any;
    opened: boolean;
    position: string;
    entities: any[];
    fields: any[];
    search: string;

    // Private
    private _backdrop: HTMLElement | null = null;
    private _player: AnimationPlayer;
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _animationBuilder: AnimationBuilder,
        private _changeDetectorRef: ChangeDetectorRef,
        private _renderer: Renderer2,
        private _entityManager: EntityManagerService
    )
    {
        this.fields   = [];
        this.entities = [];
        this.opened   = false;
        this.position = 'right';

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._entityManager.getService('EntitySchema').entities$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(entities =>
            {
                this.entities = entities;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(100),
                distinctUntilChanged()
            )
            .subscribe(() =>
            {
                this.search = this.filter.nativeElement.value;

                // Mark for check
                this._changeDetectorRef.markForCheck();
            });

        // Setup visibility
        this._setupVisibility();

        // Setup position
        this._setupPosition();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Open the sidebar
     */
    open(): void
    {
        if (this.opened)
        {
            return;
        }

        // Show the sidebar
        this._showSidebar();

        // Show the backdrop
        this._showBackdrop();

        // Set the opened status
        this.opened = true;

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * Close the sidebar
     */
    close(): void
    {
        if (!this.opened)
        {
            return;
        }

        // Hide the backdrop
        this._hideBackdrop();

        // Set the opened status
        this.opened = false;

        // Hide the sidebar
        this._hideSidebar();

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * Toggle open/close the sidebar
     */
    toggleOpen(): void
    {
        if (this.opened)
        {
            this.close();
        }
        else
        {
            this.open();
        }
    }

    /**
     * Select entity
     *
     * @param entity
     */
    selectEntity(entity: any): void
    {
        this.selectedEntity = entity;
        this.resetSearch();

        if (!!entity)
        {
            this.fields = Object.keys(this.selectedEntity.fields).map(key =>
            {
                const value = this.selectedEntity.fields[key];
                return {
                    name: key,
                    type: typeof value === 'string' ? value : value.name
                };
            });
        }
    }

    /**
     * Reset search
     */
    resetSearch(): void
    {
        this.search                     = '';
        this.filter.nativeElement.value = '';
    }

    /**
     * Search placeholder
     */
    get searchPlaceholder(): string
    {
        return !!this.selectedEntity ? 'Search field...' : 'Search Entity...';
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Setup the visibility of the sidebar
     *
     * @private
     */
    private _setupVisibility(): void
    {
        // Remove the existing box-shadow
        this._renderer.setStyle(this.sidebar.nativeElement, 'box-shadow', 'none');

        // Make the sidebar invisible
        this._renderer.setStyle(this.sidebar.nativeElement, 'visibility', 'hidden');
    }

    /**
     * Setup the sidebar position
     *
     * @private
     */
    private _setupPosition(): void
    {
        // Add the correct class name to the sidebar
        // element depending on the position attribute
        if (this.position === 'right')
        {
            this._renderer.addClass(this.sidebar.nativeElement, 'right-positioned');
        }
        else
        {
            this._renderer.addClass(this.sidebar.nativeElement, 'left-positioned');
        }
    }

    /**
     * Show the backdrop
     *
     * @private
     */
    private _showBackdrop(): void
    {
        // Create the backdrop element
        this._backdrop = this._renderer.createElement('div');

        // Add a class to the backdrop element
        this._backdrop.classList.add('sidebar-overlay');

        // Append the backdrop to the parent of the sidebar
        this._renderer.appendChild(this.sidebar.nativeElement.parentElement, this._backdrop);

        // Create the enter animation and attach it to the player
        this._player =
            this._animationBuilder
                .build([
                    animate('300ms ease', style({opacity: 1}))
                ]).create(this._backdrop);

        // Play the animation
        this._player.play();

        // Add an event listener to the overlay
        this._backdrop.addEventListener('click', () =>
            {
                this.close();
            }
        );

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * Hide the backdrop
     *
     * @private
     */
    private _hideBackdrop(): void
    {
        if (!this._backdrop)
        {
            return;
        }

        // Create the leave animation and attach it to the player
        this._player =
            this._animationBuilder
                .build([
                    animate('300ms ease', style({opacity: 0}))
                ]).create(this._backdrop);

        // Play the animation
        this._player.play();

        // Once the animation is done...
        this._player.onDone(() =>
        {

            // If the backdrop still exists...
            if (this._backdrop)
            {
                // Remove the backdrop
                this._backdrop.parentNode.removeChild(this._backdrop);
                this._backdrop = null;
            }
        });

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * Change some properties of the sidebar
     * and make it visible
     *
     * @private
     */
    private _showSidebar(): void
    {
        // Remove the box-shadow style
        this._renderer.removeStyle(this.sidebar.nativeElement, 'box-shadow');

        // Make the sidebar invisible
        this._renderer.removeStyle(this.sidebar.nativeElement, 'visibility');

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }

    /**
     * Change some properties of the sidebar
     * and make it invisible
     *
     * @private
     */
    private _hideSidebar(delay = true): void
    {
        const delayAmount = delay ? 300 : 0;

        // Add a delay so close animation can play
        setTimeout(() =>
        {

            // Remove the box-shadow
            this._renderer.setStyle(this.sidebar.nativeElement, 'box-shadow', 'none');

            // Make the sidebar invisible
            this._renderer.setStyle(this.sidebar.nativeElement, 'visibility', 'hidden');
        }, delayAmount);

        // Mark for check
        this._changeDetectorRef.markForCheck();
    }
}
