import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'search'
})
export class SearchPipe implements PipeTransform
{
    transform(items: any[], search: string): any
    {
        if (!search)
        {
            return items;
        }
        return items.filter(item => item.name.toLowerCase().includes(search));
    }

}
