export type IntFilter = 'isNull'
    | 'equalTo'
    | 'notEqualTo'
    | 'distinctFrom'
    | 'notDistinctFrom'
    | 'lessThan'
    | 'lessThanOrEqualTo'
    | 'greaterThan'
    | 'greaterThanOrEqualTo'
    | 'in'
    | 'notIn';

export type StringFilter = 'isNull'
    | 'equalTo'
    | 'notEqualTo'
    | 'distinctFrom'
    | 'notDistinctFrom'
    | 'lessThan'
    | 'lessThanOrEqualTo'
    | 'greaterThan'
    | 'greaterThanOrEqualTo'
    | 'in'
    | 'notIn'
    | 'includes'
    | 'notIncludes'
    | 'includesInsensitive'
    | 'notIncludesInsensitive'
    | 'startsWith'
    | 'notStartsWith'
    | 'startsWithInsensitive'
    | 'notStartsWithInsensitive'
    | 'endsWith'
    | 'notEndsWith'
    | 'endsWithInsensitive'
    | 'notEndsWithInsensitive'
    | 'like'
    | 'notLike'
    | 'likeInsensitive'
    | 'notLikeInsensitive'
    | 'similarTo'
    | 'notSimilarTo';

export type DatetimeFilter = 'isNull'
    | 'equalTo'
    | 'notEqualTo'
    | 'distinctFrom'
    | 'notDistinctFrom'
    | 'lessThan'
    | 'lessThanOrEqualTo'
    | 'greaterThan'
    | 'greaterThanOrEqualTo'
    | 'in'
    | 'notIn';

export type BooleanFilter = 'isNull'
    | 'equalTo'
    | 'notEqualTo'
    | 'distinctFrom'
    | 'notDistinctFrom'
    | 'in'
    | 'notIn';

export type StringListFilter = 'isNull'
    | 'equalTo'
    | 'notEqualTo'
    | 'distinctFrom'
    | 'notDistinctFrom'
    | 'lessThan'
    | 'lessThanOrEqualTo'
    | 'greaterThan'
    | 'greaterThanOrEqualTo'
    | 'anyEqualTo'
    | 'anyNotEqualTo'
    | 'anyLessThan'
    | 'anyLessThanOrEqualTo'
    | 'anyGreaterThan'
    | 'anyGreaterThanOrEqualTo';

export type IntListFilter = 'isNull'
    | 'equalTo'
    | 'notEqualTo'
    | 'distinctFrom'
    | 'notDistinctFrom'
    | 'lessThan'
    | 'lessThanOrEqualTo'
    | 'greaterThan'
    | 'greaterThanOrEqualTo'
    | 'anyEqualTo'
    | 'anyNotEqualTo'
    | 'anyLessThan'
    | 'anyLessThanOrEqualTo'
    | 'anyGreaterThan'
    | 'anyGreaterThanOrEqualTo';

export type BigFloatFilter = 'isNull'
    | 'equalTo'
    | 'notEqualTo'
    | 'distinctFrom'
    | 'notDistinctFrom'
    | 'lessThan'
    | 'lessThanOrEqualTo'
    | 'greaterThan'
    | 'greaterThanOrEqualTo'
    | 'in'
    | 'notIn';

export type BigIntFilter = 'isNull'
    | 'equalTo'
    | 'notEqualTo'
    | 'distinctFrom'
    | 'notDistinctFrom'
    | 'lessThan'
    | 'lessThanOrEqualTo'
    | 'greaterThan'
    | 'greaterThanOrEqualTo'
    | 'greaterThanOrEqualTo'
    | 'notIn';

export type Filter = IntFilter
    | StringFilter
    | DatetimeFilter
    | BooleanFilter
    | StringListFilter
    | IntListFilter
    | BigFloatFilter
    | BigIntFilter;

