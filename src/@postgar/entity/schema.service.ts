import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { EntityCollectionService, EntityDataService, EntityDefinitionService, EntityServices } from 'ngrx-data';

import { PostgarConfigService } from '../services/config.service';
import { DefaultDataService } from './default-data-service';
import { EntitySchema, PostgarConfig } from '../types';
import { StoreView, View } from './models';
import { ViewsService } from './views.service';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';


@Injectable()
export class SchemaService
{
    private _entitySchemaService: EntityCollectionService<EntitySchema>;

    /**
     * Constructor
     *
     * @param _httpClient
     * @param _entityDefService
     * @param _entityDataService
     * @param _defaultDataService
     * @param _postgarConfigService
     * @param _entityServices
     * @param _viewsService
     */
    constructor(
        private _httpClient: HttpClient,
        private _entityDefService: EntityDefinitionService,
        private _entityDataService: EntityDataService,
        private _defaultDataService: DefaultDataService,
        private _postgarConfigService: PostgarConfigService,
        private _entityServices: EntityServices,
        private _viewsService: ViewsService
    )
    {
        this._entitySchemaService = _entityServices.getEntityCollectionService('EntitySchema');
    }

    /**
     * Is loaded
     */
    get loaded$(): Observable<boolean>
    {
        return this._entitySchemaService.loaded$.pipe(filter(loaded => loaded));
    }

    /**
     * Set schema
     */
    setSchema(): Promise<any>
    {
        return new Promise((resolve, reject) =>
        {
            this._postgarConfigService.config
                .subscribe((config: PostgarConfig) =>
                {
                    this.registerViews(config.views);

                    this._httpClient.get(config.graphqlSchemaUrl)
                        .subscribe((response: any[]) =>
                        {
                            this.registerMetadataMap(response, config.customEntityMetadata);
                            this.registerServices(response);

                            this._entitySchemaService.addAllToCache(response);

                            resolve(response);
                        }, reject);
                });
        });
    }

    /**
     * Register store services
     *
     * @param schema
     */
    private registerServices(schema: any[]): void
    {
        const dataServices = schema.reduce((services, entity) =>
        {
            return {
                ...services,
                [entity.name]: this._defaultDataService.createService(entity.name)
            };
        }, {});

        this._entityDataService.registerServices(dataServices);
    }

    /**
     * Register store metadata
     *
     * @param schema
     * @param customEntityMetadata
     */
    private registerMetadataMap(schema: any[], customEntityMetadata: any): void
    {
        const lazyMetadataMap = schema.reduce((metadata, entity) =>
        {
            return {
                ...metadata,
                [entity.name]: {}
            };
        }, {});

        this._entityDefService.registerMetadataMap(lazyMetadataMap);

        if (customEntityMetadata)
        {
            this._entityDefService.registerMetadataMap(customEntityMetadata);
        }
    }

    /**
     * Register store views
     *
     * @param views
     */
    private registerViews(views: StoreView[] = []): void
    {
        views = views.map(view => new View(view));
        this._viewsService.upsertManyInCache(views);
    }
}
