import { PostgarUtils } from '../../utils/postgar-utils';

export class GridInfo
{
    pageIndex: number;
    sortActive: string;
    sortDirection: string;
    sortQuery: string;

    constructor(data: any)
    {
        data               = data || {};
        this.pageIndex     = data.pageIndex || 0;
        this.sortActive    = data.sortActive || '';
        this.sortDirection = data.sortDirection || '';
        this.sortQuery     = this.constructSortQuery();
    }

    constructSortQuery(): string
    {
        return this.sortDirection.length > 0 && this.sortActive.length > 0
            ? PostgarUtils.lowDashUpper(`${this.sortActive}_${this.sortDirection}`)
            : 'NATURAL';
    }
}
