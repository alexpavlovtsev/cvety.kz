export interface ColumnsDataItem {
    order: number;
    renderable: boolean;
    fieldName?: string;
    label?: string;
}
