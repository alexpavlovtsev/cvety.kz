export interface FiltersDataItem {
    query: any;
    input: {
        choice: number;
        value?: any;
        startDate?: string;
        dueDate?: string;
        startTime?: string;
        dueTime?: string;
        value_2?: any;
    };
}
