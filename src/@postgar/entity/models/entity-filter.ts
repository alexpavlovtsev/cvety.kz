import * as _ from 'lodash';

export class EntityFilter
{
    name: string;
    data: any;

    constructor(name: string)
    {
        this.name = name;
        this.init();
    }

    init(): void
    {
        this.data = {
            [this.name]: null,
            and        : [{[this.name]: null}],
            or         : [{[this.name]: null}],
            not        : [{[this.name]: null}],
        };
    }

    set(data: any): void
    {
        data = data || {};
        let array = [];

        if (data.hasOwnProperty('and'))
        {
            array = data['and'].map(item =>
            {
                return {[this.name]: item};
            });
            this.data = _.merge({}, this.data, {and: array});

        } else if (data.hasOwnProperty('or'))
        {
            array = data['or'].map(item =>
            {
                return {[this.name]: item};
            });
            this.data = _.merge({}, this.data, {or: array});

        } else if (data.hasOwnProperty('not'))
        {
            array = data['not'].map(item =>
            {
                return {[this.name]: item};
            });
            this.data = _.merge({}, this.data, {not: array});
        }
        else
        {
            this.data = _.merge({}, this.data, {[this.name]: data});
        }
    }
}
