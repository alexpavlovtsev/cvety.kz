export class PageInfo
{
    hasNextPage: boolean;
    hasPreviousPage: boolean;
    startCursor: string;
    endCursor: string;

    constructor(data: any)
    {
        data                 = data || {};
        this.hasNextPage     = data.hasNextPage;
        this.hasPreviousPage = data.hasPreviousPage;
        this.startCursor     = data.startCursor;
        this.endCursor       = data.endCursor;
    }
}
