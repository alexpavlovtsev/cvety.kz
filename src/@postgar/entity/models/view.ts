import { PageInfo } from './page-info';
import { PostgarUtils } from '../../utils/postgar-utils';

export interface StoreView
{
    id: any;
    queryName?: string;
    entityName: string;
    changed?: boolean;
    nodeIds?: any[];
    selectedIds?: any[];
    totalCount?: number;
    pageInfo?: PageInfo;
    loaded?: boolean;
    loading?: boolean;
    sorted?: boolean;
    withRefEntities?: boolean;
    queryParams?: any;
    query?: any;
    setof?: boolean;
    pageSize?: number;
    pageOffset?: number;
    filters?: any;
    sortActive?: string;
    sortDirection?: string;
    onlyFields?: string[];
}

export class View implements StoreView
{
    id: any;
    queryName: string;
    entityName: string;
    changed: boolean;
    nodeIds: any[];
    selectedIds: any[];
    totalCount: number;
    columns: string[];
    sortActive: string;
    sortDirection: string;
    pageInfo: PageInfo;
    loaded: boolean;
    loading: boolean;
    refEntity: boolean;
    query: any;
    setof: boolean;
    pageSize: number;
    // pageIndex: number;
    pageOffset: number;
    filters: any;
    onlyFields: string[];

    constructor(data?: any)
    {
        data               = data || {};
        this.id            = data.id || PostgarUtils.generateGUID();
        this.entityName    = data.entityName;
        this.changed       = data.changed;
        this.nodeIds       = data.nodeIds || [];
        this.totalCount    = data.totalCount || 0;
        this.columns       = data.columns || [];
        this.selectedIds   = data.selectedIds || [];
        this.sortActive    = data.sortActive;
        this.sortDirection = data.sortDirection;
        this.pageInfo      = new PageInfo(data.pageInfo);
        this.loaded        = !!data.loaded;
        this.loading       = !!data.loading;
        this.queryName     = data.queryName;
        this.refEntity     = data.hasOwnProperty('refEntity') ? data.refEntity : true;
        this.setof         = data.hasOwnProperty('setof') ? data.setof : true;
        this.query         = data.query || {};
        this.pageSize      = data.pageSize || 25;
        // this.pageIndex     = data.pageIndex;
        this.pageOffset    = data.pageOffset || 0;
        this.filters       = data.filters || {};
        this.onlyFields    = data.onlyFields || [];
    }

    get pageIndex(): number
    {
        if (this.pageOffset)
        {
            return Math.ceil(this.pageOffset / (this.pageSize || 25));
        }
        else
        {
            return 0;
        }
    }
}
