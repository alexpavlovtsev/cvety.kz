import { Injectable, Optional } from '@angular/core';
import { EntitySchema, FilterInfo } from '../types';
import { EntityCollectionService, EntityServices } from 'ngrx-data';
import { ViewsService } from './views.service';
import * as _ from 'lodash';
import { Observable } from 'rxjs';
import { distinctUntilChanged, filter, map, switchMap, take } from 'rxjs/operators';
import { compareDeep, FlattenObject } from '../utils';

@Injectable({
    providedIn: 'root'
})
export class FiltersService
{
    name: string;
    viewId: string;
    data: any;

    // private
    private _entitySchemaService: EntityCollectionService<EntitySchema>;

    constructor(
        @Optional() private _entityServices: EntityServices,
        @Optional() private _viewsService: ViewsService,
    )
    {
        // Set private
        this._entitySchemaService = _entityServices.getEntityCollectionService('EntitySchema');
    }

    clear(): void
    {

    }

    /**
     * Set data
     *
     * @param data
     */
    set(data: any): void
    {
        this.reset();
        data      = data || {};
        let array = [];

        if (data.hasOwnProperty('and'))
        {
            array     = data['and'].map(item =>
            {
                return {[this.name]: item};
            });
            this.data = _.merge({}, this.data, {and: array});

        } else if (data.hasOwnProperty('or'))
        {
            array     = data['or'].map(item =>
            {
                return {[this.name]: item};
            });
            this.data = _.merge({}, this.data, {or: array});

        } else if (data.hasOwnProperty('not'))
        {
            array     = data['not'].map(item =>
            {
                return {[this.name]: item};
            });
            this.data = _.merge({}, this.data, {not: array});
        }
        else
        {
            this.data = _.merge({}, this.data, {[this.name]: data});
        }

        this._viewsService.updateFilters(this.data, this.viewId);
    }

    /**
     * Filter value changes
     */
    get valueChanges$(): Observable<FilterInfo[]>
    {
        return this._entitySchemaService.entityMap$
            .pipe(
                take(1),
                switchMap(entityMap =>
                {
                    return this._viewsService.view(this.viewId)
                        .pipe(
                            map(view =>
                            {
                                const entity  = entityMap[view.entityName];
                                const kind    = !!(entity && entity.fields[this.name]) ? entity.fields[this.name] : '';
                                const flatted = FlattenObject(view.filters);

                                let values = Object.keys(flatted)
                                    .filter(key => key.includes(`${this.name}.`))
                                    .map(key =>
                                    {
                                        return {
                                            fieldName: this.name,
                                            value   : flatted[key],
                                            operator: key.split('.').slice(-1)[0]
                                        };
                                    });

                                if (values.length === 0)
                                {
                                    return [
                                        {
                                            fieldName: this.name,
                                            value   : null,
                                            operator: null
                                        }
                                    ];
                                }

                                if (typeof kind === 'string' && kind.includes('Int'))
                                {
                                    values = values.map(item =>
                                    {
                                        return {
                                            ...item,
                                            value: parseInt(item.value, 10)
                                        };
                                    });
                                }

                                return values;
                            }),
                            filter(values => values.length > 0),
                            distinctUntilChanged(compareDeep)
                        );
                })
            );
    }

    /**
     * Create filter service
     *
     * @param name
     * @param viewId
     */
    createFilter(name: string, viewId: string): FiltersService
    {
        const service = new FiltersService(
            this._entityServices,
            this._viewsService,
        );

        service.name   = name;
        service.viewId = viewId;

        return service;
    }

    /**
     * Reset data
     */
    private reset(): void
    {
        this.data = {
            [this.name]: null,
            and        : [{[this.name]: null}],
            or         : [{[this.name]: null}],
            not        : [{[this.name]: null}],
        };
    }
}
