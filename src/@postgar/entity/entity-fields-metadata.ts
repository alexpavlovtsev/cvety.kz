const simpleFields = {
    Address         : [
        'id',
        'countryCode',
        'regionCode',
        'cityCode',
        'street',
        'address',
        'updatedAt',
        'createdAt',
        'refTable',
        'refId'
    ],
    Aspirant        : [
        'id',
        'birthday',
        'relocate',
        'addressId',
        'userId',
        'updatedAt',
        'createdAt',
        'facebook',
        'instagram',
        'vk',
        'twitter',
        'images',
        'mainImage',
        'aspirantCategoryIds',
        'cityCode',
        'regionCode',
        'countryCode',
        'phone',
        'fullName',
    ],
    AspirantCategory: [
        'id',
        'name'
    ],
    Employer        : [
        'id',
        'userId',
        'updatedAt',
        'createdAt',
        'facebook',
        'instagram',
        'vk',
        'twitter',
        'images',
        'mainImage',
        'addressId',
        'clubPhone',
        'website',
        'shortDescription',
        'clubName',
        'categoryId',
        'cityCode',
        'regionCode',
        'countryCode',
        'phone',
    ],
    EmployerCategory: [
        'id',
        'name'
    ],
    EmployerPlan    : [
        'id',
        'planId',
        'employerId',
        'endAt',
        'startAt',
        'addressId'
    ],
    Place           : [
        'id',
        'addressId',
        'description',
        'clubName',
        'position',
        'endAt',
        'startAt',
        'resumeId'
    ],
    Plan            : [
        'id',
        'name',
        'price',
        'period'
    ],
    Resume          : [
        'id',
        'title',
        'aspirantCategoryIds',
        'description',
        'addressIds',
        'updatedAt',
        'createdAt',
        'aspirantId',
        'cityCodes',
        'countryCodes',
        'regionCodes',
        `aspirantByAspirantId {
            id
            mainImage
            fullName
        }`
    ],
    User            : [
        'id',
        'firstName',
        'lastName',
        'phone',
        'email',
        'createdAt',
        'updatedAt',
        'role',
        'fullName',
        'roleText'
    ],
    Vacancy         : [
        'id',
        'title',
        'minPrice',
        'maxPrice',
        'description',
        'mainImage',
        'addressId',
        'aspirantCategoryIds',
        'updatedAt',
        'createdAt',
        'employerId',
        'cityCode',
        'regionCode',
        'countryCode',
        `employerByEmployerId {
            id
            clubName
        }`
    ]
};

export const EntityFieldsMetadata = {
    Address         : {
        simpleFields: simpleFields.Address,
        allFields   : [
            ...simpleFields.Address
        ]
    },
    Aspirant        : {
        simpleFields: simpleFields.Aspirant,
        allFields   : [
            ...simpleFields.Aspirant
        ]
    },
    AspirantCategory: {
        simpleFields: simpleFields.AspirantCategory,
        allFields   : [
            ...simpleFields.AspirantCategory
        ]
    },
    Employer        : {
        simpleFields: simpleFields.Employer,
        allFields   : [
            ...simpleFields.Employer
        ]
    },
    EmployerCategory: {
        simpleFields: simpleFields.EmployerCategory,
        allFields   : [
            ...simpleFields.EmployerCategory
        ]
    },
    EmployerPlan    : {
        simpleFields: simpleFields.EmployerPlan,
        allFields   : [
            ...simpleFields.EmployerPlan
        ]
    },
    Place           : {
        simpleFields: simpleFields.Place,
        allFields   : [
            ...simpleFields.Place
        ]
    },
    Plan            : {
        simpleFields: simpleFields.Plan,
        allFields   : [
            ...simpleFields.Plan
        ]
    },
    Resume          : {
        simpleFields: simpleFields.Resume,
        allFields   : [
            ...simpleFields.Resume
        ]
    },
    User            : {
        simpleFields: simpleFields.User,
        allFields   : [
            ...simpleFields.User
        ]
    },
    Vacancy         : {
        simpleFields: simpleFields.Vacancy,
        allFields   : [
            ...simpleFields.Vacancy
        ]
    }
};
