import { EntityCollectionService } from 'ngrx-data';
import { Observable } from 'rxjs';
import { View } from '@postgar/entity';

/**
 * A facade for managing
 * a cached collection of T entities in the ngrx store.
 */
export interface EntityStoreService<T> extends EntityCollectionService<T>
{
    getWithQuery(params: View | {}): Observable<T[]>;
}
