import { Injectable } from '@angular/core';

import { EntityCollectionService, EntitySelectors$, EntityServices } from 'ngrx-data';
import { EntityStoreService } from '@postgar/entity';

@Injectable({
    providedIn: 'root'
})
export class EntityManagerService
{

    constructor(
        private entityServices: EntityServices
    )
    {
    }

    /** Get (or create) the singleton instance of an EntityCollectionService
     * @param entityName {string} Name of the entity type of the service
     */
    getService<T, S$ extends EntitySelectors$<T> = EntitySelectors$<T>>(entityName: string): EntityStoreService<T>
    {
        return this.entityServices.getEntityCollectionService(entityName);
    }
}
