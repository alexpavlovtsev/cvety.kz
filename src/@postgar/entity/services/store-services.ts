import { Injectable } from '@angular/core';
import { EntityServices, EntityServicesBase, EntityServicesElements } from 'ngrx-data';

@Injectable({
    providedIn: 'root'
})
export class StoreServices extends  EntityServicesBase implements EntityServices
{
    constructor(entityServicesElements: EntityServicesElements)
    {
        super(entityServicesElements);
    }
}
