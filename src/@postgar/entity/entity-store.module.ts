import { NgModule } from '@angular/core';
import { EntityDataService, EntityDefinitionService, NgrxDataModule } from 'ngrx-data';

import { entityMetadata } from './entity-metadata';
import { EasyGraphqlModule } from '../graphql/graphql.module';
import { DefaultDataService } from './default-data-service';
import { QueryDesignerService } from './query-designer.service';
import { SchemaService } from './schema.service';
import { SchemaSidebarComponent } from './components/schema-sidebar/schema-sidebar.component';
import { CommonModule } from '@angular/common';
import { SearchPipe } from './components/schema-sidebar/search.pipe';


@NgModule({
    imports     : [
        NgrxDataModule.forRoot({
            entityMetadata: entityMetadata
        }),

        EasyGraphqlModule,

        CommonModule,
    ],
    providers   : [
        DefaultDataService,
        QueryDesignerService,
        SchemaService,
    ],
    declarations: [SchemaSidebarComponent, SearchPipe],
    exports     : [SchemaSidebarComponent]
})
export class EntityStoreModule
{
    /**
     * Constructor
     *
     * @param _entityDataService
     * @param _defaultDataService
     * @param _schemaService
     * @param _entityDefService
     */
    constructor(
        private _entityDataService: EntityDataService,
        private _defaultDataService: DefaultDataService,
        private _schemaService: SchemaService,
        private _entityDefService: EntityDefinitionService
    )
    {
        this._schemaService.setSchema();
    }
}
