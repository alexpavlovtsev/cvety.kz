import { Injectable } from '@angular/core';
import { EntityCollectionService, EntityCollectionServiceBase, EntityCollectionServiceElementsFactory, EntityServices } from 'ngrx-data';
import { View } from './models/view';

import { distinctUntilChanged, filter, map, switchMap, take } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import * as _ from 'lodash';
import { compareDeep, undefinedToNull } from '../utils';
import { EntitySchema } from '../types';
import { EntityManagerService } from '@postgar/entity/services';

@Injectable({
    providedIn: 'root'
})
export class ViewsService extends EntityCollectionServiceBase<View>
{
    // private
    private _entitySchemaService: EntityCollectionService<EntitySchema>;

    /**
     * Constructor
     *
     * @param elementsFactory
     * @param entityManager
     * @param entityServices
     */
    constructor(
        private elementsFactory: EntityCollectionServiceElementsFactory,
        private entityManager: EntityManagerService,
        private entityServices: EntityServices)
    {
        super('View', elementsFactory);

        // Set private
        this._entitySchemaService = entityServices.getEntityCollectionService('EntitySchema');
    }

    /**
     * Get view
     *
     * @param viewId
     * @returns {Observable<View>}
     */
    view(viewId: any): Observable<View>
    {
        return this.entityMap$.pipe(
            map(entities => new View(entities[viewId])),
            distinctUntilChanged(compareDeep)
        );
    }

    /**
     * Get view nodes
     *
     * @param viewId
     * @returns {Observable<any>}
     */
    nodes(viewId: any): Observable<any>
    {
        return this.entityMap$
            .pipe(
                map(entities =>
                {
                    return !!entities[viewId]
                        ? {nodeIds: entities[viewId].nodeIds, entityName: entities[viewId].entityName}
                        : {nodeIds: [], entityName: ''};
                }),
                distinctUntilChanged(compareDeep),
                switchMap((data: {nodeIds: number[], entityName: string}) =>
                {
                    if (data.nodeIds.length === 0)
                    {
                        return of([]);
                    }
                    else
                    {
                        const service = this.entityServices.getEntityCollectionService(data.entityName);

                        return service.entityMap$.pipe(
                            map(entityMap =>
                            {
                                const entities = data.nodeIds
                                    .map(id => entityMap[id])
                                    .filter(entity => !!entity);

                                return entities;
                            }),
                            filter(entities => entities.length > 0),
                            distinctUntilChanged(compareDeep)
                        );
                    }
                })
            );
    }

    /**
     * Get first firstNode
     *
     * @param viewId
     */
    firstNode(viewId: any): Observable<any>
    {
        return this.entityMap$.pipe(
            map(entities => entities[viewId]),
            distinctUntilChanged(compareDeep),
            switchMap((view: View) =>
            {
                if (!view)
                {
                    return of({});
                }
                else
                {
                    const service = this.entityServices.getEntityCollectionService(view.entityName);

                    return service.entityMap$.pipe(
                        map(entityMap =>
                        {
                            return view.nodeIds.map(id => entityMap[id])[0] || {};
                        })
                    );
                }
            })
        );
    }

    /**
     * Get total count
     *
     * @param viewId
     * @returns {Observable<number>}
     */
    totalCount(viewId: any): Observable<number>
    {
        return this.view(viewId).pipe(
            map(view => view.totalCount),
            distinctUntilChanged()
        );
    }

    /**
     * Set nodes to view
     *
     * @param nodes
     * @param viewId
     * @param entityName
     */
    setNodesToCache(nodes: any[], viewId: any, entityName?: string): void
    {
        // nodes = (nodes || []).filter(node => !!node);

        this.view(viewId)
            .pipe(take(1))
            .subscribe(view =>
            {
                entityName = view.entityName || entityName;
                if (!entityName)
                {
                    return;
                }
                if (entityName === 'City')
                {
                    // console.log(nodes);
                }
                const service = this.entityServices.getEntityCollectionService(entityName);

                service.upsertManyInCache(nodes);

                const nodeIds = nodes.map(node => node.id);
                const value   = {
                    id     : viewId,
                    changed: true,
                    nodeIds,
                    entityName
                };
                this.upsertOneInCache(value);
            });
    }

    /**
     * Get with query
     *
     * @param view
     */
    query(view: any): Observable<any>
    {
        if (!view.id)
        {
            throw new Error(`View has no id property in query function`);
        }
        return this.view(view.id).pipe(
            take(1),
            map((view: View) =>
            {
                if (!view.entityName)
                {
                    throw new Error(`View "${view.id}" does not have a entityName property`);
                }
                const service = this.entityManager.getService(view.entityName);

                return service.getWithQuery(view);
            }),
        );
    }

    /**
     * Get page index
     *
     * @param {string} viewId
     * @returns {Observable<any>}
     */
    pageIndex(viewId: any): Observable<any>
    {
        return this.view(viewId).pipe(
            map(view => view.pageIndex),
            distinctUntilChanged()
        );
    }

    /**
     * Get page size info
     *
     * @param {string} viewId
     * @returns {Observable<any>}
     */
    pageSize(viewId: any): Observable<any>
    {
        return this.view(viewId).pipe(
            map(view => view.pageSize),
            distinctUntilChanged()
        );
    }

    /**
     * Get view loaded info
     *
     * @param viewId
     */
    loaded(viewId: any): Observable<boolean>
    {
        return this.view(viewId).pipe(
            map(view => view.loaded),
            distinctUntilChanged()
        );
    }

    /**
     * Update sorters data
     *
     * @param sortActive
     * @param sortDirection
     * @param viewId
     */
    updateSorterData(sortActive: string, sortDirection: string, viewId: any): void
    {
        const value = {
            id     : viewId,
            changed: true,
            sortActive,
            sortDirection
        };
        this.upsertOneInCache(value);
    }

    /**
     * Select All
     *
     * @param viewId
     */
    selectAll(viewId: any): void
    {
        this.view(viewId)
            .pipe(take(1))
            .subscribe(view =>
            {
                view = !!view ? view : new View();

                const value = {
                    id         : viewId,
                    changed    : true,
                    selectedIds: [...view.nodeIds]
                };
                this.upsertOneInCache(value);
            });
    }

    /**
     * Deselect All
     *
     * @param viewId
     */
    deselectAll(viewId: any): void
    {
        const value = {
            id         : viewId,
            changed    : true,
            selectedIds: []
        };
        this.upsertOneInCache(value);
    }

    /**
     * Toggle select firstNode
     *
     * @param viewId
     * @param nodeId
     */
    toggleInSelected(viewId: any, nodeId: number): void
    {
        this.view(viewId)
            .pipe(take(1))
            .subscribe(view =>
            {
                view = !!view ? view : new View();

                let selectedIds = [...view.selectedIds];

                if (selectedIds.find(id => id === nodeId) !== undefined)
                {
                    selectedIds = selectedIds.filter(id => id !== nodeId);
                }
                else
                {
                    selectedIds = [...selectedIds, nodeId];
                }

                const value = {
                    id     : viewId,
                    changed: true,
                    selectedIds
                };
                this.upsertOneInCache(value);
            });
    }

    /**
     * Select nodes by parameter
     *
     * @param viewId
     * @param filter
     */
    selectByParameter(viewId: any, filter: any): void
    {
        this.nodes(viewId)
            .pipe(take(1))
            .subscribe(nodes =>
            {
                const selectedIds = nodes.filter(node => node[filter.parameter] === filter.value)
                    .map(mail => mail.id);

                const value = {
                    id     : viewId,
                    changed: true,
                    selectedIds
                };
                this.upsertOneInCache(value);
            });
    }

    /**
     * Update page size
     *
     * @param pageSize
     * @param viewId
     */
    updatePageSize(pageSize: number, viewId: any): void
    {
        const value = {
            id     : viewId,
            changed: true,
            pageSize
        };
        this.upsertOneInCache(value);
    }

    /**
     * Update columns data
     *
     * @param {any[]} columns
     * @param {string} viewId
     */
    updateColumns(columns: any[], viewId: any): void
    {
        const value = {
            id     : viewId,
            changed: true,
            columns
        };
        this.upsertOneInCache(value);
    }

    /**
     * Update filers
     *
     * @param filter
     * @param viewId
     */
    updateFilters(filter: any, viewId: any): void
    {
        // Replace properties with an undefined value by null
        const filterData = undefinedToNull(filter);

        // Get the value from the view service
        this.view(viewId)
            .pipe(take(1))
            .subscribe(view =>
            {
                view = !!view ? view : new View();

                // Merge the new config
                const values = _.merge({}, view.filters, filterData);

                const value = {
                    id     : viewId,
                    changed: true,
                    filters: values
                };
                this.upsertOneInCache(value);
            });
    }
}
