import { Injectable } from '@angular/core';
import { View } from './models/view';
import { EntityCollectionService, EntityServices } from 'ngrx-data';

import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import * as _ from 'lodash';

import { filteredObject, PostgarUtils } from '../utils/postgar-utils';
import { EntitySchema } from '../types';

export interface QueryData
{
    query: string;
    variables?: any;
    view: View;
}

const EMPTY_OBJECT = '{}';
const EMPTY_ARRAY  = '[]';

@Injectable()
export class QueryDesignerService
{
    viewsService: EntityCollectionService<View>;

    /**
     * Constructor
     *
     * @param _entityServices
     */
    constructor(private _entityServices: EntityServices)
    {
        this.viewsService = _entityServices.getEntityCollectionService('View');
    }

    /**
     * View and query processing
     *
     * @param entityName
     * @param params
     * @param entityMap
     */
    viewWithQuery(entityName: string, params: any, entityMap: {[name: string]: EntitySchema}): Observable<QueryData>
    {
        // Unique view id
        const viewId = params['id'];

        // New query params
        const newFilters = params['filters'];

        // Entity config from the store
        const entity = entityMap[entityName];

        return this.viewsService.entityMap$
            .pipe(
                map(entities =>
                {
                    // Define the view
                    const view = !!entities[viewId]
                        ? new View({...entities[viewId], ...params})
                        : new View({id: viewId, entityName, ...params});

                    // Update queryName property
                    view.queryName = view.queryName ? view.queryName : `all${entity.pluralName}`;

                    // Define query params
                    view.filters = !!newFilters ? newFilters : view.filters;

                    // Define the query fields
                    const fields = view.refEntity
                        ? PostgarUtils.allFields(entityMap, entityName)
                        : PostgarUtils.simpleFields(entity);

                    // Class to build the query
                    const queryWorker = new QueryWorker(view, fields, entity);

                    return {
                        query    : queryWorker.getQuery(),
                        variables: {},
                        view     : view
                    };
                }),
                take(1)
            );
    }

    /**
     * Set view
     *
     * @param {View} view
     */
    setView(view: View): void
    {
        this.viewsService.upsertOneInCache(view);
    }
}

class QueryWorker
{
    view: View;
    filters: any;
    query: string;
    fields: string;
    entity: EntitySchema;

    constructor(view: View, fields: string, entity: EntitySchema)
    {
        this.view   = view;
        this.entity = entity;

        this.setFields(fields, view);
        this.filterOutEmptyParams();
        // this.deleteEmptyContainers();
        // this.checkLogicalOperators();
        this.deleteEmptyContainers();
        this.queryToString();
    }

    /**
     * Set fields
     *
     * @param fields
     * @param view
     */
    private setFields(fields: string, view: View): void
    {
        this.fields = view.onlyFields && view.onlyFields.length > 0 ? view.onlyFields.join(' ') : fields;
    }

    /**
     * Select non-empty options
     */
    private filterOutEmptyParams(): void
    {
        this.filters = filteredObject(this.view.filters, [null, undefined]);
    }

    /**
     * Remove empty containers (such as "{}", "[]") from the query
     */
    private deleteEmptyContainers(): void
    {
        const queryStringRaw = JSON.stringify(this.filters);

        const queryString = queryStringRaw
            .replace(new RegExp(EMPTY_OBJECT, 'g'), 'null')
            .replace(new RegExp(EMPTY_ARRAY, 'g'), 'null');

        const query = JSON.parse(queryString);

        // Clear query from empty parameters
        this.filters = filteredObject(query, [null]);
    }

    /**
     * Check logical operators (such as "not", "or")
     */
    private checkLogicalOperators(): void
    {
        this.filters = this.arrayValidate(this.filters);
    }

    /**
     * Delete logical operators (such as "not", "or") if they have one value
     *
     * @param obj
     */
    private arrayValidate(obj: any): object
    {
        obj = obj || {};
        return Object.keys(obj)
            .reduce((newObj, k) =>
            {
                if (_.isArray(obj[k]) && obj[k].length === 1)
                {
                    return Object.assign(newObj, {[k]: this.arrayValidate(obj[k][0])});
                }
                return Object.assign(newObj, {[k]: obj[k]});
            }, {});
    }

    /**
     * Check the matching of field types
     *
     * @param obj
     */
    checkType(obj: any): object
    {
        obj = obj || {};
        return Object.keys(obj)
            .reduce((newObj, k) =>
            {
                if (_.isArray(obj[k]))
                {
                    const array = obj[k].map(item => this.checkType(item));
                    return Object.assign(newObj, {[k]: array});
                }
                else if
                (_.isObject(obj[k]) && this.entity.fields.hasOwnProperty(k))
                {
                    return Object.assign(newObj, {[k]: checkInteger(obj[k], this.entity.fields[k])});
                }
                else if (_.isObject(obj[k]))
                {
                    return Object.assign(newObj, {[k]: this.checkType(obj[k])});
                }
                else
                {
                    // @todo boolean value type
                    // console.log(k, obj[k]);
                }
                return Object.assign(newObj, {[k]: obj[k]});
            }, {});

        function checkInteger(_obj: any, kind: any): any
        {
            if (typeof kind === 'string' && kind.includes('Int'))
            {
                const _key = Object.keys(_obj)[0];

                if (_.isArray(_obj[_key]))
                {
                    _obj[_key] = _obj[_key].map(item => parseFloat(item));
                }
                else if (!!parseFloat(_obj[_key]) || parseFloat(_obj[_key]) === 0)
                {
                    _obj[_key] = parseFloat(_obj[_key]);
                }
            }

            return _obj;
        }
    }

    /**
     * Convert a query object into a string
     */
    private queryToString(): void
    {
        this.filters    = this.checkType(this.filters);
        let value       = PostgarUtils.stringify(this.filters);
        const container = [];
        if (!!value && this.view.setof)
        {
            container.push(`filter: ${value}`);
        }
        else if (!!value)
        {
            value = value.startsWith('{') ? value.substring(1) : value;
            value = value.endsWith('}') ? value.slice(0, -1) : value;
            container.push(value);
        }
        if (!!this.view.pageOffset && this.view.setof)
        {
            container.push(`offset: ${this.view.pageOffset}`);
        }
        if (!!this.view.pageSize && this.view.setof)
        {
            container.push(`first: ${this.view.pageSize}`);
        }
        if (!!(this.view.sortActive && this.view.sortDirection) && this.view.setof)
        {
            const order = this.view.sortDirection.length > 0 && this.view.sortActive.length > 0
                ? PostgarUtils.lowDashUpper(`${this.view.sortActive}_${this.view.sortDirection}`)
                : 'NATURAL';

            container.push(`orderBy: ${order}`);
        }

        this.query = value.length > 0 ? `( ${container.join(', ')} )` : '';
    }

    /**
     * If the query should return multiple rows
     */
    private setOfQuery(): string
    {
        return `
            query ${this.view.queryName} {
              ${this.view.queryName} ${this.query} {
                totalCount
                pageInfo {
                  hasNextPage
                  hasPreviousPage
                }
                nodes { ${this.fields} }
              }
            }
            `;
    }

    /**
     * If the query should return one row
     */
    private singleQuery(): string
    {
        return `query ${this.view.queryName} { ${this.view.queryName} ${this.query} { ${this.fields} } }`;
    }

    /**
     * GraphQl query string
     */
    getQuery(): string
    {
        return this.view.setof ? this.setOfQuery() : this.singleQuery();
    }
}

export const MUTATION_NODE_UPDATE = (className: string, lowerName: string, fields: string) => `
mutation update${className}ById($input: Update${className}ByIdInput!) {
  update${className}ById(input: $input) {
    ${lowerName} {
      ${fields}
    }
  }
}`;

export const MUTATION_CREATE_NODE = (className: string, lowerName: string) => `
mutation create${className}($input: Create${className}Input!) {
  create${className}(input: $input) {
    ${lowerName} {
      id
    }
  }
}`;

export const MUTATION_DELETE_NODE = (className: string, lowerName: string) => `
mutation delete${className}ById($input: Delete${className}ByIdInput!) {
  delete${className}ById(input: $input) {
    ${lowerName} {
      id
    }
  }
}`;

export const NODE_BY_ID_QUERY = (className: string, fields: string) =>
{
    const lowerName = PostgarUtils.lowerFirstLetter(className);

    return `
        query ${lowerName}ById($id: Int!) {
          ${lowerName}ById(id: $id) {
            ${fields}
          }
        }`;
};

export const ALL_NODES_QUERY = (queryName: string, fields: string) =>
{
    return `
        query ${queryName} {
          ${queryName} {
            totalCount
            pageInfo {
              hasNextPage
              hasPreviousPage
            }
            nodes {
              ${fields}
            }
          }
        }`;
};

