import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { EasyGraphqlModule } from './graphql/graphql.module';
import { EntityStoreModule } from './entity/entity-store.module';
import { POSTGAR_CONFIG } from './services/config.service';
import { AppStorage, CookieStorage } from '@postgar/storage';
import { CookieService } from 'ngx-cookie-service';

@NgModule({
    imports  : [
        EasyGraphqlModule,
        EntityStoreModule,
    ],
    providers: [
        CookieService,
        {provide: AppStorage, useClass: CookieStorage}
    ],
    exports  : [
        EasyGraphqlModule,
        EntityStoreModule,
    ]
})
export class PostgarModule
{
    static forRoot(config): ModuleWithProviders
    {
        return {
            ngModule : PostgarModule,
            providers: [
                {
                    provide : POSTGAR_CONFIG,
                    useValue: config
                }
            ]
        };
    }

    constructor(@Optional() @SkipSelf() parentModule: PostgarModule)
    {
        if ( parentModule )
        {
            throw new Error('PostgarModule is already loaded. Import it in the AppModule only!');
        }
    }
}
