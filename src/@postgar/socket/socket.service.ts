import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import * as socketIo from 'socket.io-client';

import { POSTGAR_CONFIG } from '@postgar/services';
import { PostgarConfig } from '@postgar/types';

import { Message } from './model/message';
import { Event } from './model/event';

@Injectable({
    providedIn: 'root'
})
export class SocketService
{
    private readonly url: string;
    private socket: any;

    /**
     * Constructor
     *
     * @param _config
     */
    constructor(
        @Inject(POSTGAR_CONFIG) private _config: PostgarConfig
    )
    {
        this.url = _config.sockedUrl;
    }

    public initSocket(): void
    {
        this.socket = socketIo(this.url);
    }

    public send(message: Message): void
    {
        this.socket.emit('message', message);
    }

    public onMessage(): Observable<Message>
    {
        return new Observable<Message>(observer =>
        {
            this.socket.on('message', (data: Message) => observer.next(data));
        });
    }

    public onEvent(event: Event): Observable<any>
    {
        return new Observable<Event>(observer =>
        {
            this.socket.on(event, () => observer.next());
        });
    }
}
