export interface Message {
    from?: string;
    value?: Content;
}

interface Content {
  table: string;
  id: number;
  type: string;
  row: any;
}
