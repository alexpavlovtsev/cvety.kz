export interface EntityFieldSchema
{
    kind: string;
    name: string;
}

export interface EntitySchema
{
    name: string;
    pluralName: string;
    fields: {[key: string]: string | EntityFieldSchema};
}
