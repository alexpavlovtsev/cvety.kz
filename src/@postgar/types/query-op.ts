// https://github.com/graphile-contrib/postgraphile-plugin-connection-filter

export enum QueryOp
{
    // Logical Operators
    and                         = 'and',
    or                          = 'or',
    not                         = 'not',

    // Comparison Operators
    isNull                      = 'isNull',
    equalTo                     = 'equalTo',
    notEqualTo                  = 'notEqualTo',
    distinctFrom                = 'distinctFrom',
    notDistinctFrom             = 'notDistinctFrom',
    lessThan                    = 'lessThan',
    lessThanOrEqualTo           = 'lessThanOrEqualTo',
    greaterThan                 = 'greaterThan',
    greaterThanOrEqualTo        = 'greaterThanOrEqualTo',
    in                          = 'in',
    notIn                       = 'notIn',
    includes                    = 'includes',
    notIncludes                 = 'notIncludes',
    includesInsensitive         = 'includesInsensitive',
    notIncludesInsensitive      = 'notIncludesInsensitive',
    startsWith                  = 'startsWith',
    notStartsWith               = 'notStartsWith',
    startsWithInsensitive       = 'startsWithInsensitive',
    notStartsWithInsensitive    = 'notStartsWithInsensitive',
    endsWith                    = 'endsWith',
    notEndsWith                 = 'notEndsWith',
    endsWithInsensitive         = 'endsWithInsensitive',
    notEndsWithInsensitive      = 'notEndsWithInsensitive',
    like                        = 'like',
    notLike                     = 'notLike',
    likeInsensitive             = 'likeInsensitive',
    notLikeInsensitive          = 'notLikeInsensitive',
    similarTo                   = 'similarTo',
    notSimilarTo                = 'notSimilarTo',
    contains                    = 'contains',
    containedBy                 = 'containedBy',
    inetContainedBy             = 'inetContainedBy',
    inetContainedByOrEquals     = 'inetContainedByOrEquals',
    inetContains                = 'inetContains',
    inetContainsOrEquals        = 'inetContainsOrEquals',
    inetContainsOrIsContainedBy = 'inetContainsOrIsContainedBy',

    // List Comparison Operators
    anyEqualTo                  = 'anyEqualTo',
    anyNotEqualTo               = 'anyNotEqualTo',
    anyLessThan                 = 'anyLessThan',
    anyLessThanOrEqualTo        = 'anyLessThanOrEqualTo',
    anyGreaterThan              = 'anyGreaterThan',
    anyGreaterThanOrEqualTo     = 'anyGreaterThanOrEqualTo'
}
