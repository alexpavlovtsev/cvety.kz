import { StoreView } from '@postgar/entity';

export interface PostgarConfig
{
    graphqlUrl: string;
    graphqlSchemaUrl: string;
    sockedUrl: string;
    appErrorUrl: string;
    ssrMode: boolean;
    customEntityMetadata: any;
    authTokenKey: string;
    views: StoreView[];
}
