export interface FilterInfo
{
    operator: string;
    fieldName: string;
    value: any;
}
