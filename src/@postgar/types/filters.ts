export enum IntFilter
{
    isNull               = 'isNull',
    equalTo              = 'equalTo',
    notEqualTo           = 'notEqualTo',
    distinctFrom         = 'distinctFrom',
    notDistinctFrom      = 'notDistinctFrom',
    lessThan             = 'lessThan',
    lessThanOrEqualTo    = 'lessThanOrEqualTo',
    greaterThan          = 'greaterThan',
    greaterThanOrEqualTo = 'greaterThanOrEqualTo',
    in                   = 'in',
    notIn                = 'notIn'
}

export enum StringFilter
{
    isNull                   = 'isNull',
    equalTo                  = 'equalTo',
    notEqualTo               = 'notEqualTo',
    distinctFrom             = 'distinctFrom',
    notDistinctFrom          = 'notDistinctFrom',
    lessThan                 = 'lessThan',
    lessThanOrEqualTo        = 'lessThanOrEqualTo',
    greaterThan              = 'greaterThan',
    greaterThanOrEqualTo     = 'greaterThanOrEqualTo',
    in                       = 'in',
    notIn                    = 'notIn',
    includes                 = 'includes',
    notIncludes              = 'notIncludes',
    includesInsensitive      = 'includesInsensitive',
    notIncludesInsensitive   = 'notIncludesInsensitive',
    startsWith               = 'startsWith',
    notStartsWith            = 'notStartsWith',
    startsWithInsensitive    = 'startsWithInsensitive',
    notStartsWithInsensitive = 'notStartsWithInsensitive',
    endsWith                 = 'endsWith',
    notEndsWith              = 'notEndsWith',
    endsWithInsensitive      = 'endsWithInsensitive',
    notEndsWithInsensitive   = 'notEndsWithInsensitive',
    like                     = 'like',
    notLike                  = 'notLike',
    likeInsensitive          = 'likeInsensitive',
    notLikeInsensitive       = 'notLikeInsensitive',
    similarTo                = 'similarTo',
    notSimilarTo             = 'notSimilarTo'
}

export enum DatetimeFilter
{
    isNull               = 'isNull',
    equalTo              = 'equalTo',
    notEqualTo           = 'notEqualTo',
    distinctFrom         = 'distinctFrom',
    notDistinctFrom      = 'notDistinctFrom',
    lessThan             = 'lessThan',
    lessThanOrEqualTo    = 'lessThanOrEqualTo',
    greaterThan          = 'greaterThan',
    greaterThanOrEqualTo = 'greaterThanOrEqualTo',
    in                   = 'in',
    notIn                = 'notIn'
}

export enum BooleanFilter
{
    isNull          = 'isNull',
    equalTo         = 'equalTo',
    notEqualTo      = 'notEqualTo',
    distinctFrom    = 'distinctFrom',
    notDistinctFrom = 'notDistinctFrom',
    in              = 'in',
    notIn           = 'notIn'
}

export enum StringListFilter
{
    isNull                  = 'isNull',
    equalTo                 = 'equalTo',
    notEqualTo              = 'notEqualTo',
    distinctFrom            = 'distinctFrom',
    notDistinctFrom         = 'notDistinctFrom',
    lessThan                = 'lessThan',
    lessThanOrEqualTo       = 'lessThanOrEqualTo',
    greaterThan             = 'greaterThan',
    greaterThanOrEqualTo    = 'greaterThanOrEqualTo',
    anyEqualTo              = 'anyEqualTo',
    anyNotEqualTo           = 'anyNotEqualTo',
    anyLessThan             = 'anyLessThan',
    anyLessThanOrEqualTo    = 'anyLessThanOrEqualTo',
    anyGreaterThan          = 'anyGreaterThan',
    anyGreaterThanOrEqualTo = 'anyGreaterThanOrEqualTo'
}

export enum IntListFilter
{
    isNull                  = 'isNull',
    equalTo                 = 'equalTo',
    notEqualTo              = 'notEqualTo',
    distinctFrom            = 'distinctFrom',
    notDistinctFrom         = 'notDistinctFrom',
    lessThan                = 'lessThan',
    lessThanOrEqualTo       = 'lessThanOrEqualTo',
    greaterThan             = 'greaterThan',
    greaterThanOrEqualTo    = 'greaterThanOrEqualTo',
    anyEqualTo              = 'anyEqualTo',
    anyNotEqualTo           = 'anyNotEqualTo',
    anyLessThan             = 'anyLessThan',
    anyLessThanOrEqualTo    = 'anyLessThanOrEqualTo',
    anyGreaterThan          = 'anyGreaterThan',
    anyGreaterThanOrEqualTo = 'anyGreaterThanOrEqualTo'
}

export enum BigFloatFilter
{
    isNull               = 'isNull',
    equalTo              = 'equalTo',
    notEqualTo           = 'notEqualTo',
    distinctFrom         = 'distinctFrom',
    notDistinctFrom      = 'notDistinctFrom',
    lessThan             = 'lessThan',
    lessThanOrEqualTo    = 'lessThanOrEqualTo',
    greaterThan          = 'greaterThan',
    greaterThanOrEqualTo = 'greaterThanOrEqualTo',
    in                   = 'in',
    notIn                = 'notIn'
}

export enum BigIntFilter
{
    isNull               = 'isNull',
    equalTo              = 'equalTo',
    notEqualTo           = 'notEqualTo',
    distinctFrom         = 'distinctFrom',
    notDistinctFrom      = 'notDistinctFrom',
    lessThan             = 'lessThan',
    lessThanOrEqualTo    = 'lessThanOrEqualTo',
    greaterThan          = 'greaterThan',
    greaterThanOrEqualTo = 'greaterThanOrEqualTo',
    in                   = 'greaterThanOrEqualTo',
    notIn                = 'notIn'
}

export type Op =
    IntFilter
    | StringFilter
    | DatetimeFilter
    | BooleanFilter
    | StringListFilter
    | IntListFilter
    | BigFloatFilter
    | BigIntFilter;

export enum Fs
{
    Int      = <any>IntFilter,
    Str      = <any>StringFilter,
    Dt       = <any>DatetimeFilter,
    Bool     = <any>BooleanFilter,
    StrList  = <any>StringListFilter,
    IntList  = <any>IntListFilter,
    BigFloat = <any>BigFloatFilter,
    BigInt   = <any>BigIntFilter
}

