import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot } from '@angular/router';
import { EntityCollectionService, EntityServices } from 'ngrx-data';
import { catchError, delay, filter, switchMap, take, } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { EntitySchema } from '../types';

@Injectable({
    providedIn: 'root'
})
export class PostgarGuard implements CanActivate, CanActivateChild
{
    private _entitySchemaService: EntityCollectionService<EntitySchema>;

    /**
     * Constructor
     *
     * @param _entityServices
     */
    constructor(
        private _entityServices: EntityServices
    )
    {
        this._entitySchemaService = _entityServices.getEntityCollectionService('EntitySchema');
    }

    /**
     * Can activate
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<boolean>}
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>
    {
        return this.canLoaded().pipe(
            switchMap(() => of(true)),
            catchError(() => of(false))
        );
    }

    /**
     * Can activate child
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<boolean>}
     */
    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>
    {
        return this.canLoaded().pipe(
            switchMap(() => of(true)),
            catchError(() => of(false))
        );
    }

    /**
     * Entity config loaded
     */
    canLoaded(): any
    {
        return this._entitySchemaService.loaded$
            .pipe(
                filter(loaded => loaded),
                take(1),
                delay(10)
            );
    }
}
