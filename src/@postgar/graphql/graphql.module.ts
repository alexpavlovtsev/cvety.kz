import { Inject, NgModule, PLATFORM_ID } from '@angular/core';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';
import { TransferState, makeStateKey } from '@angular/platform-browser';
import { BrowserTransferStateModule } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { ApolloModule, Apollo } from 'apollo-angular';
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http';
import { InMemoryCache, NormalizedCache } from 'apollo-cache-inmemory';
import { ApolloLink, from } from 'apollo-link';
import { onError } from 'apollo-link-error';

import { PostgarConfigService } from '../services/config.service';
import { PostgarConfig } from '../types';
import { AppStorage } from '../storage';
import { isPlatformBrowser } from '@angular/common';
import { GraphQLClient } from '@postgar/graphql/graphql.service';

const STATE_KEY = makeStateKey<any>('apollo.state');

const countUrl = 'https://floracrm.com:5218/graphql';

@NgModule({
    imports  : [
        HttpClientModule,
        ApolloModule,
        BrowserTransferStateModule,
        HttpLinkModule,
    ],
    providers: [],
    exports  : [
        HttpClientModule,
        ApolloModule,
        BrowserTransferStateModule,
        HttpLinkModule,
    ]
})
export class EasyGraphqlModule
{
    private readonly _cache: InMemoryCache;
    private _config: PostgarConfig;

    /**
     * Constructor
     */
    constructor(
        private _apollo: Apollo,
        private _httpLink: HttpLink,
        private _transferState: TransferState,
        private _router: Router,
        @Inject(AppStorage) private appStorage: Storage,
        private _postgarConfigService: PostgarConfigService,
        private _graphQL: GraphQLClient,
        @Inject(PLATFORM_ID) private platformId: Object
    )
    {
        this._cache = new InMemoryCache();

        this._postgarConfigService.config.subscribe((config: PostgarConfig) =>
        {
            this._config = config;
            this.init();
        });
    }

    /**
     * Apollo client init
     */
    init(): void
    {
        // Http link
        const http      = this._httpLink.create({uri: this._config.graphqlUrl});
        const httpCount = this._httpLink.create({uri: countUrl});

        const authMiddleware = new ApolloLink((operation, forward) =>
        {
            const token = this.appStorage.getItem(this._config.authTokenKey);

            if (token)
            {
                // add the authorization to the headers
                // we assume `headers` as a defined instance of HttpHeaders
                operation.setContext(({headers}) => ({
                    headers: new HttpHeaders().set('Authorization', `Bearer ${token}`),
                }));
            }
            return forward(operation);
        });

        const errorMiddleware = onError(({networkError, graphQLErrors}) =>
        {
            if (networkError && networkError['status'] === 401)
            {
                // UNAUTHORIZED
                console.log('UNAUTHORIZED');
                this.resetUser();
            }
            else if (networkError && networkError['status'] === 403)
            {
                // FORBIDDEN
                console.log('FORBIDDEN');
            }
            else if (networkError && networkError['status'] === 500)
            {
                // UNAUTHORIZED
                console.log('INTERNAL SERVER ERROR');
                this.resetUser();
            }
        });

        // Create Apollo Link
        this._apollo.create({
            ssrMode: !!this._config.ssrMode,
            link   : from([authMiddleware, errorMiddleware, http]),
            cache  : this._cache
        });

        this._apollo.create({
            link : httpCount,
            cache: this._cache,
        }, 'counts');

        const isBrowser = this._transferState.hasKey<NormalizedCache>(STATE_KEY);

        if (isBrowser)
        {
            this.onBrowser();
        }
        else
        {
            this.onServer();
        }
    }

    onServer(): void
    {
        this._transferState.onSerialize(STATE_KEY, () =>
            this._cache.extract()
        );
    }

    onBrowser(): void
    {
        const state = this._transferState.get<any>(STATE_KEY, null);

        this._cache.restore(state);
    }

    resetUser(): void
    {
        this._graphQL.client.getClient()
            .resetStore()
            .then(() =>
            {
                this.appStorage.clear();
                this._router.navigateByUrl(this._config.appErrorUrl);

                if (isPlatformBrowser(this.platformId))
                {
                    setTimeout(() =>
                    {
                        location.reload();
                    }, 100);
                }
            });
    }
}
