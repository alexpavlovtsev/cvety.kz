import { Inject, Injectable, PLATFORM_ID } from '@angular/core';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/catch';

import { Observable, of } from 'rxjs';

import { Apollo } from 'apollo-angular';

import gql from 'graphql-tag';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class GraphQLClient
{
    client: Apollo;
    fetchPolicy: any;

    /**
     * Constructor
     *
     * @param apollo
     * @param platformId
     */
    constructor(
        private apollo: Apollo,
        @Inject(PLATFORM_ID) private platformId: Object)
    {
        this.fetchPolicy = 'network-only';
        this.client      = this.apollo;

        if (isPlatformBrowser(this.platformId))
        {
            setTimeout(() =>
            {
                this.fetchPolicy = 'no-cache';
            }, 5000);
        }
    }

    /**
     * Get query
     *
     * @param query
     * @param variables
     * @param name
     * @param fetchPolicy
     */
    get(query: string, variables: any, name: string = null, fetchPolicy: any = 'network-only'): Observable<any>
    {
        // fetchPolicy  = isPlatformBrowser(this.platformId) ? 'no-cache' : 'network-only';
        const apollo = !!name ? this.apollo.use(name) : this.apollo;
        const ref    = apollo.watchQuery<any>({
            variables  : variables,
            query      : gql(query),
            fetchPolicy: this.fetchPolicy,
        });

        return ref
            .valueChanges
            .map(result => JSON.parse(JSON.stringify(result.data)))
            .catch(error => of(error))
            .take(1);
    }

    /**
     * Make a mutation
     *
     * @param query
     * @param variables
     * @param name
     */
    mutation(query: string, variables: any, name: string = null): Observable<any>
    {
        const apollo = !!name ? this.apollo.use(name) : this.apollo;
        const mutate = apollo.mutate({
            mutation : gql(query),
            variables: variables,
        });

        return mutate
            .map(result => result.data)
            .catch((error: any) =>
            {
                console.log(error);
                return of(error);
            })
            .take(1);
    }
}
