import { Injectable } from '@angular/core';
import { LocationService } from '@shared/services/location.service';
import { Breadcrumb } from './breadcrumb';
import { FilterInfoService, filterItemConvert } from '@shared/services/filter-info.service';


@Injectable({
    providedIn: 'root'
})
export class BreadcrumbsService
{
    constructor()
    {
    }

    /**
     * Creation breadcrumbs based on active categories
     */
    breadcrumbs(items: any[], language: any, city: any): Breadcrumb[]
    {
        items                = items.map(item => filterItemConvert(item));
        const baseUrl        = LocationService.validFullUrl(`/${city.value}/${language.prefix}`);
        const baseBreadcrumb = new Breadcrumb(baseUrl, null);

        switch (language.code)
        {
            case 'ru':
                baseBreadcrumb.title = 'Цветы в ' + city.secondName;
                break;
            case 'en':
                baseBreadcrumb.title = 'Flowers in ' + city.code;
                break;
        }

        const breadcrumbs = [baseBreadcrumb];

        const step_1 = items.filter(node => node.filterId === 3);
        const step_2 = items.filter(node => node.filterId === 4);
        const step_3 = items.filter(node => node.filterId === 5);
        const step_4 = items.filter(node => node.filterId === 6);
        const step_5 = items.filter(node => node.filterId === 7);
        const step_6 = items.filter(node => node.filterId === 10);
        const step_7 = items.filter(node => node.filterId === 25);

        const info = {};

        info[1] = step_1.length > 0 ? [...step_1] : null;
        info[2] = step_2.length > 0 ? [...step_1, ...step_2] : null;
        info[3] = step_3.length > 0 ? [...step_1, ...step_2, ...step_3] : null;
        info[4] = step_4.length > 0 ? [...step_1, ...step_2, ...step_3, ...step_4] : null;
        info[5] = step_5.length > 0 ? [...step_1, ...step_2, ...step_3, ...step_4, ...step_5] : null;
        info[6] = step_6.length > 0 ? [...step_1, ...step_2, ...step_3, ...step_4, ...step_5, ...step_6] : null;
        info[7] = step_7.length > 0 ? [...step_1, ...step_2, ...step_3, ...step_4, ...step_5, ...step_6, ...step_7] : null;

        Object.keys(info).forEach(key =>
        {
            if (info[key])
            {
                const fullData   = FilterInfoService.fullData(info[key], language, city);
                const breadcrumb = new Breadcrumb(fullData.url, fullData.label);
                breadcrumbs.push(breadcrumb);
            }
        });

        return breadcrumbs;
    }
}
