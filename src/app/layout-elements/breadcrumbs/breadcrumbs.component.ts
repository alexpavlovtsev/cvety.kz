import { Component, OnInit } from '@angular/core';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

import { select, Store } from '@ngrx/store';
import { getRouterState, State } from '../../store/reducers';
import { combineLatest, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { TranslationLoaderService } from '@shared/services/translation-loader.service';
import { LocationService } from '@shared/services/location.service';
import { Views } from '../../postgar-config/views';
import { ViewsService } from '@postgar/entity';
import { FilterHelperService } from '@shared/services/filter-helper.service';
import { BreadcrumbsService } from './breadcrumbs.service';

@Component({
    selector   : 'app-breadcrumbs',
    templateUrl: './breadcrumbs.component.html',
    styleUrls  : ['./breadcrumbs.component.css']
})
export class BreadcrumbsComponent implements OnInit
{
    breadcrumbs: Observable<any[]>;

    /**
     * Constructor
     */
    constructor(
        private _store: Store<State>,
        private _translationLoaderService: TranslationLoaderService,
        private locationService: LocationService,
        private _viewsService: ViewsService,
        private _filterHelper: FilterHelperService,
        private _breadcrumbsService: BreadcrumbsService
    )
    {
        // Set the defaults
        this._translationLoaderService.loadTranslations(english, russian);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.breadcrumbs = combineLatest(
            this._viewsService.firstNode(Views.Language),
            this._viewsService.firstNode(Views.City)
        )
            .pipe(
                switchMap(([language, city]) =>
                {
                    return this._store.pipe(select(getRouterState))
                        .pipe(
                            map(route =>
                            {
                                const path = route.state.url.split('/').filter(item => !!item).pop();

                                const activeFilterItems = this._filterHelper.getActiveItems(path, language.id);

                                return this._breadcrumbsService.breadcrumbs(activeFilterItems, language, city);
                            })
                        );
                })
            );
    }
}
