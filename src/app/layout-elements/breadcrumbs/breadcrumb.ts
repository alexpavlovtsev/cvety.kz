import { LocationService } from '@shared/services/location.service';
import { Utils } from '@shared/utils';

export class Breadcrumb
{
    id: string;
    url: string;
    title: string;
    hasLink: boolean;

    constructor(url: string, title: string)
    {
        this.id    = Utils.generateGUID();
        this.url   = LocationService.validFullUrl(url || '');
        this.title = title;
    }
}
