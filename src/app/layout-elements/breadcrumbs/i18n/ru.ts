export const locale = {
    lang: 'ru',
    data: {
        'BREADCRUMBS': {
            'FRONT_PAGE'      : 'Главная',
            'CATALOG_BOUQUETS': 'Каталог букетов',
            'CATALOG_SHOPS'   : 'Каталог магазинов',
            'CITY_PREFIX'     : 'Цветы в '
        }
    }
};
