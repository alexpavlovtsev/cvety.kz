export const locale = {
    lang: 'en',
    data: {
        'BREADCRUMBS': {
            'FRONT_PAGE'      : 'Front Page',
            'CATALOG_BOUQUETS': 'Catalog of Bouquets',
            'CATALOG_SHOPS'   : 'Catalog of Shops',
            'CITY_PREFIX'     : 'Flowers in '
        }
    }
};
