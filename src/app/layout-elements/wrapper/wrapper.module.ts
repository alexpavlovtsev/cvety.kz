import { NgModule } from '@angular/core';
import { WrapperComponent } from './wrapper.component';
import { RouterModule } from '@angular/router';
import { MenuModule } from '../menu/menu.module';
import { PreFooterModule } from '../pre-footer/pre-footer.module';
import { TranslateModule } from '@ngx-translate/core';
import { DirectivesModule } from '@shared/directives/directives.module';
import { FooterModule } from '../footer/footer.module';
import { HeaderModule } from '../header/header.module';
import { SubFooterModule } from '../sub-footer/sub-footer.module';

@NgModule({
    imports     : [
        RouterModule,

        TranslateModule,
        DirectivesModule,

        FooterModule,
        HeaderModule,
        MenuModule,
        SubFooterModule,
        PreFooterModule,
    ],
    declarations: [WrapperComponent]
})
export class WrapperModule
{
}
