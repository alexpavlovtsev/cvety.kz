import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Go } from '../../../store/actions';
import { FilterInfoService, filterItemConvert } from '@shared/services/filter-info.service';
import { Store } from '@ngrx/store';
import { State } from '../../../store/reducers';
import { FilterHelperService } from '@shared/services/filter-helper.service';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { EntityManagerService } from '@postgar/entity';
import { FilterValidVariants } from '@shared/pipes/filter-valid-link.pipe';

const filterItemIds = [
    17, // red
    19, // orange
    26, // white
    24, // violet
    18  // pink
];

@Component({
    selector     : 'app-color',
    templateUrl  : './color.component.html',
    styleUrls    : ['./color.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ColorComponent implements OnInit, OnChanges
{
    @Input() isCatalog: boolean;
    @Input() urls: any;
    @Input() language: any;
    @Input() city: any;
    @Input() path: string;
    @Input() langId: number;
    @Input() filterItemsTr: any[];
    @Input() activeFilterItems: any;

    items: any[];
    counts: any;
    filterId: number;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _entityManager: EntityManagerService,
        private _store: Store<State>,
        private _filterHelper: FilterHelperService,
        private _cd: ChangeDetectorRef,
    )
    {
        this.items    = [];
        this.filterId = 4;

        this.counts          = {};
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._entityManager.getService('BouquetCount').entities$
            .pipe(
                map((nodes: any[]) => nodes.length > 0 ? nodes[0].counts : {}),
                takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.counts = value;
                this.refresh();
            });
    }

    ngOnChanges(changes: SimpleChanges): void
    {
        if (changes['filterItemsTr'])
        {
            this.items = this.filterItemsTr
                .filter(item => item.filterItemByFilterItemId.filterId === this.filterId)
                .filter(item => filterItemIds.includes(item.filterItemId))
                .sort((a, b) =>
                {
                    return filterItemIds.indexOf(a.filterItemId) - filterItemIds.indexOf(b.filterItemId);
                });
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    goTo(url: string, filterItemId: number): void
    {
        if (this.isActive(filterItemId))
        {
            this.disableFilterItem(filterItemId);
        } else
        {
            this._store.dispatch(new Go({path: [url]}));
        }
    }

    disableFilterItem(filterItemId: number): void
    {
        const activeItems = this._filterHelper
            .getActiveItems(this.path, this.langId)
            .filter(item => item.filterItemId !== filterItemId)
            .map(value => filterItemConvert(value));

        const url = FilterInfoService.fullData(activeItems, this.language, this.city).url;
        this._store.dispatch(new Go({path: [url]}));
    }

    isActive(filterItemId: number): boolean
    {
        return this.activeFilterItems.hasOwnProperty(filterItemId);
    }

    isValid(label: any): boolean
    {
        const activeItems = this._filterHelper.getActiveItems(this.path, this.langId).map(item => item.filterItemByFilterItemId);
        const isValid     = FilterValidVariants([...activeItems, label.filterItemByFilterItemId]);

        return !!(this.counts[label.filterItemId] > 2 && !this.isActive(label.filterItemId)) && isValid;
    }

    refresh(): void
    {
        if (!this._cd['destroyed'])
        {
            this._cd.detectChanges();
        }
    }
}
