import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../../store/reducers';
import { FilterHelperService } from '@shared/services/filter-helper.service';
import { Go } from '../../../store/actions';
import { FilterInfoService, filterItemConvert } from '@shared/services/filter-info.service';
import { Subject } from 'rxjs';
import { EntityManagerService } from '@postgar/entity';
import { map, takeUntil } from 'rxjs/operators';

@Component({
    selector     : 'app-quantity',
    templateUrl  : './quantity.component.html',
    styleUrls    : ['./quantity.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class QuantityComponent implements OnInit
{
    @Input() isCatalog: boolean;
    @Input() urls: any;
    @Input() language: any;
    @Input() city: any;
    @Input() path: string;
    @Input() langId: number;
    @Input() activeFilterItems: any;

    items: any[];
    counts: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _entityManager: EntityManagerService,
        private _store: Store<State>,
        private _filterHelper: FilterHelperService,
    )
    {
        this.counts          = {};
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this.items = [
            {
                id   : 933,
                label: '7'
            },
            {
                id   : 11,
                label: '25'
            },
            {
                id   : 14,
                label: '51'
            },
            {
                id   : 16,
                label: '101'
            },
        ];

        this._entityManager.getService('BouquetCount').entities$
            .pipe(
                map((nodes: any[]) => nodes.length > 0 ? nodes[0].counts : {}),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe(value =>
            {
                this.counts = value;
            });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    goTo(url: string, filterItemId: number): void
    {
        if (this.isActive(filterItemId))
        {
            this.disableFilterItem(filterItemId);
        }
        else
        {
            this._store.dispatch(new Go({path: [url]}));
        }
    }

    disableFilterItem(filterItemId: number): void
    {
        const activeItems = this._filterHelper
            .getActiveItems(this.path, this.langId)
            .filter(item => item.filterItemId !== filterItemId)
            .map(value => filterItemConvert(value));

        const url = FilterInfoService.fullData(activeItems, this.language, this.city).url;
        this._store.dispatch(new Go({path: [url]}));
    }

    isActive(filterItemId: number): boolean
    {
        return this.activeFilterItems.hasOwnProperty(filterItemId);
    }
}
