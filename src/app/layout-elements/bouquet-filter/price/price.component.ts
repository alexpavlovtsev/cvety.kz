import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { BouquetFilterComponent } from '../bouquet-filter.component';

const sortOrder = [81, 82, 80, 152, 153, 151];

@Component({
    selector   : 'app-price',
    templateUrl: './price.component.html',
    styleUrls  : ['./price.component.scss']
})
export class PriceComponent implements OnChanges
{
    @Input() isCatalog: boolean;
    @Input() urls: any;
    @Input() language: any;
    @Input() city: any;
    @Input() path: string;
    @Input() langId: number;
    @Input() activeFilterItems: any;
    @Input() filterItemsTr: any[];
    @Input() parentComp: BouquetFilterComponent;

    items: number[];
    filterId: number;

    constructor()
    {
        this.filterId = 10;
        this.items    = [];
    }

    ngOnChanges(changes: SimpleChanges): void
    {
        if (changes['filterItemsTr'])
        {
            this.items = this.filterItemsTr
                .filter(item => item.filterItemByFilterItemId.filterId === this.filterId)
                .sort((a, b) =>
                {
                    return sortOrder.indexOf(a.id) - sortOrder.indexOf(b.id);
                });
        }
    }
}
