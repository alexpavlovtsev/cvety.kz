import { NgModule } from '@angular/core';
import { BouquetFilterComponent } from './bouquet-filter.component';
import { SharedModule } from '@shared/shared.module';
import { ColorComponent } from './color/color.component';
import { QuantityComponent } from './quantity/quantity.component';
import { PriceComponent } from './price/price.component';

@NgModule({
    imports     : [
        SharedModule
    ],
    declarations: [BouquetFilterComponent, ColorComponent, QuantityComponent, PriceComponent],
    exports     : [BouquetFilterComponent]
})
export class BouquetFilterModule
{
}
