import { ChangeDetectorRef, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Go } from '../../store/actions';
import { Store } from '@ngrx/store';
import { State } from '../../store/reducers';
import { FilterInfoService, filterItemConvert } from '@shared/services/filter-info.service';
import { FilterHelperService } from '@shared/services/filter-helper.service';
import { EntityManagerService } from '@postgar/entity';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FilterValidVariants } from '@shared/pipes/filter-valid-link.pipe';
import { MetaService } from '@shared/services/meta.service';

@Component({
    selector     : 'app-bouquet-filter',
    templateUrl  : './bouquet-filter.component.html',
    styleUrls    : ['./bouquet-filter.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class BouquetFilterComponent implements OnInit
{
    @Input() isCatalog: boolean;
    @Input() urls: any;
    @Input() node: any;
    @Input() index: number;
    @Input() filterItemsTr: any[];
    @Input() langId: number;
    @Input() activeFilterItems: any;
    @Input() language: any;
    @Input() city: any;
    @Input() path: string;

    counts: any;
    comp: BouquetFilterComponent;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _metaService: MetaService,
        private _entityManager: EntityManagerService,
        private _store: Store<State>,
        private _filterHelper: FilterHelperService,
        private _cd: ChangeDetectorRef,
    )
    {
        this.counts = {};
        this.comp   = this;

        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._entityManager.getService('BouquetCount').entities$
            .pipe(
                map((nodes: any[]) => nodes.length > 0 ? nodes[0].counts : {}),
                distinctUntilChanged(),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe(value =>
            {
                this.counts = {...value};
                this.refresh();
            });

        const isValid = FilterValidVariants(this.activeItems);

        if (!isValid)
        {
            // this._metaService.setTag('robots', 'noindex, follow');
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    get activeItems(): any[]
    {
        return this._filterHelper
            .getActiveItems(this.path, this.langId)
            .map(value => filterItemConvert(value));
    }

    goTo(url: string, filterItemId: number): void
    {
        if (this.isActive(filterItemId))
        {
            this.disableFilterItem(filterItemId);
        } else
        {
            this._store.dispatch(new Go({path: [url]}));
        }
    }

    disableFilterItem(filterItemId: number): void
    {
        const activeItems = this._filterHelper
            .getActiveItems(this.path, this.langId)
            .filter(item => item.filterItemId !== filterItemId)
            .map(value => filterItemConvert(value));

        const url = FilterInfoService.fullData(activeItems, this.language, this.city).url;
        this._store.dispatch(new Go({path: [url]}));
    }

    isActive(filterItemId: number): boolean
    {
        return this.activeFilterItems.hasOwnProperty(filterItemId);
    }

    isCommon(filterId): boolean
    {
        return filterId !== 4 && filterId !== 3 && filterId !== 10;
    }

    refresh(): void
    {
        if (!this._cd['destroyed'])
        {
            this._cd.detectChanges();
        }
    }

    isValid(label: any): boolean
    {
        const activeItems = this._filterHelper.getActiveItems(this.path, this.langId).map(item => item.filterItemByFilterItemId);
        const isValid     = FilterValidVariants([...activeItems, label.filterItemByFilterItemId]);

        return !!(this.counts[label.filterItemId] > 2 && !this.isActive(label.filterItemId)) && isValid;
    }
}
