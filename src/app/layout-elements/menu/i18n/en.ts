export const locale = {
    lang: 'en',
    data: {
        'MENU': {
            'FLOWERS': 'Flowers',
            'GIFTS'  : 'Gifts',
            'BASKETS': 'Baskets',
            'PLANTS' : 'Plants',
            'BALLS'  : 'Balls',
            'REVIEWS': 'Reviews',
        }
    }
};
