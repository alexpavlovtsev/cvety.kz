export const locale = {
    lang: 'ru',
    data: {
        'MENU': {
            'FLOWERS': 'Цветы',
            'GIFTS'  : 'Подарки',
            'BASKETS': 'Корзины',
            'PLANTS' : 'Растения',
            'BALLS'  : 'Шары'
        }
    }
};
