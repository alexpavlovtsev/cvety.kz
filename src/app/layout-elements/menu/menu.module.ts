import { NgModule } from '@angular/core';
import { MenuComponent } from './menu.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { LanguageSelectorModule } from '../language-selector/language-selector.module';

@NgModule({
    imports     : [
        SharedModule,

        RouterModule,

        TranslateModule,
        LanguageSelectorModule,
    ],
    declarations: [MenuComponent],
    exports     : [MenuComponent]
})
export class MenuModule
{
}
