import { Component, OnDestroy, OnInit } from '@angular/core';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

import { TranslationLoaderService } from '@shared/services/translation-loader.service';
import { Observable, Subject } from 'rxjs';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Views } from '../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { LocationService } from '@shared/services/location.service';

@Component({
    selector   : 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls  : ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnDestroy
{
    langId: number;
    currencies$: Observable<any[]>;
    bouquetPath: string;
    currency: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
        private _translationLoaderService: TranslationLoaderService,
        public locationService: LocationService,
    )
    {
        this._translationLoaderService.loadTranslations(english, russian);
        this.currencies$     = this._entityManager.getService('Currency').entities$;
        this.currency        = {};
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._viewsService.firstNode(Views.Currency)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.currency = value;
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                this.bouquetPath = value.id === 1 ? '/bukety' : '/en/bouquets';
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.langId = value.id;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    getUrl(url: string): string
    {
        return this.langId === 1 ? url : `/en/${url}`;
    }

    /**
     * Set currency
     *
     * @param {any} value
     */
    setCurrency(value: any): void
    {
        const view = {
            id        : Views.Currency,
            nodeIds   : [value.id],
            entityName: 'Currency'
        };
        this._viewsService.upsertOneInCache(view);
    }
}
