import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LanguageSelectorComponent } from './language-selector.component';
import { DirectivesModule } from '../../shared/directives/directives.module';

@NgModule({
    imports     : [
        CommonModule,

        DirectivesModule,
    ],
    declarations: [LanguageSelectorComponent],
    exports     : [LanguageSelectorComponent]
})
export class LanguageSelectorModule
{
}
