import { Component, Inject, OnDestroy, OnInit, Optional, PLATFORM_ID, ViewEncapsulation } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { getRouterState, State } from '../../store/reducers';
import { Subject } from 'rxjs';
import * as fromStore from '../../store';
import { switchMap, take, takeUntil } from 'rxjs/operators';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { LocationService } from '@shared/services/location.service';
import { Views } from '../../postgar-config/views';
import { Utils } from '@shared/utils';
import { MAIN_CITY_CODE } from '../../config';
import { FilterHelperService } from '@shared/services/filter-helper.service';
import { QueryOp } from '@postgar/types';
import { TranslateService } from '@ngx-translate/core';
import { FilterInfoService, filterItemConvert } from '@shared/services/filter-info.service';
import { LinkDefinition, LinkService } from '@shared/services/link.service';
import { isPlatformServer } from '@angular/common';

@Component({
    selector     : 'app-language-selector',
    templateUrl  : './language-selector.component.html',
    styleUrls    : ['./language-selector.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LanguageSelectorComponent implements OnInit, OnDestroy
{
    language: any;
    languages: any[];
    openLanguage: boolean;
    url: string;
    cityCode: string;
    city: any;
    links: any;
    params: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _store: Store<State>,
        private _viewsService: ViewsService,
        private _locationService: LocationService,
        private _entityManager: EntityManagerService,
        private _filterHelper: FilterHelperService,
        private _translateService: TranslateService,
        @Optional() private linkService: LinkService,
        @Inject(PLATFORM_ID) private platformId: Object,
        private locationService: LocationService,
    )
    {
        this.languages    = [];
        this.openLanguage = false;
        this.links        = {};

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(node =>
            {
                this.language = node;
            });

        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.cityCode = node.code === MAIN_CITY_CODE ? '' : node.code;
                this.city     = node;
            });

        this._entityManager.getService('Language').entities$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(nodes =>
            {
                this.languages = nodes;
            });

        this._store.pipe(select(getRouterState))
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(route =>
            {
                this.params = route.state.params;
                this.url    = route.state.url;
                this.start(route.state.data.type);
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Go to
     */
    changeUrl(url: string, event: any, language: any): void
    {
        Utils.stopPropagation(event);
        this._store.dispatch(new fromStore.Go({path: [url]}));
        this.openLanguage = false;
        this._translateService.use(language.code);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Start process
     *
     * @param routeType
     */
    start(routeType: string): void
    {
        const url = this.url.split('/').filter(item => !!item).pop();

        switch (routeType)
        {
            case 'front':
                this.setLinks('');
                break;

            case 'bouquet':
                this.forBouquet();
                break;

            case 'bouquets':
                this.forBouquets();
                break;

            case 'thematic':
                this.forThematic(url);
                break;

            case 'page':
                this.setLinks(url);
                break;

            case 'order':
                this.setLinks('order');
                break;

            case 'sitemap':
                this.setLinks('sitemap');
                break;

            case 'catalog':
                this.forCatalog();
                break;

            case '404':
                this.setLinks('404');
                break;
        }
    }

    /**
     * For catalog bouquets
     */
    private forBouquets(): void
    {
        this.links = this.languages.reduce((_entities: any, lang: any) =>
        {
            const path = lang.code === 'ru' ? 'bukety' : 'bouquets';
            return {
                ..._entities,
                [lang.code]: LocationService.validFullUrl(`/${this.cityCode}/${langCode(lang.code)}/${path}`)
            };
        }, {});

        this.setHrefLang();
    }

    private forThematic(url: string): void
    {
        switch (url)
        {
            case 'populyarnye-cvety':
            case 'featured-flowers':

                this.links = {
                    'ru': LocationService.validFullUrl(`/${this.cityCode}/populyarnye-cvety/`),
                    'en': LocationService.validFullUrl(`/${this.cityCode}/en/featured-flowers/`)
                };

                this.setHrefLang();

                break;
        }
    }

    /**
     * Set links for bouquet page
     */
    private forBouquet(): void
    {
        this._viewsService.firstNode(Views.Bouquet)
            .pipe(
                take(1),
                takeUntil(this._unsubscribeAll),
                switchMap((value: any) =>
                {
                    return this._entityManager
                        .getService('BouquetTranslate')
                        .getWithQuery({
                            id     : 'bouquetForLangBlock',
                            filters: {
                                bouquetId: {
                                    [QueryOp.equalTo]: value.bouquetId
                                }
                            }
                        });
                })
            )
            .subscribe((nodes: any[]) =>
            {
                this.links = this.languages.reduce((_entities: any, lang: any) =>
                {
                    const translate = nodes.find(tr => tr.languageId === lang.id);

                    const url              = translate === undefined ? '' : translate.url;
                    const bouquetUrlPrefix = lang.id === 1 ? 'bukety' : 'bouquets';
                    const fullPath         = `/${this.cityCode}/${langCode(lang.code)}/${bouquetUrlPrefix}/${url}/`;

                    return {
                        ..._entities,
                        [lang.code]: LocationService.validFullUrl(fullPath)
                    };
                }, {});

                this.setHrefLang();
            });
    }

    /**
     * Set locale urls
     *
     * @param path
     */
    private setLinks(path: string): void
    {
        this.links = this.languages.reduce((_entities: any, lang: any) =>
        {
            return {
                ..._entities,
                [lang.code]: LocationService.validFullUrl(`/${this.cityCode}/${langCode(lang.code)}/${path}`)
            };
        }, {});

        this.setHrefLang();
    }

    /**
     * Set links for bouquet landing
     */
    forCatalog(): void
    {
        const activeIds = this._filterHelper.getActiveIds(this.params.catalog);

        this.links = this.languages.reduce((_entities: any, lang: any) =>
        {
            const activeLocalItems = this._filterHelper.filterItemTr
                .filter((item: any) => activeIds.some(id => id === item.filterItemId))
                .filter((item: any) => item.languageId === lang.id)
                .map(value => filterItemConvert(value));

            const url = FilterInfoService.fullData(activeLocalItems, lang, this.city).url;

            return {
                ..._entities,
                [lang.code]: LocationService.validFullUrl(`/${url}`)
            };
        }, {});

        this.setHrefLang();
    }

    private setHrefLang(): void
    {
        const ru = {
            rel     : 'alternate',
            hreflang: 'ru',
            href    : LocationService.validFullUrl(this.locationService.absolutePrefix + this.links.ru)
        };

        const en = {
            rel     : 'alternate',
            hreflang: 'en',
            href    : LocationService.validFullUrl(this.locationService.absolutePrefix + this.links.en)
        };

        if (isPlatformServer(this.platformId))
        {
            this.linkService.inject(ru);
            this.linkService.inject(en);
        }
    }
}

function langCode(code: string): string
{
    return code === 'ru' ? '' : 'en';
}
