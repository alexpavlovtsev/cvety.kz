import { NgModule } from '@angular/core';
import { DeliveryBlockComponent } from './delivery-block.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared/shared.module';
import { NguiInviewModule } from '@shared/components/ngui-inview/ngui-inview.module';

@NgModule({
    imports     : [
        TranslateModule,
        SharedModule,
        NguiInviewModule,
    ],
    declarations: [DeliveryBlockComponent],
    exports     : [DeliveryBlockComponent]
})
export class DeliveryBlockModule
{
}
