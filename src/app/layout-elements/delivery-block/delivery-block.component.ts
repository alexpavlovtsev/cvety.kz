import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { GraphQLClient } from '@postgar/graphql';
import { ScriptService } from '@shared/services/script.service';

const CallbacksQuery = `
query allCallbacks {
  allCallbacks(filter: {recipientPhoto: {isNull: false}}) {
    nodes {
      id
      orderByOrderId {
        id
        productsImages
        jsonProductName
        recipientName
      }
    }
  }
}`;

@Component({
    selector   : 'app-delivery-block',
    templateUrl: './delivery-block.component.html',
    styleUrls  : ['./delivery-block.component.scss']
})
export class DeliveryBlockComponent implements OnInit, AfterViewInit, OnDestroy
{
    list: any[];
    langId: number;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _client: GraphQLClient,
        private _viewsService: ViewsService,
        private _scriptService: ScriptService,
    )
    {
        this.list            = [];
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                this.langId = value.id;
                this.list   = list()
                    .filter(item => item.languageId === value.id)
                    .map(item =>
                    {
                        return {
                            ...item,
                            showImage: false
                        };
                    });
            });

        this._client.get(CallbacksQuery, {}).subscribe((value: any) =>
        {
            value['allCallbacks'].nodes
                .map(item =>
                {
                    return {
                        image      : item.orderByOrderId.productsImages[0],
                        title      : item.orderByOrderId.recipientName + ' с букетом',
                        bouquetName: item.orderByOrderId.jsonProductName,
                        url        : null,
                        languageId : 1,
                        showImage: false
                    };
                })
                .forEach(item =>
                {
                    this.list.push(item);
                });
        });
    }

    ngAfterViewInit(): void
    {
        setTimeout(() =>
        {
            this.initScripts();
        }, 0);
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.load(['slick', 'bootstrap']).then(() =>
            {
                const settings = scriptData();
                this._scriptService.appendScript(settings, 'slick-delivery');
            });
        });
    }
}

function list(): any[]
{
    return [
        {
            image      : 'assets/images/delivery/IMAGE-2018_04_12-142_40_28-PP.jpg',
            title      : 'Айнура с букетом',
            bouquetName: '«Надежда»',
            url        : '/bukety/nadezhda/',
            languageId : 1
        },
        {
            image      : 'assets/images/delivery/2018_04_14_13_30_05.jpg',
            title      : 'Виолетта с букетом',
            bouquetName: '«Букет из 15 синих ирисов»',
            url        : '/bukety/buket-iz-15-sinih-irisov/',
            languageId : 1
        },
        {
            image      : 'assets/images/delivery/IMAGE-2018_04_12-142_41_40-PP.jpg',
            title      : 'Дарига с букетом',
            bouquetName: '«Красивый букет для мамы из белых и красных роз»',
            url        : '/bukety/krasivyy-buket-dlya-mamy-iz-belyh-i-krasnyh-roz/',
            languageId : 1
        },
        {
            image      : 'assets/images/delivery/EKEaz_Y1QLU.jpg',
            title      : 'Жансая с букетом',
            bouquetName: '«Букет из гортензии»',
            url        : '/bukety/buket-iz-gortenzii/',
            languageId : 1
        },
        {
            image      : 'assets/images/delivery/IMAGE-2018_04_12-142_43_34-PP.jpg',
            title      : 'Макпал с букетом',
            bouquetName: '«Летний день»',
            url        : '/bukety/letniy-den/',
            languageId : 1
        },
        {
            image      : 'assets/images/delivery/2018_04_14_13_29_53.jpg',
            title      : 'Лидия с букетом',
            bouquetName: '«Моя фея»',
            url        : '/bukety/moya-feya/',
            languageId : 1
        },
        {
            image      : 'assets/images/delivery/2018_04_14_13_31_29.jpg',
            title      : 'Мадина с букетом',
            bouquetName: '«Единственная моя»',
            url        : '/bukety/edinstvennaya-moya/',
            languageId : 1
        },
        {
            image      : 'assets/images/delivery/mar.jpg',
            title      : 'Мархабат с букетом',
            bouquetName: '«Букет 15 морковных роз»',
            url        : '/bukety/buket-15-morkovnyh-roz/',
            languageId : 1
        },
        {
            image      : 'assets/images/delivery/gulm.jpg',
            title      : 'Гульмира с букетом',
            bouquetName: '«Композиция Romantic»',
            url        : '/bukety/kompoziciya-romantic/',
            languageId : 1
        },
        {
            image      : 'assets/images/delivery/2018_04_14_13_33_08.jpg',
            title      : 'Любовь с букетом',
            bouquetName: '«Заряд летних эмоций»',
            url        : '/bukety/zaryad-letnih-emociy/',
            languageId : 1
        },
        {
            image      : 'assets/images/delivery/ars.jpg',
            title      : 'Arystan with bouquet',
            bouquetName: '«Bouquet 101 white roses»',
            url        : '/en/bouquets/bouquet-101-white-roses/',
            languageId : 2
        },
        {
            image      : 'assets/images/delivery/IMAGE-2018_04_12-142_41_40-PP.jpg',
            title      : 'Dariga with bouquet',
            bouquetName: '«Beautiful bouquet for mother of white and red roses»',
            url        : '/en/bouquets/beautiful-bouquet-for-mother-of-white-and-red-roses/',
            languageId : 2
        },
        {
            image      : 'assets/images/delivery/EKEaz_Y1QLU.jpg',
            title      : 'Jansaya with bouquet',
            bouquetName: '«Bouquet of hydrangea»',
            url        : '/en/bouquets/bouquet-of-hydrangea/',
            languageId : 2
        },
        {
            image      : 'assets/images/delivery/IMAGE-2018_04_12-142_45_55-PP.jpg',
            title      : 'Princess with bouquet',
            bouquetName: '«Gift set "Peonies"»',
            url        : '/en/bouquets/gift-set-peonies/',
            languageId : 2
        },
        {
            image      : 'assets/images/delivery/2018_04_14_13_30_05.jpg',
            title      : 'Violetta with bouquet',
            bouquetName: '«Bouquet of 15 iris flowers»',
            url        : '/en/bouquets/bouquet-of-15-iris-flowers/',
            languageId : 2
        },
        {
            image      : 'assets/images/delivery/ayzhan.jpg',
            title      : 'Gulim with bouquet',
            bouquetName: '«Bouquet of 15 alstroemerias»',
            url        : '/en/bouquets/bouquet-of-15-alstroemerias/',
            languageId : 2
        },
        {
            image      : 'assets/images/delivery/mar.jpg',
            title      : 'Marhabat with bouquet',
            bouquetName: '«Bouquet of 15 orange roses»',
            url        : '/en/bouquets/bouquet-of-15-orange-roses/',
            languageId : 2
        },
        {
            image      : 'assets/images/delivery/2018_04_14_13_27_23.jpg',
            title      : 'Catherine with bouquet',
            bouquetName: '«Beautiful Desdemona»',
            url        : '/en/bouquets/beautiful-desdemona/',
            languageId : 2
        },
        {
            image      : 'assets/images/delivery/2018_04_14_13_32_14.jpg',
            title      : 'Ania with bouquet',
            bouquetName: '«For mother»',
            url        : '/en/bouquets/for-mother/',
            languageId : 2
        },
    ];
}


function scriptData(): string
{
    return `
$('.cv_delivery-carousel').slick({
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 769,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 481,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
`;
}

