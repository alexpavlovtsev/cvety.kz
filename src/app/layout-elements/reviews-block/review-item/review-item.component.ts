import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
    selector   : 'app-review-item',
    templateUrl: './review-item.component.html',
    styleUrls  : ['./review-item.component.scss']
})
export class ReviewItemComponent implements OnInit
{
    @Input() node: any;

    bouquetName: string;
    recipientName: string;
    senderName: string;
    image: string;
    deliveryDate: string;

    constructor()
    {
    }

    ngOnInit()
    {
        if (this.node.orderByOrderId)
        {
            this.bouquetName = this.node.orderByOrderId.jsonProductName
                .split('\\n')
                .join(', ');

            this.senderName    = this.node.orderByOrderId.senderName;
            this.recipientName = this.node.orderByOrderId.recipientName;
            this.image         = this.node.orderByOrderId.productsImages[0];
            this.deliveryDate  = moment(this.node.orderByOrderId.deliveryDate).format('DD.MM.YYYY');
        }
    }
}
