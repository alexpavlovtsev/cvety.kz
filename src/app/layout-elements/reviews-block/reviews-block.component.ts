import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { GraphQLClient } from '@postgar/graphql';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { ScriptService } from '@shared/services/script.service';
import { Views } from '../../postgar-config/views';
import { filter, takeUntil } from 'rxjs/operators';
import { TranslationLoaderService } from '@shared/services/translation-loader.service';
import { locale as english } from '../../pages/faq/i18n/en';
import { locale as russian } from '../../pages/faq/i18n/ru';

@Component({
    selector   : 'app-reviews-block',
    templateUrl: './reviews-block.component.html',
    styleUrls  : ['./reviews-block.component.scss']
})
export class ReviewsBlockComponent implements OnInit, AfterViewInit, OnDestroy
{
    list: any[];
    langId: number;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _client: GraphQLClient,
        private _viewsService: ViewsService,
        private _scriptService: ScriptService,
        private _entityManager: EntityManagerService,
        private _translationLoaderService: TranslationLoaderService,
    )
    {
        this._translationLoaderService.loadTranslations(english, russian);

        this.list            = [];
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._viewsService.nodes(Views.Callbacks)
            .pipe(
                filter(nodes => nodes.length > 0),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe((nodes: any[]) =>
            {
                this.list = nodes;
            });
    }

    ngAfterViewInit(): void
    {
        setTimeout(() =>
        {
            this.initScripts();
        }, 0);
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.load(['slick', 'bootstrap']).then(() =>
            {
                const settings = scriptData();
                this._scriptService.appendScript(settings, 'slick-reviews-front');
            });
        });
    }
}

function scriptData(): string
{
    return `
$('.cv_feedback-carousel').slick({
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 2,
  responsive: [
    {
      breakpoint: 769,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 481,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
`;
}
