import { NgModule } from '@angular/core';
import { ReviewsBlockComponent } from './reviews-block.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared/shared.module';
import { ReviewItemComponent } from './review-item/review-item.component';
import { PipesModule } from '@shared/pipes/pipes.module';

@NgModule({
    imports     : [
        TranslateModule,
        SharedModule,
        PipesModule,
    ],
    declarations: [ReviewsBlockComponent, ReviewItemComponent],
    exports     : [ReviewsBlockComponent]
})
export class ReviewsBlockModule
{
}
