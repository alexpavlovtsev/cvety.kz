export const locale = {
    lang: 'ru',
    data: {
        'HEADER': {
            'CITY_OF_DELIVERY'          : 'Город доставки',
            'GUARANTEE_OF_FRESH_BOUQUET': 'Гарантия свежести букета – 3 дня',
            'DELIVERY'                  : 'Доставка',
            'DELIVERY_IS_FREE'          : 'Доставка бесплатно',
            'PAYMENT'                   : 'оплата',
            'CONTACTS'                  : 'контакты',
            'GUARANTEES'                : 'гарантии',
            'CITY'                      : 'город',
            'LOGO_ALT'                  : 'Onetwoflowers.ru - доставка цветов на заказ по ',
            'LOGO_TITLE'                : 'Доставка цветов по ',
            'REVIEWS'                   : 'Отзывы',
        }
    }
};
