export const locale = {
    lang: 'en',
    data: {
        'HEADER': {
            'CITY_OF_DELIVERY'          : 'City of delivery',
            'GUARANTEE_OF_FRESH_BOUQUET': 'Guarantee of fresh bouquet - 3 days',
            'DELIVERY'                  : 'Delivery',
            'DELIVERY_IS_FREE'          : 'Delivery is free',
            'PAYMENT'                   : 'payment',
            'CONTACTS'                  : 'contacts',
            'GUARANTEES'                : 'guarantees',
            'CITY'                      : 'city',
            'LOGO_ALT'                  : 'Onetwoflowers.ru - Delivery of Flowers to Order by ',
            'LOGO_TITLE'                : 'Flowers Delivery by ',
            'REVIEWS'                   : 'Reviews',
        }
    }
};
