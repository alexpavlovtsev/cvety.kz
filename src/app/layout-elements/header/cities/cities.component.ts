import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Views } from '../../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { LocationService } from '@shared/services/location.service';
import { Go } from '../../../store/actions';
import { State } from '../../../store/reducers';
import { Store } from '@ngrx/store';
import { MAIN_CITY_CODE } from '../../../config';
import { FormControl } from '@angular/forms';
import { Grammar } from '@shared/grammar';

@Component({
    selector   : 'app-cities',
    templateUrl: './cities.component.html',
    styleUrls  : ['./cities.component.scss']
})
export class CitiesComponent implements OnInit, OnDestroy
{
    searchControl: FormControl;
    city: any;
    cities: any[];
    openCities: boolean;
    language: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _store: Store<State>,
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
        private _locationService: LocationService
    )
    {
        this.searchControl   = new FormControl();
        this.city            = {};
        this.cities          = [];
        this.language        = {};
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.city = node;
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.language = node;
            });

        this._entityManager.getService('City').entityMap$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any) =>
            {
                this.cities = Object.keys(nodes).map(key => nodes[key]);
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    selectCity(city: any): void
    {
        const cityCode  = city.code === MAIN_CITY_CODE ? '' : city.code;
        this.openCities = false;
        const langCode  = this.language.code === 'ru' ? '' : this.language.code;
        const url       = LocationService.validFullUrl('/' + cityCode + '/' + langCode);
        // console.log(url);
        this._store.dispatch(new Go({path: [url]}));
    }

    getLabel(city: any): void
    {
        return this.language.id === 1 ? city.name : city.enName;
    }
}
