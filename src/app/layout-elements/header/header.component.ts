import { Component, OnDestroy, OnInit } from '@angular/core';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

import { TranslationLoaderService } from '@shared/services/translation-loader.service';
import { LocationService } from '@shared/services/location.service';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { combineLatest, Subject } from 'rxjs';
import { MAIN_CITY_CODE } from '../../config';

@Component({
    selector   : 'app-header',
    templateUrl: './header.component.html',
    styleUrls  : ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy
{
    langId: number;
    isMain: boolean;
    secondName: string;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService,
        private _translationLoaderService: TranslationLoaderService,
        public locationService: LocationService,
    )
    {
        this._translationLoaderService.loadTranslations(english, russian);

        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        combineLatest(
            this._viewsService.firstNode(Views.Language),
            this._viewsService.firstNode(Views.City)
        )
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(([language, city]) =>
            {
                this.langId = language.id;
                this.isMain = city.code === MAIN_CITY_CODE;

                switch (city.code)
                {
                    case 'moscow':
                        this.secondName = this.langId === 1 ? 'Москве' : 'Moscow';
                        break;

                    case 'sankt-peterburg':
                        this.secondName = this.langId === 1 ? 'Санкт-Петербургу' : 'Sankt-Peterburg';
                        break;

                    case 'ekaterinburg':
                        this.secondName = this.langId === 1 ? 'Екатеринбургу' : 'Ekaterinburg';
                        break;

                    case 'nizhniy-novgorod':
                        this.secondName = this.langId === 1 ? 'Нижнему Новгороду' : 'Nizhniy-Novgorod';
                        break;

                    case 'novosibirsk':
                        this.secondName = this.langId === 1 ? 'Новосибирску' : 'Novosibirsk';
                        break;
                }
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    getUrl(url: string): string
    {
        return this.langId === 1 ? url : `/en/${url}`;
    }

    goTo(url): void
    {
        console.log(url);
    }
}
