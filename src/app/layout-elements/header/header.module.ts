import { NgModule } from '@angular/core';
import { HeaderComponent } from './header.component';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CitiesComponent } from './cities/cities.component';

@NgModule({
    imports     : [
        SharedModule,

        RouterModule,

        TranslateModule,
    ],
    declarations: [HeaderComponent, CitiesComponent],
    exports     : [HeaderComponent]
})
export class HeaderModule
{
}
