export const locale = {
    lang: 'ru',
    data: {
        'FOOTER': {
            'DESCRIPTION': 'Заказ и доставка свежих цветов по',
            'PAYMENT'    : 'Оплата',
            'GUARANTEE'  : 'Гарантия',
            'CONTACTS'   : 'Контакты',
            'DELIVERY'   : 'Доставка',
            'SITEMAP'    : 'Карта сайта',
            'REVIEWS'    : 'Отзывы'
        }
    }
};
