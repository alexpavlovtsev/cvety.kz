export const locale = {
    lang: 'en',
    data: {
        'FOOTER': {
            'DESCRIPTION': 'Order and delivery of fresh flowers in',
            'PAYMENT'    : 'Payment',
            'GUARANTEE'  : 'Guarantee',
            'CONTACTS'   : 'Contacts',
            'DELIVERY'   : 'Delivery',
            'SITEMAP'    : 'Sitemap',
            'REVIEWS'    : 'Reviews'
        }
    }
};
