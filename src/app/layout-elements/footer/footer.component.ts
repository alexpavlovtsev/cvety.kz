import { Component, OnDestroy, OnInit } from '@angular/core';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

import { TranslationLoaderService } from '@shared/services/translation-loader.service';
import { combineLatest, Observable, Subject } from 'rxjs';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { takeUntil } from 'rxjs/operators';
import { Views } from '../../postgar-config/views';
import { LocationService } from '@shared/services/location.service';
import { MAIN_CITY_CODE } from '../../config';

@Component({
    selector   : 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls  : ['./footer.component.scss']
})
export class FooterComponent implements OnInit, OnDestroy
{
    currencies$: Observable<any[]>;
    languages$: Observable<any[]>;

    citySecondName: string;
    language: any;
    currency: any;
    isMain: boolean;
    cityCode: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _translationLoaderService: TranslationLoaderService,
        private _entityManager: EntityManagerService,
        private _viewsService: ViewsService,
        public locationService: LocationService
    )
    {
        this._translationLoaderService.loadTranslations(english, russian);

        this.language = {};
        this.currency = {};

        this.currencies$ = this._entityManager.getService('Currency').entities$;
        this.languages$  = this._entityManager.getService('Language').entities$;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.Currency)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.currency = value;
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.language = value;
            });

        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.isMain   = node.code === MAIN_CITY_CODE;
                this.cityCode = this.isMain ? '' : node.code;
            });

        combineLatest(
            this._viewsService.firstNode(Views.Language),
            this._viewsService.firstNode(Views.City)
        )
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(([language, city]) =>
            {
                this.language = language;
                this.isMain = city.code === MAIN_CITY_CODE;

                switch (city.code)
                {
                    case 'moscow':
                        this.citySecondName = language.id === 1 ? 'Москве' : 'Moscow';
                        break;

                    case 'sankt-peterburg':
                        this.citySecondName = language.id === 1 ? 'Санкт-Петербургу' : 'Sankt-Peterburg';
                        break;

                    case 'ekaterinburg':
                        this.citySecondName = language.id === 1 ? 'Екатеринбургу' : 'Ekaterinburg';
                        break;

                    case 'nizhniy-novgorod':
                        this.citySecondName = language.id === 1 ? 'Нижнему Новгороду' : 'Nizhniy-Novgorod';
                        break;

                    case 'novosibirsk':
                        this.citySecondName = language.id === 1 ? 'Новосибирску' : 'Novosibirsk';
                        break;
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Set currency
     *
     * @param {any} value
     */
    setCurrency(value: any): void
    {
        const view = {
            id        : Views.Currency,
            nodeIds   : [value.id],
            entityName: 'Currency'
        };
        this._viewsService.upsertOneInCache(view);
    }

    url(path: string): string
    {
        const code = this.language.code === 'ru' ? '' : 'en';
        return LocationService.validFullUrl('/' + code + '/' + path);
    }

    urlWithCity(path: string): string
    {
        const code = this.language.code === 'ru' ? '' : 'en';
        return LocationService.validFullUrl(this.cityCode + '/' + code + '/' + path);
    }
}
