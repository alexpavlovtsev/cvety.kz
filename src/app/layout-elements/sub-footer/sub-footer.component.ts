import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Views } from '../../postgar-config/views';
import { map, switchMap, takeUntil } from 'rxjs/operators';
import { ViewsService } from '@postgar/entity';

@Component({
    selector   : 'app-sub-footer',
    templateUrl: './sub-footer.component.html',
    styleUrls  : ['./sub-footer.component.scss']
})
export class SubFooterComponent implements OnInit, OnDestroy
{
    citySecondName: string;
    langId: number;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService,
    )
    {
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(
                switchMap((language: any) =>
                {
                    this.langId = language.id;
                    return this._viewsService.firstNode(Views.City)
                        .pipe(
                            map(value =>
                            {
                                return language.id === 1 ? value.secondName : value.enName;
                            })
                        );
                }),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe(value =>
            {
                this.citySecondName = value;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
