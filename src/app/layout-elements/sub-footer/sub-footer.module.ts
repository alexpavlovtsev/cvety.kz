import { NgModule } from '@angular/core';
import { SubFooterComponent } from './sub-footer.component';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LanguageSelectorModule } from '../language-selector/language-selector.module';

@NgModule({
    imports     : [
        SharedModule,

        RouterModule,

        TranslateModule,
        LanguageSelectorModule,
    ],
    declarations: [SubFooterComponent],
    exports     : [SubFooterComponent]
})
export class SubFooterModule
{
}
