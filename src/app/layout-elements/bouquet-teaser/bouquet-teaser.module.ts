import { NgModule } from '@angular/core';
import { BouquetTeaserComponent } from './bouquet-teaser.component';
import { SharedModule } from '@shared/shared.module';
import { NguiInviewModule } from '@shared/components/ngui-inview/ngui-inview.module';

@NgModule({
    imports     : [
        SharedModule,
        NguiInviewModule,
    ],
    declarations: [BouquetTeaserComponent],
    exports     : [BouquetTeaserComponent]
})
export class BouquetTeaserModule
{
}
