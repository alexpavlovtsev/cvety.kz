import { Component, Input, OnInit } from '@angular/core';
import { Views } from '../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { ViewsService } from '@postgar/entity';
import { Subject } from 'rxjs';

@Component({
    selector   : 'app-bouquet-teaser',
    templateUrl: './bouquet-teaser.component.html',
    styleUrls  : ['./bouquet-teaser.component.scss']
})
export class BouquetTeaserComponent implements OnInit
{
    @Input() item: any;

    langId: number;
    language: any;
    currency: any;
    showImage: boolean;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService
    )
    {
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.language = node;
                this.langId   = node.id;
            });

        this._viewsService.firstNode(Views.Currency)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.currency = node;
            });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    randomRating(id: number): string
    {
        const lastDigit = id.toString().split('').pop();

        return lastDigit === '0' ? '5.0' : `4.${lastDigit}`;
    }

    view(name: string): void
    {
        console.log(name);
    }
}
