export const locale = {
    lang: 'en',
    data: {
        'PRE_FOOTER': {
            'WHERE_WE_DELIVER': 'Where we deliver',
            'ALL_CITIES'      : 'All Cities'
        }
    }
};
