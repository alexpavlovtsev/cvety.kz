export const locale = {
    lang: 'ru',
    data: {
        'PRE_FOOTER': {
            'WHERE_WE_DELIVER': 'Куда мы доставляем',
            'ALL_CITIES'      : 'Все города'
        }
    }
};
