import { NgModule } from '@angular/core';
import { PreFooterComponent } from './pre-footer.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports     : [
        SharedModule,

        RouterModule,

        TranslateModule,
    ],
    declarations: [PreFooterComponent],
    exports     : [PreFooterComponent]
})
export class PreFooterModule
{
}
