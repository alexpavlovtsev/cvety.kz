import { Component, OnDestroy, OnInit } from '@angular/core';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

import { TranslationLoaderService } from '@shared/services/translation-loader.service';

import { EntityManagerService, ViewsService } from '@postgar/entity';

import { Subject } from 'rxjs';
import { switchMap, take, takeUntil } from 'rxjs/operators';
import { Views } from '../../postgar-config/views';
import { MAIN_CITY_CODE } from '../../config';
import { Grammar } from '@shared/grammar';
import { LocationService } from '@shared/services/location.service';

@Component({
    selector   : 'app-pre-footer',
    templateUrl: './pre-footer.component.html',
    styleUrls  : ['./pre-footer.component.scss']
})
export class PreFooterComponent implements OnInit, OnDestroy
{
    cities: any[];
    container: any[][];
    langId: number;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param _viewsService
     * @param _translationLoaderService
     * @param _entityManager
     */
    constructor(
        private _viewsService: ViewsService,
        private _translationLoaderService: TranslationLoaderService,
        private _entityManager: EntityManagerService,
    )
    {
        this._translationLoaderService.loadTranslations(english, russian);
        this.cities    = [];
        this.container = [];

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(
                switchMap((language: any) =>
                    {
                        this.langId = language.id;

                        return this._entityManager.getService('City').entities$
                            .pipe(take(1));
                    }
                ),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe(cities =>
            {
                this.cities    = [...cities].splice(0, 40);
                this.container = [];

                while (this.cities.length)
                {
                    if (this.container.length === 8)
                    {
                        break;
                    }
                    this.container.push(this.cities.splice(0, 4));
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    url(code: string): string
    {
        const langCode = this.langId === 1 ? '' : 'en';
        const cityCode = code === MAIN_CITY_CODE ? '' : code;

        return LocationService.validFullUrl(`/${cityCode}/${langCode}`);
    }

    url2(path: string): string
    {
        const langCode = this.langId === 1 ? '' : 'en';

        return LocationService.validFullUrl(`/${langCode}/${path}`);
    }
}
