import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Effect, Actions } from '@ngrx/effects';
import { tap, map } from 'rxjs/operators';

import * as RouterActions from 'app/store/actions/router.action';
import { LocationService } from '@shared/services/location.service';
import { StateHelperService } from '@shared/services/state-helper.service';
import { State } from '../reducers';
import { Store } from '@ngrx/store';
import { ROUTER_NAVIGATION } from '@ngrx/router-store';

@Injectable()
export class RouterEffects
{
    /**
     * Constructor
     */
    constructor(
        private store: Store<State>,
        private actions$: Actions,
        private router: Router,
        private location: Location,
        private stateHelperService: StateHelperService
    )
    {
    }

    /**
     * Navigate
     */
    @Effect({dispatch: false})
    navigate$ = this.actions$.ofType(RouterActions.GO).pipe(
        map((action: RouterActions.Go) => action.payload),
        tap(({path, query: queryParams, extras}) => {
            const url = LocationService.doubleSlashValidation(path[0]);
            this.router.navigateByUrl(url);
        })
    );

    /**
     * Navigate back
     * @type {Observable<any>}
     */
    @Effect({dispatch: false})
    navigateBack$ = this.actions$
                        .ofType(RouterActions.BACK)
                        .pipe(tap(() => this.location.back()));

    /**
     * Navigate forward
     * @type {Observable<any>}
     */
    @Effect({dispatch: false})
    navigateForward$ = this.actions$
                           .ofType(RouterActions.FORWARD)
                           .pipe(tap(() => this.location.forward()));

    @Effect({dispatch: false})
    navigateEvent$ = this.actions$
        .ofType(ROUTER_NAVIGATION)
        .pipe(tap((ev: any) =>
        {
            // Set city, language, meta tags, and h1
            this.stateHelperService.setStateValues(ev.payload.routerState.url, ev.payload.routerState.data.type);
        }));
}
