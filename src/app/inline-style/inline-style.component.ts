import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-inline-style',
  template: '',
  styleUrls: [
      './inline-style.component.scss',
      '../../assets/css/bootstrap.min.css',
      '../../assets/slick/slick.scss',
      '../../assets/slick/slick-theme.scss',
      // '../../assets/css/main.css',
      '../../assets/scss/main.scss',
      '../../assets/scss/helper/ebgaramond.scss',
      '../../assets/scss/helper/roboto.scss',
      // '../../assets/scss/export.scss'
  ],
  encapsulation: ViewEncapsulation.None,
})
export class InlineStyleComponent {}
