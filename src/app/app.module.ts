import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PostgarModule } from '@postgar/postgar.module';

import { ToastModule } from '@shared/components/toast/toast.module';
import { ProgressBarModule } from '@shared/components/progress-bar/progress-bar.module';

import { TranslateModule } from '@ngx-translate/core';

import { RoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { AppStoreModule } from './store/store.module';
import { postgarConfig } from './postgar-config';
import { WrapperModule } from './layout-elements/wrapper/wrapper.module';

@NgModule({
    imports     : [
        BrowserModule.withServerTransition({appId: 'my-app'}),
        HttpClientModule,
        RouterModule,
        RoutingModule,
        BrowserAnimationsModule,

        TranslateModule.forRoot(),

        // App modules
        AppStoreModule,

        // Postgar module
        PostgarModule.forRoot(postgarConfig),

        ProgressBarModule,
        ToastModule,
        WrapperModule,
    ],
    declarations: [AppComponent],
    providers   : [
    ],
    bootstrap   : [AppComponent]
})
export class AppModule
{
}
