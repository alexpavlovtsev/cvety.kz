import { StoreView } from '@postgar/entity/models';

export enum Views
{
    Language,
    Currency,
    City,
    Bouquet,
    FrontPage,
    Bouquets,
    Catalog,
    Order,
    MinPrice,
    Inexpensive,
    Extraordinary,
    InBox,
    Featured,
    Featured2,
    orderReview,
    Callbacks,
}

export const VIEWS: StoreView[] = [
    // {
    //     id           : Views.Language,
    //     entityName   : 'CurrentLanguage',
    // },
    // {
    //     id           : Views.Currency,
    //     entityName   : 'CurrentCurrency',
    // },
    {
        id           : Views.Inexpensive,
        entityName   : 'BouquetTranslate',
        pageSize     : 9,
        sortActive   : 'bouquetPrice',
        sortDirection: 'asc'
    },
    {
        id           : Views.Extraordinary,
        entityName   : 'BouquetTranslate',
        pageSize     : 9,
        // sortActive   : 'bouquetPrice',
        // sortDirection: 'desc'
    },
    {
        id           : Views.InBox,
        entityName   : 'BouquetTranslate',
        pageSize     : 9,
    },
    {
        id           : Views.Featured,
        entityName   : 'BouquetTranslate',
        pageSize     : 9,
        sortActive   : 'name',
        sortDirection: 'asc'
    },
    {
        id           : Views.Featured2,
        entityName   : 'BouquetTranslate',
        pageSize     : 9,
        sortActive   : 'name',
        sortDirection: 'asc'
    },
    {
        id           : Views.FrontPage,
        entityName   : 'BouquetTranslate',
        pageSize     : 9,
        sortActive   : 'bouquetPrice',
        sortDirection: 'asc'
    },
    {
        id           : Views.Catalog,
        entityName   : 'BouquetTranslate',
        pageSize     : 9,
        sortActive   : 'bouquetPrice',
        sortDirection: 'asc'
    },
    {
        id           : Views.Bouquets,
        entityName   : 'BouquetTranslate',
        pageSize     : 9,
        sortActive   : 'bouquetPrice',
        sortDirection: 'asc'
    },
    {
        id           : Views.Callbacks,
        entityName   : 'Callback',
        pageSize     : 3,
    },
];
