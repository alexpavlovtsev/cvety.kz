import { PostgarConfig } from '@postgar/types';

import { VIEWS } from './views';

export const postgarConfig: PostgarConfig = {
    graphqlUrl          : 'https://floracrm.com:5209/graphql',
    graphqlSchemaUrl    : 'https://floracrm.com:5209/graphile-schema',
    appErrorUrl         : '/',
    sockedUrl           : null,
    ssrMode             : true,
    authTokenKey        : 'AUTH_TOKEN',
    customEntityMetadata: {
        ToastMessage   : {},
        Basket         : {},
        BouquetCount   : {},
        // CurrentCurrency: {},
        // CurrentLanguage: {},
    },
    views               : VIEWS
};

