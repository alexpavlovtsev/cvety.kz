import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router } from '@angular/router';
import { RouterStateSnapshot } from '@angular/router/src/router_state';

import { Observable, forkJoin, of } from 'rxjs';
import { switchMap, catchError, tap, take, filter, map } from 'rxjs/operators';

import { EntityManagerService, SchemaService, ViewsService } from '@postgar/entity';
import { MAIN_CITY_CODE } from './config';
import { TranslateService } from '@ngx-translate/core';
import { Views } from './postgar-config/views';

@Injectable({
    providedIn: 'root'
})
export class ResolveGuard implements CanActivate, CanActivateChild
{
    url: string;
    cities: any[];
    languageMap: any;
    currencyMap: any;

    /**
     * Constructor
     *
     * @param _schemaService
     * @param _viewsService
     * @param _entityManager
     * @param _translateService
     * @param _router
     */
    constructor(
        private _schemaService: SchemaService,
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
        private _translateService: TranslateService,
        private _router: Router,
    )
    {
        // this._schemaService.loaded$
        //     .pipe(
        //         take(1),
        //         switchMap(() => this._entityManager.getService('City').entities$)
        //     )
        //     .subscribe(value => this.cities = value);
        //
        // this._schemaService.loaded$
        //     .pipe(
        //         take(1),
        //         switchMap(() => this._entityManager.getService('Language').entityMap$)
        //     )
        //     .subscribe(value => this.languageMap = value);
        //
        // this._schemaService.loaded$
        //     .pipe(
        //         take(1),
        //         switchMap(() => this._entityManager.getService('Currency').entityMap$)
        //     )
        //     .subscribe(value => this.currencyMap = value);
    }

    /**
     * Can activate
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<boolean>}
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>
    {
        this.url = state.url;

        return this.checkStore().pipe(
            switchMap(() => of(true)),
            catchError(() => of(false))
        );
    }

    /**
     * Can activate child
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<boolean>}
     */
    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>
    {
        this.url = state.url;

        return this.checkStore().pipe(
            switchMap(() => of(true)),
            catchError(() => of(false))
        );
    }

    /**
     * Check store
     *
     * @returns {Observable<any>}
     */
    checkStore(): Observable<any>
    {
        return this._schemaService.loaded$.pipe(
            take(1),
            switchMap(() =>
            {
                return forkJoin(
                    this.getCities(),
                    this.getLanguages(),
                    this.getCurrencies(),
                    this.getFilterItemTrs(),
                    this.getMinPrice(),
                    this.getFilterItemTrs(),
                ).pipe(
                    filter(([citiesLoaded, languagesLoaded, currenciesLoaded, trLoaded, price, fit]) =>
                    {
                        return !!(citiesLoaded && languagesLoaded && currenciesLoaded && trLoaded && price && fit);
                    }),
                    take(1),
                    tap(() =>
                    {
                        // this.setSystemVariables();
                        // this.setSystemVariables1();
                    }),
                );
            })
        );
    }

    /**
     * Get filter item translates
     */
    getFilterItemTrs(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager.getService('FilterItemTranslate').loaded$
                .pipe(
                    tap(loaded =>
                    {
                        if (!loaded)
                        {
                            this._entityManager.getService('FilterItemTranslate').getAll();
                        }
                    }),
                    filter(loaded => loaded),
                    take(1)
                )
                .subscribe(value =>
                {
                    resolve(value);
                }, reject);
        });
    }

    /**
     * Get cities
     *
     * @returns {Observable<any>}
     */
    getCities(): any
    {
        return this._entityManager.getService('City').loaded$
            .pipe(
                tap(loaded =>
                {
                    if (!loaded)
                    {
                        this._entityManager.getService('City').getAll();
                    }
                }),
                filter(loaded => loaded),
                take(1)
            );
    }

    /**
     * Get languages
     *
     * @returns {Observable<any>}
     */
    getLanguages(): any
    {
        return this._entityManager.getService('Language').loaded$
            .pipe(
                tap(loaded =>
                {
                    if (!loaded)
                    {
                        this._entityManager.getService('Language').getAll();
                    }
                }),
                filter(loaded => loaded),
                take(1)
            );
    }

    /**
     * Get currencies
     * @returns {Observable<any>}
     */
    getCurrencies(): any
    {
        return this._entityManager.getService('Currency').loaded$
            .pipe(
                tap(loaded =>
                {
                    if (!loaded)
                    {
                        this._entityManager.getService('Currency').getAll();
                    }
                }),
                filter(loaded => loaded),
                take(1)
            );
    }

    getMinPrice(): any
    {
        return of(true);
        // return this._viewsService.loaded(Views.MinPrice)
        //     .pipe(
        //         tap(loaded =>
        //         {
        //             if (!loaded)
        //             {
        //                 this._entityManager.getService('Bouquet')
        //                     .getWithQuery({
        //                         id           : Views.MinPrice,
        //                         entityName   : 'Bouquet',
        //                         pageSize     : 1,
        //                         sortActive   : 'mainPrice',
        //                         sortDirection: 'asc'
        //                     });
        //             }
        //         }),
        //         filter(loaded => loaded),
        //         take(1)
        //     );
    }

    /**
     * Set system variables
     */
    setSystemVariables(): void
    {
        // Set Currency
        this._entityManager.getService('Currency').entities$
            .pipe(
                map(entities => entities.filter((e: any) => e.code === 'RUB')),
                take(1)
            )
            .subscribe(entities =>
            {
                this._entityManager.getService('CurrentCurrency').addAllToCache(entities);
                console.log('CurrentCurrency');
                // this._viewsService.setNodesToCache([entityMap['643']], Views.Currency, 'Currency');
            });

        // Language
        this._entityManager.getService('Language').entityMap$
            .pipe(take(1))
            .subscribe(entityMap =>
            {
                let language: any;

                if (this.url.includes('/en/'))
                {
                    language = entityMap['2'];
                }
                else
                {
                    language = entityMap['1'];
                }

                // console.log('Language', entityMap);

                // this._viewsService.setNodesToCache([language], Views.Language, 'Language');
                this._entityManager.getService('CurrentLanguage').addAllToCache([language]);
                console.log('CurrentLanguage');

                // Use the selected language for translations
                this._translateService.use(language.code);
            });

        // Set City
        this._entityManager.getService('City').entities$
            .pipe(
                // filter((nodes: any[]) => nodes.length > 0),
                take(1)
            )
            .subscribe((entities: any) =>
            {
                console.log('City', entities);

                const segments = this.url.split('/').filter(value => !!value);
                let activeCity = entities.find(city => segments.some(s => s === city.code));

                if (activeCity !== undefined)
                {
                    this._viewsService.setNodesToCache([activeCity], Views.City, 'City');
                }
                else
                {
                    activeCity = entities.find(city => city.code === MAIN_CITY_CODE);
                    this._viewsService.setNodesToCache([activeCity], Views.City, 'City');
                }
            });
    }

    /**
     * Set system variables
     */
    setSystemVariables1(): void
    {
        // Set Currency
        this._viewsService.setNodesToCache([this.currencyMap['398']], Views.Currency, 'Currency');

        // Language
        let language: any;

        if (this.url.includes('/en/'))
        {
            language = this.languageMap['2'];
        }
        else
        {
            language = this.languageMap['1'];
        }

        this._viewsService.setNodesToCache([language], Views.Language, 'Language');

        // Use the selected language for translations
        this._translateService.use(language.code);

        // Set City
        const segments = this.url.split('/').filter(value => !!value);
        let activeCity = this.cities.find(city => segments.some(s => s === city.code));

        if (activeCity !== undefined)
        {
            console.log(activeCity);
            this._viewsService.setNodesToCache([activeCity], Views.City, 'City');
        }
        else
        {
            activeCity = this.cities.find(city => city.code === MAIN_CITY_CODE);
            console.log('entities', this.cities);
            console.log(activeCity);
            this._viewsService.setNodesToCache([activeCity], Views.City, 'City');
        }
    }
}
