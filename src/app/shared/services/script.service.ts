import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { DOCUMENT, isPlatformServer } from '@angular/common';

interface Scripts
{
    name: string;
    src: string;
}

export const ScriptStore: Scripts[] = [
    {name: 'paypalCheckout', src: 'assets/js/checkout.js'},
    {name: 'jquery', src: 'assets/js/jquery.min.js'},
    {name: 'slick', src: 'assets/js/slick.min.js'},
    {name: 'jquery.formstyler', src: 'assets/js/jquery.formstyler.min.js'},
    {name: 'popper', src: 'assets/js/popper.min.js'},
    {name: 'bootstrap', src: 'assets/js/bootstrap.min.js'},
    {name: 'aws', src: 'assets/js/aws-sdk-2.117.0.min.js'}
];

@Injectable({
    providedIn: 'root'
})
export class ScriptService
{
    private readonly _scripts: any;
    private readonly _scriptStore: Scripts[];

    /**
     * Constructor
     *
     * @param document
     * @param platformId
     */
    constructor(
        @Inject(DOCUMENT) private document: any,
        @Inject(PLATFORM_ID) private platformId: Object,
    )
    {
        this._scripts     = {};
        this._scriptStore = ScriptStore;

        // Set scripts
        this._scriptStore.forEach((script: any) =>
        {
            this._scripts[script.name] = {
                loaded: false,
                src   : script.src
            };
        });
    }

    /**
     * Load
     *
     * @param scripts
     */
    load(scripts: string[]): Promise<any>
    {
        // Stop working on server
        if (isPlatformServer(this.platformId))
        {
            return Promise.all([]);
        }
        const promises: any[] = [];
        scripts.forEach((script) => promises.push(this.loadScript(script)));
        return Promise.all(promises);
    }

    /**
     * Load script
     *
     * @param {string} name
     * @returns {Promise<any>}
     */
    loadScript(name: string): Promise<any>
    {
        // Stop working on server
        if (isPlatformServer(this.platformId))
        {
            return new Promise<any>(resolve => resolve());
        }
        return new Promise((resolve, reject) =>
        {
            // resolve if already loaded
            if (this._scripts[name].loaded)
            {
                resolve({script: name, loaded: true, status: 'Already Loaded'});
            }
            else
            {
                // load script
                const script = this.document.createElement('script');
                script.type  = 'text/javascript';
                script.src   = this._scripts[name].src;
                if (script.readyState)
                {  // IE
                    script.onreadystatechange = () =>
                    {
                        if (script.readyState === 'loaded' || script.readyState === 'complete')
                        {
                            script.onreadystatechange  = null;
                            this._scripts[name].loaded = true;
                            resolve({script: name, loaded: true, status: 'Loaded'});
                        }
                    };
                }
                else
                {  // Others
                    script.onload = () =>
                    {
                        this._scripts[name].loaded = true;
                        resolve({script: name, loaded: true, status: 'Loaded'});
                    };
                }
                script.onerror = (error: any) => resolve({script: name, loaded: false, status: 'Loaded'});
                this.document.body.appendChild(script);
            }
        });
    }

    /**
     * Append script code
     *
     * @param code
     * @param name
     */
    appendScript(code: string, name: string): Promise<any>
    {
        // Stop working on server
        if (isPlatformServer(this.platformId))
        {
            return new Promise<any>(resolve => resolve());
        }

        return new Promise((resolve, reject) =>
        {
            // resolve if already loaded
            if (this._scripts[name] && this._scripts[name].loaded)
            {
                resolve({script: name, loaded: true, status: 'Already Loaded'});
            }
            else
            {
                this._scripts[name] = {
                    loaded: false,
                    src   : null
                };

                // Create script
                const script = this.document.createElement('script');
                script.type  = 'text/javascript';
                script.appendChild(this.document.createTextNode(code));

                if (script.readyState)
                {  // IE
                    script.onreadystatechange = () =>
                    {
                        if (script.readyState === 'loaded' || script.readyState === 'complete')
                        {
                            script.onreadystatechange  = null;
                            this._scripts[name].loaded = true;
                            resolve({script: name, loaded: true, status: 'Loaded'});
                        }
                    };
                }
                else
                {  // Others
                    script.onload = () =>
                    {
                        this._scripts[name].loaded = true;
                        resolve({script: name, loaded: true, status: 'Loaded'});
                    };
                }
                script.onerror = (error: any) => resolve({script: name, loaded: false, status: 'Loaded'});
                this.document.body.appendChild(script);
            }
        });
    }
}
