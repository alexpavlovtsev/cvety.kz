import { Injectable } from '@angular/core';

import { Grammar } from '@shared/grammar';
import { LocationService } from '@shared/services/location.service';
import { MAIN_CITY_CODE } from '../../config';
import { wordHelper } from '@shared/services/word-helper';
import { quantityUrlHelper } from '@shared/services/helpers/quantity-url-helper';

export const WORD_SORT = function (a, b)
{
    const nameA = a.toLowerCase(), nameB = b.toLowerCase();
    if (nameA < nameB)
    {
        return -1;
    }
    if (nameA > nameB)
    {
        return 1;
    }
    return 0;
};

export interface FilterInfoFullData
{
    label: string;
    url: string;
    cleanUrl?: string;
}

@Injectable({
    providedIn: 'root'
})
export class FilterInfoService
{

    static priceNameHelper(priceNames: string[]): string[]
    {
        return priceNames
            .join('-')
            .replace('Дешевые', 'дешево')
            .replace('Недорогие', 'недорого')
            .replace('Дорогие', 'дорого')
            .split('-');
    }

    static colorNameHelper(colorNames: string[]): string[]
    {
        const array = [];
        for (let i = 0; i < colorNames.length; i++)
        {
            let name = colorNames[i].trim();
            if (name[name.length - 2] === 'ы')
            {
                name = name.slice(0, -1) + 'е';
            }
            else if (name[name.length - 2] === 'о')
            {
                name = name.slice(0, -2) + 'ые';
            }
            else if (name[name.length - 2] === 'и')
            {
                name = name.slice(0, -1) + 'е';
            }
            array.push(name);
        }
        return array;
    }

    static occasionNameHelper(reasonNames: string[]): string[]
    {
        const array = [];
        for (let i = 0; i < reasonNames.length; i++)
        {
            let name = reasonNames[i].trim();
            if (name.toLowerCase().includes('свадьба') || name.toLowerCase().includes('годовщина'))
            {
                name = name.slice(0, -1) + 'у';
            }
            array.push(name);
        }
        return array;
    }

    /**
     * Build a new url based on the existing url, prepare label
     *
     * @param {FilterInfo[]} items
     * @param {Language} language
     * @param {City} city
     * @returns {FilterInfoFullData}
     */
    static fullData(items: any[], language: any, city: any): FilterInfoFullData
    {
        let containers = {};

        for (let i = 0; i < 200; i++)
        {
            containers[i] = {
                url  : [],
                label: []
            };
        }

        for (const item of items)
        {
            if (!containers[item.filterId])
            {
                containers[item.filterId] = {
                    url  : [],
                    label: []
                };
            }
            containers[item.filterId].url.push(item.filterItemUrl);
            containers[item.filterId].label.push(item.filterItemLabel);
        }

        // Helpers
        if (containers[4].label.length > 0)
        {
            // containers[4].label = FilterInfoService.colorNameHelper(containers[4].label);
        }

        if (containers[10].label.length > 0)
        {
            containers[10].label = FilterInfoService.priceNameHelper(containers[10].label);
        }

        if (containers[5].url.length === 0 && items.length > 0)
        {
            switch (language.code)
            {
                case 'ru':
                    containers[5].url.push('cvety');
                    containers[5].label.push('цветы');
                    break;
                case 'en':
                    containers[5].url.push('flowers');
                    containers[5].label.push('flowers');
                    break;
            }

        }
        else if (containers[5].url.length > 1)
        {
            containers[5].url.sort(WORD_SORT);
            containers[5].label.sort(WORD_SORT);

            const lastUrl   = containers[5].url.pop();
            const lastLabel = containers[5].label.pop();

            switch (language.code)
            {
                case 'ru':
                    containers[5].url   = [...containers[5].url, 'i', lastUrl];
                    containers[5].label = [...containers[5].label, 'и', lastLabel];
                    break;
                case 'en':
                    containers[5].url   = [...containers[5].url, 'and', lastUrl];
                    containers[5].label = [...containers[5].label, 'and', lastLabel];
                    break;
            }
        }

        if (containers[7].url.length > 0)
        {
            switch (language.code)
            {
                case 'ru':
                    containers[7].label = FilterInfoService.occasionNameHelper(containers[7].label);
                    containers[7].label = containers[7].label.map((item) => `на ${item}`);
                    containers[7].url   = containers[7].url.map((item) => `na-${item}`);
                    break;
                case 'en':
                    containers[7].label = containers[7].label.map((item) => `for ${item}`);
                    containers[7].url   = containers[7].url.map((item) => `for-${item}`);
                    break;
            }
        }

        if (containers[5].length > 0)
        {
            console.log(containers[5].length);
        }
        containers = wordHelper(containers);

        // url helpers
        if (containers[3].url.length > 0 && language.id === 1)
        {
            // containers = quantityUrlHelper(containers);
        }

        const url = [
            ...containers[3].url,
            ...containers[4].url,
            ...containers[5].url,
            ...containers[6].url,
            ...containers[7].url,
            ...containers[10].url,
            ...containers[25].url
        ].join('-');

        let label = [
            ...containers[3].label,
            ...containers[4].label,
            ...containers[5].label,
            ...containers[6].label,
            ...containers[7].label,
            ...containers[10].label,
            ...containers[25].label
        ].join(' ').trim();

        // Label helper
        switch (language.code)
        {
            case 'ru':
                label = this.russianTitleHelper(label);
                break;
            case 'en':
                label = Grammar.toTitleCase(label);
                break;
        }

        const cityCode = city.code === MAIN_CITY_CODE ? '' : city.code;
        const langCode = language.code === 'ru' ? '' : language.code;

        return {
            label   : label,
            url     : LocationService.validFullUrl(`/${cityCode}/${langCode}/` + url),
            cleanUrl: url
        };
    }

    static getBasePrefix(url: string, language: any): string
    {
        const clean = LocationService.validFullUrl(language.bouquetUrlPrefix + '-' + url);

        return clean === language.bouquetUrlPrefix ? language.bouquetUrlPrefix : clean;
    }

    /**
     * Helper for russian language label
     *
     * @param {string} text
     * @returns {string}
     */
    static russianTitleHelper(text: string): string
    {
        return Grammar.upperCaseFirstLetter(String(text).toLowerCase());
    }

    /**
     * Extend category values
     *
     * @param {FilterInfo} item
     * @param {FilterInfo[]} activeFiltersInfo
     * @param {Language} language
     * @param {City} city
     * @returns {FilterInfo}
     */
    static extendInfo(item: any, activeFiltersInfo: any[], language: any, city: any): any
    {
        item.isActive = activeFiltersInfo.indexOf(item) > -1;

        // If filter can not have multiple values,
        // then do not copy its elements
        const activeItemsCopy = item.filterId !== 5
            ? activeFiltersInfo.filter(filter => filter.filterId !== item.filterId)
            : [...activeFiltersInfo];

        const fullInfo: FilterInfoFullData = FilterInfoService.fullData([...activeItemsCopy, item], language, city);

        item.url           = fullInfo.url;
        item.label         = fullInfo.label;
        item.categoriesIds = [...activeItemsCopy, item].map(_item => _item.filterItemId);

        return item;
    }
}

export function filterItemConvert(item: any): any
{
    return {
        id             : item.id,
        filterItemId   : item.filterItemId,
        filterId       : item.filterItemByFilterItemId.filterId,
        languageId     : item.languageId,
        filterItemLabel: item.value,
        filterItemUrl  : item.url
    };
}
