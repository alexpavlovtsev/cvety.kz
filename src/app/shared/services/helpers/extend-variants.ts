export function extendVariants(items: any[])
{
    return items.map(item =>
    {
        switch (item.url)
        {
            case 'rozy':
                item.extendedUrls = [item.url, 'roz', 'pionov'];
                return item;

            case 'piony':
                item.extendedUrls = [item.url];
                return item;

            case 'gerbery':
                item.extendedUrls = [item.url];
                return item;

            case 'amarillisy':
                item.extendedUrls = [item.url];
                return item;

            case 'anturium':
                item.extendedUrls = [item.url];
                return item;

            case 'gvozdika':
                item.extendedUrls = [item.url];
                return item;

            case 'giacinty':
                item.extendedUrls = [item.url];
                return item;

            case 'gortenziya':
                item.extendedUrls = [item.url];
                return item;

            case 'irisy':
                item.extendedUrls = [item.url];
                return item;

            case 'kally':
                item.extendedUrls = [item.url];
                return item;

            case 'lilii':
                item.extendedUrls = [item.url];
                return item;

            case 'podsolnuh':
                return 'podsolnuhov';
            case 'romashki':
                item.extendedUrls = [item.url];
                return item;

            case 'siren':
                item.extendedUrls = [item.url];
                return item;

            case 'streliciya':
                item.extendedUrls = [item.url];
                return item;

            case 'frezii':
                item.extendedUrls = [item.url];
                return item;

            case 'hrizantemy':
                item.extendedUrls = [item.url];
                return item;

            case 'maki':
                item.extendedUrls = [item.url];
                return item;

            case 'vasilki':
                item.extendedUrls = [item.url];
                return item;

            case 'astry':
                item.extendedUrls = [item.url];
                return item;

            case 'eustoma':
                item.extendedUrls = [item.url];
                return item;

            case 'kustovaya-roza':
                item.extendedUrls = [item.url];
                return item;

            case 'alstromeriya':
                item.extendedUrls = [item.url];
                return item;

            case 'orhidei':
                item.extendedUrls = [item.url];
                return item;

            case 'tyulpany':
                item.extendedUrls = [item.url];
                return item;

            case 'gollandskie-rozy':
                item.extendedUrls = [item.url];
                return item;

            default:
                item.extendedUrls = [item.url];
                return item;
        }
    });
}
