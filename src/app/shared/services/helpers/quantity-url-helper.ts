export function quantityUrlHelper(containers: any): any
{
    switch (containers[3].url[0])
    {
        case '7':
            containers[5].url = containers[5].url.map(url => one(url));
            break;

        case '25':
            containers[5].url = containers[5].url.map(url => one(url));
            break;

        case '51':
            containers[5].url = containers[5].url.map(url => two(url));
            break;

        case '101':
            containers[5].url = containers[5].url.map(url => two(url));
            break;
    }

    return containers;
}

function one(url: string)
{
    switch (url)
    {
        case 'cvety':
            return 'cvetov';
        case 'rozy':
            return 'roz';
        case 'piony':
            return 'pionov';
        case 'gerbery':
            return 'gerber';
        case 'amarillisy':
            return 'amarillis';
        case 'anturium':
            return 'anturium';
        case 'gvozdika':
            return 'gvozdik';
        case 'giacinty':
            return 'giacintov';
        case 'gortenziya':
            return 'gortenziy';
        case 'irisy':
            return 'irisov';
        case 'kally':
            return 'kall';
        case 'lilii':
            return 'liliy';
        case 'podsolnuh':
            return 'podsolnuhov';
        case 'romashki':
            return 'romashek';
        case 'siren':
            return 'sireni';
        case 'streliciya':
            return 'streliciy';
        case 'frezii':
            return 'freziy';
        case 'hrizantemy':
            return 'hrizantem';
        case 'maki':
            return 'makov';
        case 'vasilki':
            return 'vasilkov';
        case 'astry':
            return 'astr';
        case 'eustoma':
            return 'eustom';
        case 'kustovaya-roza':
            return 'kustovykh-roz';
        case 'alstromeriya':
            return 'alstromeriy';
        case 'orhidei':
            return 'orhidey';
        case 'tyulpany':
            return 'tyulpanov';
        case 'gollandskie-rozy':
            return 'gollandskih-roz';
    }
}

function two(url: string)
{
    switch (url)
    {
        case 'cvety':
            return 'cvetok';
        case 'rozy':
            return 'roza';
        case 'piony':
            return 'pion';
        case 'gerbery':
            return 'gerbera';
        case 'amarillisy':
            return 'amarillis';
        case 'anturium':
            return 'anturium';
        case 'gvozdika':
            return 'gvozdika';
        case 'giacinty':
            return 'giacint';
        case 'gortenziya':
            return 'gortenziya';
        case 'irisy':
            return 'iris';
        case 'kally':
            return 'kalla';
        case 'lilii':
            return 'liliya';
        case 'podsolnuh':
            return 'podsolnuh';
        case 'romashki':
            return 'romashka';
        case 'siren':
            return 'siren';
        case 'streliciya':
            return 'streliciya';
        case 'frezii':
            return 'freziya';
        case 'hrizantemy':
            return 'hrizantema';
        case 'maki':
            return 'mak';
        case 'vasilki':
            return 'vasilek';
        case 'astry':
            return 'astra';
        case 'eustoma':
            return 'eustoma';
        case 'kustovaya-roza':
            return 'kustovaya-roza';
        case 'alstromeriya':
            return 'alstromeriya';
        case 'orhidei':
            return 'orhideya';
        case 'tyulpany':
            return 'tyulpan';
        case 'gollandskie-rozy':
            return 'gollandskaya-roza';
    }
}

