import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../store/reducers';
import { filter, map, take, takeUntil } from 'rxjs/operators';
import { MetatagService } from './metatag.service';
import { EntityManagerService, SchemaService, ViewsService } from '@postgar/entity';
import { Views } from '../../postgar-config/views';
import { MAIN_CITY_CODE } from '../../config';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
    providedIn: 'root'
})
export class StateHelperService
{

    /**
     * Constructor
     */
    constructor(
        private store: Store<State>,
        private _schemaService: SchemaService,
        private _viewsService: ViewsService,
        private _metaTagService: MetatagService,
        private _entityManager: EntityManagerService,
        private _translateService: TranslateService,
    )
    {
    }

    /**
     * Set city, language, currency
     *
     * @param url
     * @param routeType
     */
    setStateValues(url = '', routeType: string): void
    {
        const segments = url
            .split('/')
            .filter(segment => segment.length > 0);

        this._schemaService.loaded$
            .pipe(take(1))
            .subscribe(() =>
            {
                this._entityManager.getService('City').entityMap$
                    .pipe(
                        map((nodes: any) =>
                        {
                            return Object.keys(nodes).map(key => nodes[key]);
                        }),
                        filter((nodes: any[]) => nodes.length > 0),
                        take(1)
                    )
                    .subscribe((entities: any) =>
                    {
                        let activeCity = entities.find(city => segments.some(s => s === city.code));

                        if (activeCity === undefined)
                        {
                            activeCity = entities.find(city => city.code === MAIN_CITY_CODE);
                        }

                        const view = {
                            id        : Views.City,
                            nodeIds   : [activeCity.id],
                            entityName: 'City'
                        };
                        this._viewsService.upsertOneInCache(view);
                    });

                this._entityManager.getService('Language').entityMap$
                    .pipe(
                        filter((nodes: any) => Object.keys(nodes).length > 0),
                        take(1)
                    )
                    .subscribe(entityMap =>
                    {
                        let language: any;

                        if (url.includes('/en/'))
                        {
                            language = entityMap['2'];
                        }
                        else
                        {
                            language = entityMap['1'];
                        }

                        const view = {
                            id        : Views.Language,
                            nodeIds   : [language.id],
                            entityName: 'Language'
                        };
                        this._viewsService.upsertOneInCache(view);

                        // Use the selected language for translations
                        this._translateService.use(language.code);
                    });

                this._entityManager.getService('Currency').entityMap$
                    .pipe(
                        filter((nodes: any) => Object.keys(nodes).length > 0),
                        take(1)
                    )
                    .subscribe(entityMap =>
                    {
                        this._viewsService.nodes(Views.Currency)
                            .pipe(take(1))
                            .subscribe((nodes: any[]) =>
                            {
                                if (nodes.length === 0)
                                {
                                    const view = {
                                        id        : Views.Currency,
                                        nodeIds   : [entityMap['643'].id],
                                        entityName: 'Currency'
                                    };
                                    this._viewsService.upsertOneInCache(view);
                                }
                            });
                    });
            });
    }
}
