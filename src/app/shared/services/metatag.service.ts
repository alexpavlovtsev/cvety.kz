import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { LocationService } from './location.service';
import { Grammar } from '@shared/grammar';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../postgar-config/views';
import { filter, take } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class MetatagService
{
    language: any;
    city: any;
    h1_without_city: string;
    h1: string;
    phone = '+7 702 222 07 30';
    content: object;
    minPrice: number;
    cityName: string;
    citySecondName: string;

    constructor(
        private meta: Meta,
        private title: Title,
        private locationService: LocationService,
        private _viewsService: ViewsService
    )
    {
        this._viewsService.firstNode(Views.City).subscribe((node: any) =>
        {
            this.city = node;
        });

        this._viewsService.firstNode(Views.MinPrice).subscribe((node: any) =>
        {
            this.minPrice = node.mainPrice;
        });
    }

    set(pageType: string, title: string): void
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(take(1))
            .subscribe((node: any) =>
            {
                this.language = node;
                this.content  = i18()[this.language.code];

                if (this.language.code === 'ru')
                {
                    this.cityName       = this.city.name;
                    this.citySecondName = this.city.secondName;
                } else
                {
                    this.cityName       = this.city.enName;
                    this.citySecondName = this.city.enName;
                }

                title = this.language.id === 1
                    ? Grammar.upperCaseFirstLetter(title)
                    : Grammar.toTitleCase(title);

                this.h1_without_city = title;

                this.h1 = `${(this.h1_without_city || '').trim()} ${i18_title_prefix()[this.language.code]} ${this.citySecondName}`;
                switch (pageType)
                {
                    case 'front':
                        this.getContent('frontPage');
                        break;
                    case 'bouquets':
                        this.getContent('bouquets');
                        break;
                    case 'page':
                        this.getContent('page');
                        break;
                    case 'bouquet':
                        this.getContent('card');
                        break;
                    case 'catalog':
                        this.getContent('filter');
                        break;
                    case 'thematic':
                        this.getContent('thematic');
                        break;
                }
            });
    }

    private getContent(key: string): void
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(
                take(1),
                filter(data => Object.keys(data).length > 0)
            )
            .subscribe((node: any) =>
            {
                this.language = node;
                this.content  = i18()[this.language.code];

                const title       = this.replace(this.content[key].title);
                const keywords    = this.replace(this.content[key].keywords);
                const description = this.replace(this.content[key].description);

                this.setData({title: title, keywords: keywords, description: description});
            });
    }

    private replace(string: string): string
    {
        return string
            .replace('{h1}', this.h1)
            .replace('{h1_without_city}', this.h1_without_city)
            .replace('{h1_lower}', String(this.h1).toLowerCase().trim())
            .replace('{h1_without_city_lower}', this.h1_without_city.toLowerCase().trim())
            .replace('{phone}', this.phone)
            .replace('{min_price}', (Math.round(this.minPrice / this.exchange)).toString().trim())
            .replace('{city_name}', Grammar.upperCaseFirstLetter(this.cityName))
            .replace('{city_secondName}', Grammar.upperCaseFirstLetter(this.citySecondName))
            .replace(new RegExp('  ', 'g'), ' ');
    }

    setData(data: any): void
    {
        this.title.setTitle(this.setCase(data.title));
        this.meta.updateTag({name: 'keywords', content: data.keywords});
        this.meta.updateTag({name: 'description', content: this.setCase(data.description)});
    }

    private setCase(text: string): string
    {
        return !!this.language && this.language.code === 'en' ? Grammar.validationTitle(text) : text;
    }

    get exchange(): number
    {
        return this.language.code === 'en' ? 380 : 1;
    }
}


function i18(): any
{
    return {
        ru: {
            frontPage: {
                title      : `Доставка цветов в {city_secondName}: заказ букетов на дом, офис | OneTwoFlowers`,
                keywords   : `доставка цветов, заказ букетов, дом, офис, недорого, {city_name}, onetwoflowers.ru`,
                description: `Гарантия качества, круглосуточная доставка цветов в {city_secondName},
      подбор уникального и недорогого букета в каталоге onetwoflowers.ru`
            },
            bouquets : {
                title      : 'Красивые букеты цветов в {city_secondName}: доставка и заказ домой и офис | OneTwoFlowers',
                keywords   : 'красивые букеты цветов, заказ, доставка, дом, офис, недорого, {city_name}, onetwoflowers.ru',
                description: 'Купить красивые букеты в {city_secondName} с доставкой домой и офис. ' +
                    'Заказывайте круглосуточно онлайн: гарантия качества, только свежие и яркие 💐 букеты.'
            },
            card     : {
                title      : '{h1_without_city}: купить, цена от {min_price} тг, ' +
                    'доставка и заказ цветов домой и офис в {city_secondName} | OneTwoFlowers',
                keywords   : '{h1_without_city}, купить, цена, заказ, доставка, дом, офис, недорого, {city_name}, onetwoflowers.ru',
                description: 'Купить {h1_without_city_lower} в {city_secondName} с доставкой домой и офис. ' +
                    'Заказывайте круглосуточно онлайн: гарантия качества, только свежие и яркие 💐 букеты.'
            },
            filter   : {
                title      : '{h1_without_city}: доставка и заказ цветов, цена от {min_price} тг в {city_secondName} | OneTwoFlowers',
                keywords   : '{h1_without_city}, купить, цена, заказ, доставка, дом, офис, {city_name}, onetwoflowers.ru',
                description: `Купить {h1_without_city} в {city_secondName} с доставкой домой и офис.
      Заказывайте круглосуточно онлайн: гарантия качества, только свежие и яркие 💐 букеты.`
            },
            page     : {
                title      : '{h1_without_city}: OneTwoFlowers - доставка и заказ цветов и букетов',
                keywords   : '{h1_without_city}, onetwoflowers.ru',
                description: '{h1_without_city}. Заказывайте цветы круглосуточно онлайн: гарантия качества, только свежие и яркие 💐 букеты.'
            },
            thematic : {
                title      : '{h1_without_city}: OneTwoFlowers - доставка и заказ цветов и букетов',
                keywords   : '{h1_without_city}, onetwoflowers.ru',
                description: '{h1_without_city}. Заказывайте цветы круглосуточно онлайн: гарантия качества, только свежие и яркие 💐 букеты.'
            }
        },
        en: {
            frontPage: {
                title      : `Flowers Delivery in {city_secondName}: Order Bouquets to Home, Office | OneTwoFlowers`,
                keywords   : `flowers delivery, order bouquets, home, office, cheap, {city_name}, onetwoflowers.ru`,
                description: `Quality Guarantee, 24 Hours Flowers Delivery in {city_secondName},
      Selection of Unique and Inexpensive Bouquet in the OneTwoFlowers.ru Catalog.`
            },
            bouquets : {
                title      : 'Beautiful Bouquets of Flowers in {city_secondName}: Home Delivery and Order to Office | OneTwoFlowers',
                keywords   : 'beautiful bouquets of flowers, order, delivery, home, office, cheap, {city_name}, onetwoflowers.ru',
                description: 'Buy Beautiful Bouquets in {city_secondName} with Delivery to Home and Office. ' +
                    'Order 24 Hours Online Designer Bouquets. Free Shipping, Best Price.'
            },
            card     : {
                title      : '{h1_without_city}: Buy for Price by ­{min_price} $, ' +
                    'Delivery and Order Flowers to Home and Office in {city_secondName} | OneTwoFlowers',
                keywords   : '{h1_without_city}, buy, price, order, delivery, home, office, cheap, {city_name}, onetwoflowers.ru',
                description: 'Buy {h1_without_city} in {city_secondName} with Delivery to Home and Office. ' +
                    'Order 24 Hours Online Designer Bouquets. Free Shipping, Best Price.'
            },
            filter   : {
                title      : '{h1_without_city}: Delivery and Order Flowers, Price from {min_price} $ in {city_secondName} | OneTwoFlowers',
                keywords   : '{h1_without_city}, buy, price, order, delivery, house, office, {city_name}, onetwoflowers.ru',
                description: 'Buy {h1_without_city} in {city_secondName} with Delivery to Home and Office. ' +
                    'Order 24 Hours Online Designer Bouquets. Free Shipping, Best Price.'
            },
            page     : {
                title      : '{h1_without_city}: OneTwoFlowers - Order Flowers and Bouquets with Shipping',
                keywords   : '{h1_without_city}, onetwoflowers.ru',
                description: '{h1_without_city}. OneTwoFlowers - Order Flowers Around the Clock Online Designer Bouquets. Free Shipping, Best Price.'
            },
            thematic : {
                title      : '{h1_without_city}: OneTwoFlowers - Order Flowers and Bouquets with Shipping',
                keywords   : '{h1_without_city}, onetwoflowers.ru',
                description: '{h1_without_city}. OneTwoFlowers - Order Flowers Around the Clock Online Designer Bouquets. Free Shipping, Best Price.'
            }
        }
    };
}

function i18_title_prefix()
{
    return {
        'ru': ' в',
        'en': ' in'
    };
}
