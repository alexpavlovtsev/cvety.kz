import { Injectable } from '@angular/core';
import { GraphQLClient } from '@postgar/graphql';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { combineLatest } from 'rxjs';
import { Views } from '../../postgar-config/views';
import { take } from 'rxjs/operators';
import { FilterHelperService } from '@shared/services/filter-helper.service';
import { filterItemConvert } from '@shared/services/filter-info.service';

export const PRODUCT_COUNTS_QUERY = `
query getAllProductCounts($itemsJson: JSON, $city: String) {
  getAllProductCounts(itemsJson: $itemsJson, city: $city)
}
`;

interface ProductCounts
{
    categories_ids: any;
    id: number;
}

@Injectable({
    providedIn: 'root'
})
export class BouquetCounterService
{

    constructor(
        private _entityManager: EntityManagerService,
        private _graphQlClient: GraphQLClient,
        private _viewsService: ViewsService,
        private _filterHelper: FilterHelperService
    )
    {
    }

    /**
     * Get product counts
     */
    public static getProductCounts(allItems: any[], activeItems: any[], language: any): any[]
    {
        const array: ProductCounts[] = [];
        const otherItems             = allItems.filter(item =>
        {
            return activeItems.indexOf(item) === -1 && item.languageId === language.id;
        });

        for (const item of otherItems)
        {
            let categories_ids = [];
            const isMulti      = item.filterId === 5;
            const obj          = {categories_ids: '', id: item.filterItemId};

            if (isMulti)
            {
                categories_ids = [...activeItems.map(el => el.filterItemId), item.filterItemId];
            }
            else
            {
                categories_ids = [item.filterItemId];

                for (const active of activeItems)
                {
                    if (item.filterId !== active.filterId)
                    {
                        categories_ids.push(active.filterItemId);
                    }
                }
            }
            obj.categories_ids = categories_ids.join(',');
            array.push(obj);
        }
        return array;
    }

    /**
     * Run
     *
     * @param {string} url
     */
    run(url: string): void
    {
        combineLatest(
            this._entityManager.getService('FilterItemTranslate').entities$,
            this._viewsService.firstNode(Views.Language),
            this._viewsService.firstNode(Views.City),
        ).pipe(
            take(1)
        ).subscribe(([items, language, city]) =>
            {
                const activeItems = this._filterHelper.getActiveItems(url, language.id).map(item => filterItemConvert(item));
                items             = items.map(item => filterItemConvert(item));

                const productCounts = BouquetCounterService.getProductCounts(items, activeItems, language);
                const variables     = {city: city.code, itemsJson: JSON.stringify({'values': productCounts})};

                this._graphQlClient.get(PRODUCT_COUNTS_QUERY, variables, 'counts').subscribe(result =>
                {
                    this._entityManager.getService('BouquetCount').upsertOneInCache({
                        id    : 1,
                        counts: typeof result['getAllProductCounts'] === 'string'
                            ? JSON.parse(result['getAllProductCounts'])
                            : (result['getAllProductCounts'] || {})
                    });
                });
            },
            err =>
            {
                console.error('Oops:', err.message);
            });
    }
}
