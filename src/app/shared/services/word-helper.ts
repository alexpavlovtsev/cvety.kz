export function wordHelper(containers: any): any
{
    if (containers[3].label.length > 0 && (containers[4].label.length > 0 || containers[5].label.length > 0))
    {
        switch (containers[3].label[0])
        {
            case '15':
                containers = convert_1(containers);
                break;

            case '25':
                containers = convert_1(containers);
                break;

            case '35':
                containers = convert_1(containers);
                break;

            case '51':
                containers = convert_2(containers);
                break;

            case '77':
                containers = convert_1(containers);
                break;

            case '101':
                containers = convert_2(containers);
                break;

            case '21':
                containers = convert_2(containers);
                break;

            case '33':
                containers = convert_3(containers);
                break;

            case '55':
                containers = convert_1(containers);
                break;

            case '151':
                containers = convert_2(containers);
                break;

            case '201':
                containers = convert_2(containers);
                break;

            case '501':
                containers = convert_2(containers);
                break;

            case '9':
                containers = convert_1(containers);
                break;

            case '7':
                containers = convert_1(containers);
                break;
        }
    }
    else if (containers[4].label.length > 0 && containers[5].label.length > 0)
    {
        containers = convert_4(containers);
    }

    return containers;
}

function convert_1(containers: any): any
{
    containers[4].label = containers[4].label.map(item =>
    {
        switch (item)
        {
            case 'Красный':
                return 'Красных';
            case 'Розовый':
                return 'Розовых';
            case 'Оранжевый':
                return 'Оранжевых';
            case 'Желтый':
                return 'Желтых';
            case 'Зеленый':
                return 'Зеленых';
            case 'Голубой':
                return 'Голубых';
            case 'Синий':
                return 'Синих';
            case 'Фиолетовый':
                return 'Фиолетовых';
            case 'Белый':
                return 'Белых';
            case 'Кремовый':
                return 'Кремовых';
            case 'Морковный':
                return 'Морковных';
            default:
                return item;
        }
    });

    containers[5].label = containers[5].label.map(item =>
    {
        switch (item)
        {
            case 'Розы':
                return 'Роз';
            case 'Пионы':
                return 'Пионов';
            case 'Герберы':
                return 'Гербер';
            case 'Альстромерии':
                return 'Альстромерий';
            case 'Амариллисы':
                return 'Амариллисов';
            case 'Антуриумы':
                return 'Антуриум';
            case 'Гвоздики':
                return 'Гвоздик';
            case 'Гиацинты':
                return 'Гиацинтов';
            case 'Гортензии':
                return 'Гортензий';
            case 'Ирисы':
                return 'Ирисов';
            case 'Каллы':
                return 'Калл';
            case 'Лилии':
                return 'Лилий';
            case 'Орхидеи':
                return 'Орхидей';
            case 'Подсолнухи':
                return 'Подсолнухов';
            case 'Ромашки':
                return 'Ромашек';
            case 'Сирень':
                return 'Сиреней';
            case 'Стрелиции':
                return 'Стрелиций';
            case 'Тюльпаны':
                return 'Тюльпанов';
            case 'Фрезии':
                return 'Фрезий';
            case 'Хризантемы':
                return 'Хризантем';
            case 'Васильки':
                return 'Васильков';
            case 'Астры':
                return 'Астр';
            case 'Эустомы':
                return 'Эустом';
            case 'Голландские розы':
                return 'Голландских роз';
            case 'Кустовые розы':
                return 'Кустовый роз';
            case 'цветы':
                return 'цветов';
            default:
                return item;
        }
    });

    return containers;
}

function convert_2(containers: any): any
{
    const colorDef = containers[4].label[0];

    containers[4].label = containers[4].label.map(item =>
    {
        switch (item)
        {
            case 'Красный':
                return 'Красная';
            case 'Розовый':
                return 'Розовая';
            case 'Оранжевый':
                return 'Оранжевая';
            case 'Желтый':
                return 'Желтая';
            case 'Зеленый':
                return 'Зеленая';
            case 'Голубой':
                return 'Голубая';
            case 'Синий':
                return 'Синяя';
            case 'Фиолетовый':
                return 'Фиолетовая';
            case 'Белый':
                return 'Белая';
            case 'Кремовый':
                return 'Кремовая';
            case 'Морковный':
                return 'Морковная';
            default:
                return item;
        }
    });

    containers[5].label = containers[5].label.map(item =>
    {
        switch (item)
        {
            case 'Розы':
                return 'Роза';
            case 'Пионы':
                containers[4].label = [colorDef];
                return 'Пион';
            case 'Герберы':
                return 'Гербера';
            case 'Альстромерии':
                return 'Альстромерия';
            case 'Амариллисы':
                containers[4].label = [colorDef];
                return 'Амариллис';
            case 'Антуриумы':
                containers[4].label = [colorDef];
                return 'Антуриум';
            case 'Гвоздики':
                return 'Гвоздика';
            case 'Гиацинты':
                containers[4].label = [colorDef];
                return 'Гиацинт';
            case 'Гортензии':
                return 'Гортензия';
            case 'Ирисы':
                containers[4].label = [colorDef];
                return 'Ирис';
            case 'Каллы':
                return 'Калла';
            case 'Лилии':
                return 'Лилия';
            case 'Орхидеи':
                return 'Орхидея';
            case 'Подсолнухи':
                containers[4].label = [colorDef];
                return 'Подсолнух';
            case 'Ромашки':
                return 'Ромашка';
            case 'Сирень':
                return 'Сирень';
            case 'Стрелиции':
                return 'Стрелиция';
            case 'Тюльпаны':
                containers[4].label = [colorDef];
                return 'Тюльпан';
            case 'Фрезии':
                return 'Фрезия';
            case 'Хризантемы':
                return 'Хризантема';
            case 'Васильки':
                containers[4].label = [colorDef];
                return 'Васильек';
            case 'Астры':
                return 'Астра';
            case 'Эустомы':
                return 'Эустома';
            case 'Голландские розы':
                return 'Голландская роза';
            case 'Кустовые розы':
                return 'Кустовая роза';
            case 'цветы':
                containers[4].label = [colorDef];
                return 'цветок';
            default:
                return item;
        }
    });

    return containers;
}


function convert_3(containers: any): any
{
    containers[4].label = containers[4].label.map(item =>
    {
        switch (item)
        {
            case 'Красный':
                return 'Красные';
            case 'Розовый':
                return 'Розовые';
            case 'Оранжевый':
                return 'Оранжевые';
            case 'Желтый':
                return 'Желтые';
            case 'Зеленый':
                return 'Зеленые';
            case 'Голубой':
                return 'Голубые';
            case 'Синий':
                return 'Синие';
            case 'Фиолетовый':
                return 'Фиолетовые';
            case 'Белый':
                return 'Белые';
            case 'Кремовый':
                return 'Кремовые';
            case 'Морковный':
                return 'Морковные';
            default:
                return item;
        }
    });

    containers[5].label = containers[5].label.map(item =>
    {
        switch (item)
        {
            case 'Розы':
                return 'Розы';
            case 'Пионы':
                return 'Пиона';
            case 'Герберы':
                return 'Герберы';
            case 'Альстромерия':
                return 'Альстромерии';
            case 'Амариллисы':
                return 'Амариллиса';
            case 'Антуриум':
                return 'Антуриума';
            case 'Гвоздика':
                return 'Гвоздики';
            case 'Гиацинты':
                return 'Гиацинта';
            case 'Гортензия':
                return 'Гортензии';
            case 'Ирисы':
                return 'Ириса';
            case 'Каллы':
                return 'Калла';
            case 'Лилии':
                return 'Лилии';
            case 'Орхидеи':
                return 'Орхидеи';
            case 'Подсолнух':
                return 'Подсолнуха';
            case 'Ромашки':
                return 'Ромашки';
            case 'Сирень':
                return 'Сирени';
            case 'Стрелиция':
                return 'Стрелиции';
            case 'Тюльпаны':
                return 'Тюльпана';
            case 'Фрезии':
                return 'Фрезии';
            case 'Хризантемы':
                return 'Хризантемы';
            case 'Васильки':
                return 'Василька';
            case 'Астры':
                return 'Астры';
            case 'Эустома':
                return 'Эустомы';
            case 'Голландские розы':
                return 'Голландских розы';
            case 'Кустовая роза':
                return 'Кустовые розы';
            case 'цветы':
                return 'цветка';
            default:
                return item;
        }
    });

    return containers;
}

function convert_4(containers: any): any
{
    containers[4].label = containers[4].label.map(item =>
    {
        switch (item)
        {
            case 'Красный':
                return 'Красные';
            case 'Розовый':
                return 'Розовые';
            case 'Оранжевый':
                return 'Оранжевые';
            case 'Желтый':
                return 'Желтые';
            case 'Зеленый':
                return 'Зеленые';
            case 'Голубой':
                return 'Голубые';
            case 'Синий':
                return 'Синие';
            case 'Фиолетовый':
                return 'Фиолетовые';
            case 'Белый':
                return 'Белые';
            case 'Кремовый':
                return 'Кремовые';
            case 'Морковный':
                return 'Морковные';
            default:
                return item;
        }
    });

    containers[5].label = containers[5].label.map(item =>
    {
        switch (item)
        {
            case 'Розы':
                return 'Розы';
            case 'Пионы':
                return 'Пионы';
            case 'Герберы':
                return 'Герберы';
            case 'Альстромерия':
                return 'Альстромерии';
            case 'Амариллисы':
                return 'Амариллисы';
            case 'Антуриум':
                return 'Антуриумы';
            case 'Гвоздика':
                return 'Гвоздики';
            case 'Гиацинты':
                return 'Гиацинты';
            case 'Гортензия':
                return 'Гортензии';
            case 'Ирисы':
                return 'Ирисы';
            case 'Каллы':
                return 'Каллы';
            case 'Лилии':
                return 'Лилии';
            case 'Орхидеи':
                return 'Орхидеи';
            case 'Подсолнух':
                return 'Подсолнухи';
            case 'Ромашки':
                return 'Ромашки';
            case 'Сирень':
                return 'Сирени';
            case 'Стрелиция':
                return 'Стрелиции';
            case 'Тюльпаны':
                return 'Тюльпаны';
            case 'Фрезии':
                return 'Фрезии';
            case 'Хризантемы':
                return 'Хризантемы';
            case 'Васильки':
                return 'Васильки';
            case 'Астры':
                return 'Астры';
            case 'Эустома':
                return 'Эустомы';
            case 'Голландские розы':
                return 'Голландские розы';
            case 'Кустовая роза':
                return 'Кустовые розы';
            case 'цветы':
                return 'цветы';
            default:
                return item;
        }
    });

    return containers;
}

