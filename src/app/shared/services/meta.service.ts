import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Injectable({
    providedIn: 'root'
})
export class MetaService
{

    /**
     * Constructor
     *
     * @param {Meta} meta
     * @param {Title} title
     */
    constructor(private meta: Meta,
                private title: Title
    )
    {
    }

    setTitle(title): void
    {
        this.title.setTitle(title);
    }

    setTag(tag: string, content: string): void
    {
        this.meta.addTag({name: tag, content: content});
    }

    setKeywords(keywords: string): void
    {
        this.meta.updateTag({name: 'keywords', content: keywords});
    }

    setDescription(desc: string): void
    {
        this.meta.updateTag({name: 'description', content: desc});
    }
}
