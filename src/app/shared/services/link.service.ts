import { Inject, Injectable, Optional, RendererFactory2, ViewEncapsulation } from '@angular/core';
import { PlatformState } from '@angular/platform-server';
import { REQUEST, RESPONSE } from '@nguniversal/express-engine/tokens';

@Injectable()
export class LinkService {

  constructor(private rendererFactory: RendererFactory2,
              private state: PlatformState,
              @Optional() @Inject(RESPONSE) private response: any,
              @Optional() @Inject(REQUEST) private request: any)
  {
  }

  addCanonicalTag(): void
  {
    const host = this.request.get('host');
    this.inject({rel: 'canonical', href: `https://${host}${this.request.path}`});
  }

  addBaseTag(): void
  {
    const host = this.request.get('host');
    const document: any = this.state.getDocument();

    try
    {
      const renderer = this.rendererFactory.createRenderer(document, {
        id           : '-1',
        encapsulation: ViewEncapsulation.None,
        styles       : [],
        data         : {}
      });

      const head = document.head;

      if (head === null)
      {
        throw new Error('<head> not found within DOCUMENT.');
      }

      let headChildren = head.getElementsByTagName('base');

      if (headChildren.length > 0)
      {
        renderer.setAttribute(headChildren[0], 'href', `https://${host}/`)
      }

    } catch (e)
    {
      console.error('Error within linkService : ', e);
    }
  }

  /**
   * Inject the State into the bottom of the <head>
   */
  inject(tag: LinkDefinition, forceCreation?: boolean)
  {

    const document: any = this.state.getDocument();

    try
    {
      const renderer = this.rendererFactory.createRenderer(document, {
        id           : '-1',
        encapsulation: ViewEncapsulation.None,
        styles       : [],
        data         : {}
      });

      const link = renderer.createElement('link');
      const head = document.head;

      if (head === null)
      {
        throw new Error('<head> not found within DOCUMENT.');
      }

      Object.keys(tag).forEach((prop: string) =>
      {
        return renderer.setAttribute(link, prop, tag[prop]);
      });

      let headChildren = head.getElementsByTagName('link');

      for (let i = 0; i < headChildren.length; i++) {
        let element = headChildren[i];
        let href = element.getAttribute('href');

        if (href === tag.href) {
          renderer.removeChild(head, element);
        }
      }

      renderer.appendChild(head, link);

    } catch (e)
    {
      console.error('Error within linkService : ', e);
    }
  }

  private _parseSelector(tag: LinkDefinition): string
  {
    // Possibly re-work this
    const attr: string = tag.rel ? 'rel' : 'hreflang';
    return `${attr}="${tag[attr]}"`;
  }
}

export declare type LinkDefinition = {
  charset?: string;
  crossorigin?: string;
  href?: string;
  hreflang?: string;
  media?: string;
  rel?: string;
  rev?: string;
  sizes?: string;
  target?: string;
  type?: string;
} & {
  [prop: string]: string;
};