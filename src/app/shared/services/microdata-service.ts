import { Injectable } from '@angular/core';

const BASE_CODE = {
    '@context'    : 'http://schema.org',
    '@type'       : 'Organization',
    'name'        : 'Onetwoflowers.ru',
    'url'         : 'https://onetwoflowers.ru/',
    'logo'        : 'https://onetwoflowers.ru/assets/images/logo.svg',
    'contactPoint': [{
        '@type'      : 'ContactPoint',
        'telephone'  : '+7 (702) 222 07 30',
        'contactType': 'sales'
    }],
    'address'     : {
        '@type'          : 'PostalAddress',
        'addressLocality': 'Республика Казахстан, г. Астана',
        'postalCode'     : '010000',
        'streetAddress'  : 'улица Достык 5'
    },
    'sameAs'      : [
        'https://vk.com/cvety_kz',
        'https://www.facebook.com/%D0%94%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B0-%D1%86%D0%B2%D0%B5%D1%82%D0%BE%D0%B2-%D0%B2-%D0%90%D1%81%D1%82%D0%B0%D0%BD%D0%B5-cvetykz-636187769897028/',
        'https://www.instagram.com/cvetykz/'
    ]
};

const FLORIST_CODE = {
    '@context'    : 'http://schema.org',
    '@type'       : 'Organization',
    'name'        : 'Onetwoflowers.ru',
    'url'         : 'https://onetwoflowers.ru/',
    'image'       : 'https://onetwoflowers.ru/assets/images/logo.svg',
    'telephone'   : '+7 (702) 222 07 30',
    'contactPoint': [{
        '@type'      : 'ContactPoint',
        'telephone'  : '+7 (702) 222 07 30',
        'contactType': 'sales'
    }]
};

@Injectable({
    providedIn: 'root'
})
export class MicrodataService
{
    constructor()
    {
    }

    getCode(city: any, language: any): any
    {
        const code = BASE_CODE;

        if (language.code === 'en')
        {
            code.address.addressLocality = 'The Republic of Kazakhstan, ' + city.name;
            code.address.streetAddress   = 'Dostyk street 5';
        }

        switch (city.code)
        {
            case 'aktobe':

                switch (language.code)
                {
                    case 'ru':
                        // code.address.postalCode    = '030000';
                        // code.address.streetAddress = 'Есет Батыра 126';
                        break;

                    case 'en':
                        // code.address.postalCode    = '030000';
                        // code.address.streetAddress = 'Eset Batyr 126';
                        break;
                }

                break;
        }

        return code;
    }
}
