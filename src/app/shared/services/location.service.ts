import { Inject, Injectable, Optional, PLATFORM_ID } from '@angular/core';
import { DOCUMENT, isPlatformBrowser, isPlatformServer, Location } from '@angular/common';
import { Router } from '@angular/router';
import { REQUEST, RESPONSE } from '@nguniversal/express-engine/tokens';
import { ViewsService } from '../../../@postgar/entity';
import { Views } from '../../postgar-config/views';
import { MAIN_CITY_CODE } from '../../config';

@Injectable({
    providedIn: 'root'
})
export class LocationService
{
    langCode: string;
    cityCode: string;


    private readonly urlParser: any;

    public static existLastSlash(href: string = ''): boolean
    {
        if (href && href.length > 0)
        {
            const lastChar = href.substr(href.length - 1);
            return lastChar === '/';
        }
        return false;
    }

    /**
     * If request path contains uppercase letter
     * @param {string} path
     * @returns {boolean}
     */
    public static uppercaseCheck(path: string): boolean
    {
        const symbols = ['-', '.', '/', '!', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        let url       = path;

        for (let i = 0; i < symbols.length; i++)
        {
            const regex = new RegExp(symbols[i], 'g');
            if (url.includes(symbols[i]))
            {
                url = url.replace(regex, '');
            }
        }

        const characters = url.split('');

        for (let i = 0; i < characters.length; i++)
        {
            const upperChar = characters[i].toUpperCase();

            if (upperChar === characters[i])
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Exeption not exist
     * @param {string[]} exceptions
     * @param req
     * @returns {boolean}
     */
    public static notException(exceptions: string[], req: any): boolean
    {
        for (let i = 0; i < exceptions.length; i++)
        {
            if (req.path.endsWith(exceptions[i]))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Delete suffix and redirect
     * @param {string} replaceString
     * @param req
     * @returns {string}
     */
    public static suffixReplace(replaceString: string, req: any): string
    {
        const path = req.path.replace(replaceString, '');
        return LocationService.doubleSlashValidation(path);
    }

    /**
     * Delete double slash
     * @param {string} path
     * @returns {string}
     */
    public static doubleSlashValidation(path: string): string
    {
        if (path && path.includes('//'))
        {
            path = path.replace('//', '/');
            return LocationService.doubleSlashValidation(path);
        }
        if (!LocationService.existLastSlash(path))
        {
            path = path + '/';
        }

        return path;
    }

    public static validFullUrl(url: string): string
    {
        if (!url)
        {
            url = '';
        }

        if (url.includes('://'))
        {
            const array: string[] = url.split('://');
            return array[0] + '://' + LocationService.doubleSlashValidation(array[1]);
        } else
        {
            return LocationService.doubleSlashValidation(url);
        }
    }

    /**
     * constructor
     */
    constructor(
        @Optional() @Inject(REQUEST) private request: any,
        private location: Location,
        private router: Router,
        @Inject(DOCUMENT) private document,
        @Optional() @Inject(RESPONSE) private response: any,
        @Inject(PLATFORM_ID) private platformId: Object,
        private _viewsService: ViewsService,
    )
    {
        if (isPlatformBrowser(this.platformId))
        {
            this.urlParser = document.createElement('a');
        }
        this._viewsService.firstNode(Views.Language)
            .subscribe((node: any) => this.langCode = node.code);

        this._viewsService.firstNode(Views.City)
            .subscribe((node: any) => this.cityCode = node.code === MAIN_CITY_CODE ? '' : node.code);
    }

    get absolutePrefix(): string
    {
        return 'https://' + this.getDomain() + '/';
    }

    baseUrl(): string
    {
        const langCode = this.langCode === 'ru' ? '' : 'en';
        const cityCode = this.cityCode === MAIN_CITY_CODE ? '' : this.cityCode;
        return LocationService.validFullUrl(`/${cityCode}/${langCode}/`);
    }

    getDomain(): string
    {
        if (isPlatformServer(this.platformId))
        {
            return this.request.get('host');
        } else
        {
            return document.location.hostname;
        }
    }

    set404(): void
    {
        if (isPlatformServer(this.platformId))
        {
            this.response.status(404);
        }
    }

    go(url: string)
    {
        if (!url)
        {
            return;
        }

        // url = this.stripSlashes(url);
        if (/^http/.test(url))
        {
            // Has http protocol so leave the site
            this.goExternal(url);
        } else
        {

            url = LocationService.doubleSlashValidation(url);
            this.router.navigateByUrl(url);
        }
    }

    replace(url: string)
    {
        window.location.replace(url);
    }

    goExternal(url: string)
    {
        location.assign(url);
    }

    stripSlashes(url: string)
    {
        return url.replace(/^\/+/, '').replace(/\/+(\?|#|$)/, '$1');
    }

    goTo404(): void
    {
        this.set404();
        this.go(`404/`);
    }

    /**
     * Handle user's anchor click
     *
     * @param anchor {HTMLAnchorElement} - the anchor element clicked
     * @param button Number of the mouse button held down. 0 means left or none
     * @param ctrlKey True if control key held down
     * @param metaKey True if command or window key held down
     * @return false if service navigated with `go()`; true if browser should handle it.
     */
    handleAnchorClick(anchor: HTMLAnchorElement, button = 0, ctrlKey = false, metaKey = false)
    {

        // Check for modifier keys and non-left-button, which indicate the user wants to control navigation
        if (button !== 0 || ctrlKey || metaKey)
        {
            return true;
        }

        // If there is a target and it is not `_self` then we take this
        // as a signal that it doesn't want to be intercepted.
        // TODO: should we also allow an explicit `_self` target to opt-out?
        const anchorTarget = anchor.target;
        if (anchorTarget && anchorTarget !== '_self')
        {
            return true;
        }

        if (anchor.getAttribute('download') != null)
        {
            return true; // let the download happen
        }

        const {pathname, search, hash} = anchor;
        const relativeUrl              = pathname + search + hash;
        this.urlParser.href            = relativeUrl;

        // don't navigate if external link or has extension
        if (anchor.href !== this.urlParser.href ||
            !/\/[^/.]*$/.test(pathname))
        {
            return true;
        }

        if (anchor.href.includes('/#'))
        {
            return true;
        }

        // approved for navigation
        this.go(relativeUrl);
        return false;
    }
}
