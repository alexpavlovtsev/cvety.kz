import { Injectable } from '@angular/core';

import { Message } from '@postgar/socket';

@Injectable({
  providedIn: 'root'
})
export class SocketWorkerService
{

  constructor()
  {
  }

  /**
   * Check info from socket
   * @param message
   */
  checkPgInfo(message: Message): void
  {
    // Switch table
    switch (message.value.table)
    {
      case 'table':
        break;
    }
  }
}
