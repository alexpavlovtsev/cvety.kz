import { Injectable } from '@angular/core';
import { EntityManagerService, ViewsService } from '@postgar/entity/index';
import { Views } from '../../postgar-config/views';
import { MAIN_CITY_CODE } from '../../config';
import { LocationService } from '@shared/services/location.service';
import { FilterInfoService, filterItemConvert } from '@shared/services/filter-info.service';
import { Observable } from 'rxjs';
import { deprecateFilterItemTrs } from '@shared/utils';

@Injectable({
    providedIn: 'root'
})
export class FilterHelperService
{
    langId: number;
    filterItemTr: any[];
    cityCode: string;
    language: any;
    city: any;

    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService
    )
    {
        this._entityManager.getService('FilterItemTranslate').entities$
            .subscribe(entities =>
            {
                this.filterItemTr = deprecateFilterItemTrs(entities).filter(item =>
                {
                    return item.filterItemByFilterItemId.filterId !== 25 && item.filterItemByFilterItemId.filterId !== 3;
                });

            });

        this._viewsService.firstNode(Views.Language)
            .subscribe((node: any) =>
            {
                this.langId   = node.id;
                this.language = node;
            });

        this._viewsService.firstNode(Views.City)
            .subscribe((node: any) =>
            {
                this.cityCode = node.code === MAIN_CITY_CODE ? '' : node.code;
                this.city     = node;
            });
    }

    getActiveItems(url: string, langId: number): any[]
    {
        const filterItems = [...this.filterItemTr];

        return filterItems
            .sort((a, b) => b.url.length - a.url.length)
            .filter((node: any) =>
            {
                const includes = `-${url}-`.includes(`-${node.url}-`) && node.languageId === langId;

                if (includes)
                {
                    url = url.replace(node.url, '');

                    return true;
                }

                return false;
            });
    }

    getActiveIds(url: string): number[]
    {
        return this.getActiveItems(url, this.langId).map((node: any) => node.filterItemId);
    }

    getUrls(url: string): any
    {
        const items = this.getActiveItems(url, this.langId);

        return this.filterItemTr
            .reduce((newObj, node) =>
            {
                return {
                    ...newObj,
                    [node.id]: this.getUrl([node, ...items], this.langId)
                };
            }, {});
    }

    getUrls2(url: string): any
    {
        const activeFiltersInfo = this.getActiveItems(url, this.langId).map(value => filterItemConvert(value));

        return this.filterItemTr
            .filter(item => item.languageId === this.langId)
            .map(value => filterItemConvert(value))
            .map(item =>
            {
                return FilterInfoService.extendInfo(item, activeFiltersInfo, this.language, this.city);
            })
            .reduce((newObj, node) =>
            {
                return {
                    ...newObj,
                    [node.filterItemId]: node.url
                };
            }, {});

        // return this.filterItemTr
        //     .reduce((newObj, node) =>
        //     {
        //         const _items = [node, ...items].map(value => filterItemConvert(value));
        //
        //         return {
        //             ...newObj,
        //             [node.id]: FilterInfoService.fullData(_items, this.language, this.city).url
        //         };
        //     }, {});
    }

    getUrl(nodes: any[], langId: number): any
    {
        const quantityFilter = getUrls(nodes, 3, [], '');
        const colorFilter    = getUrls(nodes, 4, [], '');

        const nameDef  = langId === 1 ? 'cvety' : 'flowers';
        let nameFilter = getUrls(nodes, 5, [nameDef], '', true);

        const whomFilter = getUrls(nodes, 6, [], '');

        const reasonPrefix = langId === 1 ? 'na-' : 'for-';
        const reasonFilter = getUrls(nodes, 7, [], reasonPrefix);

        const priceFilter       = getUrls(nodes, 10, [], '');
        const compositionFilter = getUrls(nodes, 25, [], '');

        if (
            colorFilter.length === 0 &&
            whomFilter.length === 0 &&
            reasonFilter.length === 0 &&
            priceFilter.length === 0 &&
            compositionFilter.length === 0 &&
            nameFilter.length === 1
        )
        {
            nameFilter = nameFilter.filter(item => item !== 'cvety' && item !== 'flowers');
        }

        const path = [
            ...quantityFilter,
            ...colorFilter,
            ...nameFilter,
            ...whomFilter,
            ...reasonFilter,
            ...priceFilter,
            ...compositionFilter,
        ]
            .filter(item => !!item)
            .join('-');

        const prefix = langId === 1 ? 'flowers' : 'bouquets';

        return this.getValidUrl(`${path}`);
    }

    private getValidUrl(url: string): string
    {
        const langCode = this.langId === 1 ? '' : 'en';
        return LocationService.validFullUrl(`/${this.cityCode}/${langCode}/` + url);
    }

    checkUrl(url: string): boolean
    {
        const activeItems = this.getActiveItems(url, this.langId).map(item => filterItemConvert(item));
        const validUrl = FilterInfoService.fullData(activeItems, this.language, this.city).cleanUrl;

        return validUrl === url;
    }
}

function getUrls(nodes: any[], filterId: number, def: any[], prefix: string, multiple: boolean = false)
{
    const filtered = nodes
        .filter(node => node.filterItemByFilterItemId.filterId === filterId)
        .map((item: any) => prefix + item.url);

    if (multiple)
    {
        return !!filtered[0] ? filtered : def;
    }

    return !!filtered[0] ? [filtered[0]] : def;
}

function getLabels(nodes: any[], filterId: number, def: any[], prefix: string, multiple: boolean = false)
{
    const filtered = nodes
        .filter(node => node.filterItemByFilterItemId.filterId === filterId)
        .map((item: any) => prefix + item.value);

    if (multiple)
    {
        return !!filtered[0] ? filtered : def;
    }

    return !!filtered[0] ? [filtered[0]] : def;
}
