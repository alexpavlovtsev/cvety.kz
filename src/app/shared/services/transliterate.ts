export const TRANSLITERATE = {
    'ый': 'iy',
    'ЫЙ': 'iy',
    'а' : 'a',
    'А' : 'a',
    'б' : 'b',
    'Б' : 'b',
    'в' : 'v',
    'В' : 'v',
    'г' : 'g',
    'Г' : 'g',
    'д' : 'd',
    'Д' : 'd',
    'е' : 'e',
    'Е' : 'e',
    'ё' : 'e',
    'Ё' : 'e',
    'ж' : 'zh',
    'Ж' : 'zh',
    'з' : 'z',
    'З' : 'z',
    'и' : 'i',
    'И' : 'i',
    'й' : 'y',
    'Й' : 'y',
    'к' : 'k',
    'К' : 'k',
    'л' : 'l',
    'Л' : 'l',
    'м' : 'm',
    'М' : 'm',
    'н' : 'n',
    'Н' : 'n',
    'о' : 'o',
    'О' : 'o',
    'п' : 'p',
    'П' : 'p',
    'р' : 'r',
    'Р' : 'r',
    'с' : 's',
    'С' : 's',
    'т' : 't',
    'Т' : 't',
    'у' : 'u',
    'У' : 'u',
    'ф' : 'f',
    'Ф' : 'f',
    'х' : 'h',
    'Х' : 'h',
    'ц' : 'c',
    'Ц' : 'c',
    'ч' : 'ch',
    'Ч' : 'ch',
    'ш' : 'sh',
    'Ш' : 'sh',
    'щ' : 'shch',
    'Щ' : 'shch',
    'ъ' : '',
    'Ъ' : '',
    'ы' : 'y',
    'Ы' : 'y',
    'ь' : '',
    'Ь' : '',
    'э' : 'e',
    'Э' : 'e',
    'ю' : 'yu',
    'Ю' : 'yu',
    'я' : 'ya',
    'Я' : 'ya'
};

export const TRANSLITERATE_KZ = {
    'Ғ': 'g',
    'ғ': 'g',
    'Қ': 'q',
    'қ': 'q',
    'Ң': 'n',
    'ң': 'n',
    'Ө': 'o',
    'ө': 'o',
    'Ұ': 'u',
    'ұ': 'u',
    'Ү': 'u',
    'ү': 'u',
    'H': 'h',
    'h': 'h',
    'I': 'i',
    'i': 'i',
    'ә': 'a'
};

export class Transliterate
{
    public static convert(word: string = ''): string
    {
        const configObj = Object.assign({}, TRANSLITERATE, TRANSLITERATE_KZ);

        for (const item of Object.keys(configObj))
        {
            const regex = new RegExp(item, 'g');
            word        = word.replace(regex, configObj[item]);
        }

        return this.handleize(word);
    }

    public static handleize(text): string
    {
        return text.toString().toLowerCase()
            .replace(/\_+/g, '-')           // Replace _ with -
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
    }
}
