export class Grammar
{
    constructor()
    {
    }

    /**
     * Capitalize first letter
     *
     * @param {string} s
     * @returns {string}
     */
    public static capitalize(s: string)
    {
        const lower = (s || '').toLowerCase();
        return lower.length > 0 ? lower[0].toUpperCase() + lower.slice(1) : '';
    }

    public static validationTitle(string: string = ''): string
    {
        string = this.toTitleCase(string);
        return this.firstLastWord(string);
    }

    public static firstLastWord(string: string = ''): string
    {
        const words: string[] = string.split(' ');

        if (words.length > 0)
        {
            words[0]                = this.upperCaseFirstLetter(words[0]);
            words[words.length - 1] = this.upperCaseFirstLetter(words[words.length - 1]);
        }

        return words.join(' ');
    }

    public static upperCaseFirstLetter(string: string = ''): string
    {
        if (!string)
        {
            return '';
        }
        return string.charAt(0).toUpperCase() + string.substring(1);
    }

    /**
     * Title case library
     * @param {string} string
     * @returns {string}
     */
    public static toTitleCase(string: string = ''): string
    {
        string           = string || '';
        const smallWords = /^(from|with|a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;

        return string.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function (match, index, title)
        {
            if (index > 0 && index + match.length !== title.length &&
                match.search(smallWords) > -1 && title.charAt(index - 2) !== ':' &&
                (title.charAt(index + match.length) !== '-' || title.charAt(index - 1) === '-') &&
                title.charAt(index - 1).search(/[^\s-]/) < 0)
            {
                return match.toLowerCase();
            }

            if (match.substr(1).search(/[A-Z]|\../) > -1)
            {
                return match;
            }

            return match.charAt(0).toUpperCase() + match.substr(1);
        });
    }
}
