import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { UploadComponent } from './upload.component';

@NgModule({
    imports        : [
        CommonModule,
        HttpClientModule,
    ],
    declarations   : [UploadComponent],
    exports        : [UploadComponent],
})
export class UploadModule
{
}
