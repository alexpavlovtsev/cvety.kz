import { Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { fromEvent, of, Subject } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';

@Component({
    selector   : 'app-upload',
    templateUrl: './upload.component.html',
    styleUrls  : ['./upload.component.scss']
})
export class UploadComponent implements OnInit, OnDestroy
{
    @Output()
    uploadCancel: EventEmitter<string>;

    @Output()
    startUpload: EventEmitter<boolean>;

    @ViewChild('fileInput')
    fileInput: ElementRef;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
    )
    {
        this.startUpload  = new EventEmitter();
        this.uploadCancel = new EventEmitter();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Upload file
        fromEvent(this.fileInput.nativeElement, 'input')
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((event: any) =>
            {
                if (event.target.files && event.target.files.length > 0)
                {
                    this.fileUpload(event.target.files[0]);
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Uploading file to the server
     *
     * @param file
     */
    fileUpload(file: any): void
    {
        this.startUpload.emit(true);

        const formData: FormData = new FormData();
        formData.append('file', file);

        const options = {
            params        : new HttpParams(),
            reportProgress: true,
        };

        // Upload file
        this._httpClient.post('https://striphunter.com:5200/upload', formData, options)
            .pipe(
                catchError(err =>
                {
                    console.log(err);
                    return of(err);
                }),
            )
            .subscribe((res: any) =>
            {
                this.uploadCancel.emit(res.filename);
            });
    }
}
