import { NgModule } from '@angular/core';
import { ClickOutsideDirective } from './click-outside.directive';
import { DynamicComponentDirective } from './dynamic-component.directive';
import { IfOnDomDirective } from './if-on-dom.directive';
import { HrefDirective } from '@shared/directives/href.directive';

@NgModule({
    declarations: [
        ClickOutsideDirective,
        DynamicComponentDirective,
        IfOnDomDirective,
        HrefDirective,
    ],
    exports     : [
        ClickOutsideDirective,
        DynamicComponentDirective,
        IfOnDomDirective,
        HrefDirective,
    ]
})
export class DirectivesModule
{
}
