import {
    AfterViewChecked, AfterViewInit, Directive, ElementRef,
    Renderer2
} from '@angular/core';
import { LocationService } from '../services/location.service';

@Directive({
    selector: '[href]'
})
export class HrefDirective implements AfterViewInit, AfterViewChecked {

    href: string;

    constructor(private _elementRef: ElementRef,
                private _renderrer: Renderer2)
    {
    }

    ngAfterViewInit()
    {
        this.href = this._elementRef.nativeElement.getAttribute('href');
        this.attributeChange();
    }

    ngAfterViewChecked()
    {
        this.href = this._elementRef.nativeElement.getAttribute('href');
        this.attributeChange();
    }

    attributeChange(): void
    {
        if (this.href.startsWith('#'))
        {
            return;
        }
        this.href = LocationService.validFullUrl(this.href);
        this._renderrer.setAttribute(this._elementRef.nativeElement, 'href', this.href);
    }
}
