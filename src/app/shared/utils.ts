export function isNil(obj: any): boolean
{
    return obj === undefined || obj === null;
}

export function isString(value: any): boolean
{
    return typeof value === 'string';
}

export function isClass(model: any): boolean
{
    return !!model && typeof model === 'function';
}

/**
 * Capitalize First Letter
 *
 * @param string
 * @returns {string}
 */
export function capitalizeFirstLetter(string: string = ''): string
{
    if (!string)
    {
        return '';
    }
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export function getById(value: any[], id: number): any
{
    return value.find(item =>
    {
        if (item.id !== undefined)
        {
            return item.id === id;
        }

        return false;
    });
}

export function scrollTo(element, to, duration)
{
    const start     = element.scrollTop,
          change    = to - start,
          increment = 20;
    let currentTime = 0;

    const animateScroll = () =>
    {
        currentTime += increment;
        const val         = easeInOutQuad(currentTime, start, change, duration);
        element.scrollTop = val;
        if (currentTime < duration)
        {
            setTimeout(animateScroll, increment);
        }
    };
    animateScroll();
}

const easeInOutQuad = (t, b, c, d) =>
{
    t /= d / 2;
    if (t < 1)
    {
        return c / 2 * t * t + b;
    }
    t--;
    return -c / 2 * (t * (t - 2) - 1) + b;
};

export class Utils
{
    /**
     * Filter array by string
     *
     * @param mainArr
     * @param searchText
     * @returns {any}
     */
    public static filterArrayByString(mainArr, searchText): any
    {
        if (searchText === '')
        {
            return mainArr;
        }

        searchText = searchText.toLowerCase();

        return mainArr.filter(itemObj =>
        {
            return this.searchInObj(itemObj, searchText);
        });
    }

    /**
     * Search in object
     *
     * @param itemObj
     * @param searchText
     * @returns {boolean}
     */
    public static searchInObj(itemObj, searchText): boolean
    {
        for (const prop in itemObj)
        {
            if (!itemObj.hasOwnProperty(prop))
            {
                continue;
            }

            const value = itemObj[prop];

            if (typeof value === 'string')
            {
                if (this.searchInString(value, searchText))
                {
                    return true;
                }
            }

            else if (Array.isArray(value))
            {
                if (this.searchInArray(value, searchText))
                {
                    return true;
                }
            }

            if (typeof value === 'object')
            {
                if (this.searchInObj(value, searchText))
                {
                    return true;
                }
            }
        }
    }

    /**
     * Search in array
     *
     * @param arr
     * @param searchText
     * @returns {boolean}
     */
    public static searchInArray(arr, searchText): boolean
    {
        for (const value of arr)
        {
            if (typeof value === 'string')
            {
                if (this.searchInString(value, searchText))
                {
                    return true;
                }
            }

            if (typeof value === 'object')
            {
                if (this.searchInObj(value, searchText))
                {
                    return true;
                }
            }
        }
    }

    /**
     * Search in string
     *
     * @param value
     * @param searchText
     * @returns {any}
     */
    public static searchInString(value, searchText): any
    {
        return value.toLowerCase().includes(searchText);
    }

    /**
     * Generate a unique GUID
     *
     * @returns {string}
     */
    public static generateGUID(): string
    {
        function S4(): string
        {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }

        return S4() + S4();
    }

    /**
     * Toggle in array
     *
     * @param item
     * @param array
     */
    public static toggleInArray(item, array): void
    {
        if (array.indexOf(item) === -1)
        {
            array.push(item);
        }
        else
        {
            array.splice(array.indexOf(item), 1);
        }
    }

    /**
     * Handleize
     *
     * @param text
     * @returns {string}
     */
    public static handleize(text): string
    {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
    }

    public static stopPropagation(event: any): void
    {
        event.preventDefault();
        event.stopPropagation();
    }
}

export function deprecateFilterTrs(filters: any[])
{
    const deps = {25: true, 3: true};
    return filters.filter(value => !deps[value.filterId]);
}

export function deprecateFilterItemTrs(filters: any[])
{
    const deps = {'1619': true, '84': true, '85': true, '157': true, '159': true};
    return filters.filter(value => !deps[value.filterItemId]);
}
