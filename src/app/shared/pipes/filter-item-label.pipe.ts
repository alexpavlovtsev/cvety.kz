import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterItemLabel'
})
export class FilterItemLabelPipe implements PipeTransform
{

    transform(nodes: any[], filterId: number): any
    {
        return nodes.filter((item: any) =>
        {
            return item.filterItemByFilterItemId.filterId === filterId;
        });
    }

}
