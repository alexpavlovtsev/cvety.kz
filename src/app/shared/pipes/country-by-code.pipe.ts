import { Pipe, PipeTransform } from '@angular/core';
import { Observable, of } from 'rxjs';

@Pipe({
    name: 'countryByCode'
})
export class CountryByCodePipe implements PipeTransform
{

    transform(value: string): Observable<string>
    {
        return of('Украина');
    }

}
