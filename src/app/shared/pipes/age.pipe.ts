import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'age'
})
export class AgePipe implements PipeTransform
{
    transform(value: any): any
    {
        return !!value ? moment(value).diff(moment(), 'years') : '';
    }
}
