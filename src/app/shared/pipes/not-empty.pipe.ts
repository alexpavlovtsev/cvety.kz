import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'notEmpty'
})
export class NotEmptyPipe implements PipeTransform
{

    transform(array: any[], args?: any): any
    {
        if (array && array.length > 0)
        {
            return array.filter(item => !!item);
        }

        return [];
    }
}
