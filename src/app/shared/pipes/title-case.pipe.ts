import { Pipe, PipeTransform } from '@angular/core';
import { Grammar } from '@shared/grammar';

@Pipe({
  name: 'titleCase'
})
export class TitleCasePipe implements PipeTransform {

  transform(value: string, langId: number): string {
    if (!value) {
      return null;
    }

    return langId === 2 ? Grammar.validationTitle(value) : Grammar.upperCaseFirstLetter(value);
  }
}
