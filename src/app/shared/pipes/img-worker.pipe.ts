import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'imgWorker'
})
export class ImgWorkerPipe implements PipeTransform
{
    transform(value: string, width: number, height: number): any
    {
        if (value && value.startsWith('http'))
        {
            if (value.includes('d1nzqupm6yqdz3.cloudfront.net'))
            {
                return value.replace('https://d1nzqupm6yqdz3.cloudfront.net/', `https://floracrm.com:1338/${width}x${height}/`);
            }
            return value;
        }
        else if (value && value.startsWith('/image/'))
        {
            value = value.replace('/image/', '');

            if (!width || !height)
            {
                return 'https://floracrm.com:1338/cvety/' + value;
            }
            else
            {
                return `https://floracrm.com:1338/cvety/${value}?width=${width}&height=${height}`;
            }
        }
    }
}
