import { Pipe, PipeTransform } from '@angular/core';
import { Utils } from '@shared/utils';

@Pipe({name: 'filter'})
export class FilterPipe implements PipeTransform
{
    /**
     * Transform
     *
     * @param {any[]} mainArr
     * @param {string} searchText
     * @param {string} property
     * @returns {any}
     */
    transform(mainArr: any[], searchText: string, property: string): any
    {
        searchText = searchText || '';
        return Utils.filterArrayByString(mainArr, searchText);
    }
}
