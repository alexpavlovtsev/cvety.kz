import { Pipe, PipeTransform } from '@angular/core';

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { EntityStoreService, StoreServices } from '@postgar/entity';

@Pipe({
  name: 'categoryById'
})
export class CategoryByIdPipe implements PipeTransform {

    // private
    private _categoryService: EntityStoreService<any>;

    /**
     * Constructor
     *
     * @param _entityServices
     */
    constructor(
        private _entityServices: StoreServices
    )
    {
        this._categoryService = _entityServices.getEntityCollectionService('AspirantCategory');
    }

    transform(id: number): Observable<string>
    {
        if (!id)
        {
            return of('');
        }
        return this._categoryService.entities$.pipe(
            map(entities =>
            {
                const cat = entities.find(_cat => _cat.id === id);

                return cat === undefined ? '' : cat.name;
            })
        );
    }

}
