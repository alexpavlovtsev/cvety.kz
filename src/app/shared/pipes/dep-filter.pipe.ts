import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'depFilter'
})
export class DepFilterPipe implements PipeTransform
{
    transform(values: any[], ...ids: number[]): any
    {
        return values.filter(value => !ids.some(id => id === value.id));
    }
}
