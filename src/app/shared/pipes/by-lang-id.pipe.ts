import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'byLangId'
})
export class ByLangIdPipe implements PipeTransform
{
    transform(nodes: any[] = [], langId: number): any
    {
        return nodes.filter(node => node.languageId === langId);
    }
}
