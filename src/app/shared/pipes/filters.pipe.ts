import { Pipe, PipeTransform } from '@angular/core';

const sortOrder = [5, 3, 4, 7, 25, 6, 10];

@Pipe({
    name: 'filterOrder'
})
export class FilterOrderPipe implements PipeTransform
{

    transform(values: any[]): any
    {
        return values.sort((a, b) =>
        {
            return sortOrder.indexOf(a.filterId) - sortOrder.indexOf(b.filterId);
        });
    }

}
