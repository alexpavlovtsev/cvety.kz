import { NgModule } from '@angular/core';

import { KeysPipe } from './keys.pipe';
import { GetByIdPipe } from './get-by-id.pipe';
import { HtmlToPlaintextPipe } from './html-to-plaintext.pipe';
import { FilterPipe } from './filter.pipe';
import { CamelCaseToDashPipe } from './camel-case-to-dash.pipe';
import { CityByCodePipe } from './city-by-code.pipe';
import { TruncatePipe } from './truncate.pipe';
import { AgePipe } from './age.pipe';
import { CountryByCodePipe } from './country-by-code.pipe';
import { CategoryByIdPipe } from '@shared/pipes/category-by-id.pipe';
import { SafeHtmlPipe } from '@shared/pipes/safe-html';
import { NotEmptyPipe } from './not-empty.pipe';
import { OrderByPipe } from '@shared/pipes/order-by.pipe';
import { FilterItemLabelPipe } from './filter-item-label.pipe';
import { ByLangIdPipe } from './by-lang-id.pipe';
import { ImgWorkerPipe } from './img-worker.pipe';
import { CurrencyPipe } from './my-currency.pipe';
import { DepFilterPipe } from './dep-filter.pipe';
import { FilterOrderPipe } from '@shared/pipes/filters.pipe';
import { TitleCasePipe } from '@shared/pipes/title-case.pipe';
import { FilterValidLinkPipe } from '@shared/pipes/filter-valid-link.pipe';

@NgModule({
    imports     : [],
    declarations: [
        CityByCodePipe,
        KeysPipe,
        GetByIdPipe,
        HtmlToPlaintextPipe,
        FilterPipe,
        CamelCaseToDashPipe,
        TruncatePipe,
        AgePipe,
        CountryByCodePipe,
        CategoryByIdPipe,
        SafeHtmlPipe,
        NotEmptyPipe,
        OrderByPipe,
        FilterItemLabelPipe,
        ByLangIdPipe,
        ImgWorkerPipe,
        CurrencyPipe,
        DepFilterPipe,
        FilterOrderPipe,
        TitleCasePipe,
        FilterValidLinkPipe,
    ],
    exports     : [
        CityByCodePipe,
        KeysPipe,
        GetByIdPipe,
        HtmlToPlaintextPipe,
        FilterPipe,
        CamelCaseToDashPipe,
        TruncatePipe,
        AgePipe,
        CountryByCodePipe,
        CategoryByIdPipe,
        SafeHtmlPipe,
        NotEmptyPipe,
        OrderByPipe,
        FilterItemLabelPipe,
        ByLangIdPipe,
        ImgWorkerPipe,
        CurrencyPipe,
        DepFilterPipe,
        FilterOrderPipe,
        TitleCasePipe,
        FilterValidLinkPipe,
    ]
})
export class PipesModule
{
}
