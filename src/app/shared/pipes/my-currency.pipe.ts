import { Pipe, PipeTransform } from '@angular/core';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../postgar-config/views';

const Translates = {
    'KZT': {en: 'KZT', ru: '₸', kz: 'KZT'},
    'EUR': {en: '€', ru: '€', kz: '€'},
    'USD': {en: '$', ru: '$', kz: '$'},
    'RUB': {en: 'RUB', ru: 'Руб', kz: 'RUB'}
};

@Pipe({
    name: 'myCurrency'
})
export class CurrencyPipe implements PipeTransform
{
    pricingFactor = 1;
    city: any;

    constructor(
        private _viewsService: ViewsService
    )
    {
        this._viewsService.firstNode(Views.City).subscribe((node: any) =>
        {
            this.city = node || {};
            if (this.city.pricingFactor)
            {
                this.pricingFactor = this.city.pricingFactor;
            }
        });
    }

    transform(value: any, currency: any, language: any, isMore = 0): any
    {
        const rawWal = value;

        if (!currency || !language)
        {
            return value;
        }

        if (typeof value === 'string')
        {
            value = parseFloat(value);
        }

        const price = pricePrepared(value, currency, isMore, this.pricingFactor);

        let string: string = price.toString();
        const strClean     = Math.round(price).toString().replace('.', '');

        if (strClean.length === 4)
        {
            string = string.charAt(0) + ' ' + string.slice(1);
        } else if (strClean.length === 5)
        {
            string = string.charAt(0) + string.charAt(1) + ' ' + string.slice(2);
        } else if (strClean.length === 6)
        {
            string = string.charAt(0) + string.charAt(1) + string.charAt(2) + ' ' + string.slice(3);
        }

        return string + ' ' + Translates[currency.code][language.code];
    }
}

function pricePrepared(value: number, currency: any, isMore = 0, pricingFactor): number
{
    let price: number = value / currency.exchange;

    if (isMore === 60)
    {
        price = price + (price * 0.6);
    }
    else if (isMore === 30)
    {
        price = price + (price * 0.3);
    }

    return Math.round((price * pricingFactor) * 100) / 100;
}
