import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'validLink'
})
export class FilterValidLinkPipe implements PipeTransform
{
    transform(activeFiltersInfo: any[], filterInfo: any, counts: object): boolean
    {
        const activeFilterItemIds = activeFiltersInfo.map(item => item.filterItemId);
        const notActive           = activeFilterItemIds.indexOf(filterInfo.filterItemId) === -1;
        const productMoreThen2    = counts[filterInfo.filterItemId] > 2;
        const validVariants       = FilterValidVariants([...activeFiltersInfo, filterInfo]);

        return notActive && productMoreThen2 && activeFiltersInfo.length < 5 && validVariants;
    }
}

const SortNumber = (a, b) =>
{
    return a - b;
};

export function FilterValidVariants(activeFiltersInfo: any[]): boolean
{
    const str = activeFiltersInfo.map(item => item.filterId).sort(SortNumber).join('-');

    if (str.length === 1)
    {
        return true;
    }

    switch (str)
    {
        case '3-5': // количество + цветок
            return true;

        case '6-7': // кому + повод
            return true;

        case '4-6': // цвет + кому
            return true;

        case '4-7':  // цвет + повод
            return true;

        case '4-5':  // цвет + цветок
            return true;

        case '5-6':  // цветок + кому
            return true;

        case '5-7':  // цветок + повод
            return true;

        case '5-5':  // цветок + цветок
            return true;

        case '5-10':  // цветок + цена
            return true;

        case '3-4-5':  // количество + цвет + цветок
            return true;

        case '3-5-6':  // количество + цветок + кому
            return true;

        case '3-5-10':  // количество + цветок + цена
            return true;

        default:
            return false;
    }
}
