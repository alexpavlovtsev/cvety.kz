import { Pipe, PipeTransform } from '@angular/core';

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { EntityStoreService, StoreServices } from '@postgar/entity';

@Pipe({
    name: 'cityByCode'
})
export class CityByCodePipe implements PipeTransform
{
    // private
    private _cityService: EntityStoreService<any>;

    /**
     * Constructor
     *
     * @param _entityServices
     */
    constructor(
        private _entityServices: StoreServices
    )
    {
        this._cityService = _entityServices.getEntityCollectionService('City');
    }

    transform(value: string): Observable<string>
    {
        if (!value)
        {
            return of('');
        }
        return this._cityService.entities$.pipe(
            map(entities =>
            {
                const city = entities.find(_city => _city.value === value);

                return city === undefined ? '' : city.name;
            })
        );
    }

}
