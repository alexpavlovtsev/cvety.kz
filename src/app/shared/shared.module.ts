import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DirectivesModule } from '@shared/directives/directives.module';
import { PipesModule } from '@shared/pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
import { DeferLoadModule } from '@trademe/ng-defer-load';

@NgModule({
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        TranslateModule,

        DirectivesModule,
        PipesModule,
        DeferLoadModule,
    ],
})
export class SharedModule
{
}
