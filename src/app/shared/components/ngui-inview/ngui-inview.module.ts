import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NguiInviewComponent } from './ngui-inview.component';

@NgModule({
    imports     : [
        CommonModule
    ],
    declarations: [NguiInviewComponent],
    exports     : [NguiInviewComponent]
})
export class NguiInviewModule
{
}
