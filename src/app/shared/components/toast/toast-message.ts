import { PostgarUtils } from '@postgar/utils';

export class ToastMessage
{
    id: string;
    content: string;
    style: string;
    dismissed: boolean = false;

    constructor(content: string, style?)
    {
        this.id      = PostgarUtils.generateGUID();
        this.content = content;
        this.style   = style || 'info';
    }

}
