import { Injectable } from '@angular/core';
import { ToastMessage } from '@shared/components/toast/toast-message';
import { EntityStoreService, StoreServices } from '@postgar/entity';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ToastService
{
    private _toastService: EntityStoreService<any>;

    /**
     * Constructor
     *
     * @param _entityServices
     */
    constructor(
        private _entityServices: StoreServices
    )
    {
    }

    getMessages(): Observable<ToastMessage[]>
    {
        this.initService();

        return this._toastService.entities$;
    }

    sendMessage(content, time, style?): Promise<string>
    {
        this.initService();

        return new Promise<string>(resolve =>
        {
            const message = new ToastMessage(content, style);
            this._toastService.addOneToCache(message);

            resolve(message.id);

            if (!!time)
            {
                setTimeout(() =>
                {
                    this.dismissMessage(message.id);
                }, time);
            }
        });
    }

    dismissMessage(id: string)
    {
        this.initService();
        this._toastService.updateOneInCache({'dismissed': true, id});
    }

    initService(): void
    {
        if (!this._toastService)
        {
            this._toastService = this._entityServices.getEntityCollectionService('ToastMessage');
        }
    }
}
