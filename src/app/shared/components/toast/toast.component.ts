import { Component, OnInit } from '@angular/core';
import { ToastService } from '@shared/components/toast/toast.service';
import { fuseAnimations } from '@shared/animations';
import { SchemaService } from '@postgar/entity';
import { filter, switchMap } from 'rxjs/operators';

@Component({
    selector   : 'app-toast',
    templateUrl: './toast.component.html',
    styleUrls  : ['./toast.component.scss'],
    animations : fuseAnimations
})
export class ToastComponent implements OnInit
{
    messages: any[];

    constructor(
        private _toast: ToastService,
        private _schemaService: SchemaService
    )
    {
        this.messages = [];
    }

    ngOnInit(): void
    {
        this._schemaService.loaded$
            .pipe(
                filter(loaded => loaded),
                switchMap(() => this._toast.getMessages())
            )
            .subscribe((messages) =>
            {
                this.messages = messages;
            });
    }

    dismiss(id: string): void
    {
        this._toast.dismissMessage(id);
    }
}
