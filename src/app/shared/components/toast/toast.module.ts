import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastComponent } from './toast.component';
import { ReversePipe } from './reverse.pipe';

@NgModule({
    imports     : [
        CommonModule
    ],
    declarations: [ToastComponent, ReversePipe],
    exports     : [ToastComponent]
})
export class ToastModule
{
}
