import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompletedSpinnerComponent } from './completed-spinner.component';

@NgModule({
    imports     : [
        CommonModule
    ],
    declarations: [CompletedSpinnerComponent],
    exports     : [CompletedSpinnerComponent]
})
export class CompletedSpinnerModule
{
}
