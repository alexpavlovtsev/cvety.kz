import { ChangeDetectorRef, Component, OnInit } from '@angular/core';

@Component({
    selector   : 'app-completed-spinner',
    templateUrl: './completed-spinner.component.html',
    styleUrls  : ['./completed-spinner.component.scss']
})
export class CompletedSpinnerComponent implements OnInit
{
    success: boolean;

    /**
     * Constructor
     *
     * @param {ChangeDetectorRef} _cd
     */
    constructor(
        private _cd: ChangeDetectorRef
    )
    {
        this.success = false;
    }

    ngOnInit()
    {
        setTimeout(() =>
        {
            this.success = true;
            refresh(this._cd);
        }, 1500);
    }
}

function refresh(cd: ChangeDetectorRef): void
{
    if (!cd['destroyed'])
    {
        cd.detectChanges();
    }
}
