import { NgModule } from '@angular/core';
import {
    Routes,
    RouterModule,
    Router
} from '@angular/router';

import 'rxjs/add/operator/filter';

import { PostgarGuard } from '@postgar/services/postgar.guard';

import { WrapperComponent } from './layout-elements/wrapper/wrapper.component';
import { ResolveGuard } from './app.resolve.guard';
import { CityCodes } from '@shared/data/city-codes';

const routes: Routes = [
    {
        path            : '',
        component       : WrapperComponent,
        canActivateChild: [PostgarGuard, ResolveGuard],
        children        : [
            // front
            {path: '', data: {type: 'front'}, loadChildren: './pages/front/front.module#FrontModule'},
            {path: 'en/', data: {type: 'front'}, loadChildren: './pages/front/front.module#FrontModule'},

            // bouquet
            {path: `bukety`, data: {type: 'bouquet'}, loadChildren: './pages/bouquet/bouquet.module#BouquetModule'},
            {path: 'en/bouquets', data: {type: 'bouquet'}, loadChildren: './pages/bouquet/bouquet.module#BouquetModule'},

            // bouquets
            {path: `bukety/`, data: {type: 'bouquets'}, loadChildren: './pages/bouquets/bouquets.module#BouquetsModule'},
            {path: 'en/bouquets/', data: {type: 'bouquets'}, loadChildren: './pages/bouquets/bouquets.module#BouquetsModule'},

            // payment result
            {path: `payment/error/`, data: {type: 'page'}, loadChildren: './pages/pay-callback/pay-callback.module#PayCallbackModule'},
            {path: 'en/payment/error/', data: {type: 'page'}, loadChildren: './pages/pay-callback/pay-callback.module#PayCallbackModule'},
            {path: `payment/success/:orderId`, data: {type: 'page'}, loadChildren: './pages/pay-callback/pay-callback.module#PayCallbackModule'},
            {path: 'en/payment/success/:orderId', data: {type: 'page'}, loadChildren: './pages/pay-callback/pay-callback.module#PayCallbackModule'},

            // delivery
            {path: `delivery/`, data: {type: 'page'}, loadChildren: './pages/faq/faq.module#FaqModule'},
            {path: 'en/delivery/', data: {type: 'page'}, loadChildren: './pages/faq/faq.module#FaqModule'},

            // guarantee
            {path: `guarantee/`, data: {type: 'page'}, loadChildren: './pages/faq/faq.module#FaqModule'},
            {path: 'en/guarantee/', data: {type: 'page'}, loadChildren: './pages/faq/faq.module#FaqModule'},

            // payment
            {path: `payment/`, data: {type: 'page'}, loadChildren: './pages/faq/faq.module#FaqModule'},
            {path: 'en/payment/', data: {type: 'page'}, loadChildren: './pages/faq/faq.module#FaqModule'},

            // contacts
            {path: `contacts/`, data: {type: 'page'}, loadChildren: './pages/faq/faq.module#FaqModule'},
            {path: 'en/contacts/', data: {type: 'page'}, loadChildren: './pages/faq/faq.module#FaqModule'},

            // reviews
            {path: `reviews/`, data: {type: 'page'}, loadChildren: './pages/faq/faq.module#FaqModule'},
            {path: 'en/reviews/', data: {type: 'page'}, loadChildren: './pages/faq/faq.module#FaqModule'},

            // delivery photos
            {path: `delivery-photos/`, data: {type: 'page'}, loadChildren: './pages/faq/faq.module#FaqModule'},
            {path: 'en/delivery-photos/', data: {type: 'page'}, loadChildren: './pages/faq/faq.module#FaqModule'},

            // order
            {path: `order/`, data: {type: 'order'}, loadChildren: './pages/order/order.module#OrderModule'},
            {path: 'en/order/', data: {type: 'order'}, loadChildren: './pages/order/order.module#OrderModule'},

            // sitemap
            {path: `sitemap/`, data: {type: 'sitemap'}, loadChildren: './pages/sitemap/sitemap.module#SitemapModule'},
            {path: 'en/sitemap/', data: {type: 'sitemap'}, loadChildren: './pages/sitemap/sitemap.module#SitemapModule'},

            // customer feedback form
            {path: `o`, data: {type: 'add-review'}, loadChildren: './pages/reviews/reviews.module#ReviewsModule'},
            {path: `en/o/`, data: {type: 'add-review'}, loadChildren: './pages/reviews/reviews.module#ReviewsModule'},

            // all cities
            {path: `all-cities/`, data: {type: 'page'}, loadChildren: './pages/cities/cities.module#CitiesModule'},
            {path: 'en/all-cities/', data: {type: 'page'}, loadChildren: './pages/cities/cities.module#CitiesModule'},

            // feedback form for recipients
            {
                path        : `rf`, data: {type: 'add-review'},
                loadChildren: './pages/recipient-feedback/recipient-feedback.module#RecipientFeedbackModule'
            },
            // {
            //     path        : `rf/oс/`, data: {type: 'add-review'},
            //     loadChildren: './pages/recipient-feedback/recipient-feedback.module#RecipientFeedbackModule'
            // },

            // order status info
            {path: `os/:order`, data: {type: 'order-inform'}, loadChildren: './pages/order-inform/order-inform.module#OrderInformModule'},
            {path: `en/os/:order`,
                data: {type: 'order-inform'}, loadChildren: './pages/order-inform/order-inform.module#OrderInformModule'},

            // thematic
            {path: `populyarnye-cvety/`, data: {type: 'thematic'}, loadChildren: './pages/thematic/thematic.module#ThematicModule'},
            {path: 'en/featured-flowers/', data: {type: 'thematic'}, loadChildren: './pages/thematic/thematic.module#ThematicModule'},
            // {path: `cvety-nedorogo/`, data: {type: 'catalog'}, loadChildren: './pages/thematic/thematic.module#ThematicModule'},
            // {path: 'en/flowers-cheap/', data: {type: 'catalog'}, loadChildren: './pages/thematic/thematic.module#ThematicModule'},
            // {path: `extraordinary/`, data: {type: 'thematic'}, loadChildren: './pages/thematic/thematic.module#ThematicModule'},
            // {path: 'en/extraordinary/', data: {type: 'thematic'}, loadChildren: './pages/thematic/thematic.module#ThematicModule'},
            // {path: `in-box/`, data: {type: 'thematic'}, loadChildren: './pages/thematic/thematic.module#ThematicModule'},
            // {path: 'en/in-box/', data: {type: 'thematic'}, loadChildren: './pages/thematic/thematic.module#ThematicModule'},

            // not found
            {path: `404/`, data: {type: '404'}, loadChildren: './pages/not-found/not-found.module#NotFoundModule'},
            {path: `en/404/`, data: {type: '404'}, loadChildren: './pages/not-found/not-found.module#NotFoundModule'},

            // catalog
            {path: `:catalog/`, data: {type: 'catalog'}, loadChildren: './pages/catalog/catalog.module#CatalogModule'},
            {path: 'en/:catalog/', data: {type: 'catalog'}, loadChildren: './pages/catalog/catalog.module#CatalogModule'},

            {path: '**', data: {type: '404'}, loadChildren: './pages/not-found/not-found.module#NotFoundModule'}
        ],
    },
];


@NgModule({
    imports: [
        RouterModule.forRoot(routes, {initialNavigation: 'enabled', enableTracing: false})
    ],
    exports: [RouterModule],
})
export class RoutingModule
{
    constructor(
        private router: Router,
    )
    {
        this.setRoutes();
    }

    setRoutes(): void
    {
        // All pages with language and city segments
        for (const city of CityCodes)
        {
            // catalog
            this.router.config[0].children.unshift(
                {path: `${city}/:catalog/`, data: {type: 'catalog'}, loadChildren: './pages/catalog/catalog.module#CatalogModule'},
                {path: `${city}/en/:catalog/`, data: {type: 'catalog'}, loadChildren: './pages/catalog/catalog.module#CatalogModule'}
            );

            // bouquets
            this.router.config[0].children.unshift(
                {path: `${city}/bukety/`, data: {type: 'bouquets'}, loadChildren: './pages/bouquets/bouquets.module#BouquetsModule'},
                {path: `${city}/en/bouquets/`, data: {type: 'bouquets'}, loadChildren: './pages/bouquets/bouquets.module#BouquetsModule'}
            );

            // bouquet
            this.router.config[0].children.unshift(
                {path: `${city}/bukety`, data: {type: 'bouquet'}, loadChildren: './pages/bouquet/bouquet.module#BouquetModule'},
                {path: `${city}/en/bouquets`, data: {type: 'bouquet'}, loadChildren: './pages/bouquet/bouquet.module#BouquetModule'}
            );

            // order
            this.router.config[0].children.unshift(
                {path: `${city}/order/`, data: {type: 'order'}, loadChildren: './pages/order/order.module#OrderModule'},
                {path: `${city}/en/order/`, data: {type: 'order'}, loadChildren: './pages/order/order.module#OrderModule'},
            );

            // new review
            this.router.config[0].children.unshift(
                {path: `${city}/o`, data: {type: 'add-review'}, loadChildren: './pages/reviews/reviews.module#ReviewsModule'},
                {path: `${city}/en/o/`, data: {type: 'add-review'}, loadChildren: './pages/reviews/reviews.module#ReviewsModule'},
            );

            // thematic
            this.router.config[0].children.unshift(
                {
                    path        : `${city}/populyarnye-cvety/`,
                    data        : {type: 'thematic'},
                    loadChildren: './pages/thematic/thematic.module#ThematicModule'
                },
                {
                    path        : `${city}/en/featured-flowers/`,
                    data        : {type: 'thematic'},
                    loadChildren: './pages/thematic/thematic.module#ThematicModule'
                },
                // {
                //     path: `${city}/cvety-nedorogo/`,
                //     data: {type: 'catalog'}, loadChildren: './pages/thematic/thematic.module#ThematicModule'},
                // {
                //     path        : `${city}/en/flowers-cheap/`,
                //     data        : {type: 'catalog'},
                //     loadChildren: './pages/thematic/thematic.module#ThematicModule'
                // },
                // {path: `${city}/extraordinary/`,
                // data: {type: 'thematic'}, loadChildren: './pages/thematic/thematic.module#ThematicModule'},
                // {
                //     path        : `${city}/en/extraordinary/`,
                //     data        : {type: 'thematic'},
                //     loadChildren: './pages/thematic/thematic.module#ThematicModule'
                // },
                // {path: `${city}/in-box/`, data: {type: 'thematic'}, loadChildren: './pages/thematic/thematic.module#ThematicModule'},
                // {path: `${city}/en/in-box/`, data: {type: 'thematic'}, loadChildren: './pages/thematic/thematic.module#ThematicModule'},
            );

            // sitemap
            this.router.config[0].children.unshift(
                {path: `${city}/sitemap/`, data: {type: 'sitemap'}, loadChildren: './pages/sitemap/sitemap.module#SitemapModule'},
                {path: `${city}/en/sitemap/`, data: {type: 'sitemap'}, loadChildren: './pages/sitemap/sitemap.module#SitemapModule'},
            );

            // front page
            this.router.config[0].children.unshift(
                {path: `${city}/`, data: {type: 'front'}, loadChildren: './pages/front/front.module#FrontModule'},
                {path: `${city}/en/`, data: {type: 'front'}, loadChildren: './pages/front/front.module#FrontModule'}
            );
        }
    }
}


