// angular
import { NgModule } from '@angular/core';
import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
// libs
import { TransferHttpCacheModule } from '@nguniversal/common';
import { REQUEST } from '@nguniversal/express-engine/tokens';

import { AppComponent } from './app.component';
import { AppModule } from './app.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { InlineStyleModule } from './inline-style/inline-style.module';
import { InlineStyleComponent } from './inline-style/inline-style.component';

// the Request object only lives on the server
export function getRequest(): any
{
    return {headers: {cookie: document.cookie}};
}

@NgModule({
    bootstrap: [AppComponent, InlineStyleComponent],
    imports  : [
        BrowserModule.withServerTransition({appId: 'my-app'}),
        TransferHttpCacheModule,
        BrowserTransferStateModule,
        AppModule,
        InlineStyleModule,
        ServiceWorkerModule.register('/ngsw-worker.js', {enabled: false})
    ],
    providers: [
        {
            // The server provides these in main.server
            provide: REQUEST, useFactory: (getRequest)
        },
        {provide: 'ORIGIN_URL', useValue: location.origin}
    ]
})
export class AppBrowserModule
{
}
