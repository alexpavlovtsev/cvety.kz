import { Component, HostListener, Inject, OnInit, Optional, PLATFORM_ID, Renderer2 } from '@angular/core';

import { AppStorage } from '@postgar/storage';
import { PostgarConfigService } from '@postgar/services';
import { PostgarConfig } from '@postgar/types';
import { Message, SocketService } from '@postgar/socket';
import { compareDeep } from '@postgar/utils';

import { debounceTime, distinctUntilChanged, filter, map, take } from 'rxjs/operators';

import { ToastService } from '@shared/components/toast/toast.service';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { SocketWorkerService } from '@shared/services/socket-worker.service';
import { TranslateService } from '@ngx-translate/core';
import { LocationService } from '@shared/services/location.service';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Views } from './postgar-config/views';
import { MicrodataInject } from '../server-helper/microdata/microdata-inject';
import { MicrodataService } from '@shared/services/microdata-service';
import { combineLatest } from 'rxjs';

@Component({
    selector: 'app-root',
    template: `
        <app-progress-bar></app-progress-bar>
        <router-outlet></router-outlet>
        <app-toast></app-toast>
        <!--<schema-sidebar></schema-sidebar>-->
    `
})
export class AppComponent implements OnInit
{
    ioConnection: any;

    /**
     * Constructor
     */
    constructor(
        private _entityManager: EntityManagerService,
        private _translateService: TranslateService,
        private _renderer: Renderer2,
        @Inject(AppStorage) private appStorage: Storage,
        private _postgarConfigService: PostgarConfigService,
        private _socketService: SocketService,
        private _toastService: ToastService,
        private _socketWorker: SocketWorkerService,
        @Inject(PLATFORM_ID) private platformId: Object,
        private locationService: LocationService,
        private _viewsService: ViewsService,
        private microdataService: MicrodataService,
        @Optional() private microdataInject: MicrodataInject,
    )
    {
        // Add languages
        this._translateService.addLangs(['en', 'ru']);

        // Set the default language
        this._translateService.setDefaultLang('ru');

        // Use a language
        this._translateService.use('ru');

        if (isPlatformBrowser(this.platformId))
        {
            // Socket init
            // this.initIoConnection();
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Get current user info
        this._postgarConfigService.config
            .pipe(take(1))
            .subscribe((config: PostgarConfig) =>
            {
                if (this.appStorage.getItem(config.authTokenKey))
                {
                    // GetCurrentUser
                }

                if (isPlatformBrowser(this.platformId))
                {
                    this.basketListen();
                }
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(
                map(node => node.code),
                distinctUntilChanged()
            )
            .subscribe(node =>
            {
                this._translateService.use(node);
            });

        combineLatest(
            this._viewsService.firstNode(Views.Language),
            this._viewsService.firstNode(Views.City)
        ).pipe(
            filter(([lang, city]) => !!lang.code && !!city.code),
            take(1)
        ).subscribe(([lang, city]) =>
        {
            this.setMicrodata(city, lang);
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Transform href to angular friendly
     *
     * @param eventTarget
     * @param button
     * @param ctrlKey
     * @param metaKey
     * @param altKey
     */
    @HostListener('click', ['$event.target', '$event.button', '$event.ctrlKey', '$event.metaKey', '$event.altKey'])
    onClick(eventTarget: any, button: number, ctrlKey: boolean, metaKey: boolean, altKey: boolean): boolean
    {
        // Deal with anchor clicks; climb DOM tree until anchor found (or null)
        let target = eventTarget;
        while (target && !(target instanceof HTMLAnchorElement))
        {
            target = target.parentElement;
        }
        if (target instanceof HTMLAnchorElement)
        {
            return this.locationService.handleAnchorClick(target, button, ctrlKey, metaKey);
        }

        // Allow the click to pass through
        return true;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Listen update orders event
     */
    private initIoConnection(): void
    {
        this._socketService.initSocket();

        this.ioConnection = this._socketService.onMessage()
            .pipe(debounceTime(1000), distinctUntilChanged(compareDeep))
            .subscribe((message: Message) => this._socketWorker.checkPgInfo(message));
    }

    /**
     * Refresh product basket
     *
     * @constructor
     */
    basketListen(): void
    {
        const storageKey = 'basket';

        if (isPlatformBrowser(this.platformId))
        {
            // Rehydrate
            const localeBasketStore = localStorage.getItem(storageKey);
            if (!!localeBasketStore)
            {
                this._entityManager.getService('Basket').addAllToCache(JSON.parse(localeBasketStore));
            }

            // Listen
            this._entityManager.getService('Basket').entities$
                .pipe(map(entities => JSON.stringify(entities)))
                .subscribe(entitiesStr =>
                {
                    const currentState = localStorage.getItem(storageKey);

                    if (entitiesStr !== currentState)
                    {
                        // Update state
                        localStorage.setItem(storageKey, entitiesStr);
                    }
                });
        }
    }

    private setMicrodata(city: any, language: any): void
    {
        if (isPlatformServer(this.platformId))
        {
            const data = this.microdataService.getCode(city, language);
            this.microdataInject.inject(data);
        }
    }
}
