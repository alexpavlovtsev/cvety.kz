import { Component, OnDestroy, OnInit } from '@angular/core';
import { ScriptService } from '@shared/services/script.service';
import { MetatagService } from '@shared/services/metatag.service';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
    selector   : 'app-reviews',
    templateUrl: './reviews.component.html',
    styleUrls  : ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit, OnDestroy
{
    callback: any;

    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _scriptService: ScriptService,
        private _metatagService: MetatagService,
        private _viewsService: ViewsService,
    )
    {
        this.initScripts();

        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                const title = value.id === 1
                    ? 'Отзывы'
                    : 'Reviews';
                this._metatagService.set('page', title);
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    setCallback(callback: any): void
    {
        this.callback = callback;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.load(['jquery.formstyler', 'popper', 'bootstrap']);
        });
    }
}

