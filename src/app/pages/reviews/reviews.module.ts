import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '@shared/shared.module';

import { ReviewsComponent } from './reviews.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { ReviewsContentComponent } from './reviews-content/reviews-content.component';
import { ProgressComponent } from './progress/progress.component';
import { ReviewsService } from './reviews.service';
import { NewReviewComponent } from './new-review/new-review.component';
import { NewReviewService } from './new-review/new-review.service';
import { CompletedSpinnerModule } from '@shared/components/completed-spinner/completed-spinner.module';

const routes: Routes = [
    {
        path     : ':orderNumber/',
        component: NewReviewComponent,
        resolve  : {
            data: NewReviewService
        }
    },
];

@NgModule({
  imports: [
      RouterModule.forChild(routes),

      TranslateModule,

      CompletedSpinnerModule,

      SharedModule,
  ],
  declarations: [
      ReviewsComponent,
      BreadcrumbComponent,
      ReviewsContentComponent,
      ProgressComponent,
      NewReviewComponent
  ],
    providers: [ReviewsService, NewReviewService]
})
export class ReviewsModule { }
