import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { forkJoin, Subject } from 'rxjs';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { takeUntil } from 'rxjs/operators';
import { Callback } from '../callback';
import { Views } from '../../../postgar-config/views';

import { locale as english } from '../i18n/en';
import { locale as russian } from '../i18n/ru';
import { TranslationLoaderService } from '@shared/services/translation-loader.service';
import { ScriptService } from '@shared/services/script.service';
import * as fromStore from '../../../store';
import { Store } from '@ngrx/store';
import { State } from '../../../store';

@Component({
    selector   : 'app-new-review',
    templateUrl: './new-review.component.html',
    styleUrls  : ['./new-review.component.scss']
})
export class NewReviewComponent implements OnInit, OnDestroy
{
    langId: number;
    order: any;
    isLoaded: boolean;
    callback: any;
    disappointed: {};
    content: {};

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _cd: ChangeDetectorRef,
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
        private _translationLoaderService: TranslationLoaderService,
        private _scriptService: ScriptService,
        private _store: Store<State>,
    )
    {
        // Set the defaults
        this._translationLoaderService.loadTranslations(english, russian);

        this.isLoaded = false;
        this.callback = new Callback();
        this.content  = {
            'NOT_FRESH'                 : 'Цветы не свежие',
            'NOT_LIKE_IN_PHOTO'         : 'Букет не как на фото',
            'COURIER_NOT_RIGHT'         : 'Курьер не пунктуальный',
            'COURIER_RUDE'              : 'Курьер нагрубил',
            'WRONG_BOUQUET'             : 'Получил не тот букет',
            'FLORIST_NEEDS_TO_LEARN'    : 'Флористу надо подучиться',
            'SOME_FLOWERS_WILTED'       : 'Некоторые цветы завяли',
            'NOT_EXACTLY_WHAT_I_ORDERED': 'Не совсем то, что заказывал',
            'THE_WHERE_THE_ADDRESS_IS'  : 'Курьер уточнял где адрес',
            'COURIER_LITTLE_LATE'       : 'Курьер чуть опоздал',
            'FRESH_FLOWERS'             : 'Цветы свежие',
            'COURTEOUS_COURIER'         : 'Вежливый курьер',
            'BOUQUET_IS_BETTER'         : 'Букет лучше, чем заказывал',
            'BEAUTIFULLY_DECORATED'     : 'Красиво оформлен',
            'PUNCTUAL'                  : 'Пунктуальный',
            'SUPER_FLORIST'             : 'Супер флорист',
            'FAST_SHIPPING'             : 'Быстрая доставка',
        };
        this.disappointClear();


        // Set the private defaults
        this._unsubscribeAll = new Subject();

        // this.initScripts();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.orderReview)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.order                   = node;
                this.callback.organizationId = 1;
                this.callback.orderId        = node.id;
                this.callback.bouquetId      = node.bouquetIds && node.bouquetIds.length > 0 ? node.bouquetIds[0] : null;
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.langId = node.id;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Add callback
     */
    addCallback(): void
    {
        this.isLoaded = true;
        this._cd.detectChanges();

        this.callback.disappointed = Object.keys(this.disappointed)
            .reduce((list, key) =>
            {
                if (this.disappointed[key])
                {
                    return [
                        ...list,
                        this.content[key]
                    ];
                } else
                {
                    return list;
                }
            }, []);

        const callback = {
            disappointed: this.callback.disappointed,
            rating      : this.callback.rating,
            comment     : this.callback.comment,
            bouquetId   : this.callback.bouquetId,
            orderId     : this.callback.orderId,
            senderEmail : this.order.senderEmail,
            from        : 'sender'
        };

        const callbackOp = this._entityManager.getService('Callback').add(callback);

        const orderOp = this._entityManager.getService('Order')
            .update({
                id            : this.order.id,
                reviewAccepted: true
            });

        forkJoin(callbackOp, orderOp).subscribe(() =>
        {
        }, (error1) =>
        {
            console.log(error1);
        }, () =>
        {
            const langCode = this.langId === 1 ? '' : 'en/';

            setTimeout(() =>
            {
                this._store.dispatch(new fromStore.Go({path: [`/${langCode}`]}));
            }, 2000);
        });
    }

    /**
     * Set rating
     *
     * @param {number} rating
     */
    setRating(rating: number): void
    {
        if (this.callback.rating !== rating)
        {
            this.disappointClear();
            this.callback.rating = rating;
        }
        this._cd.detectChanges();
    }

    /**
     * Change
     *
     * @param {string} key
     */
    disappointChange(key: string): void
    {
        this.disappointed[key] = !this.disappointed[key];
    }

    /**
     * Clear disappoint
     */
    disappointClear(): void
    {
        this.disappointed = Object.keys(this.content).reduce((newObj, key) =>
        {
            return {
                ...newObj,
                [key]: false
            };
        }, {});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        // this._scriptService.loadScript('jquery').then(() =>
        // {
        //     this._scriptService.load(['slick', 'popper', 'jquery.formstyler', 'bootstrap']).then(() =>
        //     {
        //         const settings = scriptData();
        //         this._scriptService.appendScript(settings, 'slick-front-settings');
        //     });
        // });
    }
}

function scriptData(): string
{
    return `
    // filters

(function(){
    $('.js-toggle-content').on('click', function () {
        var _this = $(this),
            text = _this.find('.js-toggle-text'),
            el = _this.prev();
        if (!el.hasClass('activated')) {
            curHeight = el.height();
            el.addClass('activated');
        };
        var autoHeight = el.css('height', 'auto').height(),
            textHide = _this.data('text-hide'),
            textShow = _this.data('text-show');
        if (_this.hasClass('active')) {
            _this.removeClass('active');
            text.text(textShow);
            el.animate({height: curHeight}, 400);
        }
        else {
            _this.addClass('active');
            text.text(textHide);
            el.height(curHeight+20).animate({height: autoHeight}, 400);
        }
        return false;
    });
}());
$('.cv_ratepage-rate input').styler('destroy');
$('.cv_ratepage-right-check input').styler();`;
}
