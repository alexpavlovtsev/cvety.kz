export class Callback
{
    id: number;
    disappointed: string[];
    rating: number;
    comment: string;
    bouquetId: number;
    organizationId: number;
    createdAt: string;
    productName: string;
    orderId: number;

    constructor(data?)
    {
        data                = data || {};
        this.id             = data.id;
        this.disappointed   = data.disappointed || [];
        this.rating         = data.rating || 0;
        this.comment        = data.comment;
        this.bouquetId      = data.bouquetId;
        this.organizationId = data.organizationId;
        this.createdAt      = data.createdAt;
        this.productName    = data.productName;
        this.orderId        = data.orderId;
    }
}
