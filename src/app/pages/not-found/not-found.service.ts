import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { LocationService } from '@shared/services/location.service';
import { Views } from '../../postgar-config/views';
import { QueryOp } from '@postgar/types';
import { take } from 'rxjs/operators';
import { EntityManagerService, ViewsService } from '@postgar/entity';

@Injectable()
export class NotFoundService implements Resolve<any>
{
    cityCode: string;
    langId: number;

    /**
     * Constructor
     */
    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
        private locationService: LocationService
    )
    {
        this._viewsService.firstNode(Views.City)
            .subscribe((node: any) =>
            {
                this.cityCode = node.code;
            });

        this._viewsService.firstNode(Views.Language)
            .subscribe((node: any) =>
            {
                this.langId = node.id;
            });
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.locationService.set404();

        return new Promise((resolve, reject) =>
        {

            Promise.all([
                this.getExtraordinary(),
                this.getFeatured()
            ]).then(
                () =>
                {
                    resolve();
                },
                reject
            );
        });
    }

    getExtraordinary(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.Extraordinary,
                    pageSize: 9,
                    filters : {
                        cityCodes   : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId  : {[QueryOp.equalTo]: this.langId},
                        bouquetPrice: {[QueryOp.greaterThan]: '0'},
                    },
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }

    getFeatured(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.Featured,
                    pageSize: 9,
                    filters : {
                        cityCodes   : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId  : {[QueryOp.equalTo]: this.langId},
                        bouquetPrice: {[QueryOp.greaterThan]: '0'},
                    },
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }
}
