export const locale = {
    lang: 'en',
    data: {
        'FRONT': {
            'TITLE'             : 'This page was not found',
            'DESCRIPTION'       : `You may have incorrectly entered the URL. Or the page has been deleted, its name has
        changed or it is temporarily unavailable.`,
            'LINK'              : 'Go back to main page',
            'EXCLUSIVE_BOUQUETS': 'Exclusive bouquets',
            'POPULAR_BOUQUETS'  : 'Popular Bouquets',
            'FROM'              : 'of',
            'FROM_1'            : 'from',
            'EXCLUSIVE_DESC'    : 'Bouquets That Surprise the Most Capricious',
            'POPULAR_DESC_1'    : 'Bouquets That Take and Leave a Review',
            'POPULAR_MORE'      : 'See all popular bouquets',
        }
    }
};
