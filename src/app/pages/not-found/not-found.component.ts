import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../store/reducers';

import { TranslationLoaderService } from '@shared/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

import { Subject } from 'rxjs';
import { Views } from '../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { ViewsService } from '@postgar/entity';
import { MAIN_CITY_CODE } from '../../config';


@Component({
    selector   : 'app-not-found',
    templateUrl: './not-found.component.html',
    styleUrls  : ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit, OnDestroy
{
    themPrefix: string;
    city: string;
    langId: number;
    featured: any[];
    extraordinary: any[];
    totalFeatured: number;
    prefix: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _viewsService: ViewsService,
        private _store: Store<State>,
        private _translationLoaderService: TranslationLoaderService,
    )
    {
        // Set the defaults
        this._translationLoaderService.loadTranslations(english, russian);
        this.featured      = [];
        this.extraordinary = [];

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.langId     = node.id;
                this.prefix     = this.langId === 1 ? '/bukety/' : '/en/bouquets/';
                this.themPrefix = this.langId === 1 ? '/' : '/en/';
            });

        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.city = node.code === MAIN_CITY_CODE ? '/' : `${node.code}/`;
            });

        this._viewsService.nodes(Views.Featured)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any[]) =>
            {
                this.featured = [...nodes.splice(0, 5)];
            });

        this._viewsService.nodes(Views.Extraordinary)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any[]) =>
            {
                this.extraordinary = [...nodes.splice(0, 5)];
            });

        this._viewsService.view(Views.Featured)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(view =>
            {
                this.totalFeatured = view.totalCount;
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
