import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '@shared/shared.module';

import { NotFoundService } from './not-found.service';
import { NotFoundComponent } from './not-found.component';
import { BouquetTeaserModule } from '../../layout-elements/bouquet-teaser/bouquet-teaser.module';

@NgModule({
    imports     : [
        SharedModule,
        BouquetTeaserModule,

        RouterModule.forChild([
            {
                path     : '',
                component: NotFoundComponent,
                resolve  : {
                    data: NotFoundService
                }
            }
        ]),
    ],
    declarations: [NotFoundComponent],
    providers   : [NotFoundService],
})
export class NotFoundModule
{
}
