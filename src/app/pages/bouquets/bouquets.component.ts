import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { ScriptService } from '@shared/services/script.service';
import { TranslationLoaderService } from '@shared/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';
import { MetatagService } from '@shared/services/metatag.service';
import { Views } from '../../postgar-config/views';
import { take, takeUntil } from 'rxjs/operators';
import { ViewsService } from '@postgar/entity';
import { Subject } from 'rxjs';
import { frontScriptData } from '../front/front.component';

@Component({
    selector   : 'app-bouquets',
    templateUrl: './bouquets.component.html',
    styleUrls  : ['./bouquets.component.scss']
})
export class BouquetsComponent implements AfterViewInit, OnInit, OnDestroy
{
    title: string;

    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _translationLoaderService: TranslationLoaderService,
        private _scriptService: ScriptService,
        private _metatagService: MetatagService,
        private _viewsService: ViewsService,
    )
    {
        this._translationLoaderService.loadTranslations(english, russian);

        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                this.title = value.id === 1
                    ? 'Красивые букеты цветов с доставкой'
                    : 'Beautiful bouquets of flowers with delivery';
                this._metatagService.set('bouquets', this.title);
            });
    }

    ngAfterViewInit(): void
    {
        setTimeout(() =>
        {
            this.initScripts();
        }, 0);
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.loadScript('popper').then(() =>
            {
                this._scriptService.load(['slick', 'jquery.formstyler', 'bootstrap']).then(() =>
                {
                    const settings = frontScriptData();
                    this._scriptService.appendScript(settings, 'front-settings');
                });
            });
        });
    }
}
