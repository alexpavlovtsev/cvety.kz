export const locale = {
    lang: 'ru',
    data: {
        'FRONT': {
            'SEE_ALL'           : 'Смотреть все',
            'MORE'              : 'Смотреть Еще',
            'NOT_EXIST_MESSAGE' : 'Нет товаров для данного города',
            'LOAD_MORE'         : 'загрузить еще',
            'YOU_SHOW'          : 'Вы посмотрели',
            'FROM'              : 'из',
            'VARIANTS'          : 'вариантов',
            'RESET'             : 'Сбросить',
            'FILTERS'           : 'Фильтры',
            'DELIVERY_PHOTOS'   : 'Фотографии доставок',
            'SHOW_ALL'          : 'показать все',
            'REVIEWS'           : 'Отзывы',
            'TITLE'             : 'Доставка и заказ цветов в',
            'SORT'              : 'Сортировать',
            'AT_FIRST_EXPENSIVE': 'сначала дороже',
            'FIRST_CHEAPER'     : 'сначала дешевле',
            'FROM_1'            : 'от',
            'REVIEWS_1'         : 'отзывов',
            'LESS'              : 'меньше'
        }
    }
};
