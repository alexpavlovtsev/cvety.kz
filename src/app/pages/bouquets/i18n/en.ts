export const locale = {
    lang: 'en',
    data: {
        'FRONT': {
            'SEE_ALL'           : 'See all',
            'MORE'              : 'See More',
            'NOT_EXIST_MESSAGE' : 'No products for this city',
            'LOAD_MORE'         : 'Load more',
            'YOU_SHOW'          : 'Have you looked',
            'FROM'              : 'of',
            'VARIANTS'          : 'options',
            'RESET'             : 'Reset',
            'FILTERS'           : 'Filters',
            'DELIVERY_PHOTOS'   : 'Delivery photos',
            'SHOW_ALL'          : 'show all',
            'REVIEWS'           : 'Reviews',
            'TITLE'             : 'Delivery and order of flowers in',
            'SORT'              : 'Sort',
            'AT_FIRST_EXPENSIVE': 'at first expensive',
            'FIRST_CHEAPER'     : 'first cheaper',
            'FROM_1'            : 'from',
            'REVIEWS_1'         : 'reviews',
            'LESS'              : 'less'
        }
    }
};
