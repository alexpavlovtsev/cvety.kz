import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { MAIN_CITY_CODE } from '../../../../config';

@Component({
    selector   : 'app-items',
    templateUrl: './items.component.html',
    styleUrls  : ['./items.component.scss']
})
export class ItemsComponent implements OnInit, OnDestroy
{
    bouquetTr$: Observable<any[]>;
    langId: number;
    prefix: string;
    city: string;
    currency: any;
    language: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param _viewsService
     */
    constructor(
        private _viewsService: ViewsService
    )
    {
        this.bouquetTr$ = this._viewsService.nodes(Views.Bouquets);
        this.currency   = {};
        this.language   = {};

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.langId   = node.id;
                this.prefix   = this.langId === 1 ? '/bukety/' : '/en/bouquets/';
                this.language = node;
            });

        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.city = node.code === MAIN_CITY_CODE ? '/' : `${node.code}/`;
            });

        this._viewsService.firstNode(Views.Currency)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.currency = node;
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    randomRating(id: number): string
    {
        const lastDigit = id.toString().split('').pop();

        return lastDigit === '0' ? '5.0' : `4.${lastDigit}`;
    }
}
