import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { EntityManagerService, View, ViewsService } from '@postgar/entity';
import { Views } from '../../../postgar-config/views';
import { map, switchMap, takeUntil } from 'rxjs/operators';
import { Grammar } from '@shared/grammar';
import { Utils } from '@shared/utils';

@Component({
    selector   : 'app-title',
    templateUrl: './title.component.html',
    styleUrls  : ['./title.component.scss']
})
export class TitleComponent implements OnInit, OnDestroy
{
    langId: number;
    citySecondName: string;
    sortDirection: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(
                switchMap((language: any) =>
                {
                    return this._viewsService.firstNode(Views.City)
                        .pipe(
                            map(value =>
                            {
                                return language.id === 1 ? value.secondName : value.enName;
                            })
                        );
                }),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe(value =>
            {
                this.citySecondName = value;
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.langId = value.id;
            });

        this._viewsService.view(Views.Bouquets)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((view: View) =>
            {
                this.sortDirection = view.sortDirection;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    changeOrderBy(sortDirection: string, event: any): void
    {
        Utils.stopPropagation(event);
        this.sortDirection = sortDirection;

        this._entityManager
            .getService('BouquetTranslate')
            .getWithQuery({
                id: Views.Bouquets,
                sortDirection,
            });
    }
}
