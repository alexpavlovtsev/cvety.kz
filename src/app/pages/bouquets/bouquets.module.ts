import { NgModule } from '@angular/core';
import { BouquetsComponent } from './bouquets.component';
import { BouquetsService } from './bouquets.service';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { TitleComponent } from './title/title.component';
import { ProgressComponent } from './progress/progress.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { ContentComponent } from './content/content.component';
import { FiltersComponent } from './content/filters/filters.component';
import { ItemsComponent } from './content/items/items.component';
import { DeliveryBlockModule } from '../../layout-elements/delivery-block/delivery-block.module';
import { BouquetTeaserModule } from '../../layout-elements/bouquet-teaser/bouquet-teaser.module';
import { BouquetFilterModule } from '../../layout-elements/bouquet-filter/bouquet-filter.module';

const routes: Routes = [
    {
        path     : '',
        component: BouquetsComponent,
        resolve  : {
            data: BouquetsService
        }
    },
];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),

        SharedModule,
        DeliveryBlockModule,

        BouquetFilterModule,
        BouquetTeaserModule,
    ],
    declarations: [
        BouquetsComponent,
        TitleComponent,
        ProgressComponent,
        FeedbackComponent,
        DeliveryComponent,
        ContentComponent,
        FiltersComponent,
        ItemsComponent
    ],
    providers   : [BouquetsService]
})
export class BouquetsModule
{
}
