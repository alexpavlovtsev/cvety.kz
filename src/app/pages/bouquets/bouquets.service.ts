import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { EntityManagerService, ViewsService } from '@postgar/entity';
import { filter, take, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Views } from '../../postgar-config/views';
import { QueryOp } from '@postgar/types';
import { BouquetCounterService } from '@shared/services/bouquet-counter.service';

@Injectable()
export class BouquetsService implements Resolve<any>
{
    cityCode: string;
    langId: number;

    /**
     * Constructor
     */
    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
        private _bouquetCounterService: BouquetCounterService
    )
    {
        this._viewsService.firstNode(Views.City)
            .subscribe((node: any) =>
            {
                this.cityCode = node.code;
            });

        this._viewsService.firstNode(Views.Language)
            .subscribe((node: any) =>
            {
                this.langId = node.id;
            });
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        // this._bouquetCounterService.run('');
        return new Promise((resolve, reject) =>
        {
            Promise.all([
                this.getFilterTrs(),
                this.getFilterItemTrs(),
                this.getBouquets(),
            ]).then(
                () =>
                {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get filter translates
     */
    getFilterTrs(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager.getService('FilterTranslate').loaded$
                .pipe(
                    tap(loaded =>
                    {
                        if (!loaded)
                        {
                            this._entityManager.getService('FilterTranslate').getAll();
                        }
                    }),
                    filter(loaded => loaded),
                    take(1)
                )
                .subscribe(value =>
                {
                    resolve(value);
                }, reject);
        });
    }

    /**
     * Get filter item translates
     */
    getFilterItemTrs(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager.getService('FilterItemTranslate').loaded$
                .pipe(
                    tap(loaded =>
                    {
                        if (!loaded)
                        {
                            this._entityManager.getService('FilterItemTranslate').getAll();
                        }
                    }),
                    filter(loaded => loaded),
                    take(1)
                )
                .subscribe(value =>
                {
                    resolve(value);
                }, reject);
        });
    }

    /**
     * Get bouquets
     */
    getBouquets(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.Bouquets,
                    pageSize: 9,
                    filters : {
                        cityCodes   : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId  : {[QueryOp.equalTo]: this.langId},
                        bouquetPrice: {[QueryOp.greaterThan]: '0'}
                    },
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }
}
