import { NgModule } from '@angular/core';
import { SitemapComponent } from './sitemap.component';
import { RouterModule } from '@angular/router';
import { SitemapService } from './sitemap.service';
import { SharedModule } from '@shared/shared.module';

@NgModule({
    imports     : [
        SharedModule,

        RouterModule.forChild([
            {
                path     : '',
                component: SitemapComponent,
                resolve  : {
                    data: SitemapService
                }
            }
        ]),
    ],
    declarations: [SitemapComponent],
    providers   : [SitemapService]
})
export class SitemapModule
{
}
