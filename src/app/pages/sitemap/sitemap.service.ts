import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { filter, take, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class SitemapService implements Resolve<any>
{
    /**
     * Constructor
     */
    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService
    )
    {
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) =>
        {

            Promise.all([
                this.getFilterTrs(),
                this.getFilterItemTrs(),
            ]).then(
                () =>
                {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get filter translates
     */
    getFilterTrs(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager.getService('FilterTranslate').loaded$
                .pipe(
                    tap(loaded =>
                    {
                        if (!loaded)
                        {
                            this._entityManager.getService('FilterTranslate').getAll();
                        }
                    }),
                    filter(loaded => loaded),
                    take(1)
                )
                .subscribe(value =>
                {
                    resolve(value);
                }, reject);
        });
    }

    /**
     * Get filter item translates
     */
    getFilterItemTrs(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager.getService('FilterItemTranslate').loaded$
                .pipe(
                    tap(loaded =>
                    {
                        if (!loaded)
                        {
                            this._entityManager.getService('FilterItemTranslate').getAll();
                        }
                    }),
                    filter(loaded => loaded),
                    take(1)
                )
                .subscribe(value =>
                {
                    resolve(value);
                }, reject);
        });
    }
}
