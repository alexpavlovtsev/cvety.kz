import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../store/reducers';
import { Subject } from 'rxjs';
import * as fromStore from '../../store';
import { LocationService } from '@shared/services/location.service';
import { fuseAnimations } from '@shared/animations';
import { Utils } from '@shared/utils';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { map, switchMap, take, takeUntil } from 'rxjs/operators';
import { Views } from '../../postgar-config/views';
import { FilterHelperService } from '@shared/services/filter-helper.service';
import { ScriptService } from '@shared/services/script.service';
import { MetatagService } from '@shared/services/metatag.service';
import { FilterInfoService, filterItemConvert } from '@shared/services/filter-info.service';
import { MAIN_CITY_CODE } from '../../config';
import { frontScriptData } from '../front/front.component';

@Component({
    selector   : 'app-sitemap',
    templateUrl: './sitemap.component.html',
    styleUrls  : ['./sitemap.component.scss'],
    animations : fuseAnimations
})
export class SitemapComponent implements OnInit, OnDestroy, AfterViewInit
{
    languageId: any;
    counts: object;
    nodes: any[];
    city: any;
    cityName: string;
    isMain: boolean;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _store: Store<State>,
        private _entityManager: EntityManagerService,
        public locationService: LocationService,
        private _viewsService: ViewsService,
        private _filterHelper: FilterHelperService,
        private _scriptService: ScriptService,
        private _metatagService: MetatagService,
    )
    {
        this.nodes  = [];
        this.counts = {};

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                this.city   = value;
                this.isMain = value.code === MAIN_CITY_CODE;
            });

        this._entityManager.getService('FilterItemTranslate').entities$
            .pipe(
                take(1),
                switchMap((entities: any[]) =>
                {
                    return this._viewsService.firstNode(Views.Language)
                        .pipe(
                            map((language: any) =>
                            {
                                entities = entities.filter(node => node.languageId === language.id);

                                return [entities, language];
                            })
                        );
                }),
            )
            .subscribe(([entities, language]) =>
            {
                this.nodes = entities
                    .map(node =>
                    {
                        const filterItem = filterItemConvert(node);

                        node.url = FilterInfoService.fullData([filterItem], language, this.city).url;
                        return node;
                    })
                    .filter(node =>
                    {
                        const value = filterItemConvert(node);

                        const depFilters     = {'25': true, '3': true};
                        const depFilterItems = {'1619': true, '84': true, '85': true, '157': true, '159': true};

                        return !depFilterItems[value.filterItemId] && !depFilters[value.filterId];
                    });
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                this.languageId = value.id;

                if (value.id === 1)
                {
                    this.cityName = this.city.name;

                    if (this.isMain)
                    {
                        this._metatagService.set('page', `Карта сайта`);
                    } else
                    {
                        this._metatagService.set('page', `Карта сайта (${this.city.name})`);
                    }
                } else
                {
                    this.cityName = this.city.enName;

                    if (this.isMain)
                    {
                        this._metatagService.set('page', `Sitemap`);
                    } else
                    {
                        this._metatagService.set('page', `Sitemap (${this.city.enName})`);
                    }
                }
            });
    }

    ngAfterViewInit(): void
    {
        setTimeout(() =>
        {
            this.initScripts();
        }, 0);
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Go to
     *
     * @param path
     * @param event
     */
    goTo(path: string, event: any): void
    {
        Utils.stopPropagation(event);
        this._store.dispatch(new fromStore.Go({path: [path]}));
    }

    /**
     * Has own property
     *
     * @param filterItemId
     */
    hasOwnPropertyInCount(filterItemId: number): boolean
    {
        return this.counts.hasOwnProperty(filterItemId);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.loadScript('popper').then(() =>
            {
                this._scriptService.load(['slick', 'jquery.formstyler', 'bootstrap']).then(() =>
                {
                    const settings = frontScriptData();
                    this._scriptService.appendScript(settings, 'front-settings');
                });
            });
        });
    }
}
