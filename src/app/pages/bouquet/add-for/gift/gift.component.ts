import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { GiftVariant } from '../../model/gift-variant';
import { takeUntil } from 'rxjs/operators';
import { Views } from '../../../../postgar-config/views';

@Component({
    selector   : 'app-gift',
    templateUrl: './gift.component.html',
    styleUrls  : ['./gift.component.scss']
})
export class GiftComponent implements OnInit, OnChanges, OnDestroy
{
    @Input() gift: any;

    variants: GiftVariant[];
    variant: GiftVariant;
    added: boolean;
    currency: any;
    language: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
    )
    {
        this.currency        = {};
        this.language        = {};
        this.gift            = {};
        this.variants        = [];
        this.variant         = new GiftVariant();
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        this._entityManager.getService('Basket').entityMap$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(entityMap =>
            {
                this.added = entityMap.hasOwnProperty(`${this.gift.giftId}-gift`);
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.language = node;
            });

        this._viewsService.firstNode(Views.Currency)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.currency = node;
            });
    }

    ngOnChanges(): void
    {
        if (this.gift)
        {
            this.setModel(this.gift);
        }
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    setModel(gift: any): void
    {
        this.variants = typeof gift.giftByGiftId.variants === 'string'
            ? JSON.parse(gift.giftByGiftId.variants)
            : gift.giftByGiftId.variants || [];

        const def = this.variants.find(value => value.isDefault);

        if (def !== undefined)
        {
            this.variant = new GiftVariant(def);
        }
    }

    selectVariant(variant): void
    {
        this.variant = new GiftVariant(variant);
    }

    addToOrder(): void
    {
        this.variant.giftId   = this.gift.giftId;
        this.variant.giftName = this.gift.name;

        this._entityManager
            .getService('Basket')
            .upsertOneInCache({
                id  : `${this.gift.giftId}-gift`,
                ...this.variant,
                name: this.gift.name
            });
    }
}
