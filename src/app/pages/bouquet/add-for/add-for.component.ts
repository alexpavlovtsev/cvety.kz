import { Component } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Views } from '../../../postgar-config/views';
import { map, switchMap } from 'rxjs/operators';

@Component({
    selector   : 'app-add-for',
    templateUrl: './add-for.component.html',
    styleUrls  : ['./add-for.component.scss']
})
export class AddForComponent
{
    adds$: Observable<any[]>;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
    )
    {
        this.adds$ = this._viewsService.firstNode(Views.Language)
            .pipe(
                switchMap((language: any) =>
                {
                    return this._entityManager.getService('GiftTranslate').entities$
                        .pipe(
                            map((entities: any[]) =>
                            {
                                return entities.filter(entity => entity.languageId === language.id)
                                    .filter(entity => entity.giftByGiftId.enabled);
                            })
                        );
                })
            );

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
}
