import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '@shared/shared.module';

import { BouquetComponent } from './bouquet.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { ItemCardComponent } from './item-card/item-card.component';
import { ItemCardSliderComponent } from './item-card/item-card-slider/item-card-slider.component';
import { ItemCardRatingComponent } from './item-card/item-card-rating/item-card-rating.component';
import { ItemCardSizeComponent } from './item-card/item-card-size/item-card-size.component';
import { ItemCardAdditionalComponent } from './item-card/item-card-additional/item-card-additional.component';
import { AddForComponent } from './add-for/add-for.component';
import { FeedbackListComponent } from './feedback-list/feedback-list.component';
import { DeliveredComponent } from './delivered/delivered.component';
import { BouquetService } from './bouquet.service';
import { GiftComponent } from './add-for/gift/gift.component';
import { FullPriceComponent } from './item-card/full-price/full-price.component';
import { DeliveryBlockModule } from '../../layout-elements/delivery-block/delivery-block.module';
import { ReviewsComponent } from './reviews/reviews.component';

const routes: Routes = [
    {
        path     : ':bouquet/',
        component: BouquetComponent,
        resolve  : {
            data: BouquetService
        }
    },
];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),

        SharedModule,
        DeliveryBlockModule,
    ],
    declarations: [
        BouquetComponent,
        BreadcrumbComponent,
        ItemCardComponent,
        ItemCardSliderComponent,
        ItemCardRatingComponent,
        ItemCardSizeComponent,
        ItemCardAdditionalComponent,
        AddForComponent,
        FeedbackListComponent,
        DeliveredComponent,
        GiftComponent,
        FullPriceComponent,
        ReviewsComponent
    ],
    providers: [BouquetService]
})
export class BouquetModule
{
}
