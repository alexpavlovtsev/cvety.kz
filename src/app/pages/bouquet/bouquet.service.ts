import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { filter, map, take, tap } from 'rxjs/operators';
import { QueryOp } from '@postgar/types';
import { LocationService } from '@shared/services/location.service';
import { Views } from '../../postgar-config/views';
import { BouquetVariant } from './model/bouquet-variant';

@Injectable()
export class BouquetService implements Resolve<any>
{
    selectedVariant: BehaviorSubject<any>;
    params: any;

    constructor(
        private _entityManager: EntityManagerService,
        private _locationService: LocationService,
        private _viewsService: ViewsService,
    )
    {
        this.selectedVariant = new BehaviorSubject<any>({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.params = route.params;

        return new Promise((resolve, reject) =>
        {
            Promise.all([
                this.getBouquet(),
                this.getGifts(),
            ]).then(
                () =>
                {
                    resolve();
                },
                reject
            );
        });
    }

    getBouquet(): Promise<any>
    {
        return new Promise<any>((resolve) =>
        {
            const url = this.params['bouquet'];

            this._entityManager.getService('BouquetTranslate').entities$
                .pipe(
                    take(1),
                    map(entities => entities.find((node: any) => node.url === url)),
                    take(1)
                )
                .subscribe(node =>
                {
                    if (node !== undefined)
                    {
                        resolve();
                        this.setBaseVariant(node);
                        this._viewsService.setNodesToCache([node], Views.Bouquet, 'BouquetTranslate');
                        return;
                    }
                    else
                    {
                        this._entityManager
                            .getService('BouquetTranslate')
                            .getWithQuery({
                                id     : Views.Bouquet,
                                filters: {
                                    url: {[QueryOp.equalTo]: url}
                                }
                            })
                            .pipe(take(1))
                            .subscribe((nodes: any[]) =>
                            {
                                if (nodes.length > 0)
                                {
                                    this._viewsService.setNodesToCache(nodes, Views.MinPrice, 'BouquetTranslate');

                                    this.setBaseVariant(nodes[0]);
                                    resolve();
                                    return;
                                }
                                else
                                {
                                    this._locationService.goTo404();
                                    resolve();
                                }
                            });
                    }
                });
        });
    }

    private setBaseVariant(value: any): void
    {
        const variants = typeof value.bouquetByBouquetId.variants === 'string'
            ? JSON.parse(value.bouquetByBouquetId.variants)
            : value.bouquetByBouquetId.variants || [];

        const def     = variants.find(value1 => value1.isDefault);
        const variant = def === undefined ? new BouquetVariant() : new BouquetVariant(def);

        this.selectedVariant.next(new BouquetVariant({
            isDefault: true,
            mainImage: value.bouquetByBouquetId.mainImage,
            images   : variant.images,
            price    : value.bouquetByBouquetId.mainPrice,
            quantity : variant.quantity
        }));
    }

    getGifts(): Promise<any>
    {
        return new Promise<any>((resolve, reject) =>
        {
            this._entityManager.getService('GiftTranslate').loaded$
                .pipe(
                    tap(loaded =>
                    {
                        if (!loaded)
                        {
                            this._entityManager
                                .getService('GiftTranslate')
                                .getAll();
                        }
                    }),
                    filter(loaded => loaded),
                    take(1)
                ).subscribe(value =>
            {
                resolve(value);
            }, reject);
        });
    }
}
