import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Views } from '../../../postgar-config/views';
import { take, takeUntil } from 'rxjs/operators';
import { BouquetService } from '../bouquet.service';
import { BouquetVariant } from '../model/bouquet-variant';
import * as fromStore from '../../../store';
import { Store } from '@ngrx/store';
import { State } from '../../../store';
import { MetatagService } from '@shared/services/metatag.service';
import * as moment from 'moment';
import { Grammar } from '@shared/grammar';
import { MAIN_CITY_CODE } from '../../../config';

@Component({
    selector   : 'app-item-card',
    templateUrl: './item-card.component.html',
    styleUrls  : ['./item-card.component.scss']
})
export class ItemCardComponent implements OnInit, OnDestroy
{
    bouquet: any;
    langId: number;
    variant: BouquetVariant;
    freshDay: number;
    deliveryDay: string;
    deliveryTime: string;
    title: string;
    titleSuffix: string;
    cityCode: string;
    priceValidUntil: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _store: Store<State>,
        private _bouquetService: BouquetService,
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
        private _metatagService: MetatagService,
    )
    {
        this.bouquet = {};
        this.variant = new BouquetVariant();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.deliveryTime = moment().add(3, 'hours').format('HH:mm');

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.langId = node.id;

                this.dateSet(node.code);
            });

        this._viewsService.firstNode(Views.Bouquet)
            .pipe(take(1), takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                this.bouquet = value;
                this.title   = Grammar.upperCaseFirstLetter(value.name);
                this._metatagService.set('bouquet', value.name);
            });

        this._bouquetService.selectedVariant
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.variant = value;

                setTimeout(() =>
                {
                    this._entityManager
                        .getService('Basket')
                        .upsertOneInCache({
                            id  : `${this.bouquet.bouquetId}-bouquet`,
                            name: this.bouquet.name,
                            ...this.variant,
                        });
                });
            });

        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.cityCode = node.code === MAIN_CITY_CODE ? '/' : `${node.code}/`;

                if (this.langId === 1)
                {
                    this.titleSuffix = 'в ' + node.secondName;
                } else
                {
                    this.titleSuffix = 'in ' + node.enName;
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    checkout(): void
    {
        const langCode = this.langId === 1 ? '' : 'en';
        this._store.dispatch(new fromStore.Go({path: [`/${langCode}${this.cityCode}order/`]}));
    }

    dateSet(localeCode: string): void
    {
        this.deliveryDay = moment().locale(localeCode).format('DD MMMM');
        this.freshDay    = 7;
        const dow        = moment().day();

        switch (dow)
        {
            case 1:
                this.freshDay    = this.freshDay - 2;
                this.deliveryDay = moment().locale(localeCode).subtract(2, 'day').format('DD MMMM');
                break;

            case 2:
                this.freshDay    = this.freshDay - 3;
                this.deliveryDay = moment().locale(localeCode).subtract(3, 'day').format('DD MMMM');
                break;

            case 3:
                break;

            case 4:
                this.freshDay    = this.freshDay - 1;
                this.deliveryDay = moment().locale(localeCode).subtract(1, 'day').format('DD MMMM');
                break;

            case 5:
                this.freshDay    = this.freshDay - 2;
                this.deliveryDay = moment().locale(localeCode).subtract(2, 'day').format('DD MMMM');
                break;

            case 6:
                break;

            case 7:
                this.freshDay    = this.freshDay - 1;
                this.deliveryDay = moment().locale(localeCode).subtract(1, 'day').format('DD MMMM');
                break;
        }
    }
}
