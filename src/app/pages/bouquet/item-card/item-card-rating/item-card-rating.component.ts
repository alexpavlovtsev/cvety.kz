import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../../../postgar-config/views';
import { take, takeUntil } from 'rxjs/operators';

@Component({
    selector   : 'app-item-card-rating',
    templateUrl: './item-card-rating.component.html',
    styleUrls  : ['./item-card-rating.component.scss']
})
export class ItemCardRatingComponent implements OnInit, OnDestroy
{
    reviewCount: number;
    languageId: number;
    reviewWord: string;
    bouquetId: number;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService,
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                this.languageId = value.id;
                this.setWord();
            });

        this._viewsService.firstNode(Views.Bouquet)
            .pipe(take(1), takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                this.bouquetId   = value.id;
                this.reviewCount = !!value.bouquetByBouquetId
                    ? value.bouquetByBouquetId.reviewCount
                    : 0;
                this.setWord();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    setWord(): void
    {
        if (!this.languageId || !this.reviewCount)
        {
            return;
        }
        if (this.languageId === 2)
        {
            this.reviewWord = 'reviews';
            return;
        }

        switch (true)
        {
            case this.reviewCount === 1:
                this.reviewWord = 'отзыв';
                break;

            case this.reviewCount > 1 && this.reviewCount < 5:
                this.reviewWord = 'отзыва';
                break;

            case this.reviewCount > 4:
                this.reviewWord = 'отзывов';
                break;
        }
    }

    randomRating(): string
    {
        const lastDigit = (this.bouquetId || 0).toString().split('').pop();

        return lastDigit === '0' ? '5.0' : `4.${lastDigit}`;
    }
}
