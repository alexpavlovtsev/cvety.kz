import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { Views } from '../../../../postgar-config/views';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Component({
    selector   : 'app-full-price',
    templateUrl: './full-price.component.html',
    styleUrls  : ['./full-price.component.scss']
})
export class FullPriceComponent implements OnInit, OnDestroy
{
    fullPrice: number;
    currency: any;
    language: any;
    priceValidUntil: string;
    fullUrl: string;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _cd: ChangeDetectorRef,
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
        private _router: Router,
    )
    {
        this.currency        = {};
        this.language        = {};
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this.priceValidUntil = moment().add(30, 'days').format('YYYY-MM-DD');
        this.fullUrl         = this._router.url;

        this._entityManager.getService('Basket').entities$
            .pipe(
                map((entities: any[]) =>
                {
                    const basketPrice: number = entities.reduce((fullPrice, item) =>
                    {
                        return parseFloat(fullPrice) + parseFloat(item.price);
                    }, 0);

                    return parseFloat(`${basketPrice}`);
                }),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe(value =>
            {
                this.fullPrice = value;
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.language = node;
            });

        this._viewsService.firstNode(Views.Currency)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.currency = node;
            });

        // this._bouquetService.selectedVariant
        //     .pipe(
        //         switchMap((variant: BouquetVariant) =>
        //         {
        //             return this._entityManager.getService('Basket').entities$
        //                 .pipe(
        //                     map((entities: any[]) =>
        //                     {
        //                         if (entities.length === 0)
        //                         {
        //                             return parseFloat(`${variant.price}`);
        //                         }
        //
        //                         const basketPrice: number = entities.reduce((fullPrice, item) =>
        //                         {
        //                             return parseFloat(fullPrice) + parseFloat(item.price);
        //                         }, 0);
        //
        //                         return parseFloat(`${basketPrice}`) + parseFloat(`${variant.price}`);
        //                     })
        //                 );
        //         }),
        //         takeUntil(this._unsubscribeAll)
        //     )
        //     .subscribe(value =>
        //     {
        //         this.fullPrice = value;
        //     });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
