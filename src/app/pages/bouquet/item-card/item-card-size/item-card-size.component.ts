import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../../../postgar-config/views';
import { take, takeUntil } from 'rxjs/operators';
import { BouquetVariant } from '../../model/bouquet-variant';
import { BouquetService } from '../../bouquet.service';

@Component({
    selector   : 'app-item-card-size',
    templateUrl: './item-card-size.component.html',
    styleUrls  : ['./item-card-size.component.scss']
})
export class ItemCardSizeComponent implements OnInit, OnDestroy
{
    name: string;
    variants: BouquetVariant[];
    variant: BouquetVariant;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _bouquetService: BouquetService,
        private _viewsService: ViewsService,
    )
    {
        this.variants = [];
        this.variant  = new BouquetVariant();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngOnInit()
    {
        this._viewsService.firstNode(Views.Bouquet)
            .pipe(take(1), takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                this.name     = value.name;
                this.variants = typeof value.bouquetByBouquetId.variants === 'string'
                    ? JSON.parse(value.bouquetByBouquetId.variants)
                    : value.bouquetByBouquetId.variants || [];
            });

        this._bouquetService.selectedVariant
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.variant = value;
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    selectVariant(variant: BouquetVariant): void
    {
        this._bouquetService.selectedVariant.next(new BouquetVariant(variant));
    }
}
