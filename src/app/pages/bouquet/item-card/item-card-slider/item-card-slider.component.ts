import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../../../postgar-config/views';
import { take, takeUntil } from 'rxjs/operators';
import { BouquetVariant } from '../../model/bouquet-variant';
import { BouquetService } from '../../bouquet.service';
import { Grammar } from '@shared/grammar';

@Component({
    selector   : 'app-item-card-slider',
    templateUrl: './item-card-slider.component.html',
    styleUrls  : ['./item-card-slider.component.scss']
})
export class ItemCardSliderComponent implements OnInit, OnDestroy
{
    city: any;
    variant: BouquetVariant;
    name: string;
    selectedImage: string;
    alt: any;
    title: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _bouquetService: BouquetService,
        private _viewsService: ViewsService,
    )
    {
        this.variant = new BouquetVariant();
        this.city    = {};
        this.alt     = {0: null, 1: null, 2: null};
        this.title   = {0: null, 1: null, 2: null};

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.city = node;
            });

        this._bouquetService.selectedVariant
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.variant       = value;
                this.selectedImage = this.variant.mainImage;
            });

        this._viewsService.firstNode(Views.Bouquet)
            .pipe(take(1), takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                this.name = value.languageId === 2
                    ? Grammar.validationTitle(value.name)
                    : Grammar.upperCaseFirstLetter(value.name);

                const price = value.bouquetByBouquetId.mainPrice;

                this._viewsService.firstNode(Views.Language)
                    .pipe(takeUntil(this._unsubscribeAll))
                    .subscribe((node: any) =>
                    {
                        switch (node.id)
                        {
                            case 1:
                                this.alt   = {
                                    0: Grammar.upperCaseFirstLetter(`${this.name}: доставка цветов в ${this.getCityLabel(node.id)}`),
                                    1: Grammar.upperCaseFirstLetter(`${this.name}: заказ цветов онлайн`),
                                    2: Grammar.upperCaseFirstLetter(`${this.name}: цена ${price} тг`)
                                };
                                this.title = {
                                    0: Grammar.upperCaseFirstLetter(`${this.name}`),
                                    1: Grammar.upperCaseFirstLetter(`${this.name} - 2`),
                                    2: Grammar.upperCaseFirstLetter(`${this.name} - 3`)
                                };
                                break;

                            case 2:
                                this.alt   = {
                                    0: Grammar.upperCaseFirstLetter(`${this.name}: Flowers Delivery in ${this.getCityLabel(node.id)}`),
                                    1: Grammar.upperCaseFirstLetter(`${this.name}: Order Fresh Flowers Online`),
                                    2: Grammar.upperCaseFirstLetter(`${this.name}: price ${price} KZT`)
                                };
                                this.title = {
                                    0: Grammar.upperCaseFirstLetter(`${this.name}`),
                                    1: Grammar.upperCaseFirstLetter(`${this.name} - 2`),
                                    2: Grammar.upperCaseFirstLetter(`${this.name} - 3`)
                                };
                                break;
                        }
                    });
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    selectImage(img: string): void
    {
        this.selectedImage = img;
    }

    getCityLabel(langId: number): void
    {
        return langId === 1 ? this.city.secondName : this.city.enName;
    }
}
