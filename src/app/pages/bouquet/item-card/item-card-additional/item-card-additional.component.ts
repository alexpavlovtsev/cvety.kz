import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Views } from '../../../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector   : 'app-item-card-additional',
    templateUrl: './item-card-additional.component.html',
    styleUrls  : ['./item-card-additional.component.scss']
})
export class ItemCardAdditionalComponent implements OnInit, OnDestroy
{
    @Input() bouquetId: number;

    makeInBox: boolean;
    addBoxRaffaello: boolean;

    inBoxPrice: number;
    rafaelloPrice: number;

    currency: any;
    language: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
    )
    {
        this.makeInBox       = false;
        this.addBoxRaffaello = false;
        this.inBoxPrice      = 3000;
        this.rafaelloPrice   = 2400;

        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.language = node;
            });

        this._viewsService.firstNode(Views.Currency)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.currency = node;
            });
    }

    ngOnDestroy(): void
    {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    makeInBoxClick(): void
    {
        this.makeInBox = !this.makeInBox;
        const id       = `${this.bouquetId}_MAKE_IN_BOX`;
        const item     = {
            id,
            name : 'MAKE_IN_BOX',
            price: 3000
        };

        if (this.makeInBox)
        {
            this._entityManager.getService('Basket').upsertOneInCache(item);
        }
        else
        {
            this._entityManager.getService('Basket').removeOneFromCache({id});
        }
    }

    addBoxRaffaelloClick(): void
    {
        this.addBoxRaffaello = !this.addBoxRaffaello;

        const id = `${this.bouquetId}_ADD_BOX_RAFFAELLO`;

        if (this.addBoxRaffaello)
        {
            this._entityManager.getService('Basket').upsertOneInCache({
                id,
                name : 'ADD_BOX_RAFFAELLO',
                price: 2400
            });
        }
        else
        {
            this._entityManager.getService('Basket').removeOneFromCache({id});
        }
    }
}
