import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { ScriptService } from '@shared/services/script.service';
import { TranslationLoaderService } from '@shared/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';
import { EntityManagerService } from '@postgar/entity';
import { Subject } from 'rxjs';

@Component({
    selector   : 'app-bouquet',
    templateUrl: './bouquet.component.html',
    styleUrls  : ['./bouquet.component.scss']
})
export class BouquetComponent implements AfterViewInit, OnInit, OnDestroy
{
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _translationLoaderService: TranslationLoaderService,
        private _scriptService: ScriptService,
        private _entityManager: EntityManagerService
    )
    {
        this._translationLoaderService.loadTranslations(english, russian);

        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        this.resetBasket();
    }

    ngAfterViewInit(): void
    {
        setTimeout(() =>
        {
            this.initScripts();
        }, 0);
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.loadScript('popper').then(() =>
            {
                this._scriptService.load(['slick', 'jquery.formstyler', 'bootstrap']).then(() =>
                {
                    const settings = scriptData();
                    this._scriptService.appendScript(settings, 'bouquet-settings');
                });
            });
        });
    }

    resetBasket(): void
    {
        this._entityManager.getService('Basket').clearCache();
    }
}

function scriptData(): string
{
    return `
      $('.cv_addfor-carousel').slick({
      dots: true,
      infinite: true,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
      {
      breakpoint: 1024,
      settings: {
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: true,
      dots: false
      }
      },
      {
      breakpoint: 769,
      settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      dots: false
      }
      },
      {
      breakpoint: 481,
      settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      dots: false
      }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
      ]
      });
      $('.cv_feedback-carousel').slick({
      dots: true,
      infinite: true,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 2,
      responsive: [
      {
      breakpoint: 769,
      settings: {
      slidesToShow: 1,
      slidesToScroll: 2,
      infinite: true,
      dots: false
      }
      },
      {
      breakpoint: 481,
      settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      dots: false
      }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
      ]
      });
      $('.cv_itemcard-slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.cv_itemcard-slider'
      });
      $('.cv_itemcard-slider').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      asNavFor: '.cv_itemcard-slider-for',
      dots: false,
      centerMode: false,
      focusOnSelect: true,
      arrows: false,
      variableWidth: true
      });
        $(".cv_itemcard-color input").styler();
        $(".cv_itemcard-size input").styler();
        $(".cv_itemcard-addit input").styler();
  `;
}
