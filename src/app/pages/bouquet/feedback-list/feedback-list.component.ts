import { Component, OnDestroy, OnInit } from '@angular/core';
import { Views } from '../../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { ViewsService } from '@postgar/entity';
import { Subject } from 'rxjs';
import { LocationService } from '@shared/services/location.service';

@Component({
    selector   : 'app-feedback-list',
    templateUrl: './feedback-list.component.html',
    styleUrls  : ['./feedback-list.component.scss']
})
export class FeedbackListComponent implements OnInit, OnDestroy
{
    langCode: string;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService,
    )
    {
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.langCode = value.code;
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    url(path: string): string
    {
        const code = this.langCode === 'ru' ? '' : 'en';
        return LocationService.validFullUrl('/' + code + '/' + path);
    }
}
