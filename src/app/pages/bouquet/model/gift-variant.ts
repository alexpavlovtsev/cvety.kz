export class GiftVariant
{
    price: number;
    images: string[];
    mainImage: string;
    quantity: number;
    isDefault: boolean;
    giftId?: number;
    giftName?: string;

    constructor(data?)
    {
        data           = data || {};
        this.price     = data.price;
        this.images    = data.images || [];
        this.mainImage = data.mainImage;
        this.quantity  = data.quantity;
        this.isDefault = data.isDefault;
    }
}
