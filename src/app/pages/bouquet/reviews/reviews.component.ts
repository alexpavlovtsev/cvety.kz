import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Views } from '../../../postgar-config/views';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ViewsService } from '../../../../@postgar/entity';
import { GraphQLClient } from '../../../../@postgar/graphql';

@Component({
    selector   : 'app-reviews',
    templateUrl: './reviews.component.html',
    styleUrls  : ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit
{
    bouquetId: number;
    nodes: any[] = [];

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _client: GraphQLClient,
        private _viewsService: ViewsService,
    )
    {
    }

    ngOnInit()
    {
        this._viewsService.firstNode(Views.Bouquet)
            .pipe(take(1), takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                this.bouquetId = value.id;
            });
    }

    getDate(node: any): string
    {
        return moment(node.orderByOrderId.deliveryDate).format('DD.MM.YYYY');
    }

    getBouquetName(node: any): string
    {
        return node.orderByOrderId.jsonProductName
            .split('\\n')
            .join(', ');
    }

    loadReviews(bouquetId: number): void
    {
        this._client.get(``, {}).subscribe(value =>
        {

        });
    }
}
