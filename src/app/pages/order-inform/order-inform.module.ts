import { NgModule } from '@angular/core';
import { OrderInformComponent } from './order-inform.component';
import { RouterModule, Routes } from '@angular/router';
import { OrderInformService } from './order-inform.service';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared/shared.module';

const routes: Routes = [
    {
        path     : '',
        component: OrderInformComponent,
        resolve  : {
            data: OrderInformService
        }
    },
];


@NgModule({
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        SharedModule,
    ],
    declarations: [OrderInformComponent],
    providers   : [OrderInformService]
})
export class OrderInformModule
{
}
