export const locale = {
    lang: 'ru',
    data: {
        'ORDER_INFORM': {
            'CONFIRMED'                    : 'Подтвержден',
            'ASSEMBLED'                    : 'Собран',
            'WAY'                          : 'В пути',
            'DELIVERED'                    : 'Доставлен',
            'INFORMATION_ABOUT'            : 'Информация о заказе',
            'RECIPIENT'                    : 'Получатель',
            'TIME_OF_DELIVERY'             : 'Время доставки',
            'BOUQUET_ASSEMBLED'            : 'Букет собран',
            'BOUQUET_DELIVERED'            : 'Букет доставлен',
            'WHEN_DELIVERED'               : 'Когда доставим букет, мы отправим вам смс с информацией о доставке.',
            'BOUQUET_PHOTO'                : 'Фото букета',
            'ORDER_STATUS_NO'              : 'Статус заказа №',
            'NOT_DELIVERED_YET'            : 'Еще не доставлен',
            'ONLINE'                       : 'на сайте',
            'BEFORE_DELIVERY'              : 'до доставки',
            'WITH_THE_RECIPIENT'           : 'с получателем',
            'READ_MORE'                    : 'Подробнее',
            'BOUQUET_PHOTO_BEFORE_DELIVERY': 'Фото букета до доставки',
            'PHOTOS_WITH_CUSTOMER'         : 'Фото букета с заказчиком',
            'BEFORE'                       : 'до',
            'FRONT'                        : 'Главная',
        }
    }
};
