export const locale = {
    lang: 'en',
    data: {
        'ORDER_INFORM': {
            'CONFIRMED'                    : 'Confirmed',
            'ASSEMBLED'                    : 'Assembled',
            'WAY'                          : 'On the Way',
            'DELIVERED'                    : 'Delivered',
            'INFORMATION_ABOUT'            : 'Information about order',
            'RECIPIENT'                    : 'Recipient',
            'TIME_OF_DELIVERY'             : 'Time of delivery',
            'BOUQUET_ASSEMBLED'            : 'Bouquet assembled',
            'BOUQUET_DELIVERED'            : 'Bouquet delivered',
            'WHEN_DELIVERED'               : 'When we deliver a bouquet, we will send you an SMS with information about the delivery.',
            'BOUQUET_PHOTO'                : 'Bouquet photo',
            'ORDER_STATUS_NO'              : 'Order Status No.',
            'NOT_DELIVERED_YET'            : 'Not delivered yet',
            'ONLINE'                       : 'Online',
            'BEFORE_DELIVERY'              : 'before delivery',
            'WITH_THE_RECIPIENT'           : 'with the recipient',
            'READ_MORE'                    : 'Read more',
            'BOUQUET_PHOTO_BEFORE_DELIVERY': 'Bouquet photo before delivery',
            'PHOTOS_WITH_CUSTOMER'         : 'Photos of the bouquet with the customer',
            'BEFORE'                       : 'before',
            'FRONT'                        : 'Front Page',
        }
    }
};
