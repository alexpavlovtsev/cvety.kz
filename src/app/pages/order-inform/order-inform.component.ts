import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ViewsService } from '@postgar/entity';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

import { TranslationLoaderService } from '@shared/services/translation-loader.service';
import { Views } from '../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import * as moment from 'moment';

@Component({
    selector   : 'app-order-inform',
    templateUrl: './order-inform.component.html',
    styleUrls  : ['./order-inform.component.scss']
})
export class OrderInformComponent implements OnInit, OnDestroy
{
    order: any;
    statuses: any[];
    deliveryDate: string;

    onlinePhoto: string;
    resultPhoto: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService,
        private _translationLoaderService: TranslationLoaderService,
    )
    {
        // Set the defaults
        this.statuses = [
            {id: 4, label: 'ORDER_INFORM.CONFIRMED'},
            {id: 5, label: 'ORDER_INFORM.ASSEMBLED'},
            {id: 8, label: 'ORDER_INFORM.WAY'},
            {id: 9, label: 'ORDER_INFORM.DELIVERED'}
        ];
        this._translationLoaderService.loadTranslations(english, russian);
        this.order = {statusId: 0};

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.orderReview)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.order        = node;
                this.order.productsdata = JSON.parse(this.order.productsdata);
                this.deliveryDate = moment(this.order.deliveryDate).format('DD.MM.YYYY');
                this.onlinePhoto  = Array.isArray(node.productsImages) ? node.productsImages[0] : null;
                this.resultPhoto  = Array.isArray(node.resultImages) ? node.resultImages[0] : null;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

}
