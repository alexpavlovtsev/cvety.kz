export const locale = {
    lang: 'en',
    data: {
        'PAY_CALLBACK': {
            'SUCCESS': 'Payment was successful',
            'ERROR'  : 'Payment failed',
            'LINK'   : 'Go back to main page'
        }
    }
};
