export const locale = {
    lang: 'ru',
    data: {
        'PAY_CALLBACK': {
            'SUCCESS': 'Оплата выполнена успешно',
            'ERROR'  : 'Оплата завершилась неудачно',
            'LINK'   : 'Вернуться на главную страницу'
        }
    }
};
