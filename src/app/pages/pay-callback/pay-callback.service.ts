import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { ViewsService } from '@postgar/entity';

@Injectable({
    providedIn: 'root'
})
export class PayCallbackService implements Resolve<any> {

    /**
     * Constructor
     *
     * @param _viewsService
     */
    constructor(
        private _viewsService: ViewsService,
    )
    {
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) =>
        {

            Promise.all([
                this.getData()
            ]).then(
                () =>
                {
                    resolve();
                },
                reject
            );
        });
    }

    getData(): Promise<any>
    {
        return new Promise((resolve, reject) =>
        {
            resolve();
        });
    }
}
