import { AfterViewInit, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit, Optional, PLATFORM_ID } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../store/reducers';
import * as fromStore from '../../store';
import { isPlatformBrowser } from '@angular/common';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

import { takeUntil } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';
import { TranslationLoaderService } from '@shared/services/translation-loader.service';
import { WindowToken } from '@shared/services/window';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { LocationService } from '@shared/services/location.service';
import { MetaService } from '@shared/services/meta.service';

@Component({
    selector   : 'app-pay-callback',
    templateUrl: './pay-callback.component.html',
    styleUrls  : ['./pay-callback.component.scss']
})
export class PayCallbackComponent implements OnInit, AfterViewInit, OnDestroy
{
    baseUrl$: Observable<string>;
    isSuccess: boolean;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _cd: ChangeDetectorRef,
        private _store: Store<State>,
        private _translationLoaderService: TranslationLoaderService,
        @Inject(PLATFORM_ID) private platformId: Object,
        @Optional() @Inject(WindowToken) private window: Window,
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
        private _locationService: LocationService,
        private _metaService: MetaService,
    )
    {
        // Set the defaults
        this._translationLoaderService.loadTranslations(english, russian);
        this.baseUrl$ = of(_locationService.baseUrl());

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._metaService.setTag('robots', 'noindex, follow');
    }

    /**
     * After init
     */
    ngAfterViewInit(): void
    {
        this._store.select(fromStore.getRouterState)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.isSuccess = value.state.params['name'] === 'success';
                this.refresh();

                if (value.state.params['orderId'])
                {
                    // this.loadOrder(value.state.params['orderId']);
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Load order
     *
     * @param orderId
     */
    private loadOrder(orderId: string): void
    {
        this._entityManager
            .getService('Order')
            .getWithQuery({
                id: 'callback-pay',
                filters: {orderId: {equalTo: orderId}}
            })
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any[]) =>
            {
                if (nodes.length > 0 && !nodes[0].showSuccessPage)
                {
                    const order        = Object.assign({}, nodes[0]);
                    order.productsdata = JSON.parse(order.productsdata);
                    this.setGTM(order);
                }
                else
                {
                    this._store.dispatch(new fromStore.Go({path: ['/404']}));
                }
            });
    }

    /**
     * Set gtm
     *
     * @param order
     */
    private setGTM(order: any): void
    {
        if (isPlatformBrowser(this.platformId))
        {
            const dataLayer = (this.window as any)['dataLayer'];

            dataLayer.push({
                'transactionId'         : order.id,
                'transactionAffiliation': 'leken.kz',
                'transactionTotal'      : order.budgetAmountValue, // общая сумма заказа
                'transactionProducts'   : [{
                    'id'      : order.id, // ID заказа
                    'sku'     : order.productsdata[0].id, // код товара
                    'name'    : order.productsdata[0].name, // название товара
                    'category': 'букеты', // категория товара
                    'price'   : order.productsdata[0].price, // цена единицы товара
                    'quantity': '1' // количество товара
                }
                ],
                'transactionCurrency'   : order.budgetAmountCurrency,
                'event'                 : 'trackTrans',
            });

            this.setShowPage(order.id);
        }
    }

    refresh(): void
    {
        this._cd.detectChanges();
    }

    /**
     * Set showed property to order
     *
     * @param id
     */
    private setShowPage(id: number): void
    {
        this._entityManager
            .getService('Order')
            .update({id: id, showSuccessPage: true});
    }
}
