import { NgModule } from '@angular/core';
import { PayCallbackComponent } from './pay-callback.component';
import { RouterModule } from '@angular/router';
import { PayCallbackService } from './pay-callback.service';
import { SharedModule } from '@shared/shared.module';

@NgModule({
    imports     : [
        RouterModule.forChild([
            {
                path     : '',
                component: PayCallbackComponent,
                resolve  : {
                    data: PayCallbackService
                }
            }
        ]),

        SharedModule,
    ],
    declarations: [PayCallbackComponent],
    providers   : [PayCallbackService]
})
export class PayCallbackModule
{
}
