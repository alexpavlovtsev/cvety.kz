import { NgModule } from '@angular/core';
import { CitiesComponent } from './cities.component';
import { CitiesService } from './cities.service';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@shared/shared.module';


const routes: Routes = [
    {
        path     : '',
        component: CitiesComponent,
        resolve  : {
            data: CitiesService
        }
    },
];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),

        SharedModule,
    ],
    declarations: [CitiesComponent],
    providers   : [CitiesService]
})
export class CitiesModule
{
}
