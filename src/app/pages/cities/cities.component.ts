import { Component, OnInit } from '@angular/core';
import { Views } from '../../postgar-config/views';
import { switchMap, take, takeUntil } from 'rxjs/operators';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Subject } from 'rxjs';
import { MAIN_CITY_CODE } from '../../config';
import { LocationService } from '@shared/services/location.service';
import { ScriptService } from '@shared/services/script.service';

@Component({
    selector   : 'app-cities',
    templateUrl: './cities.component.html',
    styleUrls  : ['./cities.component.scss']
})
export class CitiesComponent implements OnInit
{
    cities: any[];
    letters: string[];
    langId: number;
    container: {[k: string]: any[][]};

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _entityManager: EntityManagerService,
        private _viewsService: ViewsService,
        private _scriptService: ScriptService,
    )
    {
        this.cities    = [];
        this.letters   = [];
        this.container = {};

        this._unsubscribeAll = new Subject();
    }

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(
                take(1),
                switchMap((language: any) =>
                    {
                        this.langId = language.id;

                        return this._entityManager.getService('City').entities$
                            .pipe(take(1));
                    }
                ),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe(cities =>
            {
                this.container = {};
                this.cities    = [...cities];
                this.letters   = this.getLetters();

                for (const letter of this.letters)
                {
                    const cities = this.cities.filter((city: any) =>
                    {
                        return this.langId === 1
                            ? city.name.toLowerCase().startsWith(letter.toLowerCase())
                            : city.code.toLowerCase().startsWith(letter.toLowerCase())
                    });

                    this.container[letter] = [];

                    while (cities.length)
                    {
                        this.container[letter].push(cities.splice(0, 4));
                    }
                }
            });

        setTimeout(() =>
        {
            this.initScripts();
        }, 0);
    }

    getLetters(): string[]
    {
        const object = this.cities
            .sort((a, b) =>
            {
                if (a.name < b.name)
                {
                    return -1;
                }
                if (a.name > b.name)
                {
                    return 1;
                }
                return 0;
            })
            .map((city: any) => this.langId === 1 ? city.name.charAt(0) : city.code.charAt(0))
            .reduce((obj, string) =>
            {
                return {
                    ...obj,
                    [string]: true
                };
            }, {});

        return Object.keys(object);
    }

    url(code: string): string
    {
        const langCode = this.langId === 1 ? '' : 'en';
        const cityCode = code === MAIN_CITY_CODE ? '' : code;

        return LocationService.validFullUrl(`/${cityCode}/${langCode}`);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.loadScript('popper').then(() =>
            {
                this._scriptService.load(['slick', 'jquery.formstyler', 'bootstrap']).then(() =>
                {
                });
            });
        });
    }
}
