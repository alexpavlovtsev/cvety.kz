import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { LocationService } from '@shared/services/location.service';
import { Observable } from 'rxjs';
import { Views } from '../../postgar-config/views';
import { QueryOp } from '@postgar/types';

@Injectable()
export class RecipientFeedbackService implements Resolve<any>
{
    params: any;
    exist: boolean;

    /**
     * Constructor
     */
    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
        private _locationService: LocationService,
    )
    {
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.params = route.params;

        return new Promise((resolve, reject) =>
        {
            Promise.all([
                this.getOrder()
            ]).then(
                () =>
                {
                    resolve();
                    if (!this.exist)
                    {
                        this._locationService.goTo404();
                    }
                },
                reject
            );
        });
    }

    getOrder(): Promise<any>
    {
        return new Promise((resolve) =>
        {
            this._entityManager
                .getService('Order')
                .getWithQuery({
                    id      : Views.orderReview,
                    pageSize: 1,
                    filters : {
                        randomUuid: {[QueryOp.equalTo]: this.params['orderNumber']},
                    },
                })
                .subscribe((nodes: any[]) =>
                {
                    this.exist = nodes.length > 0;
                    resolve();
                });
        });
    }
}
