import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { config } from '../../../config';

@Component({
    selector   : 'app-step-two',
    templateUrl: './step-two.component.html',
    styleUrls  : ['./step-two.component.scss']
})
export class StepTwoComponent implements OnInit
{
    @Output() changeUrl = new EventEmitter();
    @Output() submit    = new EventEmitter();

    url: string;
    file: any;

    constructor()
    {
    }

    ngOnInit()
    {
    }

    fileEvent(event: any): void
    {
        const files = event.target.files;
        this.file   = files[0];
        this.uploadFile();
    }

    cancel(): void
    {
        this.submit.emit();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private uploadFile(): void
    {
        const uniqueFolder = generateGUID();
        const imageName    = generateGUID();
        const amazonUrl    = uniqueFolder + '/' + imageName + '.jpg';

        AWS.config.accessKeyId     = config.AWS_accessKeyId;
        AWS.config.secretAccessKey = config.AWS_secretAccessKey;
        const bucket               = new AWS.S3({
            region: 'eu-west-2',
            params: {Bucket: config.AWS_bucket}
        });

        const params = {Key: amazonUrl, Body: this.file, ACL: 'public-read'};
        bucket.upload(params, (err, data) =>
        {
            if (!err)
            {
                this.url = `${config.AWS_base_url}${amazonUrl}`;
                this.changeUrl.emit(this.url);
            }
            else
            {
                console.log(err);
            }
        });
    }
}

function generateGUID()
{
    function S4()
    {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return S4() + S4();
}
