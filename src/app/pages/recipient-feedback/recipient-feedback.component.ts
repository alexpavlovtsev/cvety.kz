import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ScriptService } from '@shared/services/script.service';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Subject } from 'rxjs';
import { Views } from '../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector   : 'app-recipient-feedback',
    templateUrl: './recipient-feedback.component.html',
    styleUrls  : ['./recipient-feedback.component.scss']
})
export class RecipientFeedbackComponent implements OnInit, AfterViewInit
{
    formNumber: number;
    callback: any;
    imageUrl: string;
    order: any;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _scriptService: ScriptService,
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
    )
    {
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this.setFormNumber(1);

        this._viewsService.firstNode(Views.orderReview)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.order = node;
            });
    }

    ngAfterViewInit(): void
    {
        setTimeout(() =>
        {
            this.initScripts();
        }, 0);
    }

    setCallback(callback: any): void
    {
        this.callback = callback;
        this.setFormNumber(2);

        this.callback.senderEmail = this.order.senderEmail;
        this.callback.orderId     = this.order.id;
        this.callback.bouquetId   = this.order.bouquetIds && this.order.bouquetIds.length > 0 ? this.order.bouquetIds[0] : null;
    }

    setImageUrl(url: string): void
    {
        this.imageUrl                = url;
        this.callback.recipientPhoto = url;
    }

    setFormNumber(number: number): void
    {
        this.formNumber = number;
    }

    formSubmit(): void
    {
        this.setFormNumber(3);
        this.save();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private save(): void
    {
        if (this.imageUrl)
        {
            this._entityManager.getService('Order').update({
                id          : this.callback.orderId,
                resultImages: [this.imageUrl]
            });
        }

        this.callback.from = 'recipient';

        this._entityManager.getService('Callback').add(this.callback);
    }

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.load(['slick', 'popper', 'jquery.formstyler', 'bootstrap', 'aws']).then(() =>
            {
                const settings = scriptData();
                this._scriptService.appendScript(settings, 'feedback-form');
            });
        });
    }
}

function scriptData(): string
{
    return `
    (function(){
    $('.js-toggle-content').on('click', function () {
        var _this = $(this),
            text = _this.find('.js-toggle-text'),
            el = _this.prev();
        if (!el.hasClass('activated')) {
            curHeight = el.height();
            el.addClass('activated');
        };
        var autoHeight = el.css('height', 'auto').height(),
            textHide = _this.data('text-hide'),
            textShow = _this.data('text-show');
        if (_this.hasClass('active')) {
            _this.removeClass('active');
            text.text(textShow);
            el.animate({height: curHeight}, 400);
        }
        else {
            _this.addClass('active');
            text.text(textHide);
            el.height(curHeight+20).animate({height: autoHeight}, 400);
        }
        return false;
    });
}());
$('.cv_ratepage-rate input').styler('destroy');
$('.cv_ratepage-right-check input').styler();
$('.cv_ratepage-rate input').on('click', function(){
  var val = $(this).val();
  $('.cv_ratepage-info').hide(400);
  $('.cv_ratepage-item').hide(400);
  $('.cv_ratepage-item#rated-' + val).toggle(400);
  $('.cv_ratepage-leave').show(450);
});
    `;
}
