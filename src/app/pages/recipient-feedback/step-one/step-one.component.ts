import { ChangeDetectorRef, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Callback } from '../../reviews/callback';
import { fuseAnimations } from '@shared/animations';

@Component({
    selector   : 'app-step-one',
    templateUrl: './step-one.component.html',
    styleUrls  : ['./step-one.component.scss'],
    animations : fuseAnimations
})
export class StepOneComponent implements OnInit
{
    callback: any;
    disappointed: {};
    content: {};

    testContent: boolean;

    @Output() submit = new EventEmitter();

    constructor(
        private _cd: ChangeDetectorRef,
    )
    {
        this.callback = new Callback();
        this.content  = {
            'NOT_FRESH'                 : 'Цветы не свежие',
            'NOT_LIKE_IN_PHOTO'         : 'Букет не как на фото',
            'COURIER_NOT_RIGHT'         : 'Курьер не пунктуальный',
            'COURIER_RUDE'              : 'Курьер нагрубил',
            'WRONG_BOUQUET'             : 'Получил не тот букет',
            'FLORIST_NEEDS_TO_LEARN'    : 'Флористу надо подучиться',
            'SOME_FLOWERS_WILTED'       : 'Некоторые цветы завяли',
            'NOT_EXACTLY_WHAT_I_ORDERED': 'Не совсем то, что заказывал',
            'THE_WHERE_THE_ADDRESS_IS'  : 'Курьер уточнял где адрес',
            'COURIER_LITTLE_LATE'       : 'Курьер чуть опоздал',
            'FRESH_FLOWERS'             : 'Цветы свежие',
            'COURTEOUS_COURIER'         : 'Вежливый курьер',
            'BOUQUET_IS_BETTER'         : 'Букет лучше, чем заказывал',
            'BEAUTIFULLY_DECORATED'     : 'Красиво оформлен',
            'PUNCTUAL'                  : 'Курьер пунктуальный',
            'SUPER_FLORIST'             : 'Супер флорист',
            'FAST_SHIPPING'             : 'Быстрая доставка',
            'MORE_CHOICES'              : 'Больше выбора',
            'BOUQUET_EXACTLY'           : 'Букет в точности как на фото',
            'FLORIST_WELL_DONE'         : 'Флорист молодец',
            'SUPER_FRESH_FLOWERS'       : 'Цветы супер свежие',
            'COURIER_UMNICHKA'          : 'Курьер умничка'
        };
        this.disappointClear();

        this.testContent = false;
    }

    ngOnInit()
    {
    }

    /**
     * Set rating
     *
     * @param {number} rating
     */
    setRating(rating: number): void
    {
        if (this.callback.rating !== rating)
        {
            this.disappointClear();
            this.callback.rating = rating;
        }
    }

    /**
     * Change
     *
     * @param {string} key
     */
    disappointChange(key: string): void
    {
        this.disappointed[key] = !this.disappointed[key];
        this._cd.detectChanges();
    }

    /**
     * Clear disappoint
     */
    disappointClear(): void
    {
        this.disappointed = Object.keys(this.content).reduce((newObj, key) =>
        {
            return {
                ...newObj,
                [key]: false
            };
        }, {});
    }

    addCallback(): void
    {
        this.callback.disappointed = Object.keys(this.disappointed)
            .reduce((list, key) =>
            {
                if (this.disappointed[key])
                {
                    return [
                        ...list,
                        this.content[key]
                    ];
                }
                else
                {
                    return list;
                }
            }, []);

        const callback = {
            disappointed: this.callback.disappointed,
            rating      : this.callback.rating,
            comment     : this.callback.comment,
            bouquetId   : this.callback.bouquetId,
            orderId     : this.callback.orderId
        };

        this.submit.emit(callback);
    }
}
