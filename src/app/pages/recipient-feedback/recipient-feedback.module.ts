import { NgModule } from '@angular/core';
import { RecipientFeedbackComponent } from './recipient-feedback.component';
import { RecipientFeedbackService } from './recipient-feedback.service';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@shared/shared.module';
import { StepOneComponent } from './step-one/step-one.component';
import { StepTwoComponent } from './step-two/step-two.component';
import { StepThreeComponent } from './step-three/step-three.component';

const routes: Routes = [
    {
        path     : ':orderNumber/',
        component: RecipientFeedbackComponent,
        resolve  : {
            data: RecipientFeedbackService
        }
    },
];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,
        SharedModule,
    ],
    declarations: [RecipientFeedbackComponent, StepOneComponent, StepTwoComponent, StepThreeComponent],
    providers   : [RecipientFeedbackService]
})
export class RecipientFeedbackModule
{
}
