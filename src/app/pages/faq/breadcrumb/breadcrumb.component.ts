import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { MAIN_CITY_CODE } from '../../../config';
import { Router } from '@angular/router';
import { LocationService } from '@shared/services/location.service';

@Component({
    selector   : 'app-breadcrumb',
    templateUrl: './breadcrumb.component.html',
    styleUrls  : ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit, OnDestroy
{
    cityPath: string;
    langPath: string;
    fullUrl: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _viewsService: ViewsService,
        private _router: Router,
        private _locationService: LocationService
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.fullUrl = this._router.url;

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                this.langPath = value.id === 1 ? '/' : '/en';
            });

        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((value: any) =>
            {
                this.cityPath = value.code === MAIN_CITY_CODE ? '/' : `/${value.code}`;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
