export const locale = {
    lang: 'en',
    data: {
        'FAQ': {
            'SEE_ALL'                 : 'See all',
            'MORE'                    : 'See More',
            'NOT_EXIST_MESSAGE'       : 'No products for this city',
            'LOAD_MORE'               : 'Load more',
            'YOU_SHOW'                : 'Have you looked',
            'FROM'                    : 'of',
            'VARIANTS'                : 'options',
            'REVIEWS'                 : 'Reviews',
            'ANSWERS_TO_ALL_QUESTIONS': 'Answers to all questions',
            'FRONT'                   : 'Front Page',
            'STILL_HAVE_QUESTIONS'    : 'Still have questions',
            'ABOUT_DELIVERY'          : 'About delivery',
            'ABOUT_PAYMENT'           : 'About payment',
            'GUARANTEES_OF_FRESHNESS' : 'Guarantees of freshness',
            'CONTACTS'                : 'Contacts',
            'REVIEWS_TAB'             : 'Reviews',
            'DELIVERY_PHOTOS'         : 'Delivery photos',
            'BOUQUET'                 : 'Bouquet',
            'DELIVERY'                : 'Delivery',
            'PAYMENT'                 : 'Payment',
            'TITLE_PREFIX'            : ' in ',
            'SHOW_ALL'          : 'show all',
            'TITLES'                  : {
                'DELIVERY'       : 'Terms of Delivery',
                'PAYMENT'        : 'Payments Terms',
                'GUARANTEES'     : 'Guarantees',
                'CONTACTS'       : 'Contacts',
                'DELIVERY_PHOTOS': 'Delivery Photos',
                'REVIEWS'        : 'Reviews'
            }
        }
    }
};
