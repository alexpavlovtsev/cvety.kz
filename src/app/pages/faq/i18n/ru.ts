export const locale = {
    lang: 'ru',
    data: {
        'FAQ': {
            'SEE_ALL'                 : 'Смотреть все',
            'MORE'                    : 'Смотреть Еще',
            'NOT_EXIST_MESSAGE'       : 'Нет товаров для данного города',
            'LOAD_MORE'               : 'загрузить еще',
            'YOU_SHOW'                : 'Вы посмотрели',
            'FROM'                    : 'из',
            'VARIANTS'                : 'вариантов',
            'REVIEWS'                 : 'Отзывов',
            'ANSWERS_TO_ALL_QUESTIONS': 'Ответы на все вопросы',
            'FRONT'                   : 'Главная',
            'STILL_HAVE_QUESTIONS'    : 'Остались вопросы',
            'ABOUT_DELIVERY'          : 'Про доставку',
            'ABOUT_PAYMENT'           : 'Про оплату',
            'GUARANTEES_OF_FRESHNESS' : 'Гарантии свежести',
            'CONTACTS'                : 'Контакты',
            'REVIEWS_TAB'             : 'Отзывы',
            'DELIVERY_PHOTOS'         : 'Фотографии доставок',
            'BOUQUET'                 : 'Букет',
            'DELIVERY'                : 'Доставка',
            'PAYMENT'                 : 'Оплата',
            'TITLE_PREFIX'            : ' в ',
            'SHOW_ALL'          : 'показать все',
            'TITLES'                  : {
                'DELIVERY'       : 'Условия доставки цветов',
                'PAYMENT'        : 'Условия оплаты',
                'GUARANTEES'     : 'Гарантии',
                'CONTACTS'       : 'Контакты',
                'DELIVERY_PHOTOS': 'Фотографии доставки',
                'REVIEWS'        : 'Отзывы'
            }
        }
    }
};
