import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector   : 'app-info-page-payment',
    templateUrl: './info-page-payment.component.html',
    styleUrls  : ['./info-page-payment.component.scss']
})
export class InfoPagePaymentComponent implements OnInit
{
    @Input() content: any;

    items: any[];

    constructor()
    {
        this.items = [];
    }

    ngOnInit()
    {
        this.items = JSON.parse(this.content);
    }

}
