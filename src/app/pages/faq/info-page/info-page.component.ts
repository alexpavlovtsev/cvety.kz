import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { getRouterState, State } from '../../../store/reducers';
import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { LocationService } from '@shared/services/location.service';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Views } from '../../../postgar-config/views';
import { MetatagService } from '@shared/services/metatag.service';
import { ScriptService } from '@shared/services/script.service';
import { MAIN_CITY_CODE } from '../../../config';

@Component({
    selector   : 'app-info-page',
    templateUrl: './info-page.component.html',
    styleUrls  : ['./info-page.component.scss']
})
export class InfoPageComponent implements OnInit, AfterViewInit, OnDestroy
{
    path: string;
    tabIndex: number;
    langPath: string;
    langId: number;
    pages: any;
    url: string;
    title: string;
    isMainCity: boolean;
    citySecondName: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _store: Store<State>,
        private _viewsService: ViewsService,
        private _locationService: LocationService,
        private _entityManager: EntityManagerService,
        private _metatagService: MetatagService,
        private _scriptService: ScriptService,
    )
    {
        this.tabIndex = 0;
        this.pages    = {};

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngOnInit()
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.langPath = node.id === 1 ? '/' : '/en/';
                this.langId   = node.id;
            });

        this._entityManager.getService('Page').entities$
            .pipe(take(1), takeUntil(this._unsubscribeAll))
            .subscribe((entities: any[]) =>
            {
                this.pages = entities.reduce((newObj, node) =>
                {
                    const content = JSON.parse(node.content);

                    return {
                        ...newObj,
                        [`/${content.ru.url}`]: content.ru,
                        [`/${content.en.url}`]: content.en
                    };
                }, {});
            });

        this._store.pipe(select(getRouterState))
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(route =>
            {
                this.url = route.state.url;
                this.parseUrl(route.state.url);
            });

        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.isMainCity     = !!node && node.code === MAIN_CITY_CODE;
                this.citySecondName = this.langId === 1 ? node.secondName : node.enName;
            });
    }

    ngAfterViewInit(): void
    {
        setTimeout(() =>
        {
            this.initScripts();
        }, 0);
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    get content(): any
    {
        return this.pages.hasOwnProperty(this.url) ? this.pages[this.url].content : '';
    }

    parseUrl(url: string): void
    {
        let title;

        switch (true)
        {
            case url.endsWith('delivery/'):
                this.tabIndex = 1;
                title         = this.langId === 1 ? 'Про доставку' : 'About delivery';
                this._metatagService.set('page', title);
                break;

            case url.endsWith('payment/'):
                this.tabIndex = 2;
                title         = this.langId === 1 ? 'Про оплату' : 'About payment';
                this._metatagService.set('page', title);
                break;

            case url.endsWith('guarantee/'):
                this.tabIndex = 3;
                title         = this.langId === 1 ? 'Гарантии свежести' : 'Guarantees of freshness';
                this._metatagService.set('page', title);
                break;

            case url.endsWith('contacts/'):
                this.tabIndex = 4;
                title         = this.langId === 1 ? 'Контакты' : 'Contacts';
                this._metatagService.set('page', title);
                break;

            case url.endsWith('reviews/'):
                this.tabIndex = 5;
                title         = this.langId === 1 ? 'Отзывы' : 'Reviews';
                this._metatagService.set('page', title);
                break;

            case url.endsWith('delivery-photos/'):
                this.tabIndex = 6;
                title         = this.langId === 1 ? 'Фотографии доставок' : 'Delivery photos';
                this._metatagService.set('page', title);
                break;
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.load(['slick', 'popper', 'jquery.formstyler', 'bootstrap']).then(() =>
            {
                const settings = scriptData();
                this._scriptService.appendScript(settings, 'faq-delivery-block');
            });
        });
    }
}

function scriptData(): string
{
    return `
  // filters

// toggle content
(function(){
    $('.js-toggle-content').on('click', function () {
        var _this = $(this),
            text = _this.find('.js-toggle-text'),
            el = _this.prev();
        if (!el.hasClass('activated')) {
            curHeight = el.height();
            el.addClass('activated');
        };
        var autoHeight = el.css('height', 'auto').height(),
            textHide = _this.data('text-hide'),
            textShow = _this.data('text-show');
        if (_this.hasClass('active')) {
            _this.removeClass('active');
            text.text(textShow);
            el.animate({height: curHeight}, 400);
        }
        else {
            _this.addClass('active');
            text.text(textHide);
            el.height(curHeight+20).animate({height: autoHeight}, 400);
        }
        return false;
    });
}());
  $('.cv_feedback-carousel').slick({
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 2,
  responsive: [
    {
      breakpoint: 769,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 481,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
  $(document).ready(function(){
    var mql = window.matchMedia('(min-width: 789px)');
if (mql.matches) {
    $(".cv_filters-box").addClass('show');
  }
  });

    $(".cv_filters-filterit").on('click', function(){
      $(".cv_filters-right").addClass("show");
      $(".cv_filters-right").before ('<div class="cv_wrapper"></div>');
      $(".cv_blur").addClass("blurit");

      $(".cv_wrapper").on('click', function(){
        $(".cv_filters-right").removeClass("show");
        $(".cv_blur").removeClass("blurit");
        $(this).remove();
      });
    });`;
}
