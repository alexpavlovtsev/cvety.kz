import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector   : 'app-info-page-delivery',
    templateUrl: './info-page-delivery.component.html',
    styleUrls  : ['./info-page-delivery.component.scss']
})
export class InfoPageDeliveryComponent implements OnInit
{
    @Input() content: any;

    items: any[];

    constructor()
    {
        this.items = [];
    }

    ngOnInit()
    {
        this.items = JSON.parse(this.content);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
}
