import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Views } from '../../../../postgar-config/views';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector   : 'app-info-page-reviews',
    templateUrl: './info-page-reviews.component.html',
    styleUrls  : ['./info-page-reviews.component.scss']
})
export class InfoPageReviewsComponent implements OnInit, OnDestroy
{
    @Input() content: any;

    nodes: any[];
    first: number;
    totalCount: number;
    pageSize: number;

    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
    )
    {
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngOnInit()
    {
        this._viewsService.view(Views.Callbacks)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(view =>
            {
                this.totalCount = view.totalCount;
                this.pageSize   = view.pageSize;
                this.first      = view.pageSize * (view.pageIndex + 1);
            });

        this._viewsService.nodes(Views.Callbacks)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any[]) =>
            {
                this.nodes = nodes;
            });

        this.loadCollection();
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    loadCollection(): void
    {
        this._entityManager
            .getService('Callback')
            .getWithQuery({
                id      : Views.Callbacks,
                pageSize: this.first,
                // filters : {
                //     from: {[QueryOp.equalTo]: 'recipient'},
                // },
            });
    }

    loadMore(): void
    {
        this.first = this.first + 3;
        this.loadCollection();
    }
}
