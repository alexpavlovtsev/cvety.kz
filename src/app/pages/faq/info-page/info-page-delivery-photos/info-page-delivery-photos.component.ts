import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector   : 'app-info-page-delivery-photos',
    templateUrl: './info-page-delivery-photos.component.html',
    styleUrls  : ['./info-page-delivery-photos.component.scss']
})
export class InfoPageDeliveryPhotosComponent implements OnInit
{
    @Input() content: any;

    constructor()
    {
    }

    ngOnInit()
    {
    }

}
