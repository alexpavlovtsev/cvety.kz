import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector   : 'app-info-page-contacts',
    templateUrl: './info-page-contacts.component.html',
    styleUrls  : ['./info-page-contacts.component.scss']
})
export class InfoPageContactsComponent implements OnInit
{
    @Input() content: any;

    constructor()
    {
    }

    ngOnInit()
    {
    }

}
