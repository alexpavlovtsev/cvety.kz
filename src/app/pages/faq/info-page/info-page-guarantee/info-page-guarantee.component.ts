import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector   : 'app-info-page-guarantee',
    templateUrl: './info-page-guarantee.component.html',
    styleUrls  : ['./info-page-guarantee.component.scss']
})
export class InfoPageGuaranteeComponent implements OnInit
{
    @Input() content: any;

    constructor()
    {
    }

    ngOnInit()
    {
    }

}
