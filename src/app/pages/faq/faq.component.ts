import { AfterViewInit, Component } from '@angular/core';
import { ScriptService } from '@shared/services/script.service';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

import { TranslationLoaderService } from '@shared/services/translation-loader.service';
import { frontScriptData } from '../front/front.component';

@Component({
    selector   : 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls  : ['./faq.component.scss']
})
export class FaqComponent implements AfterViewInit
{

    /**
     * Constructor
     */
    constructor(
        private _translationLoaderService: TranslationLoaderService,
        private _scriptService: ScriptService,
    )
    {
        this._translationLoaderService.loadTranslations(english, russian);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngAfterViewInit(): void
    {
        setTimeout(() =>
        {
            this.initScripts();
        }, 0);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.loadScript('popper').then(() =>
            {
                this._scriptService.load(['slick', 'jquery.formstyler', 'bootstrap']).then(() =>
                {
                    const settings = frontScriptData();
                    this._scriptService.appendScript(settings, 'front-settings');
                });
            });
        });
    }
}
