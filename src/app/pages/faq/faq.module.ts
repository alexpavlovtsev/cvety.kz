import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '@shared/shared.module';

import { FaqComponent } from './faq.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { InfoPageComponent } from './info-page/info-page.component';
import { InfoPageDeliveryComponent } from './info-page/info-page-delivery/info-page-delivery.component';
import { InfoPagePaymentComponent } from './info-page/info-page-payment/info-page-payment.component';
import { InfoPageGuaranteeComponent } from './info-page/info-page-guarantee/info-page-guarantee.component';
import { InfoPageContactsComponent } from './info-page/info-page-contacts/info-page-contacts.component';
import { InfoPageReviewsComponent } from './info-page/info-page-reviews/info-page-reviews.component';
import { InfoPageDeliveryPhotosComponent } from './info-page/info-page-delivery-photos/info-page-delivery-photos.component';
import { FaqService } from './faq.service';
import { ReviewItemComponent } from './info-page/info-page-reviews/review-item/review-item.component';
import { DeliveryBlockModule } from '../../layout-elements/delivery-block/delivery-block.module';

const routes: Routes = [
    {
        path     : '',
        component: FaqComponent,
        resolve  : {
            data: FaqService
        }
    },
];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),

        SharedModule,
        DeliveryBlockModule,
    ],
    declarations: [
        FaqComponent,
        BreadcrumbComponent,
        InfoPageComponent,
        InfoPageDeliveryComponent,
        InfoPagePaymentComponent,
        InfoPageGuaranteeComponent,
        InfoPageContactsComponent,
        InfoPageReviewsComponent,
        InfoPageDeliveryPhotosComponent,
        ReviewItemComponent
    ],
    providers   : [FaqService]
})
export class FaqModule
{
}
