import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { EntityManagerService } from '@postgar/entity';
import { filter, take, tap } from 'rxjs/operators';

@Injectable()
export class FaqService implements Resolve<any>
{

    /**
     * Constructor
     */
    constructor(
        private _entityManager: EntityManagerService,
    )
    {
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) =>
        {

            Promise.all([
                this.getPages()
            ]).then(
                () =>
                {
                    resolve();
                },
                reject
            );
        });
    }

    getPages(): Promise<any>
    {
        return new Promise<any>((resolve, reject) =>
        {
            this._entityManager.getService('Page').loaded$
                .pipe(
                    tap(loaded =>
                    {
                        if (!loaded)
                        {
                            this._entityManager
                                .getService('Page')
                                .getAll();
                        }
                    }),
                    filter(loaded => loaded),
                    take(1)
                ).subscribe(value =>
            {
                resolve(value);
            }, reject);
        });
    }
}
