import { AfterViewInit, Component, OnInit } from '@angular/core';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

import { TranslationLoaderService } from '@shared/services/translation-loader.service';
import { ScriptService } from '@shared/services/script.service';
import { frontScriptData } from '../front/front.component';

@Component({
    selector   : 'app-thematic',
    templateUrl: './thematic.component.html',
    styleUrls  : ['./thematic.component.scss']
})
export class ThematicComponent implements OnInit, AfterViewInit
{

    constructor(
        private _scriptService: ScriptService,
        private _translationLoaderService: TranslationLoaderService,
    )
    {
        this._translationLoaderService.loadTranslations(english, russian);
    }

    ngOnInit()
    {
    }

    ngAfterViewInit(): void
    {
        setTimeout(() =>
        {
            this.initScripts();
        }, 0);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.loadScript('popper').then(() =>
            {
                this._scriptService.load(['slick', 'jquery.formstyler', 'bootstrap']).then(() =>
                {
                    const settings = frontScriptData();
                    this._scriptService.appendScript(settings, 'front-settings');
                });
            });
        });
    }
}
