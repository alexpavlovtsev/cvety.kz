import { Component, OnDestroy, OnInit } from '@angular/core';
import { combineLatest, Observable, Subject } from 'rxjs';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { MAIN_CITY_CODE } from '../../../../config';
import { select, Store } from '@ngrx/store';
import { getRouterState, State } from '../../../../store/reducers';

@Component({
    selector   : 'app-items',
    templateUrl: './items.component.html',
    styleUrls  : ['./items.component.scss']
})
export class ItemsComponent implements OnInit, OnDestroy
{
    langId: number;
    prefix: string;
    themPrefix: string;
    city: string;
    currency: any;
    language: any;
    url: string;

    nodes: any[];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _viewsService: ViewsService,
        private _store: Store<State>,
    )
    {
        this.currency = {};
        this.language = {};
        this.nodes    = [];

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        this._store.pipe(select(getRouterState))
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(route =>
            {
                this.url = route.state.url;
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.langId     = node.id;
                this.prefix     = this.langId === 1 ? '/bukety/' : '/en/bouquets/';
                this.themPrefix = this.langId === 1 ? '/' : '/en/';
                this.language   = node;
            });

        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.city = node.code === MAIN_CITY_CODE ? '/' : `${node.code}/`;
            });

        this._viewsService.firstNode(Views.Currency)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.currency = node;
            });

        this._viewsService.nodes(Views.InBox)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any[]) =>
            {
                if (this.url.includes('in-box'))
                {
                    this.nodes = nodes;
                }
            });

        combineLatest(
            this._viewsService.nodes(Views.Featured),
            this._viewsService.nodes(Views.Featured2)
        )
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(([featured, featured2]) =>
            {
                if (this.url.includes('featured') || this.url.includes('populyarnye'))
                {
                    this.nodes = [...featured, ...featured2];
                }
            });

        this._viewsService.nodes(Views.Extraordinary)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any[]) =>
            {
                if (this.url.includes('extraordinary'))
                {
                    this.nodes = nodes;
                }
            });

        this._viewsService.nodes(Views.Inexpensive)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any[]) =>
            {
                if (this.url.includes('inexpensive'))
                {
                    this.nodes = nodes;
                }
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    randomRating(id: number): string
    {
        const lastDigit = id.toString().split('').pop();

        return lastDigit === '0' ? '5.0' : `4.${lastDigit}`;
    }
}
