import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Views } from '../../postgar-config/views';
import { Observable } from 'rxjs';
import { filter, take, tap } from 'rxjs/operators';
import { QueryOp } from '@postgar/types';

@Injectable()
export class ThematicService implements Resolve<any>
{

    cityCode: string;
    langId: number;

    /**
     * Constructor
     *
     * @param _viewsService
     * @param _entityManager
     */
    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService
    )
    {
        this._viewsService.firstNode(Views.City)
            .subscribe((node: any) =>
            {
                this.cityCode = node.code;
            });

        this._viewsService.firstNode(Views.Language)
            .subscribe((node: any) =>
            {
                this.langId = node.id;
            });
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) =>
        {
            Promise.all([
                this.getFilterTrs(),
                this.getFilterItemTrs(),
                this.getPageData(state),
            ]).then(
                () =>
                {
                    resolve();
                },
                reject
            );
        });
    }

    getPageData(state: RouterStateSnapshot): Promise<any>
    {
        switch (true)
        {
            case state.url.includes('populyarnye'):
            case state.url.includes('featured'):
                return Promise.all([this.getFeatured(), this.getFeatured2()]);

            // case state.url.includes('inexpensive'):
            //     return this.getInexpensive();
            //
            // case state.url.includes('extraordinary'):
            //     return this.getExtraordinary();
            //
            // case state.url.includes('in-box'):
            //     return this.getInBox();
        }
    }

    getInexpensive(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.Inexpensive,
                    pageSize: 9,
                    filters : {
                        cityCodes   : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId  : {[QueryOp.equalTo]: this.langId},
                        bouquetPrice: {[QueryOp.greaterThan]: '0'},
                    },
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }

    getExtraordinary(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.Extraordinary,
                    pageSize: 9,
                    filters : {
                        cityCodes   : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId  : {[QueryOp.equalTo]: this.langId},
                        bouquetPrice: {[QueryOp.greaterThan]: '0'},
                    },
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }

    getInBox(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.InBox,
                    pageSize: 9,
                    filters : {
                        cityCodes   : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId  : {[QueryOp.equalTo]: this.langId},
                        bouquetPrice: {[QueryOp.greaterThan]: '0'},
                        isInBox     : {[QueryOp.equalTo]: true}
                    },
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }

    getFeatured(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.Featured,
                    pageSize: 5,
                    filters : {
                        cityCodes : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId: {[QueryOp.equalTo]: this.langId},
                        isPopular : {[QueryOp.equalTo]: true},
                    },
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }

    getFeatured2(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id        : Views.Featured2,
                    pageSize  : 9,
                    pageOffset: 5,
                    filters   : {
                        cityCodes   : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId  : {[QueryOp.equalTo]: this.langId},
                        bouquetPrice: {[QueryOp.greaterThan]: '0'},
                    },
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }

    /**
     * Get filter translates
     */
    getFilterTrs(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager.getService('FilterTranslate').loaded$
                .pipe(
                    tap(loaded =>
                    {
                        if (!loaded)
                        {
                            this._entityManager.getService('FilterTranslate').getAll();
                        }
                    }),
                    filter(loaded => loaded),
                    take(1)
                )
                .subscribe(value =>
                {
                    resolve(value);
                }, reject);
        });
    }

    /**
     * Get filter item translates
     */
    getFilterItemTrs(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager.getService('FilterItemTranslate').loaded$
                .pipe(
                    tap(loaded =>
                    {
                        if (!loaded)
                        {
                            this._entityManager.getService('FilterItemTranslate').getAll();
                        }
                    }),
                    filter(loaded => loaded),
                    take(1)
                )
                .subscribe(value =>
                {
                    resolve(value);
                }, reject);
        });
    }
}
