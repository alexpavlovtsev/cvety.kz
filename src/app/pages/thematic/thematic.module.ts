import { NgModule } from '@angular/core';

import { ThematicComponent } from './thematic.component';
import { RouterModule, Routes } from '@angular/router';
import { ThematicService } from './thematic.service';
import { SharedModule } from '@shared/shared.module';
import { ContentComponent } from './content/content.component';
import { ItemsComponent } from './content/items/items.component';
import { FiltersComponent } from './content/filters/filters.component';
import { TitleComponent } from './title/title.component';
import { ProgressComponent } from './progress/progress.component';
import { BouquetFilterModule } from '../../layout-elements/bouquet-filter/bouquet-filter.module';
import { BouquetTeaserModule } from '../../layout-elements/bouquet-teaser/bouquet-teaser.module';

const routes: Routes = [
    {
        path     : '',
        component: ThematicComponent,
        resolve  : {
            data: ThematicService
        }
    },
];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),

        SharedModule,

        BouquetFilterModule,
        BouquetTeaserModule,
    ],
    declarations: [
        ThematicComponent,
        ContentComponent,
        ItemsComponent,
        FiltersComponent,
        TitleComponent,
        ProgressComponent,
    ],
    providers   : [ThematicService]
})
export class ThematicModule
{
}
