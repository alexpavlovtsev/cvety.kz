import { Component, OnDestroy, OnInit } from '@angular/core';
import { EntityManagerService, ViewsService } from '@postgar/entity';

import { Views } from '../../../postgar-config/views';
import { Subject } from 'rxjs';
import { QueryOp } from '@postgar/types';
import { take, takeUntil } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { getRouterState, State } from '../../../store/reducers';

@Component({
    selector   : 'app-progress',
    templateUrl: './progress.component.html',
    styleUrls  : ['./progress.component.scss']
})
export class ProgressComponent implements OnInit, OnDestroy
{
    cityCode: string;
    totalCount: number;
    first: number;
    offset: number;
    langId: number;
    pageSize: number;
    viewId: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _store: Store<State>,
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService
    )
    {
        this.offset = 0;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngOnInit()
    {
        this._store.pipe(select(getRouterState))
            .pipe(take(1), takeUntil(this._unsubscribeAll))
            .subscribe(route =>
            {
                switch (true)
                {
                    case route.state.url.includes('in-box'):
                        this.offset = 0;
                        this.viewId = Views.InBox;
                        break;

                    case route.state.url.includes('populyarnye'):
                    case route.state.url.includes('featured'):
                        this.viewId = Views.Featured2;
                        this.offset = 5;
                        break;

                    case route.state.url.includes('extraordinary'):
                        this.offset = 0;
                        this.viewId = Views.Extraordinary;
                        break;

                    case route.state.url.includes('inexpensive'):
                        this.offset = 0;
                        this.viewId = Views.Inexpensive;
                        break;
                }

                this._viewsService.view(this.viewId)
                    .pipe(takeUntil(this._unsubscribeAll))
                    .subscribe(view =>
                    {
                        this.totalCount = view.totalCount;
                        this.pageSize   = view.pageSize;
                        this.first      = view.pageSize * (view.pageIndex + 1);
                    });
            });

        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.cityCode = node.code;
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.langId = node.id;
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    loadMore(): void
    {
        const data = {
            id        : this.viewId,
            pageSize  : this.first + 9,
            cityCodes : {anyEqualTo: this.cityCode},
            pageOffset: this.offset,
            languageId: {[QueryOp.equalTo]: this.langId}
        };
        this._entityManager
            .getService('BouquetTranslate')
            .getWithQuery(data);
    }
}
