import { Component, OnDestroy, OnInit } from '@angular/core';
import { forkJoin, Subject } from 'rxjs';
import { EntityManagerService, View, ViewsService } from '@postgar/entity';
import { Views } from '../../../postgar-config/views';
import { map, switchMap, takeUntil } from 'rxjs/operators';
import { Grammar } from '@shared/grammar';
import { Utils } from '@shared/utils';
import { select, Store } from '@ngrx/store';
import { getRouterState, State } from '../../../store/reducers';
import { MetatagService } from '@shared/services/metatag.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector   : 'app-title',
    templateUrl: './title.component.html',
    styleUrls  : ['./title.component.scss']
})
export class TitleComponent implements OnInit, OnDestroy
{
    citySecondName: string;
    sortDirection: string;
    language: any;
    translatePath: string;
    title: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
        private _store: Store<State>,
        private _metatagService: MetatagService,
        private _translateService: TranslateService
    )
    {
        this.translatePath = 'FRONT.TITLE';

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._store.pipe(select(getRouterState))
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(route =>
            {
                switch (true)
                {
                    // case route.state.url.includes('in-box'):
                    //     this.translatePath = 'FRONT.BOUQUETS_IN_A_BOX';
                    //     break;

                    case route.state.url.includes('featured'):
                    case route.state.url.includes('populyarnye'):
                        this.translatePath = 'FRONT.POPULAR_BOUQUETS';
                        break;

                    // case route.state.url.includes('extraordinary'):
                    //     this.translatePath = 'FRONT.EXCLUSIVE_BOUQUETS';
                    //     break;
                    //
                    // case route.state.url.includes('inexpensive'):
                    //     this.translatePath = 'FRONT.BUDGET_BOUQUETS';
                    //     break;
                }
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(
                switchMap((language: any) =>
                {
                    this.language = language;
                    return this._viewsService.firstNode(Views.City)
                        .pipe(
                            map(value =>
                            {
                                return language.id === 1 ? value.secondName : value.enName;
                            })
                        );
                }),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe(value =>
            {
                this.citySecondName = value;

                this.setTitle();
            });

        this._viewsService.view(Views.FrontPage)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((view: View) =>
            {
                this.sortDirection = view.sortDirection;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    changeOrderBy(sortDirection: string, event: any): void
    {
        Utils.stopPropagation(event);
        this.sortDirection = sortDirection;

        this._entityManager
            .getService('BouquetTranslate')
            .getWithQuery({
                id: Views.FrontPage,
                sortDirection,
            });
    }

    setTitle(): void
    {
        forkJoin(
            this._translateService.get(this.translatePath),
            this._translateService.get('FRONT.IN')
        ).subscribe(([value, value1]) =>
        {
            this.title = value + value1 + Grammar.upperCaseFirstLetter(this.citySecondName);
            this._metatagService.set('thematic', this.title);
        });
    }
}
