import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { EntityManagerService, ViewsService } from '@postgar/entity';
import { filter, take, tap } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { Views } from '../../postgar-config/views';
import { QueryOp } from '@postgar/types';
import { GraphQLClient } from '@postgar/graphql';

const BlocksQuery = `
query allBlocks {
  allBlocks {
    nodes {
      id
      bouquetIds
      content
      bouquets: bouquetTranslates {
        nodes {
          id
          url
          name
          bouquetId
          languageId
          bouquetByBouquetId {
            id
            mainImage
            mainPrice
          }
        }
      }
    }
  }
}`;

@Injectable()
export class FrontService implements Resolve<any>
{
    cityCode: string;
    langId: number;
    onBlockChange = new BehaviorSubject([]);

    /**
     * Constructor
     */
    constructor(
        private client: GraphQLClient,
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService
    )
    {
        this._viewsService.firstNode(Views.City)
            .subscribe((node: any) =>
            {
                this.cityCode = node.code;
            });

        this._viewsService.firstNode(Views.Language)
            .subscribe((node: any) =>
            {
                this.langId = node.id;
            });
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) =>
        {
            Promise.all([
                this.getFilterTrs(),
                this.getFilterItemTrs(),
                // this.getBouquets(),
                // this.getExtraordinary(),
                // this.getFeatured(),
                // this.getInBox(),
                // this.getInexpensive(),
                this.getFeedback(),
                this.getBlocks()
            ]).then(
                () =>
                {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get filter translates
     */
    getFilterTrs(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager.getService('FilterTranslate').loaded$
                .pipe(
                    tap(loaded =>
                    {
                        if (!loaded)
                        {
                            this._entityManager.getService('FilterTranslate').getAll();
                        }
                    }),
                    filter(loaded => loaded),
                    take(1)
                )
                .subscribe(value =>
                {
                    resolve(value);
                }, reject);
        });
    }

    getInexpensive(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.Inexpensive,
                    pageSize: 9,
                    filters : {
                        cityCodes   : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId  : {[QueryOp.equalTo]: this.langId},
                        bouquetPrice: {[QueryOp.greaterThan]: '0'},
                    },
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }

    getExtraordinary(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.Extraordinary,
                    pageSize: 9,
                    filters : {
                        cityCodes   : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId  : {[QueryOp.equalTo]: this.langId},
                        bouquetPrice: {[QueryOp.greaterThan]: '0'},
                    },
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }

    getInBox(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.InBox,
                    pageSize: 9,
                    filters : {
                        cityCodes   : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId  : {[QueryOp.equalTo]: this.langId},
                        bouquetPrice: {[QueryOp.greaterThan]: '0'},
                        isInBox     : {[QueryOp.equalTo]: true}
                    },
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }

    getFeatured(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.Featured,
                    pageSize: 5,
                    filters : {
                        cityCodes : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId: {[QueryOp.equalTo]: this.langId},
                        isPopular : {[QueryOp.equalTo]: true},
                    },
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }

    /**
     * Get filter item translates
     */
    getFilterItemTrs(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager.getService('FilterItemTranslate').loaded$
                .pipe(
                    tap(loaded =>
                    {
                        if (!loaded)
                        {
                            this._entityManager.getService('FilterItemTranslate').getAll();
                        }
                    }),
                    filter(loaded => loaded),
                    take(1)
                )
                .subscribe(value =>
                {
                    resolve(value);
                }, reject);
        });
    }

    /**
     * Get bouquets
     */
    getBouquets(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.FrontPage,
                    pageSize: 9,
                    filters : {
                        cityCodes   : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId  : {[QueryOp.equalTo]: this.langId},
                        bouquetPrice: {[QueryOp.greaterThan]: '0'}
                    },
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }

    getFeedback(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager
                .getService('Callback')
                .getWithQuery({
                    id      : Views.Callbacks,
                    pageSize: 8,
                })
                .pipe(take(1))
                .subscribe(() =>
                {
                    resolve(true);
                }, reject);
        });
    }

    getBlocks(): Promise<any>
    {
        return new Promise((resolve, reject) =>
        {
            this.client.get(BlocksQuery, {}).subscribe((value: any) =>
            {
                this.onBlockChange.next(value.allBlocks.nodes);

                resolve();
            });
        });
    }
}
