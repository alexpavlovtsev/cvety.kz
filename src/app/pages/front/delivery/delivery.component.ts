import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector   : 'app-delivery',
    templateUrl: './delivery.component.html',
    styleUrls  : ['./delivery.component.scss']
})
export class DeliveryComponent implements OnInit, OnDestroy
{
    path: string;
    langId: number;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService,
    )
    {
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.langId = value.id;
                this.path   = value.id === 1 ? `delivery/` : 'en/delivery/';
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    getUrl(url: string): string
    {
        return this.langId === 1 ? url : `/en/${url}/`;
    }
}
