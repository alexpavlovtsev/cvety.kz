const DEFAULT_VALUE = {
    ru: {title: '', content: ''},
    en: {title: '', content: ''},
    kz: {title: '', content: ''}
};

export class Block
{
    id: number;
    bouquetIds: number[];
    serializedData: string;
    content: object;
    bouquets: any[];

    constructor(data?: any)
    {
        data                = data || {};
        this.id             = data.id;
        this.bouquetIds     = data.bouquetIds || [];
        this.serializedData = data.serializedData;
        this.bouquets       = data.bouquets || [];
        this.setContent(data.content);
    }

    setContent(data: any): void
    {
        if (!data)
        {
            this.content = JSON.parse(JSON.stringify(DEFAULT_VALUE));
        }
        else if (typeof data === 'string')
        {
            this.content = JSON.parse(data);
        }
        else
        {
            this.content = data;
        }
    }

    getContent(): string
    {
        return JSON.stringify(this.content);
    }

    deleteBouquet(bouquet): void
    {
        if (this.bouquets.includes(bouquet))
        {
            this.bouquets.splice(this.bouquets.indexOf(bouquet), 1);
        }

        if (this.bouquetIds.includes(bouquet.bouquetId))
        {
            this.bouquetIds.splice(this.bouquetIds.indexOf(bouquet.bouquetId), 1);
        }
    }
}
