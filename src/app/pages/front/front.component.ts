import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ScriptService } from '@shared/services/script.service';
import { TranslationLoaderService } from '@shared/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

import { MetatagService } from '@shared/services/metatag.service';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { combineLatest, Subject } from 'rxjs';

@Component({
    selector   : 'app-front',
    templateUrl: './front.component.html',
    styleUrls  : ['./front.component.scss']
})
export class FrontComponent implements OnInit
{
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _translationLoaderService: TranslationLoaderService,
        private _scriptService: ScriptService,
        private _metatagService: MetatagService,
        private _viewsService: ViewsService,
    )
    {
        this._translationLoaderService.loadTranslations(english, russian);

        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        combineLatest(
            this._viewsService.firstNode(Views.Language),
            this._viewsService.firstNode(Views.City)
        )
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(([language, city]) =>
            {
                const data = {
                    title      : language.id === 1 ? city.metaTitle : city.enMetaTitle,
                    keywords   : language.id === 1 ? city.metaKeywords : city.enMetaKeywords,
                    description: language.id === 1 ? city.metaDescription : city.enMetaDescription
                };

                if (!!data.title)
                {
                    this._metatagService.setData(data);
                }
                else
                {
                    const title = language.id === 1
                        ? 'Доставка и заказ цветов'
                        : 'Delivery and order of flowers';
                    this._metatagService.set('bouquets', title);
                }
            });

        setTimeout(() =>
        {
            this.initScripts();
        }, 0);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.loadScript('popper').then(() =>
            {
                this._scriptService.load(['slick', 'jquery.formstyler', 'bootstrap']).then(() =>
                {
                    const settings = frontScriptData();
                    this._scriptService.appendScript(settings, 'front-settings');
                });
            });
        });
    }
}

export function frontScriptData(): string
{
    return `
  // filters

// toggle content
(function(){
    $('.js-toggle-content').on('click', function () {
        var _this = $(this),
            text = _this.find('.js-toggle-text'),
            el = _this.prev();
        if (!el.hasClass('activated')) {
            curHeight = el.height();
            el.addClass('activated');
        };
        var autoHeight = el.css('height', 'auto').height(),
            textHide = _this.data('text-hide'),
            textShow = _this.data('text-show');
        if (_this.hasClass('active')) {
            _this.removeClass('active');
            text.text(textShow);
            el.animate({height: curHeight}, 400);
        }
        else {
            _this.addClass('active');
            text.text(textHide);
            el.height(curHeight+20).animate({height: autoHeight}, 400);
        }
        return false;
    });
}());

  $(document).ready(function(){
    var mql = window.matchMedia('(min-width: 789px)');
if (mql.matches) {
    $(".cv_filters-box").addClass('show');
  }
  });

    $(".cv_filters-filterit").on('click', function(){
      $(".cv_filters-right").addClass("show");
      $(".cv_filters-right").before ('<div class="cv_wrapper"></div>');
      $(".cv_blur").addClass("blurit");

      $(".cv_wrapper").on('click', function(){
        $(".cv_filters-right").removeClass("show");
        $(".cv_blur").removeClass("blurit");
        $(this).remove();
      });
    });`;
}
