import { Component, OnDestroy, OnInit } from '@angular/core';
import { EntityManagerService, ViewsService } from '@postgar/entity';

import { Views } from '../../../postgar-config/views';
import { Subject } from 'rxjs';
import { QueryOp } from '@postgar/types';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector   : 'app-progress',
    templateUrl: './progress.component.html',
    styleUrls  : ['./progress.component.scss']
})
export class ProgressComponent implements OnInit, OnDestroy
{
    cityCode: string;
    totalCount: number;
    first: number;
    langId: number;
    pageSize: number;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngOnInit()
    {
        this._viewsService.view(Views.FrontPage)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(view =>
            {
                this.totalCount = view.totalCount;
                this.pageSize   = view.pageSize;
                this.first      = view.pageSize * (view.pageIndex + 1);
            });

        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.cityCode = node.code;
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.langId = node.id;
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    loadMore(): void
    {
        const data = {
            id        : Views.FrontPage,
            pageSize  : this.first + 9,
            cityCodes : {anyEqualTo: this.cityCode},
            languageId: {[QueryOp.equalTo]: this.langId}
        };
        this._entityManager
            .getService('BouquetTranslate')
            .getWithQuery(data);
    }
}
