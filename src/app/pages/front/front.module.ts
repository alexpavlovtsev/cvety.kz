import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '@shared/shared.module';

import { FrontComponent } from './front.component';
import { FrontService } from './front.service';
import { TitleComponent } from './title/title.component';
import { ContentComponent } from './content/content.component';
import { FiltersComponent } from './content/filters/filters.component';
import { ItemsComponent } from './content/items/items.component';
import { ProgressComponent } from './progress/progress.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { DeliveryBlockModule } from '../../layout-elements/delivery-block/delivery-block.module';
import { BouquetTeaserModule } from '../../layout-elements/bouquet-teaser/bouquet-teaser.module';
import { BouquetFilterModule } from '../../layout-elements/bouquet-filter/bouquet-filter.module';
import { ReviewsBlockModule } from '../../layout-elements/reviews-block/reviews-block.module';

const routes: Routes = [
    {
        path     : '',
        component: FrontComponent,
        resolve  : {
            data: FrontService
        }
    },
];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),

        SharedModule,
        DeliveryBlockModule,
        ReviewsBlockModule,

        BouquetFilterModule,
        BouquetTeaserModule,
    ],
    declarations: [
        FrontComponent,
        TitleComponent,
        ContentComponent,
        FiltersComponent,
        ItemsComponent,
        ProgressComponent,
        DeliveryComponent,
        FeedbackComponent
    ],
    providers   : [FrontService]
})
export class FrontModule
{
}
