import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { MAIN_CITY_CODE } from '../../../../config';
import { FrontService } from '../../front.service';
import { Block } from '../../block';

@Component({
    selector   : 'app-items',
    templateUrl: './items.component.html',
    styleUrls  : ['./items.component.scss']
})
export class ItemsComponent implements OnInit, OnDestroy
{
    bouquetTr$: Observable<any[]>;
    langId: number;
    prefix: string;
    themPrefix: string;
    city: string;
    currency: any;
    language: any;
    totalFeatured: number;
    blocks: Block[];

    inbox: any[];
    featured: any[];
    extraordinary: any[];
    inexpensive: any[];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _frontService: FrontService,
        private _viewsService: ViewsService
    )
    {
        this.bouquetTr$    = this._viewsService.nodes(Views.FrontPage);
        this.currency      = {};
        this.language      = {};
        this.inbox         = [];
        this.featured      = [];
        this.extraordinary = [];
        this.inexpensive   = [];

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.langId     = node.id;
                this.prefix     = this.langId === 1 ? '/bukety/' : '/en/bouquets/';
                this.themPrefix = this.langId === 1 ? '/' : '/en/';
                this.language   = node;

                this._frontService.onBlockChange
                    .pipe(takeUntil(this._unsubscribeAll))
                    .subscribe((value: any[]) =>
                    {
                        this.blocks = value.map(item =>
                        {
                            const block    = new Block(item);
                            block.bouquets = (item.bouquets.nodes || []).filter(node => node.languageId === this.langId);

                            return block;
                        });
                    });
            });

        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.city = node.code === MAIN_CITY_CODE ? '/' : `${node.code}/`;
            });

        this._viewsService.firstNode(Views.Currency)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.currency = node;
            });

        this._viewsService.nodes(Views.InBox)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any[]) =>
            {
                this.inbox = [...nodes.splice(0, 5)];
            });

        this._viewsService.view(Views.Featured)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(view =>
            {
                this.totalFeatured = view.totalCount;
            });

        this._viewsService.nodes(Views.Featured)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any[]) =>
            {
                this.featured = [...nodes.splice(0, 5)];
            });

        this._viewsService.nodes(Views.Extraordinary)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any[]) =>
            {
                this.extraordinary = [...nodes.splice(0, 5)];
            });

        this._viewsService.nodes(Views.Inexpensive)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any[]) =>
            {
                this.inexpensive = [...nodes.splice(0, 5)];
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    randomRating(id: number): string
    {
        const lastDigit = id.toString().split('').pop();

        return lastDigit === '0' ? '5.0' : `4.${lastDigit}`;
    }
}
