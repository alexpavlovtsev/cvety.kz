import { AfterViewInit, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit, PLATFORM_ID } from '@angular/core';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Observable, Subject } from 'rxjs';
import { map, take, takeUntil } from 'rxjs/operators';
import { Views } from '../../../../postgar-config/views';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { select, Store } from '@ngrx/store';
import { getRouterState, State } from '../../../../store/reducers';
import { FilterHelperService } from '../../../../shared/services/filter-helper.service';
import { LocationService } from '@shared/services/location.service';
import { deprecateFilterItemTrs, deprecateFilterTrs } from '@shared/utils';

@Component({
    selector   : 'app-filters',
    templateUrl: './filters.component.html',
    styleUrls  : ['./filters.component.scss']
})
export class FiltersComponent implements OnInit, OnDestroy, AfterViewInit
{
    filtersTr$: Observable<any[]>;
    filterItemsTr$: Observable<any[]>;
    langId: number;
    language: any;
    activeFilterItems: any;
    filterBoxShow: boolean;
    path: string;
    urls: any;
    city: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _entityManager: EntityManagerService,
        @Inject(DOCUMENT) private document,
        private _viewsService: ViewsService,
        @Inject(PLATFORM_ID) private platformId: Object,
        private _cd: ChangeDetectorRef,
        private _store: Store<State>,
        private _filterHelper: FilterHelperService,
        public _locationService: LocationService
    )
    {
        this.filtersTr$     = this._entityManager.getService('FilterTranslate').entities$
            .pipe(
                map(items => deprecateFilterTrs(items))
            );
        this.filterItemsTr$ = this._entityManager.getService('FilterItemTranslate').entities$
            .pipe(
                map((items: any[]) => deprecateFilterItemTrs(items))
            );

        this.urls              = {};
        this.activeFilterItems = {};

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngOnInit()
    {
        this._store.pipe(select(getRouterState))
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(route =>
            {
                this.urls = this._filterHelper.getUrls2('');
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.language = node;
                this.langId   = node.id;
            });

        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.city = node;
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    ngAfterViewInit(): void
    {
        if (isPlatformBrowser(this.platformId))
        {
            const mql = window.matchMedia('(min-width: 789px)');
            if (mql.matches)
            {
                this.filterBoxShow = true;
                this._cd.detectChanges();
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

}
