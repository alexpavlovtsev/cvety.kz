import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';
import { MAIN_CITY_CODE } from '../../../../config';
import { MetaService } from '@shared/services/meta.service';

@Component({
    selector   : 'app-items',
    templateUrl: './items.component.html',
    styleUrls  : ['./items.component.scss']
})
export class ItemsComponent implements OnInit, OnDestroy
{
    bouquetTr$: Observable<any[]>;
    langId: number;
    prefix: string;
    city: string;
    currency: any;
    language: any;
    items: any[];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _metaService: MetaService,
        private _viewsService: ViewsService
    )
    {
        this.bouquetTr$ = this._viewsService.nodes(Views.Catalog);
        this.currency   = {};
        this.language   = {};
        this.items      = [];

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        this.bouquetTr$.pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any[]) =>
            {
                this.items = nodes;

                if (this.items.length < 3)
                {
                    this._metaService.setTag('robots', 'noindex, follow');
                }
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.langId   = node.id;
                this.prefix   = this.langId === 1 ? '/bukety/' : '/en/bouquets/';
                this.language = node;
            });

        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.city = node.code === MAIN_CITY_CODE ? '/' : `${node.code}/`;
            });

        this._viewsService.firstNode(Views.Currency)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.currency = node;
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    randomRating(id: number): string
    {
        const lastDigit = id.toString().split('').pop();

        return lastDigit === '0' ? '5.0' : `4.${lastDigit}`;
    }
}
