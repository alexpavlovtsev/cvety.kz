import {
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Inject,
    OnDestroy,
    OnInit,
    PLATFORM_ID
} from '@angular/core';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { combineLatest, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { Views } from '../../../../postgar-config/views';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { select, Store } from '@ngrx/store';
import { getRouterState, State } from '../../../../store/reducers';
import { FilterHelperService } from '../../../../shared/services/filter-helper.service';
import { LocationService } from '@shared/services/location.service';
import { deprecateFilterItemTrs, deprecateFilterTrs } from '@shared/utils';
import { Go } from '../../../../store/actions';
import { FilterInfoService, filterItemConvert } from '@shared/services/filter-info.service';

@Component({
    selector       : 'app-filters',
    templateUrl    : './filters.component.html',
    styleUrls      : ['./filters.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FiltersComponent implements OnInit, OnDestroy, AfterViewInit
{
    counts: object;
    filtersTr$: Observable<any[]>;
    filterItemsTr$: Observable<any[]>;
    langId: number;
    activeFilterItems: any;
    filterBoxShow: boolean;
    path: string;
    urls: any;
    activeFilters: any;
    language: any;
    city: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _entityManager: EntityManagerService,
        @Inject(DOCUMENT) private document,
        private _viewsService: ViewsService,
        @Inject(PLATFORM_ID) private platformId: Object,
        private _cd: ChangeDetectorRef,
        private _store: Store<State>,
        private _filterHelper: FilterHelperService,
        public _locationService: LocationService,
    )
    {
        this.filtersTr$     = this._entityManager.getService('FilterTranslate').entities$
            .pipe(
                map(items => deprecateFilterTrs(items))
            );
        this.filterItemsTr$ = this._entityManager.getService('FilterItemTranslate').entities$
            .pipe(
                map(items => deprecateFilterItemTrs(items))
            );

        this.activeFilters     = {};
        this.urls              = {};
        this.activeFilterItems = {};

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngOnInit()
    {
        this._viewsService.firstNode(Views.City)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.city = node;
            });

        combineLatest(
            this._store.pipe(select(getRouterState)),
            this._viewsService.firstNode(Views.Language)
        )
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(([route, language]) =>
            {
                this.language = language;
                this.langId   = language.id;
                this.path     = route.state.params.catalog;
                this.urls     = this._filterHelper.getUrls2(this.path);

                this.activeFilterItems = this._filterHelper
                    .getActiveIds(this.path)
                    .reduce((newObj, id) =>
                    {
                        return {
                            ...newObj,
                            [id]: true
                        };
                    }, {});

                this.activeFilters = this._filterHelper
                    .getActiveItems(this.path, this.langId)
                    .reduce((newObj, item) =>
                    {
                        return {
                            ...newObj,
                            [item.filterItemByFilterItemId.filterId]: true
                        };
                    }, {});

                this.refresh();
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    ngAfterViewInit(): void
    {
        if (isPlatformBrowser(this.platformId))
        {
            const mql = window.matchMedia('(min-width: 789px)');
            if (mql.matches)
            {
                this.filterBoxShow = true;
                this.refresh();
            }
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    goTo(url: string, filterItemId: number): void
    {
        if (this.isActive(filterItemId))
        {
            this.disableFilterItem(filterItemId);
        }
        else
        {
            this._store.dispatch(new Go({path: [url]}));
        }
    }

    isActive(filterItemId: number): boolean
    {
        return this.activeFilterItems.hasOwnProperty(filterItemId);
    }

    disableFilterItem(filterItemId: number): void
    {
        const activeItems = this._filterHelper
            .getActiveItems(this.path, this.langId)
            .filter(item => item.filterItemId !== filterItemId)
            .map(value => filterItemConvert(value));

        const url = FilterInfoService.fullData(activeItems, this.language, this.city).url;
        this._store.dispatch(new Go({path: [url]}));
    }

    refresh(): void
    {
        this._cd.detectChanges();
    }
}
