import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { Views } from '../../postgar-config/views';
import { Observable } from 'rxjs';
import { filter, take, tap } from 'rxjs/operators';
import { FilterHelperService } from '../../shared/services/filter-helper.service';
import { QueryOp } from '@postgar/types';
import { LocationService } from '@shared/services/location.service';
import { BouquetCounterService } from '@shared/services/bouquet-counter.service';

@Injectable({
    providedIn: 'root'
})
export class CatalogService implements Resolve<any>
{
    params: any;
    cityCode: string;
    langId: number;
    filterItems: any;

    /**
     * Constructor
     */
    constructor(
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
        private _filterService: FilterHelperService,
        private _locationService: LocationService,
        private _bouquetCounterService: BouquetCounterService
    )
    {
        this._viewsService.firstNode(Views.City)
            .subscribe((node: any) =>
            {
                this.cityCode = node.code;
            });

        this._viewsService.firstNode(Views.Language)
            .subscribe((node: any) =>
            {
                this.langId = node.id;
            });
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.params = route.params;
        this._bouquetCounterService.run(this.params.catalog);

        return new Promise((resolve, reject) =>
        {
            Promise.all([
                this.getFilterTrs(),
                this.getBouquets(),
                this.getMinPrice(),
            ]).then(
                () =>
                {
                    resolve();
                    if (!this._filterService.checkUrl(this.params.catalog))
                    {
                        this._locationService.goTo404();
                    }
                },
                reject
            );
        });
    }

    /**
     * Get filter translates
     */
    getFilterTrs(): Promise<boolean>
    {
        return new Promise((resolve, reject) =>
        {
            this._entityManager.getService('FilterTranslate').loaded$
                .pipe(
                    tap(loaded =>
                    {
                        if (!loaded)
                        {
                            this._entityManager.getService('FilterTranslate').getAll();
                        }
                    }),
                    filter(loaded => loaded),
                    take(1)
                )
                .subscribe(value =>
                {
                    resolve(value);
                }, reject);
        });
    }

    /**
     * Get bouquets
     */
    getBouquets(): Promise<boolean>
    {
        return new Promise((resolve) =>
        {
            const activeIdsQuery = this._filterService
                .getActiveIds(this.params.catalog)
                .map(id =>
                {
                    return {categoriesIds: {anyEqualTo: id}};
                });

            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.Catalog,
                    pageSize: 9,
                    filters : {
                        and         : activeIdsQuery,
                        cityCodes   : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId  : {[QueryOp.equalTo]: this.langId},
                        bouquetPrice: {[QueryOp.greaterThan]: '0'}
                    }
                })
                .subscribe(() =>
                {
                    resolve();
                });
        });
    }

    getMinPrice(): Promise<boolean>
    {
        return new Promise((resolve) =>
        {
            const activeIdsQuery = this._filterService
                .getActiveIds(this.params.catalog)
                .map(id =>
                {
                    return {categoriesIds: {anyEqualTo: id}};
                });

            this._entityManager
                .getService('BouquetTranslate')
                .getWithQuery({
                    id      : Views.MinPrice,
                    pageSize: 1,
                    filters : {
                        and          : activeIdsQuery,
                        cityCodes    : {[QueryOp.anyEqualTo]: this.cityCode},
                        languageId   : {[QueryOp.equalTo]: this.langId},
                        bouquetPrice : {[QueryOp.greaterThan]: '0'},
                    },
                    sortActive   : 'bouquetPrice',
                    sortDirection: 'asc'
                })
                .subscribe(() =>
                {
                    resolve();
                });
        });
    }
}


