import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { ViewsService } from '@postgar/entity';
import { Views } from '../../../postgar-config/views';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector   : 'app-feedback',
    templateUrl: './feedback.component.html',
    styleUrls  : ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit, OnDestroy
{
    path: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _viewsService: ViewsService,
    )
    {
        this._unsubscribeAll = new Subject();
    }

    ngOnInit()
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.path = value.id === 1 ? `reviews/` : 'en/reviews/';
            });
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
