import { Component, OnDestroy, OnInit } from '@angular/core';
import { combineLatest, Observable, Subject } from 'rxjs';
import { EntityManagerService, View, ViewsService } from '@postgar/entity';
import { Views } from '../../../postgar-config/views';
import { map, switchMap, takeUntil } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { getRouterState, State } from '../../../store/reducers';
import { FilterHelperService } from '@shared/services/filter-helper.service';
import { FilterInfoService, filterItemConvert } from '@shared/services/filter-info.service';
import { MetatagService } from '@shared/services/metatag.service';
import { Grammar } from '@shared/grammar';
import { Utils } from '@shared/utils';

@Component({
    selector   : 'app-title',
    templateUrl: './title.component.html',
    styleUrls  : ['./title.component.scss']
})
export class TitleComponent implements OnInit, OnDestroy
{
    title: string;
    citySecondName: string;
    sortDirection: string;
    language: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     */
    constructor(
        private _store: Store<State>,
        private _viewsService: ViewsService,
        private _entityManager: EntityManagerService,
        private _filterHelper: FilterHelperService,
        private _metatagService: MetatagService,
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this._viewsService.firstNode(Views.Language)
            .pipe(
                switchMap((language: any) =>
                {
                    this.language = language;
                    return this._viewsService.firstNode(Views.City)
                        .pipe(
                            map(value =>
                            {
                                return language.id === 1 ? value.secondName : value.enName;
                            })
                        );
                }),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe(value =>
            {
                this.citySecondName = value;
            });

        this._viewsService.view(Views.Catalog)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((view: View) =>
            {
                this.sortDirection = view.sortDirection;
            });

        combineLatest(
            this._viewsService.firstNode(Views.Language),
            this._viewsService.firstNode(Views.City)
        )
            .pipe(
                switchMap(([language, city]) =>
                {
                    return this._store.pipe(select(getRouterState))
                        .pipe(
                            map(route =>
                            {
                                const path = route.state.params.catalog;

                                const activeFilterItems = this._filterHelper
                                    .getActiveItems(path, language.id)
                                    .map(item => filterItemConvert(item));

                                if (activeFilterItems.length === 0)
                                {
                                    return null;
                                }

                                return FilterInfoService.fullData(activeFilterItems, language, city).label;
                            })
                        );
                })
            )
            .subscribe((value: string) =>
            {
                this.title = value;
                this._metatagService.set('catalog', value);
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    changeOrderBy(sortDirection: string, event: any): void
    {
        Utils.stopPropagation(event);
        this.sortDirection = sortDirection;

        this._entityManager
            .getService('BouquetTranslate')
            .getWithQuery({
                id: Views.Catalog,
                sortDirection,
            });
    }
}
