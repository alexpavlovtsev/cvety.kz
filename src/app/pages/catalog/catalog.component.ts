import { AfterViewInit, Component } from '@angular/core';
import { ScriptService } from '@shared/services/script.service';
import { TranslationLoaderService } from '@shared/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';
import { frontScriptData } from '../front/front.component';

@Component({
    selector   : 'app-catalog',
    templateUrl: './catalog.component.html',
    styleUrls  : ['./catalog.component.scss']
})
export class CatalogComponent implements AfterViewInit
{
    /**
     * Constructor
     *
     * @param _translationLoaderService
     * @param _scriptService
     */
    constructor(
        private _translationLoaderService: TranslationLoaderService,
        private _scriptService: ScriptService,
    )
    {
        this._translationLoaderService.loadTranslations(english, russian);
    }

    ngAfterViewInit(): void
    {
        setTimeout(() =>
        {
            this.initScripts();
        }, 0);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.loadScript('popper').then(() =>
            {
                this._scriptService.load(['slick', 'jquery.formstyler', 'bootstrap']).then(() =>
                {
                    const settings = frontScriptData();
                    this._scriptService.appendScript(settings, 'front-settings');
                });
            });
        });
    }
}
