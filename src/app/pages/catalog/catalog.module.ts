import { NgModule } from '@angular/core';

import { CatalogComponent } from './catalog.component';
import { ContentComponent } from './content/content.component';
import { FiltersComponent } from './content/filters/filters.component';
import { ItemsComponent } from './content/items/items.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { ProgressComponent } from './progress/progress.component';
import { TitleComponent } from './title/title.component';
import { SharedModule } from '@shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { CatalogService } from './catalog.service';
import { DeliveryBlockModule } from '../../layout-elements/delivery-block/delivery-block.module';
import { BouquetTeaserModule } from '../../layout-elements/bouquet-teaser/bouquet-teaser.module';
import { BouquetFilterModule } from '../../layout-elements/bouquet-filter/bouquet-filter.module';

const routes: Routes = [
    {
        path     : '',
        component: CatalogComponent,
        resolve  : {
            data: CatalogService
        }
    },
];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),

        SharedModule,
        DeliveryBlockModule,

        BouquetFilterModule,
        BouquetTeaserModule,
    ],
    declarations: [
        CatalogComponent,
        ContentComponent,
        FiltersComponent,
        ItemsComponent,
        DeliveryComponent,
        FeedbackComponent,
        ProgressComponent,
        TitleComponent
    ]
})
export class CatalogModule
{
}
