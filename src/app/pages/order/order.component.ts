import { AfterViewInit, Component, ElementRef, Inject, OnDestroy, OnInit, PLATFORM_ID, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Kazkom } from './kazkom';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

import * as moment from 'moment';

import { IMyDateModel, IMyOptions } from 'mydatepicker';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ScriptService } from '@shared/services/script.service';
import { State } from '../../store/reducers';
import { Store } from '@ngrx/store';
import { EntityManagerService, ViewsService } from '@postgar/entity';
import { TranslationLoaderService } from '@shared/services/translation-loader.service';
import { datePickerFormat } from './date.model';
import { Go } from '../../store/actions';

import 'rxjs/add/operator/toPromise';
import { LocationService } from '@shared/services/location.service';
import { Views } from '../../postgar-config/views';
import { delay, take, takeUntil } from 'rxjs/operators';
import { MetaService } from '@shared/services/meta.service';
import { frontScriptData } from '../front/front.component';
import { createOrder, OrderService } from './order.service';

declare const paypal: any;
declare const cp: any;

@Component({
    selector   : 'app-order',
    templateUrl: './order.component.html',
    styleUrls  : ['./order.component.scss']
})
export class OrderComponent implements OnInit, OnDestroy, AfterViewInit
{
    @ViewChild('kazkomSubmit') kazkomSubmit: ElementRef;

    cityCode: string;
    kazkom: Kazkom;
    orderForm: FormGroup;
    myDatePickerOptions: IMyOptions;
    datePickerOpen: boolean;
    paymentType: string;
    orderId: number;
    currency: any;
    currencies: any[];
    senderEmail: string;
    deliveryType: string;
    deliveryTypeOpen: boolean;
    language: any;
    pricingFactor = 1;

    total: number;
    totalUSD: number;
    displayTotal: number;

    bouquets: any[];
    gifts: any[];
    makeInBox: any[];
    addBoxRaffaello: any[];

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _entityManager: EntityManagerService,
        private _translationLoaderService: TranslationLoaderService,
        private _viewsService: ViewsService,
        private _store: Store<State>,
        private formBuilder: FormBuilder,
        @Inject(PLATFORM_ID) private platformId: Object,
        private _scriptService: ScriptService,
        private _httpClient: HttpClient,
        public locationService: LocationService,
        private _metaService: MetaService,
        private _orderService: OrderService
    )
    {
        // Set the defaults
        this._translationLoaderService.loadTranslations(english, russian);

        this.kazkom              = new Kazkom(null);
        this.myDatePickerOptions = datePickerFormat;
        this.datePickerOpen      = false;
        this.deliveryTypeOpen    = false;
        this.paymentType         = 'kazkom';
        this.bouquets            = [];
        this.gifts               = [];
        this.total               = 0;
        this.totalUSD            = 0;
        this.orderForm           = this.createOrderForm();
        this.deliveryType        = 'В течении дня';
        this.currency            = {};
        this.language            = {};

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngOnInit()
    {
        this._viewsService.firstNode(Views.City).subscribe((node: any) =>
        {
            const city = node || {};
            if (city.pricingFactor)
            {
                this.pricingFactor = city.pricingFactor;
            }

            this.cityCode = node.code;
        });

        this._entityManager.getService('Currency').entities$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((nodes: any[]) =>
            {
                this.currencies = nodes;
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.kazkom.Language = node.code === 'ru' ? 'rus' : 'eng';
            });

        this._viewsService.firstNode(Views.Currency)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.currency = node;
            });

        // update payment type
        this.orderForm.controls['pay'].valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.paymentType = value;
            });

        // update sender email
        this.orderForm.controls['senderEmail'].valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(value =>
            {
                this.senderEmail = value;
            });

        this._entityManager.getService('Basket').entities$
            .pipe(delay(300), take(1), takeUntil(this._unsubscribeAll))
            .subscribe((items: any[]) =>
            {
                const usd            = this.currencies.find(item => item.code === 'USD');
                this.bouquets        = items.filter(item => item.id.includes('-bouquet'));
                this.gifts           = items.filter(item => item.id.includes('-gift'));
                this.makeInBox       = items.filter(item => item.id.includes('_MAKE_IN_BOX'));
                this.addBoxRaffaello = items.filter(item => item.id.includes('_ADD_BOX_RAFFAELLO'));

                this.total = items.reduce((sum, item) =>
                {
                    return sum + pricePrepared(item.price, this.currency, item.isMore, this.pricingFactor);
                }, 0);

                this.displayTotal = items.reduce((sum, item) =>
                {
                    return sum + parseFloat(item.price);
                }, 0);

                this.totalUSD = items.reduce((sum, item) =>
                {
                    return sum + pricePrepared(item.price, usd, item.isMore, this.pricingFactor);
                }, 0);

                if (items.length === 0)
                {
                    this._store.dispatch(new Go({path: [this.locationService.baseUrl()]}));
                }
            });

        this._viewsService.firstNode(Views.Language)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((node: any) =>
            {
                this.language = node;
            });
    }

    ngAfterViewInit(): void
    {
        setTimeout(() =>
        {
            this.initScripts();
        }, 0);
    }

    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create order form
     * @returns {FormGroup}
     */
    createOrderForm()
    {
        return this.formBuilder.group({
            postcardText   : [null],
            // receiveMyself     : [false],
            recipientName  : ['', Validators.required],
            recipientPhone : [null, Validators.required],
            // apartmentAndOffice: [null],
            deliveryDate   : [moment().format('DD.MM.YYYY')],
            deliveryTime   : [null],
            deliveryAddress: [null, Validators.required],
            senderName     : ['', Validators.required],
            senderPhone    : [''],
            // makePhoto         : [false],
            senderEmail    : [null],
            // additionalInform  : [null],
            pay            : ['kazkom']
        });
    }

    getFormValidationErrors(): void
    {
        Object.keys(this.orderForm.controls).forEach(key =>
        {

            const controlErrors: ValidationErrors = this.orderForm.get(key).errors;
            if (controlErrors != null)
            {
                Object.keys(controlErrors).forEach(keyError =>
                {
                    console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                });
            }
        });
    }

    /**
     * Set delivery date
     * @param {IMyDateModel} event
     */
    setDate(event: IMyDateModel): void
    {
        this.orderForm.controls['deliveryDate'].setValue(event.formatted);
        this.datePickerOpen = false;
    }

    cardPay(): void
    {
        const widget = new cp.CloudPayments();
        widget.charge({
                publicId   : 'pk_fea24daf6a95efa283d8bc4dbd808',  // id из личного кабинета
                description: 'Оплата заказа', // назначение
                amount     : this.total, // сумма
                currency   : this.currency.code, // валюта
                // invoiceId  : '1234567', // номер заказа  (необязательно)
                // accountId  : 'user@example.com', // идентификатор плательщика (необязательно)
                data       : {
                    // myProp: 'myProp value' // произвольный набор параметров
                }
            },
            (options) =>
            {
                // success
                // действие при успешной оплате
                this.saveOrder();
            },
            (reason, options) =>
            {
                // fail
                // действие при неуспешной оплате
                this.orderError();
            });
    }

    /**
     * Save order
     */
    saveOrder(): void
    {
        const orderData        = this.orderForm.getRawValue();
        orderData.currencyCode = this.currency.code;
        orderData.amount       = this.total;
        // orderData.orderId              = this.orderId;
        orderData.city         = this.cityCode;

        // set status success if paypal
        orderData.paymentResult = this.paymentType === 'paypal';

        const products = this.bouquets.map((product) =>
        {
            return {
                id       : product.id,
                isMore_30: product.isMore === 30,
                isMore_60: product.isMore === 60,
                price    : pricePrepared(product.price, this.currency, product.isMore, this.pricingFactor),
                name     : product.name,
                mainImage: product.mainImage
            };
        });

        orderData.bouquetIds     = this.bouquets.map((product) => product.id);
        orderData.productsImages = this.bouquets.map((product) => product.mainImage);

        orderData.productsdata = JSON.stringify(products);
        orderData.deliveryDate = moment(orderData.deliveryDate, 'DD.MM.YYYY').format('YYYY-MM-DD');

        if (orderData.deliveryTime && orderData.deliveryTime.length > 0)
        {
            orderData.deliveryDate = orderData.deliveryDate + `T` + orderData.deliveryTime;
        }

        delete orderData.deliveryTime;

        this._orderService.post(
            createOrder(orderData)
        ).subscribe((response: any) =>
        {
            this.orderComplete(response.data['newOrder']['order']);
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Init scripts
     */
    private initScripts(): void
    {
        this._scriptService.loadScript('jquery').then(() =>
        {
            this._scriptService.load(['slick', 'popper', 'jquery.formstyler', 'bootstrap', 'paypalCheckout'])
                .then(() =>
                {
                    this.renderButton();

                    const settings = frontScriptData();
                    this._scriptService.appendScript(settings, 'front-settings');
                });
        });
    }

    /**
     * After saving the data to the database
     */
    private orderComplete(order: any): void
    {
        this._entityManager.getService('Basket').clearCache();

        switch (this.paymentType)
        {
            case 'kazkom':

                this.orderId = order.id;
                this.kazkom  = new Kazkom(this.orderId);

                // Get kazkom data
                this.getKazkomData(this.orderId, this.total, this.currency.id)
                    .then(result =>
                    {
                        this.kazkom.Signed_Order_B64 = result.data;

                        setTimeout(() =>
                        {
                            this.kazkomSubmit.nativeElement.click();
                        }, 100);
                    })
                    .catch(error => console.log(error));

                break;

            case 'paypal':

                const path = `${this.locationService.baseUrl()}payment/success/${this.orderId}`;
                this._store.dispatch(new Go({path: [path]}));

                break;
        }
    }

    /**
     * Render paypal payment button
     */
    private renderButton(): void
    {
        const price = this.totalUSD;

        paypal.Button.render({

            env: 'production', // sandbox | production

            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            client: {
                sandbox   : 'Aa5os1KFMvByJrSeW4ckTJ_cZrbVo3yBDuybpyqznYxUeJaGoCk8HYF3Jn0Bx9lFqzL3JJPNzTvuoJZf',
                production: 'Adj-RreD5F9ECQM8s7W06N8W6Wfsyxcbwv9fxBYWcpmJiDjFwHRODFPAJExL9TkFnx-bRxctbL7Ro4qA'
            },

            style: {
                label : 'pay',  // checkout | credit | pay | buynow | generic
                size  : 'responsive', // small | medium | large | responsive
                height: 'small',
                shape : 'rect',   // pill | rect
                color : 'black'   // gold | blue | silver | black
            },

            locale: this.language.id === 1 ? 'ru_RU' : 'en_US',

            // Show the buyer a 'Pay Now' button in the checkout flow
            commit: true,

            // payment() is called when the button is clicked
            payment: (data, actions) =>
            {

                // Make a call to the REST api to create the payment
                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: {total: price, currency: 'USD'}
                            }
                        ]
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: (data, actions) =>
            {

                // Make a call to the REST api to execute the payment
                return actions.payment.execute().then(() =>
                {
                    console.log('payment data', data);
                    this.saveOrder();
                });
            },

            onCancel: (data, actions) =>
            {
                /*
                 * Buyer cancelled the payment
                 */
                console.log('onCancel');
                this.orderError();
            },

            onError: (err) =>
            {
                /*
                 * An error occurred during the transaction
                 */
                console.log('onError');
                this.orderError();
            }

        }, '#paypal-button-container');
    }

    /**
     * Go to error order
     */
    private orderError(): void
    {
        this._store.dispatch(new Go({path: [`${this.locationService.baseUrl()}/payment/error/`]}));
    }

    /**
     * Get encoded kazkom data
     * @param {number} orderId
     * @param {number} amount
     * @param {number} currencyId
     * @returns {Promise<any>}
     */
    private getKazkomData(orderId: number, amount: number, currencyId: number): Promise<any>
    {
        const obj = {
            'order_id'   : orderId,
            'amount'     : amount,
            'currency_id': currencyId
        };

        return this._httpClient.post('https://pay.leken.kz/test/paytest/get.php', obj)
            .toPromise();
    }
}

function pricePrepared(value: any, currency: any, isMore = 0, pricingFactor): number
{
    value             = parseFloat(value);
    let price: number = value / currency.exchange;

    if (isMore === 60)
    {
        price = price + (price * 0.6);
    } else if (isMore === 30)
    {
        price = price + (price * 0.3);
    }

    return Math.round((price * pricingFactor) * 100) / 100;
}
