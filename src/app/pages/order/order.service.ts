import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export interface GraphQlHttpData
{
    query: string;
    variables: any;
    operationName: string;
}

@Injectable({
  providedIn: 'root'
})
export class OrderService  implements Resolve<any>
{

    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpClient,
    )
    {
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        return new Promise((resolve, reject) =>
        {

            Promise.all([
                this.getData()
            ]).then(
                () =>
                {
                    resolve();
                },
                reject
            );
        });
    }

    getData(): Promise<any>
    {
        return new Promise((resolve, reject) =>
        {
            resolve();
        });
    }

    post(body: GraphQlHttpData): Observable<any>
    {
        const httpOptions = this.httpOptions();

        return this._httpClient.post('https://floracrm.com:5209/graphql', {...body}, httpOptions);
    }

    private httpOptions(): {headers: HttpHeaders}
    {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
    }
}

export function createOrder(data: any)
{
    return {
        query        : `
        mutation newOrder($input: NewOrderInput!) {
          newOrder(input: $input) {
            order {
              id
            }
          }
        }
        `,
        variables    : {
            input: {
                newOrder: data
            }
        },
        operationName: 'newOrder'
    };
}
