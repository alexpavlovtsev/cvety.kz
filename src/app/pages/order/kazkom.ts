export class Kazkom
{
    actionUrlTest: string;
    actionUrl: string;
    Signed_Order_B64: string;
    email: string;
    Language: string;

    BackLink: string;
    FailureBackLink: string;

    PostLink: string;
    FailurePostLink: string;

    constructor(orderId: number)
    {
        this.actionUrlTest = 'https://testpay.kkb.kz/jsp/process/logon.jsp';
        this.actionUrl     = 'https://epay.kkb.kz/jsp/process/logon.jsp';

        this.BackLink        = 'https://leken.kz/payment/success/' + orderId;
        this.FailureBackLink = 'https://leken.kz/payment/error/' + orderId;

        this.PostLink = 'http://floracrm.com:1337/kazkom-leken';
    }
}
