export const locale = {
    lang: 'ru',
    data: {
        'ORDER': {
            'CHECKOUT'            : 'Оформление заказа',
            'RECIPIENT'           : 'Получатель',
            'MY_SELF'             : 'Я сам получу цветы',
            'RECIPIENT_NAME'      : 'Имя получателя',
            'RECIPIENT_PHONE'     : 'Телефон получателя',
            'DELIVERY_ADDRESS'    : 'Адрес доставки',
            'LEARN_FROM_RECIPIENT': 'Узнать у получателя',
            'ENTER_ADDRESS'       : 'Введите адрес',
            'ENTER_DATE'          : 'Введите дату',
            'AS_SOON_AS_POSSIBLE' : 'как можно скорее',
            'BEFORE_LUNCH'        : 'до обеда',
            'DURING_THE_DAY'      : 'в течении дня',
            'WRITE_POSTCARD_TEXT' : 'Напишите текст открытки',
            'SENDER'              : 'Отправитель',
            'ANONYMOUSLY'         : 'Анонимно',
            'WHAT_NAME'           : 'Как вас зовут?',
            'YOUR_PHONE_NUMBER'   : 'Ваш телефон',
            'YOUR_MAIL'           : 'Ваша почта',
            'MAKE_PHOTO'          : 'Сделать фото с получателем?',
            'CHOOSE_PAYMENT'      : 'Выберите метод оплаты',
            'PROCEED_TO_CHECKOUT' : 'Купить сейчас',
            'WE_DELIVER'          : 'Доставим',
            'CHANGE'              : 'изменить',
            'DELIVERY'            : 'Доставка',
            'FREE'                : 'Бесплатно',
            'TOTAL_FOR_PAYMENT'   : 'Всего к оплате',
            'PROMO_CODE'          : 'Есть промокод?',
            'APPLY'               : 'Применить',
            'ENTER_PROMO'         : 'Введите промокод',
            'YOU_CAN_PAY'         : 'Вы можете оплатить',
            'WE_USE_SSL'          : 'Мы используем SSL шифрование, все ваши данные под надежной защитой',
            'AFTER_WE_SEND_LINK'  : `После оформления и оплаты заказа мы пришлем вам ссылку, по которой вы сможете
                        отслеживать ход выполнения заказа.`,
            'BOUQUET_REPLACE'     : `Если ваш букет простоит свежим менее 3 дней мы гарантируем бесплатную замену
                        букета.`,
            'MAKE_IN_BOX'         : 'Сделать в коробке',
            'ADD_BOX_RAFFAELLO'   : 'Добавить коробку рафаэлло',
        }
    }
};
