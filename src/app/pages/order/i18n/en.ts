export const locale = {
    lang: 'en',
    data: {
        'ORDER': {
            'CHECKOUT'            : 'Checkout',
            'RECIPIENT'           : 'Recipient',
            'MY_SELF'             : 'I will receive flowers myself',
            'RECIPIENT_NAME'      : 'Recipient name',
            'RECIPIENT_PHONE'     : 'Recipient phone',
            'DELIVERY_ADDRESS'    : 'Delivery address',
            'LEARN_FROM_RECIPIENT': 'Learn from recipient',
            'ENTER_ADDRESS'       : 'Enter the address',
            'ENTER_DATE'          : 'Enter the date',
            'AS_SOON_AS_POSSIBLE' : 'As soon as possible',
            'BEFORE_LUNCH'        : 'before lunch',
            'DURING_THE_DAY'      : 'during the day',
            'WRITE_POSTCARD_TEXT' : 'Write postcard text',
            'SENDER'              : 'Sender',
            'ANONYMOUSLY'         : 'Anonymously',
            'WHAT_NAME'           : 'What is your name?',
            'YOUR_PHONE_NUMBER'   : 'your phone number',
            'YOUR_MAIL'           : 'Your mail',
            'MAKE_PHOTO'          : 'Make a photo with the recipient?',
            'CHOOSE_PAYMENT'      : 'Choose your payment method',
            'PROCEED_TO_CHECKOUT' : 'Proceed to checkout',
            'WE_DELIVER'          : 'We deliver',
            'CHANGE'              : 'change',
            'DELIVERY'            : 'Delivery',
            'FREE'                : 'Free',
            'TOTAL_FOR_PAYMENT'   : 'Total for payment',
            'PROMO_CODE'          : 'Have a promo code?',
            'APPLY'               : 'Apply',
            'ENTER_PROMO'         : 'Enter your promotional code',
            'YOU_CAN_PAY'         : 'You can pay',
            'WE_USE_SSL'          : 'We use SSL encryption, all your data under reliable protection',
            'AFTER_WE_SEND_LINK'  : `After registration and payment of the order, we will send you a link on which you can
                         track the progress of the order.`,
            'BOUQUET_REPLACE'     : `If your bouquet is fresh for less than 3 days we guarantee a free replacement
                         bouquet.`,
            'MAKE_IN_BOX'         : 'Make in box',
            'ADD_BOX_RAFFAELLO'   : 'Add a box of Raffaello',
        }
    }
};
