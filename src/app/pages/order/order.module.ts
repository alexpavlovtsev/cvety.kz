import { NgModule } from '@angular/core';

import { OrderComponent } from './order.component';
import { RouterModule, Routes } from '@angular/router';

import { MyDatePickerModule } from 'mydatepicker';

import { SharedModule } from '@shared/shared.module';
import { OrderService } from './order.service';

const routes: Routes = [
    {
        path     : '',
        component: OrderComponent,
        resolve  : {
            data: OrderService
        }
    },
];

@NgModule({
  imports: [
      RouterModule.forChild(routes),

      SharedModule,

      MyDatePickerModule,
  ],
  declarations: [OrderComponent]
})
export class OrderModule { }
