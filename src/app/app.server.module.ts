import { ApplicationRef, NgModule } from '@angular/core';
import { ServerModule, ServerTransferStateModule } from '@angular/platform-server';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/first';

import { AppStorage, UniversalStorage } from '@postgar/storage';

import { AppComponent } from './app.component';
import { AppModule } from './app.module';

import { LinkService } from '@shared/services/link.service';
import { MicrodataInject } from '../server-helper/microdata/microdata-inject';
import { InlineStyleModule } from './inline-style/inline-style.module';
import { InlineStyleComponent } from './inline-style/inline-style.component';

@NgModule({
    imports  : [
        AppModule,
        NoopAnimationsModule,
        ServerTransferStateModule,
        ServerModule,
        ModuleMapLoaderModule,
    ],
    bootstrap: [AppComponent, InlineStyleComponent],
    providers: [
        {provide: AppStorage, useClass: UniversalStorage},
        LinkService,
        MicrodataInject,
        InlineStyleModule,
    ],
})
export class AppServerModule
{
    constructor(appRef: ApplicationRef,
                linkService: LinkService)
    {
        appRef.isStable
            .filter(stable => stable)
            .first()
            .subscribe((param) =>
            {
                linkService.addCanonicalTag();
                linkService.addBaseTag();
            });
    }
}
