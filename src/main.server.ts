export { AppServerModule } from './app/app.server.module';

import { Location } from '@angular/common';

const __stripTrailingSlash = (Location as any).stripTrailingSlash;
(Location as any).stripTrailingSlash = function _stripTrailingSlash(url: string): string {
    return /[^\/]\/$/.test(url) ? url : __stripTrailingSlash(url);
};
