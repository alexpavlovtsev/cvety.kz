import { createSiteMap } from './xml-sitemap';
import { EXT } from './extensions';

const EXCEPTIONS: string[] = ['index.php', 'index.html', 'index.htm'];

/**
 * Url validation
 *
 * @param req
 * @param res
 * @param next
 */
export function serverPagesWorker(req: any, res: any, next: any): void
{
    const host = req.hostname !== 'localhost' ? 'https://' + req.get('Host') : 'http://' + req.get('Host');

    if (req.originalUrl.endsWith('.xml'))
    {
        return createSiteMap(req, res, next);
    }
    else if (req.originalUrl.includes('ngsw.json'))
    {
        return next();
    }
    else if (!EXT.some(item => req.originalUrl.endsWith(`.${item}`)))
    {
        return redirect(req, res, host, next);

    } else
    {
        return next();
    }
}

/**
 * Run redirect if path contain exception
 * @param req
 * @param res
 * @param {string} host
 * @param next
 */
function redirect(req: any, res: any, host: string, next): void
{
    EXCEPTIONS.forEach((item: string) =>
    {
        if (req.path.endsWith(item))
        {
            const path = suffixReplace(item, req);
            return res.redirect(301, host + path);
        }
    });

    if (req.path.includes('//'))
    {
        const path = doubleSlashValidation(req.path);
        return res.redirect(301, host + path);
    }

    if (req.hostname.startsWith('www.'))
    {
        host = host.replace('//www.', '//');
        return res.redirect(301, host + req.path);
    }

    if (!existLastSlash(req.path) && notException(EXCEPTIONS, req))
    {
        return res.redirect(301, host + req.path + '/');
    }

    if (uppercaseCheck(req.path))
    {
        return res.redirect(301, host + req.path.toLowerCase());
    }

    return next();
}

function isReviewPage(path: string)
{
    return path.startsWith('/o/') || path.startsWith('/rf/');
}

function suffixReplace(replaceString: string, req: any): string
{
    const path = req.path.replace(replaceString, '');
    return doubleSlashValidation(path);
}

function doubleSlashValidation(path: string): string
{
    if (path && path.includes('//'))
    {
        path = path.replace('//', '/');
        return doubleSlashValidation(path);
    }
    if (!existLastSlash(path))
    {
        path = path + '/';
    }

    return path;
}

function existLastSlash(href: string = ''): boolean
{
    if (href && href.length > 0)
    {
        const lastChar = href.substr(href.length - 1);
        return lastChar === '/';
    }
    return false;
}

function notException(exceptions: string[], req: any): boolean
{
    for (let i = 0; i < exceptions.length; i++)
    {
        if (req.path.endsWith(exceptions[i]))
        {
            return false;
        }
    }
    return true;
}

function uppercaseCheck(path: string): boolean
{
    const symbols = ['-', '.', '/', '!', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    let url       = path;

    for (let i = 0; i < symbols.length; i++)
    {
        const regex = new RegExp(symbols[i], 'g');
        if (url.includes(symbols[i]))
        {
            url = url.replace(regex, '');
        }
    }

    const characters = url.split('');

    for (let i = 0; i < characters.length; i++)
    {
        const upperChar = characters[i].toUpperCase();

        if (upperChar === characters[i])
        {
            return true;
        }
    }

    return false;
}
