import 'isomorphic-fetch';

import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { postgarConfig } from '../app/postgar-config';

export const NodeApolloClient = new ApolloClient({
  link: new HttpLink({ uri: postgarConfig.graphqlUrl }),
  cache: new InMemoryCache()
});
