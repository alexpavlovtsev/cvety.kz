import * as sm from 'sitemap';
import gql from 'graphql-tag';
import { NodeApolloClient } from '../apollo-client';
import { XmlSitemapHelper } from './xml-sitemap-helper';
import { FilterInfo } from './filter-info';

export class XmlSitemap
{
    host: string;
    mainCity: string;
    bouquets: any[]   = [];
    categories: any[] = [];

    /**
     * Constructor
     *
     * @param host
     */
    constructor(host: string)
    {
        this.host     = host;
        this.mainCity = 'moscow';
    }

    /**
     * Get links for main sitemap
     *
     * @param cities
     */
    getGlobalSitemapLinks(cities: any[]): string[]
    {
        const cityUrls = cities
            .filter(city => city.code !== this.mainCity)
            .map(city => `https://${this.host}/sitemap-ru-${city.code}.xml`);

        const mainUrl = `https://${this.host}/sitemap-ru.xml`;

        const cityUrlsEn = cities
            .filter(city => city.code !== this.mainCity)
            .map(city => `https://${this.host}/sitemap-en-${city.code}.xml`);
        const mainUrlEn  = `https://${this.host}/sitemap-en.xml`;

        return [mainUrl, ...cityUrls, mainUrlEn, ...cityUrlsEn];
    }

    /**
     * Get main sitemap
     */
    getMainSitemap(code: string): Promise<any>
    {

        return new Promise((resolve, reject) =>
        {
            Promise.all([
                this.getBouquets(this.mainCity, code),
                this.getCategoryUrls(this.mainCity, code),
            ]).then(() =>
            {
                let pages;

                const urls = [
                    'delivery',
                    'guarantee',
                    'payment',
                    'contacts',
                    'reviews',
                    'delivery-photos',
                    'sitemap',
                ];

                if (code === 'ru')
                {
                    pages = urls.map(page =>
                    {
                        return {
                            url     : validFullUrl(`https://${this.host}/${page}/`),
                            priority: 0.4,
                            links   : [
                                {lang: 'ru', url: validFullUrl(`https://${this.host}/${page}/`)},
                                {lang: 'en', url: validFullUrl(`https://${this.host}/en/${page}/`)},
                            ]
                        };
                    });

                    pages.push(
                        {
                            url     : validFullUrl(`https://${this.host}/bukety/`),
                            priority: 0.6,
                            links   : [
                                {lang: 'ru', url: validFullUrl(`https://${this.host}/bukety/`)},
                                {lang: 'en', url: validFullUrl(`https://${this.host}/en/bouquets/`)},
                            ]
                        }
                    );
                }
                else
                {
                    pages = urls.map(page =>
                    {
                        return {
                            url     : validFullUrl(`https://${this.host}/en/${page}/`),
                            priority: 0.4,
                            links   : [
                                {lang: 'ru', url: validFullUrl(`https://${this.host}/${page}/`)},
                                {lang: 'en', url: validFullUrl(`https://${this.host}/en/${page}/`)},
                            ]
                        };
                    });

                    pages.push(
                        {
                            url     : validFullUrl(`https://${this.host}/en/bouquets/`),
                            priority: 0.6,
                            links   : [
                                {lang: 'ru', url: validFullUrl(`https://${this.host}/bukety/`)},
                                {lang: 'en', url: validFullUrl(`https://${this.host}/en/bouquets/`)},
                            ]
                        }
                    );
                }

                const sitemap = sm.createSitemap({
                    hostname : `https://${this.host}`,
                    cacheTime: 600000,
                    urls     : [
                        {
                            url     : `https://${this.host}`,
                            priority: 1,
                        },
                        ...pages,
                        ...this.bouquets,
                        ...this.categories,
                    ]
                });

                resolve(sitemap);
            });
        });
    }

    /**
     * Get sitemap for city
     */
    getSecondarySitemap(cityCode: string, langCode: string): Promise<any>
    {
        return new Promise((resolve, reject) =>
        {
            Promise.all([
                this.getBouquets(cityCode, langCode),
                this.getCategoryUrls(cityCode, langCode),
            ]).then(
                ([]) =>
                {
                    const sitemap = sm.createSitemap({
                        hostname : validFullUrl(`https://${this.host}`),
                        cacheTime: 600000,        // 600 sec - cache purge period
                        urls     : [
                            {
                                url     : validFullUrl(`https://${this.host}`),
                                priority: 1,
                            },
                            ...this.bouquets,
                            ...this.categories,
                        ]
                    });
                    resolve(sitemap);
                },
                reject
            );
        });
    }

    private getBouquets(cityCode: string, langCode: string): Promise<any>
    {
        const BouquetsQuery = gql`
            query allBouquets($city: StringListFilter, $en: Int, $ru: Int) {
              allBouquets(filter: {cityCodes: $city}) {
                nodes {
                  id
                  enUrl: localeUrl(languageId: $en)
                  ruUrl: localeUrl(languageId: $ru)
                  name: localeName(languageId: $ru)
                  mainImage
                }
              }
            }
            `;

        return new Promise((resolve, reject) =>
        {
            NodeApolloClient.query({
                query    : BouquetsQuery,
                variables: {en: 2, ru: 1, city: {anyEqualTo: cityCode}}
            }).then((result: any) =>
            {
                const city = cityCode === this.mainCity ? '' : cityCode;

                if (langCode === 'ru')
                {
                    this.bouquets = result.data['allBouquets'].nodes
                        .map(item =>
                        {
                            return {
                                url     : validFullUrl(`https://${this.host}/${city}/bukety/${item.ruUrl}`),
                                priority: 0.6,
                                links   : [
                                    {
                                        lang: 'en',
                                        url : validFullUrl(`https://${this.host}/${city}/en/bouquets/${item.enUrl}`)
                                    },
                                    {
                                        lang: 'ru',
                                        url : validFullUrl(`https://${this.host}/${city}/bukety/${item.ruUrl}`)
                                    },
                                ],
                            };
                        });
                }
                else
                {
                    this.bouquets = result.data['allBouquets'].nodes
                        .map(item =>
                        {
                            return {
                                url     : validFullUrl(`https://${this.host}/${city}/en/bouquets/${item.enUrl}`),
                                priority: 0.6,
                                links   : [
                                    {
                                        lang: 'en',
                                        url : validFullUrl(`https://${this.host}/${city}/en/bouquets/${item.enUrl}`)
                                    },
                                    {
                                        lang: 'ru',
                                        url : validFullUrl(`https://${this.host}/${city}/bukety/${item.ruUrl}`)
                                    },
                                ],
                            };
                        });
                }

                resolve();

            }).catch((err) => reject(err));
        });
    }

    private getCategoryUrls(cityCode: string, langCode: string): Promise<any>
    {
        const PRODUCT_COUNTS_QUERY = gql`
            query getAllProductCounts($itemsJson: JSON, $city: String) {
              getAllProductCounts(itemsJson: $itemsJson, city: $city)
            }`;

        const FILTER_ITEMS_QUERY = gql`
            query fi {
              allFilterItemTranslates {
                nodes {
                  id
                  languageId
                  filterItemId
                  value
                  url
                  filterItemByFilterItemId {
                    filterId
                  }
                }
              }
            }
            `;

        return new Promise((resolve, reject) =>
        {
            const languageId = langCode === 'ru' ? 1 : 2;

            NodeApolloClient.query({query: FILTER_ITEMS_QUERY, variables: {}})
                .then((res: any) =>
                {
                    let filterInfos = res.data['allFilterItemTranslates']['nodes']
                        .map(item => filterItemConvert(item));

                    filterInfos = deprecateFilterItems(filterInfos);

                    const helper    = new XmlSitemapHelper();
                    this.categories = helper.siteMapFilterInfo(filterInfos, this.host, cityCode);

                    resolve(this.categories);

                    // const array     = mappingIds([], languageId, filterInfos);
                    // const variables = {city: cityCode, itemsJson: JSON.stringify({'values': array})};

                    // NodeApolloClient.query({query: PRODUCT_COUNTS_QUERY, variables: variables})
                    //     .then(result =>
                    //     {
                    //         console.log(result.data['getAllProductCounts']);
                    //         // const counts = JSON.parse(result.data['getAllProductCounts'] || '{}');
                    //         // const items  = filterInfos
                    //         //     .filter(item => counts.hasOwnProperty(item.filterItemId) && counts[item.filterItemId] > 2);
                    //         //
                    //         // const helper    = new XmlSitemapHelper();
                    //         // this.categories = helper.siteMapFilterInfo(items, this.host, cityCode);
                    //
                    //         resolve(this.categories);
                    //     });
                });
        });
    }
}

export function filterItemConvert(item: any): any
{
    return {
        id             : item.id,
        filterItemId   : item.filterItemId,
        filterId       : item.filterItemByFilterItemId.filterId,
        languageId     : item.languageId,
        filterItemLabel: item.value,
        filterItemUrl  : item.url
    };
}

export function deprecateFilterItems(filters: any[]): any
{
    const depFilters     = {'25': true, '3': true};
    const depFilterItems = {'1619': true, '84': true, '85': true, '157': true, '159': true};
    return filters.filter(value => !depFilterItems[value.filterItemId] && !depFilters[value.filterId]);
}


export function validFullUrl(url: string): string
{
    if (!url)
    {
        url = '';
    }

    if (url.includes('://'))
    {
        const array: string[] = url.split('://');
        return array[0] + '://' + urlValidation(array[1]);
    }
    else
    {
        return urlValidation(url);
    }
}

function urlValidation(path: string): string
{
    if (path && path.includes('//'))
    {
        path = path.replace('//', '/');
        return urlValidation(path);
    }

    if (!existLastSlash(path))
    {
        path = path + '/';
    }

    return doubleDashValidation(path);
}

/**
 * Delete double dash
 * @param {string} path
 * @returns {string}
 */
function doubleDashValidation(path: string): string
{
    if (path && path.includes('--'))
    {
        path = path.replace('--', '-');
        return doubleDashValidation(path);
    }
    if (path && path.endsWith('-') && path.length !== 1)
    {
        return path.slice(0, -1);
    }

    return path;
}

function mappingIds(activeItems: FilterInfo[], languageId: number, filterItems: any[]): any[]
{
    const array      = [];
    const otherItems = filterItems
        .filter(item => item.languageId === languageId)
        .filter(item => activeItems.indexOf(item) === -1);

    for (const item of otherItems)
    {
        let categories_ids = [];
        const isMulti      = item.filterId === 4;
        const obj          = {categories_ids: '', id: item.filterItemId};

        if (isMulti)
        {
            categories_ids = [...activeItems.map(el => el.filterItemId), item.filterItemId];
        } else
        {
            categories_ids = [item.filterItemId];

            for (const active of activeItems)
            {
                if (item.filterId !== active.filterId)
                {
                    categories_ids.push(active.filterItemId);
                }
            }
        }
        obj.categories_ids = categories_ids.join(',');
        array.push(obj);
    }
    return array;
}

export function existLastSlash(href: string = ''): boolean
{
    if (href && href.length > 0)
    {
        const lastChar = href.substr(href.length - 1);
        return lastChar === '/';
    }
    return false;
}

