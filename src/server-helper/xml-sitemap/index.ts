import { NodeApolloClient } from '../apollo-client';
import gql from 'graphql-tag';
import * as sm from 'sitemap';
import { XmlSitemap } from './xml-sitemap';

/**
 * Generate XML sitemap
 * @param req
 * @param res
 * @param next
 * @returns {any}
 */
export function createSiteMap(req: any, res: any, next: any): any
{
    // Get cities
    NodeApolloClient.query({
        query: gql`query allCities { allCities { nodes { id code enName name } } }`
    }).then((result: any) =>
    {
        const siteMap         = new XmlSitemap(req.get('Host'));
        const cities          = result.data['allCities'].nodes;
        const city            = cities.find(c => req.originalUrl.endsWith(`-${c.code}.xml`));
        const isGlobalSitemap = req.originalUrl === `/sitemap.xml`;
        const isMainSitemap   = req.originalUrl === '/sitemap-en.xml' || req.originalUrl === '/sitemap-ru.xml';
        const langCode        = req.originalUrl.includes('-en') ? 'en' : 'ru';

        if (isGlobalSitemap)
        {
            // Global index
            return this.createGlobalSiteMapIndex(req, res, next, cities);
        }
        else if (isMainSitemap)
        {
            // Sitemap for map city
            siteMap.getMainSitemap(langCode)
                .then(data => sitemapRender(data, res))
                .catch(() => next());
        }
        else if (city)
        {
            // Sitemap for secondary city
            siteMap.getSecondarySitemap(city.code, langCode)
                .then(data => sitemapRender(data, res))
                .catch(() => next());
        }
        else
        {
            return next();
        }

    }).catch((err) =>
    {
        console.log(err);

        return next();
    });
}

/**
 * Main index for xml sitemap
 *
 * @param req
 * @param res
 * @param next
 * @param cities
 */
export function createGlobalSiteMapIndex(req: any, res: any, next: any, cities: any[]): void
{
    const siteMap = new XmlSitemap(req.get('Host'));

    const smi = sm.buildSitemapIndex({
        urls: siteMap.getGlobalSitemapLinks(cities)
    });

    res.header('Content-Type', 'application/xml');
    res.send(smi);
}


/**
 * XML Sitemap render
 * @param sitemap
 * @param res
 */
function sitemapRender(sitemap: any, res: any): void
{
    sitemap.toXML((err, xml) =>
    {
        if (err)
        {
            res.status(500).end();
            return;
        }
        res.header('Content-Type', 'application/xml');
        res.send(xml.toString());
    });
}

