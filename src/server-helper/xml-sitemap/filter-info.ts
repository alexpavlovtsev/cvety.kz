export class FilterInfo {

  protected _filterItemId: number;
  protected _filterId: number;
  protected _languageId: number;
  protected _filterItemLabel: string;
  protected _filterItemUrl: string;
  protected _filterItemCount: string;

  protected _url: string;
  protected _label: string;
  protected _isActive: boolean;
  protected _categoriesIds: number[];

  constructor(data)
  {
    this._filterItemId    = data.filterItemId;
    this._filterId        = data.filterId;
    this._languageId      = data.languageId;
    this._filterItemLabel = data.filterItemLabel;
    this._filterItemUrl   = data.filterItemUrl;
    this._filterItemCount = data.filterItemCount;
  }

  get filterItemId(): number
  {
    return this._filterItemId;
  }

  set filterItemId(value: number)
  {
    this._filterItemId = value;
  }

  get filterId(): number
  {
    return this._filterId;
  }

  set filterId(value: number)
  {
    this._filterId = value;
  }

  get languageId(): number
  {
    return this._languageId;
  }

  set languageId(value: number)
  {
    this._languageId = value;
  }

  get filterItemLabel(): string
  {
    return this._filterItemLabel;
  }

  set filterItemLabel(value: string)
  {
    this._filterItemLabel = value;
  }

  get filterItemUrl(): string
  {
    return this._filterItemUrl;
  }

  set filterItemUrl(value: string)
  {
    this._filterItemUrl = value;
  }

  get filterItemCount(): string
  {
    return this._filterItemCount;
  }

  set filterItemCount(value: string)
  {
    this._filterItemCount = value;
  }

  get count(): number
  {
    if (this.filterItemCount)
    {
      return parseFloat(this.filterItemCount);
    }
    return 0;
  }

  get url(): string
  {
    return this._url;
  }

  set url(value: string)
  {
    this._url = value;
  }

  get label(): string
  {
    return this._label;
  }

  set label(value: string)
  {
    this._label = value;
  }

  get isActive(): boolean
  {
    return this._isActive;
  }

  set isActive(value: boolean)
  {
    this._isActive = value;
  }

  get categoriesIds(): number[]
  {
    return this._categoriesIds;
  }

  set categoriesIds(value: number[])
  {
    this._categoriesIds = value;
  }
}
