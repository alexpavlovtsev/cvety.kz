import { FilterInfo } from './filter-info';
import { validFullUrl } from './xml-sitemap';
import { MAIN_CITY_CODE } from '../../app/config';

export const WORD_SORT = function (a, b)
{
    const nameA = a.toLowerCase(), nameB = b.toLowerCase();
    if (nameA < nameB)
    {
        return -1;
    }
    if (nameA > nameB)
    {
        return 1;
    }
    return 0;
};

export class XmlSitemapHelper
{

    filtersInfo: FilterInfo[];
    filtersInfoRu: FilterInfo[];
    filtersInfoEn: FilterInfo[];

    mainCity = 'moscow';

    host: string;
    city: string;

    siteMapFilterInfo(result: any, host: string, city: string): any[]
    {
        this.city        = city;
        this.host        = host;
        this.filtersInfo = [...result];

        this.filtersInfoRu = this.filtersInfo.filter(item => item.languageId === 1);
        this.filtersInfoEn = this.filtersInfo.filter(item => item.languageId === 2);

        return this.get_1();

        // return [...this.get_1(), ...this.get_2(), ...this.get_3()];
    }

    get_1(): any[]
    {
        const array = [];

        for (const item of this.filtersInfoRu)
        {
            const urlRu = this.fullUrl([item], 'ru', this.city);
            const urlEn = this.fullUrl(this.en_items([item]), 'en', this.city);

            array.push(this.itemBody(urlRu, urlEn));
            array.push(this.itemBodyEn(urlRu, urlEn));
        }

        return array;
    }


    get_2(): any[]
    {
        const array = [];

        for (const item of this.filtersInfoRu)
        {
            for (const item_2 of this.filtersInfoRu)
            {
                if (item.filterId !== item_2.filterId)
                {
                    const urlRu = this.fullUrl([item, item_2], 'ru', this.city);
                    const urlEn = this.fullUrl(this.en_items([item, item_2]), 'en', this.city);

                    array.push(this.itemBody(urlRu, urlEn));
                    array.push(this.itemBodyEn(urlRu, urlEn));
                }
            }
        }

        return array;
    }

    get_3(): any[]
    {
        const array = [];

        for (const item of this.filtersInfoRu)
        {
            for (const item_2 of this.filtersInfoRu)
            {
                for (const item_3 of this.filtersInfoRu)
                {
                    const cond_1 = item.filterId !== item_2.filterId;
                    const cond_2 = item_2.filterId !== item_3.filterId;
                    const cond_3 = item.filterId !== item_3.filterId;

                    if (cond_1 && cond_2 && cond_3)
                    {
                        const urlRu = this.fullUrl([item, item_2, item_3], 'ru', this.city);
                        const urlEn = this.fullUrl(this.en_items([item, item_2, item_3]), 'en', this.city);

                        array.push(this.itemBody(urlRu, urlEn));
                    }
                }
            }
        }

        return array;
    }

    itemBody(urlRu: string, urlEn: string): any
    {
        const city = this.city === this.mainCity ? '' : `${this.city}/`;

        return {
            url     : `https://` + this.urlValid(`${this.host}/${city}/${urlRu}`),
            priority: 0.8,
            links   : [
                {
                    lang: 'en',
                    url : `https://` + this.urlValid(`${this.host}/${city}/en/${urlEn}`)
                },
                {
                    lang: 'ru',
                    url : `https://` + this.urlValid(`${this.host}/${city}/${urlRu}`)
                },
            ],
        };
    }

    itemBodyEn(urlRu: string, urlEn: string): any
    {
        const city = this.city === this.mainCity ? '' : `${this.city}/`;

        return {
            url     : `https://` + this.urlValid(`${this.host}/${city}/en/${urlEn}`),
            priority: 0.8,
            links   : [
                {
                    lang: 'en',
                    url : `https://` + this.urlValid(`${this.host}/${city}/en/${urlEn}`)
                },
                {
                    lang: 'ru',
                    url : `https://` + this.urlValid(`${this.host}/${city}/${urlRu}`)
                },
            ],
        };
    }

    en_items(items: FilterInfo[]): FilterInfo[]
    {
        const en_items = [];

        for (const item of items)
        {
            const en_item = this.filtersInfoEn.find(en => en.filterItemId === item.filterItemId);
            if (en_item !== undefined)
            {
                en_items.push(en_item);
            }
        }

        return en_items;
    }

    fullUrl(items: any[], language: any, cityCode: string): string
    {
        let containers = {};

        for (let i = 0; i < 200; i++)
        {
            containers[i] = {
                url  : [],
                label: []
            };
        }

        for (const item of items)
        {
            if (!containers[item.filterId])
            {
                containers[item.filterId] = {
                    url  : [],
                    label: []
                };
            }
            containers[item.filterId].url.push(item.filterItemUrl);
        }

        if (containers[5].url.length === 0 && items.length > 0)
        {
            switch (language)
            {
                case 'ru':
                    containers[5].url.push('cvety');
                    containers[5].label.push('цветы');
                    break;
                case 'en':
                    containers[5].url.push('flowers');
                    containers[5].label.push('flowers');
                    break;
            }

        }
        else if (containers[5].url.length > 1)
        {
            containers[5].url.sort(WORD_SORT);
            containers[5].label.sort(WORD_SORT);

            const lastUrl   = containers[5].url.pop();
            const lastLabel = containers[5].label.pop();

            switch (language)
            {
                case 'ru':
                    containers[5].url   = [...containers[5].url, 'i', lastUrl];
                    containers[5].label = [...containers[5].label, 'и', lastLabel];
                    break;
                case 'en':
                    containers[5].url   = [...containers[5].url, 'and', lastUrl];
                    containers[5].label = [...containers[5].label, 'and', lastLabel];
                    break;
            }
        }

        if (containers[7].url.length > 0)
        {
            switch (language)
            {
                case 'ru':
                    containers[7].label = containers[7].label.map((item) => `на ${item}`);
                    containers[7].url   = containers[7].url.map((item) => `na-${item}`);
                    break;
                case 'en':
                    containers[7].label = containers[7].label.map((item) => `for ${item}`);
                    containers[7].url   = containers[7].url.map((item) => `for-${item}`);
                    break;
            }
        }

        // url helpers
        if (containers[3].url.length > 0 && language === 'ru')
        {
            containers = quantityUrlHelper(containers);
        }

        const url = [
            ...containers[3].url,
            ...containers[4].url,
            ...containers[5].url,
            ...containers[6].url,
            ...containers[7].url,
            ...containers[10].url,
            ...containers[25].url
        ].join('-');

        cityCode       = cityCode === MAIN_CITY_CODE ? '' : cityCode;
        const langCode = language === 'ru' ? '' : language;

        return validFullUrl(url);
    }

    private urlValid(url: string): string
    {
        return validFullUrl(url);
    }
}

export function quantityUrlHelper(containers: any): any
{
    switch (containers[3].url[0])
    {
        case '7':
            containers[5].url = containers[5].url.map(url => one(url));
            break;

        case '25':
            containers[5].url = containers[5].url.map(url => one(url));
            break;

        case '51':
            containers[5].url = containers[5].url.map(url => two(url));
            break;

        case '101':
            containers[5].url = containers[5].url.map(url => two(url));
            break;
    }

    return containers;
}

function one(url: string)
{
    switch (url)
    {
        case 'cvety':
            return 'cvetov';
        case 'rozy':
            return 'roz';
        case 'piony':
            return 'pionov';
        case 'gerbery':
            return 'gerber';
        case 'amarillisy':
            return 'amarillis';
        case 'anturium':
            return 'anturium';
        case 'gvozdika':
            return 'gvozdik';
        case 'giacinty':
            return 'giacintov';
        case 'gortenziya':
            return 'gortenziy';
        case 'irisy':
            return 'irisov';
        case 'kally':
            return 'kall';
        case 'lilii':
            return 'liliy';
        case 'podsolnuh':
            return 'podsolnuhov';
        case 'romashki':
            return 'romashek';
        case 'siren':
            return 'sireni';
        case 'streliciya':
            return 'streliciy';
        case 'frezii':
            return 'freziy';
        case 'hrizantemy':
            return 'hrizantem';
        case 'maki':
            return 'makov';
        case 'vasilki':
            return 'vasilkov';
        case 'astry':
            return 'astr';
        case 'eustoma':
            return 'eustom';
        case 'kustovaya-roza':
            return 'kustovykh-roz';
        case 'alstromeriya':
            return 'alstromeriy';
        case 'orhidei':
            return 'orhidey';
        case 'tyulpany':
            return 'tyulpanov';
        case 'gollandskie-rozy':
            return 'gollandskih-roz';
    }
}

function two(url: string)
{
    switch (url)
    {
        case 'cvety':
            return 'cvetok';
        case 'rozy':
            return 'roza';
        case 'piony':
            return 'pion';
        case 'gerbery':
            return 'gerbera';
        case 'amarillisy':
            return 'amarillis';
        case 'anturium':
            return 'anturium';
        case 'gvozdika':
            return 'gvozdika';
        case 'giacinty':
            return 'giacint';
        case 'gortenziya':
            return 'gortenziya';
        case 'irisy':
            return 'iris';
        case 'kally':
            return 'kalla';
        case 'lilii':
            return 'liliya';
        case 'podsolnuh':
            return 'podsolnuh';
        case 'romashki':
            return 'romashka';
        case 'siren':
            return 'siren';
        case 'streliciya':
            return 'streliciya';
        case 'frezii':
            return 'freziya';
        case 'hrizantemy':
            return 'hrizantema';
        case 'maki':
            return 'mak';
        case 'vasilki':
            return 'vasilek';
        case 'astry':
            return 'astra';
        case 'eustoma':
            return 'eustoma';
        case 'kustovaya-roza':
            return 'kustovaya-roza';
        case 'alstromeriya':
            return 'alstromeriya';
        case 'orhidei':
            return 'orhideya';
        case 'tyulpany':
            return 'tyulpan';
        case 'gollandskie-rozy':
            return 'gollandskaya-roza';
    }
}

