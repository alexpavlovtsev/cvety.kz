import { FilterInfo } from './filter-info';

export const FILTER_INFO: {[key: string]: any[]} = {
    ru: [
    ],
    en: [
    ]
};

export const FILTER_INFO_ARR = () =>
{
    return Object.keys(FILTER_INFO)
        .map(key => FILTER_INFO[key])
        .reduce((_items, item) => [..._items, ...item], [])
        .map((item, index) =>
        {
            return new FilterInfo({
                ...item,
                id: index
            });
        });
};
