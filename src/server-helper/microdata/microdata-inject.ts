import { Injectable, RendererFactory2, ViewEncapsulation } from '@angular/core';
import { PlatformState } from '@angular/platform-server';

@Injectable()
export class MicrodataInject
{
  constructor(private state: PlatformState, private rendererFactory: RendererFactory2)
  {
  }

  /**
   * Inject the State into the bottom of the <head>
   */
  inject(value: any)
  {
    try
    {
      const document: any = this.state.getDocument();
      const renderer = this.rendererFactory.createRenderer(document, {
        id           : '-1',
        encapsulation: ViewEncapsulation.None,
        styles       : [],
        data         : {}
      });

      const head = document.head;

      const jsonString = JSON.stringify(value);
      const scriptValue = JSON.stringify(JSON.parse(jsonString), null, 2);

      // let headChildren = head.getElementsByTagName('script');
      // const microLinks = this.getAllElementsWithAttribute(headChildren, `application/ld+json`);

      const script = renderer.createElement('script');
      renderer.setAttribute(script, 'type', `application/ld+json`);

      renderer.setValue(script, scriptValue);
      renderer.appendChild(head, script);

      /*if (microLinks.length > 0)
      {
        renderer.setValue(microLinks[0], scriptValue);
      } else
      {
        const script = renderer.createElement('script');
        renderer.setAttribute(script, 'type', `application/ld+json`);

        renderer.setValue(script, scriptValue);
        renderer.appendChild(head, script);
      }*/

    } catch (e)
    {
      console.error(e);
    }
  }

  getAllElementsWithAttribute(allElements: any[], attribute: string): any[]
  {
    const matchingElements = [];

    for (let i = 0, n = allElements.length; i < n; i++)
    {
      if (allElements[i].getAttribute(attribute) !== null)
      {
        // Element exists with attribute. Add to array.
        matchingElements.push(allElements[i]);
      }
    }
    return matchingElements;
  }
}